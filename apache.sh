
#!/bin/bash


path=$1
server_name=$2
document_root=$3
db_name=$4
sudo cp /etc/apache2/sites-available/template.ecolo.rocketsystems.net.conf  /etc/apache2/sites-available/$server_name.conf &&
sudo chmod  777 /etc/apache2/sites-available/$server_name.conf &&
sudo sed -i  's/YOUR_SERVER_NAME/'$server_name'/g; s+DOCUMENT_ROOT+'$document_root'+g'  /etc/apache2/sites-available/$server_name.conf;
 cd /etc/apache2/sites-available && sudo a2ensite $server_name.conf

sudo mkdir $path

cd $path && sudo git clone https://github.com/hovakimyan90/ecolo.git

cd ecolo &&  sudo  mv .env.example .env

sudo sed -i 's/YOUR_DB_NAME'/$db_name'/g' .env
sudo composer install
sudo chmod -R 777  bootstrap/
sudo chmod -R  777  storage/
sudo php artisan key:generate
sudo php artisan storage:link
sudo php artisan migrate
sudo php artisan db:seed
sudo chmod -R 777 public/

sudo service apache2 restart


#
# Maintenance routine
#
#sudo /etc/init.d/apache2 restart

