<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\App;

class DescriptionAndPathTranslationScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $builder->select('*', 'path_' . App::getLocale() . ' as path', 'description_' . App::getLocale() . ' as description', 'units_' . App::getLocale() . ' as unit');
    }
}
