<?php


namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\App;

class SoseParameterMessageTranslationScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $builder->select('*','message_'.App::getLocale().' as message');
    }

}
