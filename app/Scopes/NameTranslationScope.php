<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 8/13/2019
 * Time: 4:24 PM
 */

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\App;

class NameTranslationScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $builder->select('*', 'name_' . App::getLocale() . ' as name');
    }
}