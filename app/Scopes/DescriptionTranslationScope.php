<?php


namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\App;

class DescriptionTranslationScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $builder->select('*', 'description_' . App::getLocale() . ' as description');
    }
}
