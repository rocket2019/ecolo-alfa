<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendAdminNotificationEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @param $company_name
     * @param $client
     */
    public function __construct($company_name, $client)
    {
        $this->company_name = $company_name;
        $this->client = $client;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $address = env('MAIL_FROM_ADDRESS');
        $subject = 'Новая анкета на оценку';

        return $this->view('email.send_admin_notification')
            ->from($address, 'Альфа-Банк')
            ->replyTo($address)
            ->subject($subject)
            ->with(['company_name' => $this->company_name, 'client' => $this->client]);
    }
}
