<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EvaluationNoticeEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @param $company_name
     * @param $client
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = env('MAIL_FROM_ADDRESS');
        $subject = 'Альфа-Банк | ECOLO. Уведомление о завершении оценки';

        return $this->view('email.evaluation_notice')
            ->from($address, 'Отдел оценки')
            ->replyTo($address)
            ->subject($subject)
            ->with($this->data);
    }
}
