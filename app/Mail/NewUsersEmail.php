<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewUsersEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = env('MAIL_FROM_ADDRESS');
        $subject = 'New Users | ECOLO.';

        return $this->view('email.new_user_email')
            ->from($address, 'Ecolo New Users')
            ->replyTo($address)
            ->subject($subject)
            ->with($this->data)
            ->attach(storage_path('app/public/Ecolo_questionnaire_instruction.pdf'));
    }
}
