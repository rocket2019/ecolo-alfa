<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ClientQuestionnaireEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = env('MAIL_FROM_ADDRESS');
        $subject = 'You have a new questionnaire';

        return $this->view('email.client_questionnaire')
            ->from($address,'Альфа-Банк')
            ->replyTo($address)
            ->subject($subject)
            ->with(['url' => $this->data['url'], 'username' => $this->data['username']]);
    }
}
