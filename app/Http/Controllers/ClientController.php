<?php

namespace App\Http\Controllers;

use App\Http\Models\Client;
use App\Http\Models\CompanyList;
use App\Http\Models\GeographicalAreas;
use App\Http\Models\LegalForm;
use App\Http\Models\Questionnaire;
use App\Http\Models\QuestionnaireTree;
use App\Http\Models\Role;
use App\Http\Models\SoseAteco;
use App\Http\Models\SoseAtecoStudiesEquivalence;
use App\Http\Models\SoseCheck;
use App\Http\Models\SoseParameterValue;
use App\Http\Models\SoseQuestionnaireStatus;
use App\Http\Models\SoseStudiesParameter;
use App\Http\Models\SoseStudy;
use App\Http\Models\User;
use App\Http\Services\StatusCode;
use App\Http\Services\Constants;
use App\Http\Services\Is;
use App\Mail\ClientQuestionnaireEmail;
use App\Mail\SendAdminNotificationEmail;
use App\Mail\QuestionnaireLinkEmail;
use Complex\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Importer;
use Maatwebsite\Excel\Facades\Excel as Excel;
use Mpdf\Cache;
use Mpdf\Mpdf;
use PDF;
use Barryvdh\Snappy\Facades\SnappyPdf as newPDF;

class ClientController extends Controller
{
    public function get_user_role()
    {
        if (Auth::check()){
            if(Auth::user()->role_id == Role::get_role_id_by_name(Role::ROLE_BANK_MANAGER)){
                return $this->_response_body(true, StatusCode::HTTP_OK);
            }
            else{
                return $this->_response_body(false, StatusCode::HTTP_OK);
            }
        }
        else{
            return $this->_response_body(false, StatusCode::HTTP_OK);
        }
    }

    public function send_mail_to_admin(Request $request, $lang, $questionnaire, $client){

        $admins = ['alexey.gusev@evabeta.com', 'viktoria.romanova@evabeta.com', 'paruir.akopian@evabeta.com', 'oleg.melnik@evabeta.com'];
        $company_name = CompanyList::query()
            ->where('ID_company', '=', $client)
            ->pluck('company_name');
        try {
            foreach ($admins as $key => $mail){
                Mail::to($mail)->send(new SendAdminNotificationEmail($company_name[0], $client));
            }
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }

    public function register(Request $request, $subdomain = 'test', $lang = 'en', $token)
    {

        if ($request->isMethod('post')) {
            $manager_id = User::query()->where('client_token', $token)->first();
            if ($manager_id) {
                $manager_id = $manager_id->id;
            } else {
                echo 'manager not found';
            }

            $rules = [
                'company_name' => 'required',
                'email_manager' => 'required|email',
                'INN_number' => 'required',
                'main_activity' => 'required',
                'area_region' => 'required',
                'area_city' => 'required',
                'ateco_id' => 'required|integer',
//                'company_unit' => 'required|integer',
            ];

            $messages = [
                'INN_number.required' => 'The INN number is required.'
            ];

            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                return $this->_response_body($validator->messages(), StatusCode::HTTP_UNPROCESSABLE_ENTITY);
            }

            $client = new CompanyList();
            $client->fill($request->except(['ateco_id','area_region','area_city','checkBtn']));
            $client->manager_id = $manager_id;
            $client->region = $request->input('area_region');
            $client->region_code = $request->input('area_city');
            $client->questionnaire_status = Client::QUESTIONNAIRE_STATUS_EDITING;
            $client->save();

            $questionnaire = Questionnaire::insert($request, $client->ID_company);
            $data['url'] = '/client/questionnaire/view/' . $client->ID_company . '/' . $questionnaire;
            $data['username'] = $client->company_name;

            if($request->checkBtn == 'false'){
                try {
                    Mail::to($request->input('email_manager'))->send(new QuestionnaireLinkEmail($data));
                } catch (\Exception $e){
                    return $e->getMessage();
                }
            }

            $data['client_id'] = $client->ID_company;
            $data['questionnaire_id'] = $questionnaire;
            return $this->_response_body($data);
        }

        $ateco = QuestionnaireTree::select('questionnaire_tree.id', 'sose_study_id', 'sose_ateco_id', 'sose_okved_id', 'code', 'sort_order', 'description')
            ->join('sose_study', 'sose_study_id', '=', 'sose_study.id')
            ->get();
        $regions = GeographicalAreas::query()
            ->where('country', '=', GeographicalAreas::COUNTRY)->groupBy('region_description')->get();
        $users = User::query()->where('role_id', Role::get_role_id_by_name(Role::ROLE_FINAL_CLIENT))->get();
        $legal_forms = LegalForm::get_all_forms();

        return view('client.entryForm')->with(compact('ateco','legal_forms','regions'));

    }

    public function view_questionnaire(Request $request, $sub = 'test', $lang = 'en', $ateco_id)
    {
        $ateco_name = SoseAteco::query()
            ->where('id', '=', $ateco_id)
            ->pluck('description_' . $lang);
        if ($lang == 'ru') {
            $ateco_name = explode('[', $ateco_name[0])[0];
        }
        else{
            $ateco_name = $ateco_name[0];
        }


        $ateco_code = SoseAteco::query()
            ->where('id', '=', $ateco_id)
            ->pluck('ateco_code');
        $sose_study_id = SoseAtecoStudiesEquivalence::query()->where('soseateco_id', $ateco_id)->first();
        if ($sose_study_id) {
            $study_id = $sose_study_id->sosestudy_id;
        }


        $params = SoseStudiesParameter::get_by_study_id($study_id);


        $parameters = [];

        foreach ($params as $key => $param) {
            if (!isset($param->sose_param->path) || is_null($param->sose_param)) {
                continue;
            }
            try {

                if (strpos($param->sose_param->path, 'Group 0') !== false && $param->sose_param->visible) {

                    $parameters['O'][$key] = $param;
                }
                if (strpos($param->sose_param->path, 'Group A') !== false && $param->sose_param->visible) {
                    $parameters['A'][$key] = $param;

                }
                if (strpos($param->sose_param->path, 'Group B') !== false) {
                    $parameters['B'][$key] = $param;

                }
                if (strpos($param->sose_param->path, 'Group C') !== false && $param->sose_param->visible) {
                    $parameters['C'][$key] = $param;

                }

                if (strpos($param->sose_param->path, 'Group D') !== false && $param->sose_param->visible) {
                    $parameters['D'][$key] = $param;

                }
                if (strpos($param->sose_param->path, 'Group E') !== false && $param->sose_param->visible) {
                    $parameters['E'][$key] = $param;

                }
                if (strpos($param->sose_param->path, 'Group F') !== false && $param->sose_param->visible) {
                    $parameters['F'][$key] = $param;

                }

                if (strpos($param->sose_param->path, 'Group L') !== false && $param->sose_param->visible) {
                    $parameters['L'][$key] = $param;

                }
                if (strpos($param->sose_param->path, 'Group Z') !== false && $param->sose_param->visible) {
                    $parameters['Z'][$key] = $param;

                }
            } catch (\Exception $exception) {
                echo $param->id;
            }


        }


        $b_units = [];
        $a = 0;
        $count = 0;
        foreach ($parameters['B'] as $let => $b) {
            if ($a != 0) {
                $b_units[$let] = $b;
            } else {
                $first = $b;
            }
            $a++;
        }

        $removable_b_units = count($b_units) % 10;
        if (count($b_units) == 1){
            $b_units = array_splice($b_units, count($b_units) - $removable_b_units, $removable_b_units);
        }
        else{
            array_splice($b_units, count($b_units) - $removable_b_units, $removable_b_units);
        }

        $new_params_with_subheadings = [];
        $i = 1;
        foreach ($parameters as $item => $value) {
            if ($item == "B") {
                continue;
            }


            foreach ($parameters[$item] as $key => $parameter) {
                $find_subheading = explode("|", $parameter->sose_param->param_path);

                if (isset($find_subheading[1]) && (strlen($find_subheading[1]) > 0 && strlen(trim($find_subheading[1])) != 0)) {
                    $find_subheading = $find_subheading[1];
//                $find_subheading = str_replace(',',' ',$find_subheading);
//                echo $find_subheading.'<br>';

                } else {
                    $i++;
                    $find_subheading = 'nosub' . $i;
                }


                $new_params_with_subheadings[$item][$find_subheading][] = $parameter;

            }


        }

//        foreach ($new_params_with_subheadings['A'] as $new)
//        {
//           foreach ($new as $a )
//           {
//               echo '<pre>';
//               var_dump($a->sose_param->parameter_code,$a->sose_param->id,$a->sose_param->description_ru);
//
//           }
//
//        }
//        die;


//        if ($company) {
//            $parameters_values = SoseParameterValue::query()
//                ->where('ID_company', $company_id)->where('sose_questionnaire_id', $questionnaire_id)->get();
        if ($request->isMethod('post')) {
            $requests = [];
            foreach ($request->all() as $key => $string) {
                $string = preg_replace('/\s+/', '', $string);
                $requests[$key] = $string;


            }


        }

        $preview_mode = false;
        $product_code = false;

        if (file_exists(public_path('site/pdf/D'.$study_id.'.pdf')))
        {
            $product_code = 'site/pdf/D'.$study_id.'.pdf';
        }
        if ($request->ajax()) {

            $preview_mode = true;
            $view = View::make('client.questionnaire', [
                'b_units' => $b_units,
                'first' => $first,
                'preview_mode' => $preview_mode,
                'product_code' => $product_code,
                'ateco_name' => $ateco_name,
                'ateco_code' => $ateco_code,
//                    'questionnaire' => $questionnaire,
//                    'values' => $parameters_values,
                'parameters' => $new_params_with_subheadings]);
            return $this->_response_body($view->render());

        }



        return view('client.questionnaire',
            [
                'b_units' => $b_units,
                'first' => $first,
                'preview_mode' => $preview_mode,
                'product_code' => $product_code,
//                'regions' => $regions,
//                    'questionnaire' => $questionnaire,
//                    'values' => $parameters_values,
                'parameters' => $new_params_with_subheadings]);

//        }
        return abort(404);
    }

    public function fill_questionnaire(Request $request, $test, $lang, $client_id, $questionnaire_id)
    {
        $client = CompanyList::getByCompanyId($client_id);
        $questionnaire = Questionnaire::get_by_id($questionnaire_id);
        $params = SoseStudiesParameter::get_by_study_id($questionnaire->sose_study_id);
         //&& $param->sose_param->visible
        $parameters = [];

        foreach ($params as $key => $param) {

            if (!isset($param->sose_param->path) || is_null($param->sose_param)) {
                continue;
            }

            try {

                if (strpos($param->sose_param->path, 'Group 0') !== false && $param->sose_param->visible) {

                    $parameters['O'][$key] = $param;
                }
                if (strpos($param->sose_param->path, 'Group A') !== false && $param->sose_param->visible) {
                    $parameters['A'][$key] = $param;

                }
                if (strpos($param->sose_param->path, 'Group B') !== false) {
                    $parameters['B'][$key] = $param;

                }
                if (strpos($param->sose_param->path, 'Group C') !== false && $param->sose_param->visible) {
                    $parameters['C'][$key] = $param;

                }

                if (strpos($param->sose_param->path, 'Group D') !== false && $param->sose_param->visible) {
                    $parameters['D'][$key] = $param;

                }
                if (strpos($param->sose_param->path, 'Group E') !== false && $param->sose_param->visible) {
                    $parameters['E'][$key] = $param;

                }
                if (strpos($param->sose_param->path, 'Group F') !== false && $param->sose_param->visible) {
                    $parameters['F'][$key] = $param;

                }

                if (strpos($param->sose_param->path, 'Group L') !== false && $param->sose_param->visible) {
                    $parameters['L'][$key] = $param;

                }
                if (strpos($param->sose_param->path, 'Group Z') !== false && $param->sose_param->visible) {
                    $parameters['Z'][$key] = $param;

                }
            } catch (\Exception $exception) {
                echo $param->id;
            }
        }

        $b_units = [];
        $a = 0;
        $count = 0;
        foreach ($parameters['B'] as $let => $b) {
            if ($a != 0) {
                $b_units[$let] = $b;
            } else {
                $first = $b;
                if(count($parameters['B']) < 5){
                    $b_units[$let] = $b;
                }
            }
            $a++;
        }

//        if (count($b_units) == 1){
//            foreach ($b_units as $key => $value){
//                for ($count = $key+1; $count <= $key+10; $count++){
//                    $b_units[$count] = $value;
//                }
//            }
//        }

        $removable_b_units = count($b_units) % 10;
        if (count($b_units) < 5){
            $b_units = array_splice($b_units, count($b_units) - $removable_b_units, $removable_b_units);
        }
        else{
            array_splice($b_units, count($b_units) - $removable_b_units, $removable_b_units);
        }

        $new_params_with_subheadings = [];
        $i = 1;
        foreach ($parameters as $item => $value) {
            if ($item == "B") {
                continue;
            }

            foreach ($parameters[$item] as $key => $parameter) {
                $find_subheading = explode("|", $parameter->sose_param->param_path);
                if (isset($find_subheading[1]) && (strlen($find_subheading[1]) > 0 && strlen(trim($find_subheading[1])) != 0)) {
                    $find_subheading = $find_subheading[1];
//                $find_subheading = str_replace(',',' ',$find_subheading);
//                echo $find_subheading.'<br>';

                } else {
                    $i++;
                    $find_subheading = 'nosub' . $i;
                }


                $new_params_with_subheadings[$item][$find_subheading][] = $parameter;
            }

        }

//        foreach ($new_params_with_subheadings['A'] as $new)
//        {
//           foreach ($new as $a )
//           {
//               echo '<pre>';
//               var_dump($a->sose_param->parameter_code,$a->sose_param->id,$a->sose_param->description_ru);
//
//           }
//
//        }
//        die;
        if ($client) {
            $parameters_values = SoseParameterValue::query()
                ->where('id_company', $client_id)->where('sose_questionnaire_id', $questionnaire_id)->get();

            if ($request->isMethod('post')) {
                $requests = [];
                foreach ($request->all() as $key => $string) {
                    $string = preg_replace('/\s+/', '', $string);
                    $requests[$key] = $string;
                }

                if ($request->input('action') == 'evaluate')
                {
                    $validate = SoseCheck::validate($requests);
                    if (is_null($validate) || empty($validate)) {
                        $validate = true;
                        $status = Questionnaire::change_status($questionnaire_id, SoseQuestionnaireStatus::SUBMITTED);
                        if ($status) {
                            (new GericoController())->runEvaluation('test',App::getLocale(),$client->ID_company,$questionnaire_id);
                            return response()->json([
                                'statusMessage' => 422,
                                'output' => []
                            ],200);
                        }
                    }
                    return $this->_response_body($validate);
                }
            }

            if(Auth::check()){
//                echo "<pre>";
//                print_r($questionnaire->toArray());
//                die();
                if ($questionnaire->company->questionnaire_status == 1 || (Is::supervisor() && (in_array($questionnaire->company->questionnaire_status, array(2,3,6)))) || Is::admin()){
                    $regions = GeographicalAreas::query()
                        ->where('country', '=', GeographicalAreas::COUNTRY)->groupBy('region_description')->get();
                    return view('client.questionnaire',
                        [
                            'b_units' => $b_units,
                            'first' => $first,
                            'questionnaire' => $questionnaire,
                            'values' => $parameters_values,
                            'preview_mode' => false,
                            'regions' => $regions,
                            'client' => $client,
                            'parameters' => $new_params_with_subheadings]);
                }
                else{
                    return redirect(App::getLocale().'/manager');
                }
            }
            elseif ($questionnaire->company->questionnaire_status == 1){
                $regions = GeographicalAreas::query()
                    ->where('country', '=', GeographicalAreas::COUNTRY)->groupBy('region_description')->get();
                return view('client.questionnaire',
                    [
                        'b_units' => $b_units,
                        'first' => $first,
                        'questionnaire' => $questionnaire,
                        'values' => $parameters_values,
                        'preview_mode' => false,
                        'regions' => $regions,
                        'client' => $client,
                        'parameters' => $new_params_with_subheadings]);
            }
            else{
                return view('client.evaluated');
            }
        }
        return abort(404);
    }

    /**
     * @param Request $request
     * @param $test
     * @param $lang
     * @param $client_id
     * @param $questionnaire_id
     */
    public function edit_company_list(Request $request, $test, $lang, $client_id, $questionnaire_id)
    {
        $client = CompanyList::getByCompanyId($client_id);
        $client->company_name = $request->input('client');
        $client->email_manager = $request->input('client_mail');
        $client->INN_number = $request->input('inn');
        $client->main_activity = $request->input('activity');
        $client->region_code = $request->input('area_city');
        $client->region = $request->input('area_region');
        $client->save();
    }

    public function get_area_city(Request $request)
    {
        $progressive_region_number = GeographicalAreas::query()
            ->where('area_code', $request->input('region_number'))
            ->first();

        $city = GeographicalAreas::query()
            ->where('progressiveRegionNumber', $progressive_region_number->progressiveRegionNumber)
            ->groupBy('area_name')
            ->get();

        return $this->_response_body([$city, $progressive_region_number->region_description]);
    }

    public function product_codes(Request $request,$test, $lang, $ateco_id)
    {
        $soseStudyIdsForProductCodesPdf = [329, 331, 332, 333, 335, 336];
        $soseAtecoStudiesEquivalenceRest = SoseAtecoStudiesEquivalence::query()->whereIn('sosestudy_id', $soseStudyIdsForProductCodesPdf)->get();

        if (!empty($soseAtecoStudiesEquivalenceRest->toArray())) {
            foreach ($soseAtecoStudiesEquivalenceRest as $item) {
                if ($ateco_id == $item->soseateco_id) {
                    $product_code = env('APP_URL_NAME') . '/' . 'site/pdf/D329.pdf';
                    return $this->_response_body($product_code);
                }
            }
        }

        $sose_id = SoseAtecoStudiesEquivalence::query()->where('soseateco_id',$ateco_id)->first();
        if ($sose_id)
        {
            $sose_id = $sose_id->sosestudy_id;

            if (file_exists(public_path('/site/pdf/D'.$sose_id.'.pdf')))
            {
                $product_code = env('APP_URL_NAME').'/'.'site/pdf/D'.$sose_id.'.pdf';
                return $this->_response_body($product_code);
            }


        }

        return $this->_response_body(false);
    }

    public function delete_questionnaire(Request $request)
    {
        if ($request->isMethod('post'))
        {
            $client_id = $request->input('client_id');
            $questionnaire_id = $request->input('questionnaire');

            Questionnaire::query()->where('id',$questionnaire_id)->delete();
            CompanyList::query()->where('ID_company',$client_id)->delete();

            return $this->_response_body(true);
        }
    }


    public function print($test, $lang, $client_id, $questionnaire_id, $ateco_id)
    {
        $client = CompanyList::query()->where('ID_company', $client_id)->first();
        $questionnaire = Questionnaire::get_by_id($questionnaire_id);
        $params = SoseStudiesParameter::get_by_study_id($questionnaire->sose_study_id);
        $sose_ateco_study = SoseAtecoStudiesEquivalence::query()->where('soseateco_id', $ateco_id)->first();
        $study = SoseStudy::query()->find($sose_ateco_study->sosestudy_id);
        $sose_ateco = SoseAteco::query()->find($ateco_id);
        $study_code = $study->study_code;
        $ateco_code = $sose_ateco->ateco_code;
        $ateco_description = $sose_ateco->name;
        $parameters_all = [];
        $ateco_description = explode("[", $ateco_description);
        $area_name = GeographicalAreas::query()->where('area_code', '=', $client->region_code)->pluck('area_name')->first();

        foreach ($params as $key => $param) {
            if (!isset($param->sose_param->path) || is_null($param->sose_param)) {
                continue;
            }

            if (strpos($param->sose_param->path, 'Group 0') !== false && $param->sose_param->visible) {
                $parameters_all['0'][$key] = $param;
                $parameters_all['0_path'] = $param->sose_param;
            }
            if (strpos($param->sose_param->path, 'Group A') !== false && $param->sose_param->visible) {
                $parameters_all['A'][$key] = $param;
                $parameters_all['A_path'] = $param->sose_param;
            }
            if (strpos($param->sose_param->path, 'Group B') !== false && $param->sose_param->visible) {
                $parameters_all['B'][$key] = $param;
                $parameters_all['B_path'] = $param->sose_param;

            }
            if (strpos($param->sose_param->path, 'Group C') !== false && $param->sose_param->visible) {
                $parameters_all['C'][$key] = $param;
                $parameters_all['C_path'] = $param->sose_param;

            }

            if (strpos($param->sose_param->path, 'Group D') !== false && $param->sose_param->visible) {
                $parameters_all['D'][$key] = $param;
                $parameters_all['D_path'] = $param->sose_param;

            }
            if (strpos($param->sose_param->path, 'Group E') !== false && $param->sose_param->visible) {
                $parameters_all['E'][$key] = $param;
                $parameters_all['E_path'] = $param->sose_param;

            }
            if (strpos($param->sose_param->path, 'Group F') !== false && $param->sose_param->visible) {
                $parameters_all['F'][$key] = $param;
                $parameters_all['F_path'] = $param->sose_param;

            }
            if (strpos($param->sose_param->path, 'Group X') !== false && $param->sose_param->visible) {
                $parameters_all['X'][$key] = $param;
                $parameters_all['X_path'] = $param->sose_param;

            }
            if (strpos($param->sose_param->path, 'Group L') !== false && $param->sose_param->visible) {
                $parameters_all['L'][$key] = $param;
                $parameters_all['L_path'] = $param->sose_param;

            }
            if (strpos($param->sose_param->path, 'Group Z') !== false && $param->sose_param->visible) {
                $parameters_all['Z'][$key] = $param;
                $parameters_all['Z_path'] = $param->sose_param;

            }

        }
        $b_units = [];
        $a = 0;
        $count = 0;
        $first = [];
        foreach ($parameters_all['B'] as $let => $b) {

            if ($a != 0) {

//                    echo $let.'<pre>';
                $b_units[$let] = $b;

            } else {
                $first = $b;
                if(count($parameters_all['B']) < 5){
                    $b_units[$let] = $b;
                }
            }
            $a++;
        }
        $removable_b_units = count($b_units) % 10;
        if (count($b_units) < 5){
            $b_units = array_splice($b_units, count($b_units) - $removable_b_units, $removable_b_units);
        }
        else{
            array_splice($b_units, count($b_units) - $removable_b_units, $removable_b_units);
        }


        $parameters = [];
        $new_params_with_subheadings = [];

        $i = 0;
        foreach ($parameters_all as $item => $value) {
            if ($item == "B") {
                continue;
            }
            if (strpos($item, '_path') !== false) {
                $new_params_with_subheadings[$item] = $value;
            }
            if (strlen($item) > 1) {
                $parameters[$item] = $parameters_all[$item];
            } else {
                foreach ($parameters_all[$item] as $key => $parameter) {

                    $find_subheading = explode("|", $parameter->sose_param->param_path);
                    if (isset($find_subheading[1]) && (strlen($find_subheading[1]) > 0 && strlen(trim($find_subheading[1])) != 0)) {
                        $find_subheading = $find_subheading[1];


                    } else {
                        $i++;
                        $find_subheading = 'nosub' . $i;
                    }
                    $new_params_with_subheadings[$item][$find_subheading][] = $parameter;

                }
            }
        }
        $parameters = $new_params_with_subheadings;

//        foreach ($new_params_with_subheadings['C'] as $p => $c)
//        {
//            if (strpos($p,'nosub') === false)
//            {
//                echo $p.'<br>';
//            }
//        }
//        die;



        if ($client) {
            $parameters_values = SoseParameterValue::query()
                ->where('id_company', $client_id)->where('sose_questionnaire_id', $questionnaire_id)->get();
            $mpdf = new Mpdf();
            $mpdf->useActiveForms = true;
            $mpdf->debug = true;

            $area_region = GeographicalAreas::query()->where('area_code',$client->region)->first();
            if($area_region)
            {
                $area_region = $area_region->area_name;
            }
            $html = view('client.print',
                [
                    'area_name' => $area_name,
                    'client' => $client,
                    'study_code' => $study_code,
                    'b_units' => $b_units,
                    'first' => $first,
                    'area_region' => $area_region,
                    'ateco_description' => $ateco_description[0],
                    'ateco_code' => $ateco_code,
                    'questionnaire' => $questionnaire,
                    'parameters' => $parameters,
                    'values' => $parameters_values,
                ]);

//            $html = view('account.filled_pdf',
//                [
//                    'company' => $client,
//                    'study_code' => $study_code,
//                    'b_units' => $b_units,
//                    'first' => $first,
//                    'area_region' => $area_region,
//                    'ateco_description' => $ateco_description[0],
//                    'ateco_code' => $ateco_code,
//                    'questionnaire' => $questionnaire,
//                    'parameters' => $parameters,
//                    'values' => []]);



            $mpdf->WriteHTML($html);
//            $mpdf->SetJS('var aInputs = document.querySelectorAll("input[type=\'checkbox\']");
//        for (var i=0;i<aInputs.length;i++) {
//            if (aInputs[i].value == 1) {
//                aInputs[i].checked =true;
//            }
//        };');  //JS code with <script></script> tags.
            ob_clean();

            $mpdf->Output($study_code . '.pdf', 'D');

        }


    }

    public function generateReportFromExcelToPdf()
    {
        $path = public_path('ECOLO_ReportType1_template_ru_39_1175_20191204 (1).xls');

        $allData = Excel::load($path, function ($reader) {
            return $reader->calculate(true);

            echo '<pre>';
            dd($reader->toArray()[4]);
            die;

            foreach ($reader->toArray() as $row) {
                echo '<pre>';
                print_r($row);

            }
        });
        dd($allData->toArray());
        $pdf = newPDF::loadView('account.viewPdf');
        $pdf->setOption('enable-javascript', true);
        $pdf->setOption('javascript-delay', 5000);
        $pdf->setOption('enable-smart-shrinking', true);
        $pdf->setOption('no-stop-slow-scripts', true);
        return $pdf->stream();


        $excel = Importer::make('Excel');
        $excel->load($path);
        $collection = $excel->getCollection();

        foreach ($collection as $e) {
            echo '<pre>';
            var_dump($e);
        }

//        $file = public_path('report.xls');
//
//        $excel = Excel::import($file,function ($reader){
//            foreach ($reader->toArray() as $row) {
//                var_dump($row);
//            }
//        });


    }

    public function send_email_link(Request $request)
    {
        if ($request->isMethod('post'))
        {

            $rules = [
                'client_email' => 'required|email'
            ];

            $validator = Validator::make($request->all(),$rules);

            if ($validator->fails())
            {
                return $this->_response_body($validator->messages(),StatusCode::HTTP_UNPROCESSABLE_ENTITY);
            }

            $email = $request->input('client_email');
            $link = $request->input('link');
            $data['url'] = $link;
            $client = Client::query()->where('email',$email)->first();
            $username = $client ? $client->company_name : '';
            $data['username'] = $username;
            try {
                $mailing = Mail::to($email)->send(new QuestionnaireLinkEmail($data));
                var_dump($mailing);
            } catch (\Exception $e){
                echo '<pre>';
                var_dump($e->getMessage());
                die;
            }
        }
    }
}
