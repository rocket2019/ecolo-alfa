<?php

namespace App\Http\Controllers;

use App\Http\Services\StatusCode;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    const GET = 'get';
    const POST = 'post';

    /**
     * @param null $data
     * @param int $code
     * @param null $status_message
     * @return \Illuminate\Http\JsonResponse
     */
    protected function _response_body($data = null, $code = StatusCode::HTTP_OK, $status_message = null)
    {
        $status_message = (!empty($status_message))
            ? $status_message
            : 'HTTP response '.$code;

        return response()->json([
            'statusMessage' => $status_message,
            'body' => $data
        ],$code ? $code : StatusCode::HTTP_OK);
    }
}
