<?php

namespace App\Http\Controllers;

use App\Http\Models\Coefficient;
use App\Http\Models\CompanyList;
use App\Http\Models\Dictionary;
use App\Http\Models\GeographicalAreas;
use App\Http\Models\GlobalVariable;
use App\Http\Models\Questionnaire;
use App\Http\Models\Role;
use App\Http\Models\SoseAteco;
use App\Http\Models\SoseAtecoStudiesEquivalence;
use App\Http\Models\SoseOutputParameter;
use App\Http\Models\SoseOutputParameterValue;
use App\Http\Models\SoseParameter;
use App\Http\Models\SoseParameterValue;
use App\Http\Models\SoseQuestionnaireResult;
use App\Http\Models\SoseQuestionnaireStatus;
use App\Http\Models\SoseStudiesOutputParameter;
use App\Http\Models\SoseStudiesParameter;
use App\Http\Models\SoseStudy;
use App\Http\Services\StatusCode;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel as Excel;
use App\Exports\GericoSheets;
use PhpOffice\PhpSpreadsheet\IOFactory;
use TwigBridge\Facade\Twig;
use App\Http\Services\Constants;


class GericoController extends Controller
{
    const API_URL = 'http://161.35.18.151/runEvaluationIncoming/';

    public function runEvaluation($test, $lang, $company_id, $questionnaire_id)
    {

        try {

            $if_company_exist = CompanyList::query()->where('ID_company', $company_id)->first();
            if (!$if_company_exist) {
                throw new \Exception("The company does not exist");
            }

            $questionnaire = Questionnaire::query()->where('id', $questionnaire_id)->first();


            if (!$questionnaire) {

                throw new \Exception("The questionnaire does not exist");
            }

            $url = env('API_URL') . $company_id . '/' . $questionnaire_id.'/'.env('APP_CLIENT');

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache"

                ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                echo "cURL Error #:" . $err;
            }

            if (is_string($response) && !is_object(json_decode($response)))
            {
                $incorrectData = 'Incorrect data submitted for evaluation';

                if ($lang == "ru") {
                    $incorrectData = 'Для оценки отправлены неверные данные';
                    $italianText = explode("\n",$response);

                    $errorMessageInRussian = [
                        '--- Проверка целостности файла прошла успешно ---',
                        'Позиция $numberPosition (Налоговый код: $CF, Год выпуска: $year, Код атеко:$att , Прог.: $prog)',
                        '(*) Позиция не соответствует спецификации',
                        'Поле $field неверно.'
                    ];

                    $italianText[0] = $errorMessageInRussian[0];
                    $italianText[2] = $errorMessageInRussian[2];

                    foreach ($italianText as $key => $value) {
                        if (preg_match("~Posizione\s?(\d+)\s?.*C\.?F\.?\s?:\s?([0-9]+)\,\s?Anno Modello\s?:\s?([0-9]+)\,\s?Cod\. att\s?:\s?([0-9]+)\,?\s?Prog\s?:\s?([0-9]+)~i", $value, $match)) {
                            $italianText[$key] = str_ireplace(['$numberPosition','$CF','$year','$att','$prog'],[$match[1], $match[2], $match[3], $match[4], $match[5]], $errorMessageInRussian[1]);
                        }

                        if (preg_match("~.*campo\s?([A-Z]{1}[0-9]+)\s?e'\s?errato\.?~i", $value, $match)) {
                            $italianText[$key] = str_ireplace('$field',$match[1],$errorMessageInRussian[3]);
                        }
                    }

                    $response = implode("<br>", $italianText);
                }

                Questionnaire::change_status($questionnaire_id,SoseQuestionnaireStatus::HAVE_GERICO_ERRORS);
                CompanyList::change_status($company_id,SoseQuestionnaireStatus::HAVE_GERICO_ERRORS);
                $data = !is_null($response) ? $response :  $incorrectData;
                return $this->_response_body($data, StatusCode::HTTP_UNPROCESSABLE_ENTITY);
            }



            $output = json_decode($response);


            $sose_questionnaire_result = new SoseQuestionnaireResult();

            $parameter_values = SoseParameterValue::getQuestionnaireResults($questionnaire_id);

            $sose_questionnaire_result->sose_questionnaire_id = $questionnaire_id;

            $sose_questionnaire_result->regional_revenue_coefficient = $this->regionalRevenueCoefficients($parameter_values, $questionnaire, $if_company_exist);



            $sose_questionnaire_result->user_id = Auth::id();
            $sose_questionnaire_result->save();

            foreach ($output->output as $key => $value) {

                if (strpos($value, '=') !== false) {
                    $explode = explode("=", $value);

                    if (isset($explode[0]) && $explode[1]) {
                        $output_parameter = SoseOutputParameter::query()->where('parameter_code', $explode[0])->first();
                        if ($output_parameter) {
                            $normalize_value = $this->normalizeSoseOutputParameterValues($output_parameter, $explode[1]);

                            $if_output_parmater_exist = SoseOutputParameterValue::query()
                                ->where('parameter_id', $output_parameter->id)
                                ->where('id_company', $company_id)
                                ->first();
                            if ($if_output_parmater_exist) {
                                $sose_output_parameter_value = $if_output_parmater_exist;
                            } else {
                                $sose_output_parameter_value = new SoseOutputParameterValue();
                            }

                            $sose_output_parameter_value->parameter_id = $output_parameter->id;
                            $sose_output_parameter_value->id_company = $company_id;
                            $sose_output_parameter_value->user_id = Auth::id();
                            $sose_output_parameter_value->sose_questionnaire_result_id = $sose_questionnaire_result->id;
                            $sose_output_parameter_value->value = $normalize_value;
                            $sose_output_parameter_value->save();


                        }

                    }
                }

            }

            $gerico_output = file_get_contents($output->filepath_esito);
            $valid = 1;
            if (strpos($gerico_output, 'La posizione presenta errori bloccanti per il calcolo') !== false)
            {
                $valid = 0;
            }
            $questionnaire->output_file_url = $output->filepath_output;
            $questionnaire->sose_questionnaire_status_id = SoseQuestionnaireStatus::IN_BACKOFFICE_ELABORATION;
            $questionnaire->valid = $valid;
            $questionnaire->save();

            $file_lines = file($output->filepath_esito);
            $final_output = '';

            $russianTranslation = [
                'Сумма полей от $1 до $2 должна быть равна 100',
                'Сумма процентов в полях от $1 до $2 должна быть меньше или равна 100',
            ];
            foreach ($file_lines as $line) {
                $line = trim($this->removeSpecialChars($line));
                $trans = Dictionary::query()->where('toTranslate','like', $line)->first();
                if ($trans)
                {
                    $line = str_replace($line,$trans->translated,$line);
                }

                if ($lang == "ru") {
                    if (preg_match('~La somma delle percentuali dei campi da\s([a-z]\d+\sa\s[a-z]\d+)~i',$line,$match)) {
                        preg_match_all("~[a-z]\d+~i",$line,$codes);

                        $line = str_ireplace('$1 до $2',$codes[0][0].' до '.$codes[0][1],$russianTranslation[0]);
                    }
                }


                $final_output = $final_output.$line.'<br>';
            }


            if (!is_null(Auth::user()) && Auth::user()->role_id === 1){
                $updateArray = [
                    'questionnaire_status' => '2',
                    'OKEI_code' => 1
                ];
            }
            else{
                $updateArray = [
                    'questionnaire_status' => '2',
                ];
            }

            CompanyList::query()
                ->where('ID_company', '=', $questionnaire->id_company)
                ->update($updateArray);

            return response()->json(['output' => $final_output]);



        } catch (\Exception $exception) {
            echo '<pre>';
            var_dump($exception->getMessage(), $exception->getCode());
            die;

            // write somewhere that gerico does'nt run


        }

    }

    public function removeSpecialChars($line)
    {

        $str = '';
        $chars = ['---', '(*)'];
        foreach ($chars as $value)
        {
            $line = str_replace($value,'',$line);
        }

        return $line;

    }

    public function regionalRevenueCoefficients($parameterValues, $questionnaire, $company)
    {
        $revenue = 0.0;
        /*TROVIAMO IL REVENUE DELLA SOCIETA CHE E' SCRITTO IN CAMPO F00101 PER CONFRONTARLO CON IL THRESHOLD*/
        foreach ($parameterValues as $param) {
            /** @var SoseParameterValue $param */
            if (is_object($param->param) && $param->param->parameter_code == 'F00101') {
                $revenue = $param->value;
                break;
            }
        }




        $ateco_id = $questionnaire->sose_ateco->id;
        $ateco_equivalence = SoseAtecoStudiesEquivalence::query()->where('soseateco_id',$ateco_id)->first();
        $sose_study = SoseStudy::find($ateco_equivalence->sosestudy_id);
        $industry_group_code = $sose_study->study_code;

        $region_code = $company->region_code ? $company->region_code : $company->region;

        $coefficient = Coefficient::query()
            ->where('industry_group_code', $industry_group_code)
            ->where('region_code', $region_code)
            ->first();


        /*CERCHIAMO REGIONAL COEFFICIENT SULLA BASE DI CODICE REGIONE E CODICE GRUPPO INSUSTRALE DELLA SOCIETA*/
//        /** Regional Revenue Coefficient */
//        $coefficient = $coefficientRepo->findOneBy(array(
//            'industryGroupCode' => $soseQuestionnaire->getAteco()->getIndustryGroupCode(),
//            'regionCode'        => $soseQuestionnaire->getIdCompany()->getRegionCode()
//        ));

        if ($coefficient) {
            /*SE COEFFICIENTE E' STATO TROVATO DOBBBIAMO RECUPERARE IL VALORE DEL THRESHOLD DALLA ENTITY GLOBAL VARIABLKE
            PER CAPIRE SE QUESTA SOCIETA E' DI CATEGORIA MICRO O SMALL*/
            $micro_small_company_threshold = GlobalVariable::query()->where('variable', 'micro_small_company_threshold')->first();

            $micro_small_company_threshold = (double)$micro_small_company_threshold->value;

            if ($revenue < $micro_small_company_threshold) {
                /*SE REVENUE DELLA SOCIETA E' MINORE DEL THERSHOLD, ALLLORA E' UNA SOCIOETA
                DI CATEGORIA MICRO E QUINDI PRENDIAMO IL COEFFICIENTE DAL CAMPO MICRO*/
                return $coefficient->micro;
            } else {
                return $coefficient->small;
            }
        }

        return null;
    }

    public function normalizeSoseOutputParameterValues($parameterValue, $value)
    {

        $ruble_to_euro_tax = GlobalVariable::query()->where('variable', 'ruble_to_euro_tax')->first()->value;
        $ruble_to_euro_tax = (double)$ruble_to_euro_tax;
        $gerico_scale_factor = (double)GlobalVariable::query()->where('variable', 'gerico_scale_factor')->first()->value;
        $complete_factor = $ruble_to_euro_tax * $gerico_scale_factor;


        try {
            $parameter = $parameterValue->parameter_code;
            $valueType = $parameterValue->value_type;


            if (in_array($valueType, array('CB', 'N1'))) {

                if ($parameterValue->normalization != null) {
                    $hashmap = json_decode($parameterValue->normalization, true);
                    if (count($hashmap) > 1) {
                        $value = $hashmap[$value];
                        return $value;
                    }
                    if (array_key_exists((int)$value, $hashmap)) {
                        $value = $hashmap[(int)$value];
                    }
                }
                return $value;
            }

            if ($valueType[0] == 'N' && $parameterValue->normalization) {

                $value = $value / $parameterValue->normalization;
            }

            if ($valueType == "VAL") {
                $value = (int)(((double)$value) / $complete_factor);
            }


            return $value;
        } catch (\Exception $exception) {
            echo $exception->getLine();
            die;
        }


    }

    public function gerico_txt($test, $lang, $company_id, $questionnaire_id)
    {
        $questionnaire = Questionnaire::query()->where('id', $questionnaire_id)->first();
        $fileroot = $questionnaire->output_file_url;
        return response()->download($fileroot);
        return $fileroot;
    }

    public function generateRawResult($company_id, $questionnaire_id)
    {

        $export_data = [];
        $sose_questionnaire_result_id = SoseQuestionnaireResult::query()
            ->where('sose_questionnaire_id', $questionnaire_id)
            ->where('user_id',Auth::id())
            ->pluck('id')
            ->toArray();


        $sose_output_parameter_values = SoseOutputParameterValue::query()
            ->where('id_company', $company_id)
            ->whereIn('sose_questionnaire_result_id', $sose_questionnaire_result_id)
            ->pluck('parameter_id')
            ->toArray();


        $sose_study_id = Questionnaire::query()->find($questionnaire_id);
        if ($sose_study_id) {
            $sose_study_id = $sose_study_id->sose_study_id;
        }


        $sose_studies_output_parameters = SoseStudiesOutputParameter::query()->where('sosestudy_id', $sose_study_id)->pluck('soseoutputparameter_id')->toArray();
        $sose_output_parameters = SoseOutputParameter::query()->whereIn('id', $sose_studies_output_parameters)->get();


        foreach ($sose_output_parameters as $key => $value) {
            $raw_result_value = '0';
            if (in_array($value->id, $sose_output_parameter_values)) {
                $raw_result_value = SoseOutputParameterValue::query()
                    ->whereIn('sose_questionnaire_result_id', $sose_questionnaire_result_id)
                    ->where('parameter_id', $value->id)
                    ->first()
                    ->value;
            } else {
                if ($value->normalization != null) {
                    $hashmap = json_decode($value->normalization, true);
                    $raw_result_value = $hashmap[0];
                }
            }


            $export_data[$value->parameter_code]['id'] = $value->id;
            $export_data[$value->parameter_code]['description'] = $value->description_ru;
            $export_data[$value->parameter_code]['code'] = $value->parameter_code;
            $export_data[$value->parameter_code]['value'] = $raw_result_value;
        }

        return $export_data;


    }

    public function gericoTelematic($company_id, $questionnaire_id)
    {
        $questionnaire = Questionnaire::query()->find($questionnaire_id);

        $study_code = SoseStudy::query()->find($questionnaire->sose_study_id);

        $study_code = $study_code->study_code;


        $atecoCode = SoseAteco::query()->find($questionnaire->sose_ateco_id);

        $atecoCode = $atecoCode->ateco_code;

        $companyIdStr = str_pad($company_id, 20, "0", STR_PAD_LEFT);

        $dynamicHeader = $this->dynamicHeaderGericoAction($atecoCode, $companyIdStr, $study_code);


        /**
         * 1: head record
         * 1: info record
         * 1: footer record
         * n: a record every 100 parameters
         */
        $valuesMap = $this->getSoseParameterValuesTelematicMap($company_id, $questionnaire_id, $study_code);

        $rows = 3 + ceil(count($valuesMap) / 100);
        $rowsNum = str_pad($rows, 8, "0", STR_PAD_LEFT);

        $data['dynamicHeader'] = $dynamicHeader;
        $data['rowsNum'] = $rowsNum;
        $data['valuesMap'] = $valuesMap;
        $data['atecoCode'] = $atecoCode;
        $data['header'] = $dynamicHeader;

        $ret = Twig::render('gerico.showTelematic.twig', $data);

        $ret = str_replace("\n", "\r\n", $ret);

        return $ret;
    }

    public function generateExcel($valuesMap, $study_code, $companyIdStr, $rowsNum, $atecoCode, $dynamicHeader, $rawResults)
    {


        return Excel::download(new GericoSheets($valuesMap, $study_code, $companyIdStr, $rowsNum, $atecoCode, $dynamicHeader, $rawResults), 'test.xlsx');

    }

    public function exportExcel($test, $lang, $company_id, $questionnaire_id, $atecoCode, $study_code)
    {


        return $this->excel(App::getLocale(), $company_id, $questionnaire_id);


        $questionnaire = Questionnaire::get_by_id($questionnaire_id);

        $params = SoseStudiesParameter::get_by_study_id($questionnaire->sose_study_id);
        $date = Carbon::parse($questionnaire->created_at)->format('Y-m-d');
        $raw_result = $this->generateRawResult($company_id, $questionnaire_id);
        ob_end_clean();
        ob_start();
        $values = SoseParameterValue::query()
            ->where('id_company', $company_id)->where('sose_questionnaire_id', $questionnaire_id)->get();
        return Excel::download(new GericoSheets($study_code, $company_id, $atecoCode, $questionnaire_id, $params, $values, $raw_result),
            'ECOLO_ReportType1_template' . '_' . App::getLocale() . '_' . $company_id . '_' . $questionnaire_id . '_' . $date . '.xls');

    }

    public function dynamicHeaderGericoAction($atecoCode, $companyIdStr, $studyCode)
    {
        //UG41U UG91U UG99U VG53U VG74U VG77U VG82U

        $comune = "H501";
        $pensionato = "0";

        if ($studyCode == "UG41U") {
            $lavoroDipendenteTempoPieno = "1";
            $lavoroDipendenteTempoParziale = "0";
            $lavoroDipendenteTempoParzialeOreSettimanali = "00";
            $annoInizioAttivita = "2010";
            $numeroAnniInterruzioneAttivita = "0000";
            $spazioVuoto = "                                        ";

            $stringaRiempimento = "${comune}${lavoroDipendenteTempoPieno}${lavoroDipendenteTempoParziale}${lavoroDipendenteTempoParzialeOreSettimanali}${annoInizioAttivita}${numeroAnniInterruzioneAttivita}${spazioVuoto}";
        } elseif ($studyCode == "UG91U") {
            $settimaneLavorate = "00000000086";
            $oreLavorate = "00000000040";
            $annoInizioAttivita = "2011";
            $spazioVuoto = "                          ";
            $stringaRiempimento = "${comune}${settimaneLavorate}${oreLavorate}${annoInizioAttivita}${spazioVuoto}";
        } elseif ($studyCode == "UG99U") {
            $cooperativa = "0";
            $lavoroDipendenteTempoPieno = "1";
            $lavoroDipendenteTempoParziale = "0";
            $altreAttivita = "0";
            $annoIscrizioneAlbi = "2010";
            $annoInizioAttivita = "2010";
            $annoInterruzioneAttivita = "0000";

            $spazioVuoto = "                                   ";
            $stringaRiempimento = "${comune}${cooperativa}${lavoroDipendenteTempoPieno}${lavoroDipendenteTempoParziale}${pensionato}${altreAttivita}${annoIscrizioneAlbi}${annoInizioAttivita}${annoInterruzioneAttivita}${spazioVuoto}";
        } elseif ($studyCode == "VG53U") {
            $lavoroDipendenteTempoPieno = "1";
            $lavoroDipendenteTempoParziale = "0";
            $lavoroDipendenteTempoParzialeOreSettimanali = "00";
            $annoInizioAttivita = "2010";

            $spazioVuoto = "                                                ";
            $stringaRiempimento = "${lavoroDipendenteTempoPieno}${lavoroDipendenteTempoParziale}${lavoroDipendenteTempoParzialeOreSettimanali}${annoInizioAttivita}${spazioVuoto}";
        } elseif ($studyCode == "VG74U") {

            $lavoroDipendenteTempoPieno = "1";
            $lavoroDipendenteTempoParziale = "0";
            $lavoroDipendenteTempoParzialeOreSettimanali = "00";
            $altreAttivita = "0";
            $annoInizioAttivita = "2010";
            $iscrizioneAlbo = "0";

            $spazioVuoto = "                                         ";
            $stringaRiempimento = "${comune}${lavoroDipendenteTempoPieno}${lavoroDipendenteTempoParziale}${lavoroDipendenteTempoParzialeOreSettimanali}${pensionato}${altreAttivita}${annoInizioAttivita}${iscrizioneAlbo}${spazioVuoto}";

        } elseif ($studyCode == "VG77U") {
            $trasportoGondola = "1";
            $serviziOrmeggio = "0";
            $spazioVuoto = "                                                      ";

            $stringaRiempimento = "${trasportoGondola}${serviziOrmeggio}${spazioVuoto}";
        } elseif ($studyCode == "VG82U") {
            $lavoroDipendenteTempoPieno = "1";
            $lavoroDipendenteTempoParziale = "0";
            $spazioVuoto = "                                                  ";

            $stringaRiempimento = "${comune}${lavoroDipendenteTempoPieno}${lavoroDipendenteTempoParziale}${spazioVuoto}";
        } elseif ($studyCode == "VG34U") {
            $comune = "0   ";
            $pensionato = " ";
            $stringaRiempimento = "${comune}${pensionato}33333333333                                        ";
        } elseif ($studyCode == "UG90U") {
            $pensionato = "3";
            $stringaRiempimento = "${comune}${pensionato}33333333333                                        ";
        } else {
            $stringaRiempimento = "${comune}${pensionato}33333333333                                        ";
        }


        $data['atecoCode'] = $atecoCode;
        $data['companyIdStr'] = $companyIdStr;
        $data['stringaRiempimento'] = $stringaRiempimento;
        $data['studyCode'] = $studyCode;

        return Twig::render('gerico.gericoHeading', $data);

        //$ret = str_replace("\n", "\r\n", $ret);


    }

    public function getSavedSoseParameterValuesWithQuestionnaire($company_id, $questionnaire_id)
    {
        $ret = array();
        $parameterValues = null;

        $parameterValues = SoseParameterValue::query()
            ->where('id_company', $company_id)
            ->where('sose_questionnaire_id', $questionnaire_id)
            ->orderByDesc('balance_year')
            ->get();


        if ($parameterValues) {
            foreach ($parameterValues as $parameterValue) {
                if (!$parameterValue->parameter_id) {
                    continue;
                }
                $label = SoseParameter::query()->find($parameterValue->parameter_id);
                $label = $label->parameter_code;
                $ret[$label] = $parameterValue;
            }

            ksort($ret);
        }

        return $ret;

    }

    public function getSoseParameterValuesTelematicMap($company_id, $questionnaire_id, $study_code)
    {
//        $globalsRepo = $this->em->getRepository('EvabetaRatingFrontendBundle:GlobalVariables');
//        $ruble_to_euro_tax = (double) $globalsRepo->get('ruble_to_euro_tax');
//        $gerico_scale_factor = (double) $globalsRepo->get('gerico_scale_factor');
//        $complete_factor = $ruble_to_euro_tax * $gerico_scale_factor;

        $soseParameterValues = $this->getSavedSoseParameterValuesWithQuestionnaire($company_id, $questionnaire_id);

        $overrideStudyParams = array(
            'VG87U' => array('A00901' => 0, 'A00902' => 0)
        );

        $valuesMap = array();

        $formuleMap = [
            'VD03U' => [
                'formule' => ["F02901 = Z00101 + Z00201 + Z00301 + F02902"]
            ],
            'VD04A' => [
                'formule' => ["F02901 = Z00101 + Z00201 + Z00301 + F02902"]
            ],
            'VD05U' => [
                'formule' => ["F02901 = E02301 + E02401 + E02501 + F02902"]
            ],
            'VD09A' => [
                'formule' => [
                    "D00601 = Z02201 + Z02301 + Z02401",
                    "D00701 = Z02501 + Z02601 + Z02701"
                ]
            ],
            'VD11U' => [
                'formule' => ["F02901 = E00801 + E00901 + E01001 + F02902"]
            ],
            'VD17U' => [
                'formule' => ["F02901 = E03901 + E04001 + E04101 + F02902"]
            ],
            'VD21U' => [
                'formule' => ["F02901 = Z00301 + Z00401 + Z00501 + F02902"]
            ],
            'VD23U' => [
                'formule' => ["F02901 = E02101 + E02201 + E02301 + F02902"]
            ],
            'VD24U' => [
                'formule' => [
                    "F02901 = Z00301 + Z00401 + Z00501 + F02902",
                    "F02901 = Z01101 + Z01201 + Z01301 + F02902"
                ]
            ],
            'VD25U' => [
                'formule' => ["F02901 = E02201 + E02301 + E02401 + F02902"]
            ],
            'VD26U' => [
                'formule' => ["F02901 = Z00701 + Z00801 + Z00901 + F02902"]
            ],
            'VD27U' => [
                'formule' => ["F02901 = Z00301 + Z00401 + Z00501 + F02902"]
            ],
            'VD28U' => [
                'formule' => ["F02901 = Z01401 + Z01501 + Z01601 + F02902"]
            ],
            'VD29U' => [
                'formule' => ["F02901 = E02701 + E02801 + E02901 + F02902"]
            ],
            'VD30U' => [
                'formule' => ["F02901 = E02201 + E02301 + E02401 + F02902"]
            ],
            'VD31U' => [
                'formule' => ["F02901 = E02101 + E02201 + E02301 + F02902"]
            ],
            'VD36U' => [
                'formule' => ["F02901 = E02701 + E02801 + E02901 + F02902"]
            ],
            'VD47U' => [
                'formule' => ["F02901 = Z02801 + Z02901 + Z03001 + F02902"]
            ],
            'VD32U' => [
                'formule' => [
                    "C00901 = Z00101 + Z00201 + Z00301",
                    "F02901 = Z01301 + Z01401 + Z01501 + F02902"
                ]
            ],
            'VD20U' => [
                'formule' => [
                    "C00901 = Z00101 + Z00201 + Z00301",
                    "F02901 = Z01101 + Z01201 + Z01301 + F02902"
                ]
            ],
            'VD15U' => [
                'formule' => [
                    "F02901 = E01401 + E01501 + E01601 + F02902"
                ]
            ],
            'VD16U' => [
                'formule' => [
                    "F02901 = Z01101 + Z01201 + Z01301 + F02902"
                ]
            ]

        ];


        foreach ($soseParameterValues as $parameterValue) {

            /** @var SoseParameterValue $parameterValue */

            /**
             * Filtraggio codici con X
             */

            try {
                $parameterCode = self::getParameterCode($parameterValue);
                if (!$parameterCode) {
                    continue;
                }
                if (substr($parameterCode, -2, 1) == 'X') {
                    continue;
                }


                if (isset($formuleMap[$study_code])) {
                    foreach ($formuleMap[$study_code]['formule'] as $formula) {
                        $parametersFormula = preg_split("/[=+-]/", $formula);
                        $parametersFormula = array_map('trim', $parametersFormula);
                        if (in_array($parameterCode, $parametersFormula) && $parameterValue->value() == null) {
                            //Todo update value to 0
                            $parameterValue->value = 0;
                        }
                    }
                }

                //Todo parameter value
                if ($parameterValue->value !== null) {
                    $padZero = 0;
                    $padSpace = 11;
                    $tmp = array();

                    /** @var SoseParameter $parameter */
                    $parameter = SoseParameter::query()->find($parameterValue->parameter_id);
                    $value = $parameterValue->value;

                    // Override values using the $overrideStudyParamsMap
                    if (isset($overrideStudyParams[$study_code][$parameterCode])) {
                        $value = $overrideStudyParams[$study_code][$parameterCode];
                    }

                    //CONVERSIONE EURO -> RUBLO
                    if ($parameter->value_type == "VAL") {

                        $complete_factor = 1;
                        $doubleValue = (((double)$value) * $complete_factor);
                        $value = round($doubleValue);
                        $decValue = round(($doubleValue - $value) * 100);

                        if ($parameter->parameter_dec_code != null && $decValue != 0) {

                            $decCode = $parameter->parameter_dec_code;
                            $decValue = str_pad(
                                $decValue,
                                $padSpace,
                                " ",
                                STR_PAD_LEFT
                            );
                            $valuesMap[$decCode] = $decValue;
                        }

                        $padZero = 11;
                    }

                    //modifica per non inviare parametri uguale a 0
//                if($parameter->getValueType() == "VAL" && $value==0){
//                    continue;
//                }

                    if (preg_match('/N([\d]+)/i', $parameter->value_type, $tmp))
                        $padZero = $tmp[1];

                    if ($parameter->value_type == 'CBN') {
                        $padZero = 3;
                    }

                    if ($parameter->value_type == 'PC') {
                        $padZero = 3;
                    }

                    if ($parameter->value_type == 'CAT') {
                        $value = "       H501";
                    }

                    if ($parameterCode == 'D00901' &&
                        $parameter->value_type == 'CBN' &&
                        strpos($parameter->description, 'Regione') !== false
                    ) {
                        $value = "012";
                    }

                    $value = str_pad(
                        $value,
                        $padZero,
                        "0",
                        STR_PAD_LEFT
                    );

                    $value = str_pad(
                        $value,
                        $padSpace,
                        " ",
                        STR_PAD_LEFT
                    );

                    $valuesMap[$parameterCode] = $value;
                }


            } catch (\Exception $ex) {
                return $ex->getLine();
            }
        }


        if (isset($valuesMap['F01901'])) {
            $valuesMap['F01901'] = $this->getValue($valuesMap, 'F01901') + 1;
            $valuesMap['F01901'] = str_pad(
                abs($valuesMap['F01901']),
                11,
                "0",
                STR_PAD_LEFT
            );
        }

        /**
         * EC-41 bsiogna cancellare canpi F02401 e F02402 da tutti questionari
         * bisogna anche non considerare questi campi in calcolo del F028 e non spedirli in report excel
         */
        //$valuesMap['F02401'] = str_pad(0, 11, " ", STR_PAD_LEFT);
        //$valuesMap['F02402'] = str_pad(0, 11, " ", STR_PAD_LEFT);


        /**
         * campo F02801 ricalcolato per evitare errori di arrotondamento
         */

        $valuesMap['F02801'] =
            $this->getValue($valuesMap, 'F00101') + $this->getValue($valuesMap, 'F00201') + $this->getValue($valuesMap, 'F00301') +
            $this->getValue($valuesMap, 'F00401') + $this->getValue($valuesMap, 'F00501') - $this->getValue($valuesMap, 'F00601') +
            $this->getValue($valuesMap, 'F00701') + $this->getValue($valuesMap, 'F00801') - $this->getValue($valuesMap, 'F00901') +
            $this->getValue($valuesMap, 'F01001') - $this->getValue($valuesMap, 'F01101') - $this->getValue($valuesMap, 'F01201') +
            $this->getValue($valuesMap, 'F01301') - $this->getValue($valuesMap, 'F01401') - $this->getValue($valuesMap, 'F01501') -
            $this->getValue($valuesMap, 'F01601') - $this->getValue($valuesMap, 'F01701') - $this->getValue($valuesMap, 'F01801') -
            $this->getValue($valuesMap, 'F01901') - $this->getValue($valuesMap, 'F02001') - $this->getValue($valuesMap, 'F02101') -
            $this->getValue($valuesMap, 'F02201') - $this->getValue($valuesMap, 'F02301') +
            $this->getValue($valuesMap, 'F02401') * (intval($this->getValue($valuesMap, 'F02402')) == 1 ? -1 : 1)
            - $this->getValue($valuesMap, 'F02501') + $this->getValue($valuesMap, 'F02601') - $this->getValue($valuesMap, 'F02701');


        if ($valuesMap['F02801'] < 0) {
            $valuesMap['F02802'] = "          1";  // Segno negativo di F02801
        } else {
            $valuesMap['F02802'] = "          0";  // Segno positivo di F02801
        }

        $valuesMap['F02801'] = str_pad(
            abs($valuesMap['F02801']),
            11,
            " ",
            STR_PAD_LEFT
        );


        if (isset($formuleMap[$study_code])) {

            $formule = $formuleMap[$study_code]['formule'];

            foreach ($formule as $formula) {

                $split_formula = preg_split('/=/', $formula);

                $op1 = preg_replace(
                    '/([A-Z]{1}[0-9]{5})/',
                    '$valuesMap[\'$1\']',
                    $split_formula[0]);

                $op2 = preg_replace(
                    '/([A-Z]{1}[0-9]{5})/',
                    '$this->getValue($valuesMap, \'$1\')',
                    $split_formula[1]);

                $final_formula = "$op1 = $op2";
                $res = eval('return ' . $final_formula . ';');

                if ($res === null) {
                    throw new \Exception('Invalid formula in gerico calculation');
                }

                $op1Trimmed = trim($split_formula[0]);
                $valuesMap[$op1Trimmed] = str_pad(
                    abs($valuesMap[$op1Trimmed]),
                    11,
                    " ",
                    STR_PAD_LEFT
                );
            }

        }


        return $valuesMap;


    }

    public function getParameterCode($parameter_id)
    {
        try {
            $parameter_code = SoseParameter::query()->find($parameter_id->parameter_id);
        } catch (\Exception $ex) {
            return $ex->getFile() . $ex->getCode();
        }


        if (isset($parameter_code->parameter_code)) {
            return $parameter_code->parameter_code;
        } else {
            return null;
        }

    }

    private function getValue($array, $label)
    {
        return isset($array[$label]) ? $array[$label] : 0;
    }

    public function excel($lang, $company_id, $questionnaire_id)
    {
//        $spreadsheet = IOFactory::load(resource_path('templates/ECOLO_ReportType1_template_ru.xls'));
//        $spreadsheet->getSheet('Fromated results')->setCellValue('A1',5);
//        dd($spreadsheet);

        $templateFile = resource_path('templates/ECOLO_ReportType1_template_'.App::getLocale().'.xls');

        $company = CompanyList::find($company_id);


        $questionnaire = Questionnaire::find($questionnaire_id);


        // ask the service for a Excel5
        $phpExcelObject = IOFactory::load($templateFile);

        $phpExcelObject->getProperties()->setCreator("ECOLO")
            ->setLastModifiedBy("ECOLO")
            ->setTitle("ECOLO Evaluation Report - Company: " . $company->company_name)
            ->setSubject("Credit Request ID: " . $questionnaire_id)
            ->setDescription("Credit Outcome ID: " . $questionnaire_id);


        // Setup CLIENT

        /*$studies = $this->getSoseStudiesFromOkved($questionnaire->getIdCompany());
        $soseStudyDescription = $this->translator->trans(
            $studies->get(0)->getDescriptionTranslationKey()
        );*/


        $okvedDescription = $questionnaire->sose_okved->description;



        $ateco = $questionnaire->sose_ateco;
//        $atecoDescription = $this->translator->trans( $ateco->getDescriptionTranslationKey(), array(), 'sose' );

        $study = $questionnaire->sose_study;
        $soseStudyDescription = $study->description;


        // Todo legal form
        if ($company->legal_forms != null){
            $legal_form = $company->legal_forms->name;
        }
        else{
            $legal_form = "";
        }


        /*CERCHIAMO NOME DELLA CITTA E SE C'E - SCRIVIAMO
        IN EXCEL SIA NOME DELLA REGIONE SIA NOME DELLA CITTA
        SE NON C'E - SCRIVIAMO SOLO NOME DELLA REGIONE*/
        $companyRegionCode = $company->region_code;
        $companyRegion = $company->region;
        $areaCode = $companyRegionCode ?? $companyRegion;

        $area = GeographicalAreas::query()->where('area_code', $companyRegionCode)->first();
        $area_region = GeographicalAreas::query()->where('area_code', $companyRegion)->first();

        $MyAreaName = '';
        if ($area) {
            $areaName = $area->area_name;
            $regionName = $area->region_description;
            $MyAreaName = $areaName != $regionName ? $regionName . ', ' . $areaName : $regionName;

        } elseif ($area_region){
            $MyAreaName = $area_region->area_name;
        }

        $reg_coeff = Coefficient::query()
                ->where('industry_group_code', '=', $soseStudyDescription)
                ->where('region_code', $areaCode)
                ->pluck('small')
                ->first()
            ?? 1;

//        $regional_revenue_coefficient = SoseQuestionnaireResult::query()->where('sose_questionnaire_id', $questionnaire_id)
//            ->whereNotNull('regional_revenue_coefficient')
//            ->orderBy('id','DESC')
//            ->first();
//
//        if ($regional_revenue_coefficient) {
//            $regional_revenue_coefficient = $regional_revenue_coefficient->regional_revenue_coefficient;
//        } else {
//            $regional_revenue_coefficient = 1;
//        }

        switch (Constants::bankName()) {
            case Constants::DEV:
                $bank_code_for_credit = 6;
                break;
            case Constants::MTB:
                $bank_code_for_credit = 7;
                break;
            case Constants::KAZAN:
                $bank_code_for_credit = 8;
                break;
            case Constants::MINBANK:
                $bank_code_for_credit = 9;
                break;
            case Constants::ALFA:
                $bank_code_for_credit = 2;
                break;
            default:
                $bank_code_for_credit = 1;
        }

        /*SETTAGGIO DEI CAMPI NELLA CLIENT WORKSHEET*/
        $phpExcelObject->setActiveSheetIndex(1)
            ->setCellValue('C2', $company->company_name)
            ->setCellValue('C3', $questionnaire_id)
            ->setCellValue('C4', $soseStudyDescription)
            ->setCellValue('C5', Carbon::parse($questionnaire->created)->format('d/m/Y'))
            ->setCellValue('C6', $questionnaire->sose_okved->okved_code)
            ->setCellValue('C7', $company->INN_number)
            ->setCellValue('C8', $legal_form)
            ->setCellValue('C9', $company->location_address)
            ->setCellValue('C10', $questionnaire->sose_okved->okved_code)
            ->setCellValue('C11', $okvedDescription)
            ->setCellValue('C12', $company->OKOPF_OKFS_code)
            ->setCellValue('C13', $company->OKEI_code)
            ->setCellValue('C14', $company->legacy_id)
            ->setCellValue('C15', $ateco->ateco_code)
            ->setCellValue('C16', $ateco->description)
            ->setCellValue('C17', $MyAreaName)
            ->setCellValue('C18', $ateco->industry_group_code)
            ->setCellValue('C19', $reg_coeff)
            ->setCellValue('C20', $bank_code_for_credit)
            ->setCellValue('C21', $company->email_manager);

        /*Setup INPUT DATA WORKSHEET PRENDENDO LA LISTA DEI CAMPI DA COMPILARE DAL WORKSHEET STESSO
        DALLA ZONA DA ROW 2 alla PRIMA RIGA VUOTA*/

        $params = $this->getSavedSoseParameterValuesWithQuestionnaire(
            $company_id, $questionnaire_id);


        $sheet = $phpExcelObject->setActiveSheetIndex(2);
        for ($row = 2; $row <= $sheet->getHighestRow(); $row++) {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $sheet->getHighestColumn() . $row,
                NULL,
                TRUE,
                FALSE);
            $paramCode = $rowData[0][2];
            if (empty($paramCode)) continue;
            if (!isset($params[$paramCode]) || $params[$paramCode]->value == '' || $params[$paramCode]->value == null) {
                $sheet->setCellValue('D' . $row, '0');

            } else {
                $sheet->setCellValue('D' . $row, $params[$paramCode]->value);
            }


        }


        // Riempimento con tutti i parametri presenti nel questionario
        // Richiesta http://youtrack.evabeta.com/issue/EC-142

        $row = 25;
        foreach ($params as $paramCode => $parameterValue) {

            /** @var SoseParameterValue $parameterValue */
            $value = $parameterValue->value;
            $russianDescription = $parameterValue->param->description_ru;
            $russianPath = $parameterValue->param->path_ru;

            $sheet->setCellValue('B' . $row, $russianDescription);
            $sheet->setCellValue('C' . $row, $paramCode);
            $sheet->setCellValue('D' . $row, $value);
            $sheet->setCellValue('E' . $row, $russianPath);

            $row++;
        }

        // Setup WHO WORKS
        $phpExcelObject->setActiveSheetIndex(3)
            ->setCellValue('B2', $questionnaire->user_id)
            ->setCellValue('C2', $questionnaire->user->username);


        // Setup RAW RESULTS
        $results = $this->generateRawResult($company_id, $questionnaire_id);

//        $results = $this->getSoseOutputParameterValuesMap(
//            $company_id, $soseQuestionnaireResult, true);

        $sheet = $phpExcelObject->setActiveSheetIndex(4);

        for ($row = 2; $row <= $sheet->getHighestRow(); $row++) {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $sheet->getHighestColumn() . $row,
                NULL,
                TRUE,
                FALSE);
            $resultParamCode = $rowData[0][2];

            if (empty($resultParamCode)) continue;
            if (!isset($results[$resultParamCode])) {

                $sheet->setCellValue('D' . $row, 'NON VALORIZED');
            } else {
                $sheet->setCellValue('D' . $row, $results[$resultParamCode]['value']);
            }
        }

        // Hide first sheet
        $phpExcelObject->setActiveSheetIndex(0)
            ->setSheetState('hidden');
        $phpExcelObject->setActiveSheetIndex(0)
            ->setSheetState('veryHidden');

        // Reset on first sheet
        $phpExcelObject->setActiveSheetIndex(1);
        $date = str_replace('-', '', Carbon::now()->format('Y-m-d'));
        $appGetLocale = App::getLocale();
        $filename = "ECOLO_ReportType1_template_{$appGetLocale}_{$bank_code_for_credit}_{$company_id}_{$questionnaire_id}_{$date}.xls";
//        $phpExcelObject->getSecurity()->setLockStructure(true);
//        $phpExcelObject->getSecurity()->setWorkbookPassword('secret');
        $writer = IOFactory::createWriter($phpExcelObject, "Xlsx");
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        $writer->save("php://output");
        die;
        // create the writer
        $writer = $this->phpexcel->createWriter($phpExcelObject, 'Excel2007');
        $writer->setPreCalculateFormulas(false);
        $writer->save($outputFilePath);

        $this->logger->info("Report is done, company {$questionnaire->getIdCompany()->getId()} questionnaire {$questionnaire->getFormattedId()} with template $templateName");

        return true;
    }

    public function getSoseOutputParameterValuesMap($company, $soseQuestionnaireResult, $normalizeResults = true)
    {
        $ret = array();

        $company_output_parameters_values = $this->getSoseOutputParameterValues($company, $soseQuestionnaireResult);

        if ($normalizeResults) {
            $this->normalizeSoseOutputParameterValues($company_output_parameters_values);
        }

        foreach ($company_output_parameters_values as $outputValue) {

            $ret[$outputValue->getParameter()->getParameterCode()] = $outputValue;
        }
        return $ret;
    }


}
