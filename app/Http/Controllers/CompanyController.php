<?php

namespace App\Http\Controllers;


use App\Http\Models\BankSetting;
use App\Http\Models\Client;
use App\Http\Models\CodeTree;
use App\Http\Models\Coefficient;
use App\Http\Models\Comment;
use App\Http\Models\CompanyList;
use App\Http\Models\GeographicalAreas;
use App\Http\Models\LegalForm;
use App\Http\Models\Questionnaire;
use App\Http\Models\QuestionnaireTree;
use App\Http\Models\Role;
use App\Http\Models\SoseAteco;
use App\Http\Models\SoseAtecoStudiesEquivalence;
use App\Http\Models\SoseCheck;
use App\Http\Models\SoseCheckValidationMessage;
use App\Http\Models\SoseOutputParameter;
use App\Http\Models\SoseParameter;
use App\Http\Models\SoseParameterValue;
use App\Http\Models\SoseQuestionnaireStatus;
use App\Http\Models\SoseStudiesCheck;
use App\Http\Models\SoseStudiesParameter;
use App\Http\Models\SoseStudy;
use App\Http\Models\User;
use App\Http\Services\StatusCode;
use Barryvdh\Snappy\Facades\SnappyPdf as newPDF;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Mpdf\Mpdf;
use mysql_xdevapi\Exception;
use Spatie\PdfToText\Pdf;
use App\Mail\EvaluationNoticeEmail;
use App\Http\Models\XmlData;


class CompanyController extends Controller
{
    public function viewPdf()
    {
        return view('account.viewPdf');
    }

    public function viewPdf2()
    {
        return view('account.viewPdf2');
    }

    /**
     * @param $ID_company
     * @return array
     */
    private function emailsForNotice($ID_company): array
    {
        $emails = [];

        foreach (User::getUsersByRole(Role::get_role_id_by_name(Role::ROLE_BANK_SUPERVISOR)) as $supervisor) {
            if (strpos($supervisor['email'], 'admin') !== false || strpos($supervisor['email'], 'test') !== false) {
                continue;
            }
            $emails[] = $supervisor['email'];
        }

        $emails[] = User::query()->find(CompanyList::getByCompanyId($ID_company)->manager_id)->email;

        return $emails;
    }

    /**
     * @param $domain
     * @param $lang
     * @param $token1
     * @param $token2
     * @param Request $request
     * @return RedirectResponse
     */
    public function upload_file($domain, $lang, $token1, $token2, Request $request)
    {
        $new_file_name = strtolower($request->input('comp_name')).$token1;
        if ($request->has('report')) {
            $upload_file = $request->file('report');
            switch ($upload_file->getClientOriginalExtension()) {
                case 'pdf':
                    $upload_file->move(public_path('/site/reports/pdf'), $new_file_name.'.'.$upload_file->getClientOriginalExtension());
                    CompanyList::query()
                        ->where('ID_company', '=', $token1)
                        ->update(['questionnaire_status' => 3]);

                    $data = [
                        'company_name' => $request->input('comp_name'),
                        'url' => '/manager/'
                    ];

                    $emails = $this->emailsForNotice($token1);
                    if ($emails) {
                        try {
                            Mail::to($emails)->send(new EvaluationNoticeEmail($data));

                        } catch (\Exception $exception){
                            return redirect()->to(App::getLocale().'/account')->with('successMsg', 'File successfully uploaded');
                        }
                    }

                    return redirect()->to(App::getLocale().'/account')->with('successMsg', 'File successfully uploaded');

                default:
                    return redirect()->to(App::getLocale().'/account')->with('errorMsg', 'Incorrect file type');
            }
        } else {
            return redirect()->to(App::getLocale().'/account')->with('errorMsg', 'Please select file');
        }
    }

    public function seed()
    {

        $coefficients = Coefficient::all();
        foreach ($coefficients as $coefficient) {
            $geo_area = GeographicalAreas::query()->where('area_code', $coefficient->region_code)->first();
            if (!$geo_area) {
                $add = new GeographicalAreas();
                $add->bankCode = 1;
                $add->area_code = $coefficient->region_code;
                $add->area_name = $coefficient->region_name;
                $add->increment('progressiveRegionNumber', 1);
                $add->increment('progressiveCityNumber', 1);
                $add->region_description = $coefficient->region_name;
                $add->save();
            }
        }

        die;
        $sose_study_checks = SoseCheck::query()->whereNull('sose_study_id')->delete();
        $sose_study_checks = SoseCheckValidationMessage::query()->whereNull('sose_study_id')->delete();
        echo '<pre>';
        var_dump(count($sose_study_checks));
        die;
        die;

        $sose_study_checks = SoseCheck::query()->get();
        foreach ($sose_study_checks as $check) {
            $study_check = SoseStudiesCheck::query()->where('sosecheck_id', $check->id)->select('sosestudy_id')->get()->toArray();
            foreach ($study_check as $key => $value) {
                $new_sose_check = new SoseCheck();
                $new_sose_check->pre_check_condition = $check->pre_check_condition;
                $new_sose_check->validation_formula = $check->validation_formula;
                $new_sose_check->user_id = $check->user_id;
                $new_sose_check->sose_study_id = $value['sosestudy_id'];
                $new_sose_check->save();

                $sose_check_message = SoseCheckValidationMessage::query()->where('sose_check_id', $check->id)->first();
                $new_sose_check_message = new SoseCheckValidationMessage();
                $new_sose_check_message->sose_check_id = $new_sose_check->id;
                $new_sose_check_message->user_id = $sose_check_message->user_id;
                $new_sose_check_message->russian = $sose_check_message->russian;
                $new_sose_check_message->italian = $sose_check_message->italian;
                $new_sose_check_message->english = $sose_check_message->english;
                $new_sose_check_message->sose_study_id = $value['sosestudy_id'];

                $new_sose_check_message->save();

                echo $new_sose_check->id . '<br>';
            }

        }


        die;
        $syudy_id = 269;
        $VM03C = 251;
        $VM03D = 268;

        $sose_studies_params = SoseStudiesParameter::query()
            ->where('sosestudy_id', $syudy_id)
            ->select('soseparameter_id')
            ->get();

        $all_params_ids = [];
        foreach ($sose_studies_params as $param) {
            array_push($all_params_ids, $param->soseparameter_id);
        }

        foreach ($all_params_ids as $id) {
            $add = new SoseStudiesParameter();
            $add->soseparameter_id = $id;
            $add->sosestudy_id = $VM03D;
            $add->save();
        }


        echo '<pre>';
        var_dump($all_params_ids);
        die;


        $ours = SoseOutputParameter::all();
        ini_set('max_execution_time', 0);
        $live_db = DB::connection('ecolo_example');
        $table = $live_db->table('sose_output_parameter');
        $live_data = $table->get();
        foreach ($live_data as $live) {
            foreach ($ours as $our) {
                if ($our->id == $live->id) {
                    $our->description_en = $live->description_en;
                    $our->description_ru = $live->description_ru;
                    $our->description_it = $live->description_it;
                    $our->save();

                }
            }

        }
        return 15;
        return $live_data;
    }

    public function send_to_evaluation($test, $lang, $id)
    {
        $questionnaire = Questionnaire::query()->find($id);
        $questionnaire->sose_questionnaire_status_id = SoseQuestionnaireStatus::REGISTERED_IN_BACKOFFICE;
        $questionnaire->save();
        return $this->_response_body($questionnaire);

    }

    public function to_bank($test, $lang, $id)
    {
        $questionnaire = Questionnaire::query()->find($id);
        $questionnaire->sose_questionnaire_status_id = SoseQuestionnaireStatus::SENT_TO_BANK;
        $questionnaire->save();
        return $this->_response_body($questionnaire);

    }

    public function make_apache_confs($path, $server_name, $document_root)
    {
        $output = shell_exec('sudo chown -R www-data:www-data /etc/apache2/sites-available && sudo shmod -R g+s /etc/apache2/sites-available');
        chdir('/var/www/ecolo');
        $server_name = escapeshellarg($server_name);
        $document_root = escapeshellarg($document_root);
        $a = escapeshellarg($path);
        $output = shell_exec("sh apache.sh  2>&1 $a $server_name $document_root");
        echo "<pre>" . $output;
    }

    public function search_company(Request $request)
    {
        $result = $request->get('company_name');
        $companies = CompanyList::query()
            ->where('company_name', 'like', '%' . $result . '%')
            ->paginate(10);
        return view('account.search_company', ['companies' => $companies, 'result' => $result]);

    }

    public function comment(Request $request, $test, $lang, $company_id)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'text' => 'required|string',
                'company_id' => 'integer|required'
            ];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return $this->_response_body($validator->messages(),
                    StatusCode::HTTP_UNPROCESSABLE_ENTITY,
                    'Validation Error');
            }
            $comment = Comment::insert($request);
            return $this->_response_body($comment);


        }
        $company = CompanyList::query()->where('ID_company', $company_id)->first();

        return view('account.comment_company', ['company' => $company]);
    }

    public function new_company(Request $request)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'user_id' => 'required|integer',
                'company_name' => 'required|string|unique:company_list',
                'id_number' => 'required|string',
                'legal_form' => 'required|integer',
                'main_activity' => 'nullable|string',
                'address' => 'string|nullable',
                'area_region' => 'required_without:area_city',
            ];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return $this->_response_body($validator->messages(),
                    StatusCode::HTTP_UNPROCESSABLE_ENTITY,
                    'Validation Error');
            }


            $company = CompanyList::insert($request);
            if ($company) {
                return $this->_response_body($company->ID_company);
            }

            return $this->_response_body(false);

        }
        $regions = GeographicalAreas::query()
            ->where('country', '=', GeographicalAreas::COUNTRY)->groupBy('region_description')->get();
        $users = User::query()->where('role_id', Role::get_role_id_by_name(Role::ROLE_FINAL_CLIENT))->get();
        $legal_forms = LegalForm::get_all_forms();
        return view('account.new_company',
            ['legal_forms' => $legal_forms, 'users' => $users, 'regions' => $regions]);

    }

    public function get_area_city(Request $request)
    {
        $progressive_region_number = GeographicalAreas::query()
            ->where('area_code', $request->input('region_number'))
            ->first();

        $city = GeographicalAreas::query()
            ->where('progressiveRegionNumber', $progressive_region_number->progressiveRegionNumber)
            ->groupBy('area_name')
            ->get();
        return $this->_response_body([$city, $progressive_region_number->region_description]);
    }

    public function get_company_region(Request $request)
    {
        $company_id = CompanyList::find($request->input('company_id'));
        return $this->_response_body($company_id->region_code);
    }

    public function view_company($test, $lang, $company_id)
    {
        $company = CompanyList::query()->where('ID_company', $company_id)->first();
        $questionnaire = Questionnaire::get_company_questionnaires($company_id);
        $ateco = QuestionnaireTree::all();

//        foreach ($ateco as $at)
//        {
//            echo $at->sose_okved->okved_code.'<br>';
//        }
//        die;

        if ($company) {
            $comments = Comment::get($company_id);
            $region = GeographicalAreas::getByAreaCode($company->region);
            $city = GeographicalAreas::getByAreaCode($company->region_code);
            $region_name = is_object($region) ? $region->area_name : '';
            $city_name = is_object($city) ? $city->area_name : '';

            return view('account.view_company',
                [
                    'company' => $company,
                    'region' => $region_name,
                    'city' => $city_name,
                    'comments' => $comments,
                    'questionnaires' => $questionnaire,
                    'ateco' => $ateco,
                ]
            );

        }
        return abort(404);
    }

    public function new_questionnaire(Request $request)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'code_id' => 'integer|nullable',
                'company_id' => 'required|integer',
                'ateco_id' => 'required|integer',
                'company_unit' => 'required|integer',

            ];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return $this->_response_body($validator->messages(),
                    StatusCode::HTTP_UNPROCESSABLE_ENTITY,
                    'Validation Error');
            }
            $questionnaire = Questionnaire::insert($request);
            return $this->_response_body($questionnaire);


        }


    }

    public function get_ateco(Request $request)
    {
        $code = CodeTree::query()->find($request->input('code_id'));
        $ateco = QuestionnaireTree::get_by_code($code->code);
        return $this->_response_body($ateco);
    }

    public function delete_company($test, $lang, $id)
    {

        $user = CompanyList::query()->where('ID_company', $id)->first();
        $questionnaire = Questionnaire::query()->where('id_company', $id)->get();
        if (count($questionnaire)) {
            foreach ($questionnaire as $quest)
                $quest->delete();
        }
        if ($user) {
            $user->delete();
            return $this->_response_body($user);


        }
        return abort(404);

    }

    public function delete_questionnaire($test, $lang, $id)
    {

        $user = Questionnaire::query()->find($id);
        if ($user) {
            $user->delete();
            return $this->_response_body($user);


        }
        return abort(404);

    }

    public function edit_company(Request $request, $test, $lang, $company_id)
    {
        $company = CompanyList::query()->where('ID_company', $company_id)->first();
        $company_region = GeographicalAreas::query()->where('area_code', $company->region_code)->first();


        if ($request->isMethod('post')) {

            $rules = [
                'user_id' => 'required|integer',
                'company_name' => 'required|string|unique:company_list,company_name,' . $company_id . ',ID_company',
                'id_number' => 'required|string',
                'legal_form' => 'required|integer',
                'main_activity' => 'nullable|string',
                'address' => 'nullable|string',
                'area_region' => 'required_without:area_city',

            ];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return $this->_response_body($validator->messages(),
                    StatusCode::HTTP_UNPROCESSABLE_ENTITY,
                    'Validation Error');
            }
            $company = CompanyList::update_company($request, $company_id);
            if ($company) {
                return $this->_response_body($company->ID_company);
            }
            return $this->_response_body(false);


        }
        $regions = GeographicalAreas::query()
            ->groupBy('region_description')->get();
        $users = User::all();
        $legal_forms = LegalForm::get_all_forms();

        return view('account.new_company', ['legal_forms' => $legal_forms,
            'company' => $company,
            'company_region' => $company_region,
            'regions' => $regions,
            'users' => $users]);

    }

    public function insert_questionnaire_value(Request $request, $test, $lang, $company_id, $questionnaire_id)
    {

        if ($request->isMethod('post')) {
            $rules = [
                'current_parameter_value' => 'numeric|nullable',

            ];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return $this->_response_body($validator->messages(),
                    StatusCode::HTTP_UNPROCESSABLE_ENTITY,
                    'Validation Error');
            }

            $parameter_id = $request->input('current_parameter_id');

            $value = $request->input('current_parameter_value');
            $client_id = $request->input('client_id');
            $parameter_id = explode("[", $parameter_id);
            if (isset($parameter_id[1])) {
                $parameter_id = explode("]", $parameter_id[1]);
            } else {
                $parameter_id[0] = null;
            }
            $unit = SoseParameter::query()->where('id', $parameter_id[0])->select('units')->first();
            // if ($unit->units == '%' && $value > 100) {
            //     $data = ['current_parameter_value' => 'Value must be between 0 and 100'];
            //     return $this->_response_body($data, StatusCode::HTTP_UNPROCESSABLE_ENTITY, false);
            // }
            if ($value || $value == 0) {
                $questionnaire = SoseParameterValue::insert($parameter_id[0], $value, $company_id, $questionnaire_id, $client_id);
                if ($questionnaire) {

                    return $this->_response_body($value);

                }
            } else {
                $param_value = SoseParameter::query()->where('parameter_id', $parameter_id[0])->where('id_company', $company_id)->first();
                if ($param_value) {
                    $param_value->delete();
                    return $this->_response_body($company_id);


                }
            }


        }
    }

    public function fill_questionnaire(Request $request, $test, $lang, $company_id, $questionnaire_id)
    {
        $company = CompanyList::query()->where('ID_company', $company_id)->first();
        $questionnaire = Questionnaire::get_by_id($questionnaire_id);
        $params = SoseStudiesParameter::get_by_study_id($questionnaire->sose_study_id);

        $parameters = [];

        foreach ($params as $key => $param) {
            if (!isset($param->sose_param->path) || is_null($param->sose_param)) {
                continue;
            }
            try {

                if (strpos($param->sose_param->path, 'Group 0') !== false) {

                    $parameters['O'][$key] = $param;
                }
                if (strpos($param->sose_param->path, 'Group A') !== false) {
                    $parameters['A'][$key] = $param;

                }
                if (strpos($param->sose_param->path, 'Group B') !== false) {
                    $parameters['B'][$key] = $param;

                }
                if (strpos($param->sose_param->path, 'Group C') !== false) {
                    $parameters['C'][$key] = $param;

                }

                if (strpos($param->sose_param->path, 'Group D') !== false) {
                    $parameters['D'][$key] = $param;

                }
                if (strpos($param->sose_param->path, 'Group E') !== false) {
                    $parameters['E'][$key] = $param;

                }
                if (strpos($param->sose_param->path, 'Group F') !== false) {
                    $parameters['F'][$key] = $param;

                }

                if (strpos($param->sose_param->path, 'Group L') !== false) {
                    $parameters['L'][$key] = $param;

                }
                if (strpos($param->sose_param->path, 'Group Z') !== false) {
                    $parameters['Z'][$key] = $param;

                }
            } catch (\Exception $exception) {
                echo $param->id;
            }


        }


        $b_units = [];
        $a = 0;
        $count = 0;
        foreach ($parameters['B'] as $let => $b) {
            if ($a != 0) {
                $b_units[$let] = $b;
            } else {
                $first = $b;
                if(count($parameters['B']) < 5){
                    $b_units[$let] = $b;
                }
            }
            $a++;
        }

        $removable_b_units = count($b_units) % 10;
        if (count($b_units) < 5){
            $b_units = array_splice($b_units, count($b_units) - $removable_b_units, $removable_b_units);
        }
        else{
            array_splice($b_units, count($b_units) - $removable_b_units, $removable_b_units);
        }

        $new_params_with_subheadings = [];
        $i = 1;
        foreach ($parameters as $item => $value) {
            if ($item == "B") {
                continue;
            }


            foreach ($parameters[$item] as $key => $parameter) {
                $find_subheading = explode("|", $parameter->sose_param->param_path);

                if (isset($find_subheading[1]) && (strlen($find_subheading[1]) > 0 && strlen(trim($find_subheading[1])) != 0)) {
                    $find_subheading = $find_subheading[1];
//                $find_subheading = str_replace(',',' ',$find_subheading);
//                echo $find_subheading.'<br>';

                } else {
                    $i++;
                    $find_subheading = 'nosub' . $i;
                }


                $new_params_with_subheadings[$item][$find_subheading][] = $parameter;

            }


        }

//        foreach ($new_params_with_subheadings['A'] as $new)
//        {
//           foreach ($new as $a )
//           {
//               echo '<pre>';
//               var_dump($a->sose_param->parameter_code,$a->sose_param->id,$a->sose_param->description_ru);
//
//           }
//
//        }
//        die;

        if ($company) {
            $parameters_values = SoseParameterValue::query()
                ->where('ID_company', $company_id)->where('sose_questionnaire_id', $questionnaire_id)->get();
            if ($request->isMethod('post')) {
                $requests = [];
                foreach ($request->all() as $key => $string) {
                    $string = preg_replace('/\s+/', '', $string);
                    $requests[$key] = $string;
                }

                $requests['client_id'] = $company_id;
                $validate = SoseCheck::validate($requests);

                if (is_null($validate) || empty($validate)) {
                    $validate = true;
                    $status = Questionnaire::change_status($questionnaire_id, SoseQuestionnaireStatus::SUBMITTED);
                    if ($status) {

                        return $this->_response_body(true);

                    }
                }
                return $this->_response_body($validate);

            }

            return view('account.fill_questionnaire',
                [
                    'b_units' => $b_units,
                    'first' => $first,
                    'questionnaire' => $questionnaire,
                    'values' => $parameters_values,
                    'parameters' => $new_params_with_subheadings]);

        }
        return abort(404);

    }

    public function visible($test, $lang, $id)
    {
        $param = SoseParameter::query()->find($id);
        $param->visible = $param->visible ? 0 : 1;
        $param->save();
        return $this->_response_body($param->visible);

    }

    public function edit_questionnaire(Request $request, $test, $lang, $company_id, $questionnaire_id)
    {
        $company = CompanyList::query()->where('ID_company', $company_id)->first();
        $questionnaire = Questionnaire::get_by_id($questionnaire_id);
        $params = SoseStudiesParameter::get_by_study_id($questionnaire->sose_study_id);


        $parameters = [];

        foreach ($params as $key => $param) {
            if (!isset($param->sose_param->path) || is_null($param->sose_param)) {
                continue;
            }

            if (strpos($param->sose_param->path, 'Group 0') !== false) {
                $parameters['O'][$key] = $param;
            }
            if (strpos($param->sose_param->path, 'Group A') !== false) {
                $parameters['A'][$key] = $param;

            }
            if (strpos($param->sose_param->path, 'Group B') !== false) {
                $parameters['B'][$key] = $param;

            }
            if (strpos($param->sose_param->path, 'Group C') !== false) {
                $parameters['C'][$key] = $param;

            }

            if (strpos($param->sose_param->path, 'Group D') !== false) {
                $parameters['D'][$key] = $param;

            }
            if (strpos($param->sose_param->path, 'Group E') !== false) {
                $parameters['E'][$key] = $param;

            }
            if (strpos($param->sose_param->path, 'Group F') !== false) {
                $parameters['F'][$key] = $param;

            }
            if (strpos($param->sose_param->path, 'Group L') !== false) {
                $parameters['L'][$key] = $param;

            }
            if (strpos($param->sose_param->path, 'Group Z') !== false) {
                $parameters['Z'][$key] = $param;

            }

        }

        $b_units = [];
        $a = 0;
        $count = 0;
        foreach ($parameters['B'] as $let => $b) {

            if ($a != 0) {


                $b_units[$let] = $b;

            } else {
                $first = $b;
            }
            $a++;
        }
//        return $b_units['11'];

        $new_params_with_subheadings = [];
        $i = 0;
        foreach ($parameters as $item => $value) {
            if ($item == "B") {
                continue;
            }
            foreach ($parameters[$item] as $key => $parameter) {

                $find_subheading = explode("|", $parameter->sose_param->param_path);
                if (isset($find_subheading[1]) && (strlen($find_subheading[1]) > 0 && strlen(trim($find_subheading[1])) != 0)) {
                    $find_subheading = $find_subheading[1];
//                $find_subheading = str_replace(',',' ',$find_subheading);
//                echo $find_subheading.'<br>';

                } else {
                    $i++;
                    $find_subheading = 'nosub' . $i;
                }

                $new_params_with_subheadings[$item][$find_subheading][] = $parameter;

            }


        }


        if ($company && $parameters) {
            $parameters_values = SoseParameterValue::query()
                ->where('id_company', $company_id)->where('sose_questionnaire_id', $questionnaire_id)->get();


            return view('account.fill_questionnaire',
                [
                    'questionnaire' => $questionnaire,
                    'b_units' => $b_units,
                    'first' => $first,
                    'parameters' => $new_params_with_subheadings,
                    'values' => $parameters_values]);

        }
        return abort(404);

    }
    public function delete_report(Request $request){
        if($request->fileName){
            $fullName = $request->fileName;
            $path = explode('.', $fullName)[1];
            if($path === "xlsx" || $path === "xls"){
                $path = 'excel';
            }
            \File::delete(public_path('/site/reports/'.$path.'/'.$fullName));
        }
    }

    public function print_questionnaire()
    {
        $ateco = QuestionnaireTree::all();
        return view('account.print_questionnaire', ['ateco' => $ateco]);
    }

    public function print_filled_questionnaire($test, $lang, $company_id, $questionnaire_id, $ateco_id)
    {
        $company = CompanyList::query()->where('ID_company', $company_id)->first();
        $questionnaire = Questionnaire::get_by_id($questionnaire_id);
        $params = SoseStudiesParameter::get_by_study_id($questionnaire->sose_study_id);
        $sose_ateco_study = SoseAtecoStudiesEquivalence::query()->where('soseateco_id', $ateco_id)->first();
        $study = SoseStudy::query()->find($sose_ateco_study->sosestudy_id);
        $sose_ateco = SoseAteco::query()->find($ateco_id);
        $study_code = $study->study_code;
        $ateco_code = $sose_ateco->ateco_code;
        $ateco_description = $sose_ateco->name;
        $parameters_all = [];
        $ateco_description = explode("[", $ateco_description);


        foreach ($params as $key => $param) {
            if (!isset($param->sose_param->path) || is_null($param->sose_param)) {
                continue;
            }

            if (strpos($param->sose_param->path, 'Group 0') !== false  && $param->sose_param->visible) {
                $parameters_all['0'][$key] = $param;
                $parameters_all['0_path'] = $param->sose_param;
            }
            if (strpos($param->sose_param->path, 'Group A') !== false  && $param->sose_param->visible) {
                $parameters_all['A'][$key] = $param;
                $parameters_all['A_path'] = $param->sose_param;


            }
            if (strpos($param->sose_param->path, 'Group B') !== false  && $param->sose_param->visible) {
                $parameters_all['B'][$key] = $param;
                $parameters_all['B_path'] = $param->sose_param;

            }
            if (strpos($param->sose_param->path, 'Group C') !== false  && $param->sose_param->visible) {
                $parameters_all['C'][$key] = $param;
                $parameters_all['C_path'] = $param->sose_param;

            }

            if (strpos($param->sose_param->path, 'Group D') !== false  && $param->sose_param->visible) {
                $parameters_all['D'][$key] = $param;
                $parameters_all['D_path'] = $param->sose_param;

            }
            if (strpos($param->sose_param->path, 'Group E') !== false  && $param->sose_param->visible) {
                $parameters_all['E'][$key] = $param;
                $parameters_all['E_path'] = $param->sose_param;

            }
            if (strpos($param->sose_param->path, 'Group F') !== false  && $param->sose_param->visible) {
                $parameters_all['F'][$key] = $param;
                $parameters_all['F_path'] = $param->sose_param;

            }
            if (strpos($param->sose_param->path, 'Group X') !== false  && $param->sose_param->visible) {
                $parameters_all['X'][$key] = $param;
                $parameters_all['X_path'] = $param->sose_param;

            }
            if (strpos($param->sose_param->path, 'Group L') !== false  && $param->sose_param->visible) {
                $parameters_all['L'][$key] = $param;
                $parameters_all['L_path'] = $param->sose_param;

            }
            if (strpos($param->sose_param->path, 'Group Z') !== false && $param->sose_param->visible) {
                $parameters_all['Z'][$key] = $param;
                $parameters_all['Z_path'] = $param->sose_param;

            }

        }


        $b_units = [];
        $a = 0;
        $count = 0;
        foreach ($parameters_all['B'] as $let => $b) {

            if ($a != 0) {

//                    echo $let.'<pre>';
                $b_units[$let] = $b;

            } else {
                $first = $b;
                if(count($parameters_all['B']) < 5){
                    $b_units[$let] = $b;
                }
            }
            $a++;
        }
        $removable_b_units = count($b_units) % 10;
        if (count($b_units) < 5){
            $b_units = array_splice($b_units, count($b_units) - $removable_b_units, $removable_b_units);
        }
        else{
            array_splice($b_units, count($b_units) - $removable_b_units, $removable_b_units);
        }

        $parameters = [];
        $new_params_with_subheadings = [];

        $i = 0;
        foreach ($parameters_all as $item => $value) {
            if ($item == "B") {
                continue;
            }
            if (strpos($item, '_path') !== false) {
                $new_params_with_subheadings[$item] = $value;
            }
            if (strlen($item) > 1) {

                $parameters[$item] = $parameters_all[$item];
            } else {
                foreach ($parameters_all[$item] as $key => $parameter) {

                    $find_subheading = explode("|", $parameter->sose_param->param_path);
                    if (isset($find_subheading[1]) && (strlen($find_subheading[1]) > 0 && strlen(trim($find_subheading[1])) != 0)) {
                        $find_subheading = $find_subheading[1];


                    } else {
                        $i++;
                        $find_subheading = 'nosub' . $i;
                    }
                    $new_params_with_subheadings[$item][$find_subheading][] = $parameter;

                }
            }
        }

        $parameters = $new_params_with_subheadings;

//        foreach ($new_params_with_subheadings['C'] as $p => $c)
//        {
//            if (strpos($p,'nosub') === false)
//            {
//                echo $p.'<br>';
//            }
//        }
//        die;
        $area_region = GeographicalAreas::query()->where('area_code','=',$company->region_code)->first();
        $area_name = is_object($area_region) ? $area_region->area_name : '';

        if ($company) {
            $parameters_values = SoseParameterValue::query()
                ->where('id_company', $company_id)->where('sose_questionnaire_id', $questionnaire_id)->get();
            $mpdf = new Mpdf();
            $mpdf->useActiveForms = true;
            $mpdf->debug = true;
            $html = view('account.filled_pdf',
                [
                    'area_name' => $area_name,
                    'company' => $company,
                    'study_code' => $study_code,
                    'b_units' => $b_units,
                    'first' => $first,
                    'ateco_description' => $ateco_description[0],
                    'ateco_code' => $ateco_code,
                    'questionnaire' => $questionnaire,
                    'parameters' => $parameters,
                    'values' => $parameters_values]);
            $mpdf->WriteHTML($html);

//            $mpdf->SetJS('var aInputs = document.querySelectorAll("input[type=\'checkbox\']");
//        for (var i=0;i<aInputs.length;i++) {
//            if (aInputs[i].value == 1) {
//                aInputs[i].checked =true;
//            }
//        };');  //JS code with <script></script> tags.
            ob_clean();

            $mpdf->Output($study_code . '.pdf', 'D');

        }


    }
    public function report_filled_questionnaire($company_id, $questionnaire_id, $ateco_id){
        $pdf = newPDF::loadView('account.viewPdf');
        $pdf->setOption('enable-javascript', true);
        $pdf->setOption('javascript-delay', 5000);
        $pdf->setOption('enable-smart-shrinking', true);
        $pdf->setOption('no-stop-slow-scripts', true);
        return $pdf->stream();
    }

    public function GeneratePDF($test, $lang, $ateco_id, $company_unit)

    {

        $sose_ateco = SoseAteco::query()->find($ateco_id);
        $sose_study = SoseAtecoStudiesEquivalence::query()->where('soseateco_id', $ateco_id)->first();
        $params = SoseStudiesParameter::get_by_study_id($sose_study->sosestudy_id);
        $study = SoseStudy::query()->find($sose_study->sosestudy_id);

        $parameters_all = [];

        foreach ($params as $key => $param) {
            if (!isset($param->sose_param->path) || is_null($param->sose_param)) {
                continue;
            }
            if (strpos($param->sose_param->path, 'Group 0') !== false) {
                $parameters_all['O'][$key] = $param;
                $parameters_all['O_path'] = $param->sose_param;
            }
            if (strpos($param->sose_param->path, 'Group A') !== false) {
                $parameters_all['A'][$key] = $param;
                $parameters_all['A_path'] = $param->sose_param;


            }
            if (strpos($param->sose_param->path, 'Group B') !== false) {
                $parameters_all['B'][$key] = $param;
                $parameters_all['B_path'] = $param->sose_param;

            }
            if (strpos($param->sose_param->path, 'Group C') !== false) {
                $parameters_all['C'][$key] = $param;
                $parameters_all['C_path'] = $param->sose_param;

            }

            if (strpos($param->sose_param->path, 'Group D') !== false) {
                $parameters_all['D'][$key] = $param;
                $parameters_all['D_path'] = $param->sose_param;

            }
            if (strpos($param->sose_param->path, 'Group E') !== false) {
                $parameters_all['E'][$key] = $param;
                $parameters_all['E_path'] = $param->sose_param;

            }
            if (strpos($param->sose_param->path, 'Group F') !== false) {
                $parameters_all['F'][$key] = $param;
                $parameters_all['F_path'] = $param->sose_param;

            }

            if (strpos($param->sose_param->path, 'Group L') !== false) {
                $parameters_all['L'][$key] = $param;
                $parameters_all['L_path'] = $param->sose_param;

            }
            if (strpos($param->sose_param->path, 'Group Z') !== false) {
                $parameters_all['Z'][$key] = $param;
                $parameters_all['Z_path'] = $param->sose_param;

            }


        }
        $b_units = [];
        $a = 0;
        if (isset($parameters_all['B'])) {
            foreach ($parameters_all['B'] as $let => $b) {
                if ($a != 0) {
                    $b_units[$let] = $b;

                } else {
                    $first = $b;
                    if(count($parameters_all['B']) < 5){
                        $b_units[$let] = $b;
                    }
                }
                $a++;
            }
        }

        $removable_b_units = count($b_units) % 10;
        if (count($b_units) < 5){
            $b_units = array_splice($b_units, count($b_units) - $removable_b_units, $removable_b_units);
        }
        else{
            array_splice($b_units, count($b_units) - $removable_b_units, $removable_b_units);
        }

        $parameters = [];
        $new_params_with_subheadings = [];
        $i = 0;
        foreach ($parameters_all as $item => $value) {
            if ($item == "B") {
                continue;
            }
            if (strpos($item, '_path') !== false) {
                $new_params_with_subheadings[$item] = $value;
            }
            if (strlen($item) > 1) {

                $parameters[$item] = $parameters_all[$item];
            } else {
                foreach ($parameters_all[$item] as $key => $parameter) {
                    if ($parameter->sose_param['visible'] == 0) {
                        continue;
                    }

                    $find_subheading = explode("|", $parameter->sose_param->param_path);
                    if (isset($find_subheading[1]) && (strlen($find_subheading[1]) > 0 && strlen(trim($find_subheading[1])) != 0)) {
                        $find_subheading = $find_subheading[1];


                    } else {
                        $i++;
                        $find_subheading = 'nosub' . $i;
                    }

                    $new_params_with_subheadings[$item][$find_subheading][] = $parameter;

                }
            }
        }


        foreach ($parameters as $key => $p) {
            if (strpos($key, '_path') !== false) {
                continue;
            }
            foreach ($parameters[$key] as $subheading => $item) {

                if (strpos($subheading, 'nosub') === false) {


                    $visible = 0;
                    foreach ($parameters[$key][$subheading] as $a => $b) {
                        $visible = $b['sose_param']['visible'];
                        if ($visible == 1) {
                            break;
                        }
                    }

                    if ($visible == 0) {
                        unset($parameters[$key][$subheading]);
                    }

                }
            }

        }

        $parameters = $new_params_with_subheadings;
//        foreach ($parameters['D'] as $key => $value)
//        {
//            echo $key;
//            foreach ($value as $item=>$a)
//            {
//
//                echo '<pre>';
//                var_dump(count($a));
//
//            }
//        }
//
//        die;


        $study_code = $study->study_code;
        $ateco_code = $sose_ateco->ateco_code;
//        $ateco_description = $sose_ateco->description;
//        $ateco_description = explode("[", $ateco_description);
//        $ateco_description = $ateco_description[0];

        $ateco_description = $sose_ateco->name;
//        $parameters_all = [];
        $ateco_description = explode("[", $ateco_description);
        $ateco_description = $ateco_description[0];
        $html = view('account.pdf', compact('b_units', 'first', 'study_code', 'company_unit', 'ateco_description', 'ateco_code', 'parameters'))->render();

//   return $html;
        $mpdf = new Mpdf();

        $mpdf->useActiveForms = true;
        $mpdf->debug = true;

        $mpdf->WriteHTML($html);

        $mpdf->Output($study_code . '.pdf', 'D');

    }

    public function edit_parameter(Request $request, $test, $lang, $parameter_id)
    {
        $parameter = SoseParameter::query()->find($parameter_id);
        if ($parameter) {
            if ($request->isMethod('post')) {

                $rules = [
                    'parameter_code' => 'required',
//                    'units' => 'required',
                    'description_en' => 'required',
                    'description_it' => 'required',
                    'description_ru' => 'required',
//                    'path_en' => 'required',
//                    'path_ru' => 'required',
//                    'path_it' => 'required',

                ];
                $validator = Validator::make($request->all(), $rules);

                if ($validator->fails()) {
                    return $this->_response_body($validator->messages(),
                        StatusCode::HTTP_UNPROCESSABLE_ENTITY,
                        'Validation Error');
                }

                $visible = 1;
                if (!$request->has('visible')) {
                    $visible = 0;
                }


                $parameter = SoseParameter::update_parameter($request, $parameter_id, $visible);

                if ($parameter) {

                    return $this->_response_body($parameter);

                }


            }
            return view('account.edit_parameter', ['parameter' => $parameter]);

        }
        abort(404);

    }

    public function import_pdf_view(Request $request)
    {
        if ($request->isMethod('post')) {
            $rules = [
                "pdf" => "required|mimes:pdf"

            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {

                $message = $validator->messages();
                $message = json_decode($message);
                $message = $message->pdf[0];
                return view('account.import_pdf', ['messages' => $message]);

            }
            $book = $request->file('pdf');
            $x = (new Pdf())
                ->setPdf($book)
                ->setOptions(['layout', 'f 1'])
//                ->setOptions(['layout', 'layout'])
                ->text();
//            dd($x);

//            $string =  trim(preg_replace('/[\t\n\r\s]+/', '|', $x));
//            $array = explode('|' , $string);
//            $cleaned_keys = preg_grep('^(([A-Z]+[0-9]+)|([0-9]+[A-Z]+))[0-9A-Z]*$^', $array);
//            $result = [];
//                foreach ($cleaned_keys as $key=>$code){
//                     $value = 0;
//                    if($array[$key] === $code){
//                        if(is_numeric(trim($array[$key-1]))){
//                            $value = $array[$key-1];
//                        }elseif(is_numeric(trim($array[$key+1]))){
//                            $value = $array[$key+1];
//                        }
//                    }
//                    $result[$code] = $value;
//
//                }
////            var_dump($array);
////            $array = explode(" ", $x);
//dd($result);
            $array = preg_split("/  /", $x);
//
//

            $array = array_values($array);
            foreach ($array as $key => $value) {
//                echo  $value.'<pre>';
                if (empty($value) || trim($value) === '') {

                    unset($array[$key]);
                }

            }
            $b = 0;
            $s = 0;
            $inputs = [];
            foreach ($array as $value) {
                $inputs[$b] = $value;
                $b++;
                if (strpos($value, ' | ') !== false) {
                    $sose = $value;
                }

            }
            $sose = preg_replace('/[^A-Z0-9]/', ' ', $sose);
            $sose = str_replace("ECOLO", " ", $sose);;
            $sose = explode(" ", $sose);
            foreach ($sose as $a) {

                if ($a) {
//                    return $a;
                    $soses[$s + 1] = $a;
                    $s++;
                }
            }
            if (!ctype_digit($soses[1])) {
                $study_code = $soses[1];
                $ateco_code = $soses[2];
            } else {
                $study_code = $soses[2];
                $ateco_code = $soses[3];
            }

            $study_id = SoseStudy::query()->where('study_code', $study_code)->first();
            $ateco_id = SoseAteco::query()->where('ateco_code', $ateco_code)->first();
            $unit = 0;
            if ($study_id && $ateco_id) {
                foreach ($inputs as $key => $value) {
//                    return next($array);
//                    next($array);
                    $value = trim($value);
//                    echo $value.'<pre>';
                    if (strpos($value, 'N. ')) {
                        $unit++;
                    }
                    if ($value == 'Название компании' || $value == 'Company Name' || $value == 'Nome azienda') {
                        $company_name = $inputs[$key + 1];
                    }
                    if ($value == 'Основная деятельность' || $value == 'Main Activity') {
                        $main_activity = $inputs[$key + 1];
                    }
                    if ($value == 'Юридическая форма' || $value == 'Legal Form') {
                        $legal_form = $inputs[$key + 1];
                        $legal_form_id = LegalForm::get_id_by_name($legal_form);
                    }
                    if ($value == 'Субъект' || $value == 'Subject' || $value == 'Soggetto') {
                        $subject = $inputs[$key + 1];
                    }
                    if ($value == 'Номер ИНН' || $value == 'VAT identiﬁcation number' || $value == 'Numero IVA') {
                        $inn = $inputs[$key + 1];
                    }
                    if ($value == 'E-mail сотрудника Банка' || $value == 'E-mail employee of the Bank' || $value == 'E-mail  responsabile della Banca') {
                        $user_email = $inputs[$key + 1];
                        $user = User::query()->where('email', $user_email)->first();
                        if ($user) {
                            $user_id = $user->id;
                        } else {
                            $user_id = Auth::id();
                        }
                    }

                };
//                die;
                if (!isset($company_name)) {
                    return view('account.import_pdf', ['messages' => 'Invalid PDF']);

                }

//die;
//                $cleaned = preg_grep('/^[0-9]$/i', $inputs);
//                $cleaned = preg_replace( '/[^\d]/', '', $inputs);
//                $cleaned_keys = preg_grep('^(([A-Z]+[0-9]+)|([0-9]+[A-Z]+))[0-9A-Z]*$^', $inputs);
//                $questionnaire_values = [];
//                return $inputs;
//                foreach ($cleaned_keys as $key => $value) {
//                    $value = trim($value);
////                    echo $inputs[(int)$key-1].'-'.$inputs[(int)$key] . '-' . $inputs[(int)$key + 1] . '<pre>';
//                    foreach ($inputs as $clean_key => $clean_value) {
//                        echo $inputs[(int)$key-1].'-'.$inputs[(int)$key] . '-' . $inputs[(int)$key + 1] . '<pre>';
////                        echo $clean_key.'-'.$clean_value;
//                        if (isset($inputs[(int)$key + 1]) && is_numeric($inputs[(int)$key + 1])) {
//                            $questionnaire_values[$value] = $inputs[(int)$key+1];
//                        }
//
//                    }
//                }
//
//                return $questionnaire_values;


                $string = trim(preg_replace('/[\t\n\r\s]+/', '|', $x));
                $array = explode('|', $string);
                $cleaned_keys = preg_grep('^(([A-Z]+[0-9]+)|([0-9]+[A-Z]+))[0-9A-Z]*$^', $array);
                $result = [];
                foreach ($cleaned_keys as $key => $code) {
                    $value = 0;
                    if ($array[$key] === $code) {
                        if (is_numeric(trim($array[$key + 1]))) {
                            $value = $array[$key + 1];
                        } elseif (is_numeric(trim($array[$key - 1]))) {
                            $value = $array[$key - 1];
                        } elseif (trim($array[$key + 1]) == '✔') {
                            $value = 1;
                        } elseif (trim($array[$key - 1]) == '✔') {
                            $value = 1;
                        }
                    }
                    $result[$code] = $value;

                }

//            var_dump($array);
//            $array = explode(" ", $x);
//                dd($result);
                if (!isset($legal_form_id) || !isset($main_activity) || !isset($subject) || !isset($user_id)) {
                    return view('account.import_pdf', ['messages' => 'Invalid PDF']);

                }
                try {
                    // Begin a transaction
                    DB::beginTransaction();

                    $company = CompanyList::query()->where('company_name', trim($company_name))->first();
                    if (!$company) {

                        $company = CompanyList::new_company(trim($company_name), $user_id, $inn, $legal_form_id, $main_activity, $subject);

                    }
                    if ($company) {

                        $questionnaire = Questionnaire::new_questionnaire($user_id, $company->ID_company, $ateco_id->id, $study_id->id, $unit);
                        if ($questionnaire) {
                            foreach ($result as $code => $value) {
                                $params = SoseStudiesParameter::get_by_study_id($study_id->id);

                                foreach ($params as $param) {

                                    if ($param->sose_param && $param->sose_param->parameter_code == $code && $value !== 0) {

                                        $parameter_value = SoseParameterValue::insert($param->sose_param->id, $value, $company->ID_company, $questionnaire);

                                    }
                                }

                            }

                            DB::commit();
                            return \redirect(App::getLocale() . '/company/edit/' . $company->ID_company . '/' . $questionnaire);

                        }

                    }


                } catch (\Exception $e) {
                    // An error occured; cancel the transaction...
                    DB::rollback();

                    throw $e;


                }


            }

            return view('account.import_pdf', ['messages' => 'Invalid PDF']);
        }


        return view('account.import_pdf');

    }

    public function runEvaluation($test, $lang, $company_id, $questionnaire_id)
    {
        $lock = Session::get('gerico_run');

        try {

            $if_company_exist = CompanyList::find($company_id);
            if (!$if_company_exist) {
                throw new \Exception("The company does not exist");
            }

            $if_questionnaire_exist = Questionnaire::find($questionnaire_id);
            if (!$if_questionnaire_exist) {
                throw new \Exception("The questionnaire does not exist");
            }
            $batch_path = resource_path('EcoloBatch');
            $batch_path_exec = resource_path('EcoloBatch/ecolo.sh');
            $filepath = resource_path('EcoloBatch/test.txt');
            $filepath_output = resource_path('EcoloBatch/test_output.txt');
            $if_questionnaire_exist->sose_questionnaire_status_id = SoseQuestionnaireStatus::IN_BACKOFFICE_ELABORATION;
            $if_questionnaire_exist->save();
            chdir($batch_path);

            $output = shell_exec("sudo chown -R www-data:www-data /var/www/ecolo/resources/EcoloBatch && sh ecolo.sh  2>&1 $filepath $filepath_output");

            echo $output;
            die;


        } catch (\Exception $exception) {
            echo '<pre>';
            var_dump($exception->getMessage());
            die;

            // write somewhere that gerico does'nt run


        }

        if ($lock) {

        }
    }


}
