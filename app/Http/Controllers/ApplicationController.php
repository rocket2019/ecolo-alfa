<?php

namespace App\Http\Controllers;

use App\Http\Models\Client;
use App\Http\Models\CompanyList;
use App\Http\Models\QuestionnaireTree;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    public function index(Request $request)
    {
        if (app('router')->getRoutes()->match(app('request')->create(url()->previous()))->getName() == 'login') {
            session(['works' => true]);
        } else {
            session()->forget('works');
        }

        $clients = CompanyList::getManagerClients($request->input('search'));
        $end = [];
        foreach ($clients as $key => $client) {
            if (file_exists(base_path('public/site/reports/pdf/'.strtolower($client->company_name.$client->ID_company).'.pdf'))) {
                $end[] = $key;
            }
        }
        $counts = ['end' => count($end), 'send' => count($clients) - count($end)];
        return view('client/application')->with(compact('clients', 'counts'));
    }

    public function mail()
    {
        //$clients = Client::getManagerClients();

        return view('client.mail');
    }

}

