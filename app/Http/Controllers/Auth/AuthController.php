<?php

namespace App\Http\Controllers\Auth;

use App\Http\Models\CompanyList;
use App\Http\Models\GeographicalAreas;
use App\Http\Models\Questionnaire;
use App\Http\Models\Role;
use App\Http\Models\SoseParameter;
use App\Http\Models\SubdomainSetup;
use App\Http\Models\User;
use App\Http\Services\StatusCode;
use App\Mail\ConfirmationEmail;
use App\Mail\ResetPasswordEmail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

class AuthController extends Controller
{

    /**
     * Login function
     * Method GET - render view
     * Method POST - Log In
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */

    public function login(Request $request)
    {

//        SubdomainSetup::subDomainSetup($request->route()->subdomain);
        if ($request->isMethod(self::POST)) {
            $rules = [
                'username' => 'required',
                'password' => 'required|min:6|max:20',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return $this->_response_body($validator->messages(), StatusCode::HTTP_UNPROCESSABLE_ENTITY, 'Validation error');
            }

            $username = $request->input('username');
            $password = $request->input('password');
            if (Auth::attempt(['username' => $username, 'password' => $password])) {
                $user = Auth::user();
                User::last_login_time();
                if(Auth::user()->role_id == Role::get_role_id_by_name(Role::ROLE_BANK_MANAGER) || Auth::user()->role_id == Role::get_role_id_by_name(Role::ROLE_BANK_SUPERVISOR))
                {
                    if($user->client_token == null)
                    {
                        $user->client_token = Str::random(16);
                        $user->save();
                    }
                    $user['redirect'] = 'manager';
                }else{
                    $user['redirect'] = 'account';
                }
                return $this->_response_body($user, StatusCode::HTTP_OK);
            }
            if (App::getLocale() == 'en') {
                $data = ['password' => ["Incorrect email or password."]];
            } elseif (App::getLocale() == 'it') {

                $data = ['password' => ["Email o password errati."]];
            } else {
                $data = ['password' => ["Неверный адрес электронной почты или пароль."]];

            }


            return $this->_response_body($data, StatusCode::HTTP_BAD_REQUEST);

        }

        return view('auth.login');
    }

    public function register(Request $request)
    {

        if ($request->isMethod(self::POST)) {
            $rules = [
                'email' => 'required|email|unique:users',

            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return $this->_response_body($validator->messages(),
                    StatusCode::HTTP_UNPROCESSABLE_ENTITY,
                    'Validation Error');
            }

            DB::beginTransaction();

            try {

                $user = User::insert_email($request->input('email'));

                $data['verify_token'] = $user->confirmation_token;
                try {
                    Mail::to($user->email)->send(new ConfirmationEmail($data));
                } catch (\Exception $ex) {

                    return $this->_response_body($ex->getMessage());
                }


                DB::commit();
                return $this->_response_body(true);

            } catch (\Exception $ex) {
                DB::rollBack();
            }

        }


        return view('auth.register');
    }

    public function register_confirm(Request $request)
    {
        if ($request->isMethod(self::POST)) {


            $rules = [
                'password' => 'required|min:6|max:20',
                'confirm_password' => 'same:password',
                'username' => 'required|min:4|max:20|unique:users',
                'verify_token' => 'required|string',

            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return $this->_response_body($validator->messages(),
                    StatusCode::HTTP_UNPROCESSABLE_ENTITY,
                    'Validation Error');
            }
            $user = User::insert_user($request);
            if ($user) {
                Auth::loginUsingId($user->id);
                User::last_login_time();

                return $this->_response_body(StatusCode::HTTP_OK);

            }

        }
        return $this->_response_body(false);

    }

    public function reset_password(Request $request)
    {
        if ($request->isMethod(self::POST)) {
            $email = $request->input('email');

            $rules = [
                'email' => 'required|email|exists:users,email'
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return $this->_response_body($validator->messages(), StatusCode::HTTP_UNPROCESSABLE_ENTITY);
            }

            $user = User::query()
                ->where('email', $email)
                ->first();

            if ($user) {
                $token = Str::random(64);
                $user->reset_password_token = $token;
                $user->save();
                $data['token'] = $token;
                $data['display_name'] = $user->display_name;
                $mail = Mail::to($user->email)->send(new ResetPasswordEmail($data));
                return $this->_response_body(StatusCode::HTTP_OK);
            }
        }
        return view('auth.reset');
    }

    public function verify(Request $request, $test, $lang, $verify_token)
    {

        if ($request->isMethod('get')) {
            $user = User::query()->where('confirmation_token', $verify_token)->first();
            if ($user) {
                return view('auth.register_2', ['verify_token' => $verify_token]);
            }

        }
    }

    public function update_password(Request $request, $test, $lang, $reset_token)
    {
        if ($request->isMethod('get')) {
            $user = User::query()->where('reset_password_token', $reset_token)->first();
            if ($user) {
                return view('auth.update_password', ['token' => $reset_token]);
            }

        } else {
            $rules = [
                'password' => 'required|min:6|max:20',
                'confirm_password' => 'same:password',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return $this->_response_body($validator->messages(),
                    StatusCode::HTTP_UNPROCESSABLE_ENTITY,
                    'Validation Error');
            }
            $user = User::query()->where('reset_password_token', $reset_token)->first();
            if ($user) {
                $user->password = bcrypt($request->input('password'));
                $user->save();
                return $this->_response_body($user);
            }
        }
        return $this->_response_body(false);


    }
}
