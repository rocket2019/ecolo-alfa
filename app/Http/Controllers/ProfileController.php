<?php


namespace App\Http\Controllers;

use stdClass;

use App\Exports\GericoExport;
use App\Exports\oldDataExport;
use App\Http\Models\Bank;
use App\Http\Models\BankSetting;
use App\Http\Models\CodeTree;
use App\Http\Models\Coefficient;
use App\Http\Models\CompanyList;
use App\Http\Models\GeographicalAreas;
use App\Http\Models\GlobalVariable;
use App\Http\Models\Questionnaire;
use App\Http\Models\QuestionnaireTree;
use App\Http\Models\Role;
use App\Http\Models\SoseAteco;
use App\Http\Models\SoseCheck;
use App\Http\Models\SoseCheckValidationMessage;
use App\Http\Models\SoseStudiesCheck;
use App\Http\Models\SoseStudy;
use App\Http\Models\SubdomainSetup;
use App\Http\Models\User;
use App\Http\Services\StatusCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Str;
use Importer;
use Excel;
use File;
use function GuzzleHttp\Promise\all;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewUsersEmail;
use App\Http\Services\Is;

class ProfileController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function downloadNewInstruction(Request $request)
    {
        if (Is::bankManager()) {
            return response()->download(public_path('Ecolo_questionnaire_download_instruction_rev4_bank.pdf'));

        } elseif (Is::supervisor()) {
            return response()->download(public_path('Ecolo_questionnaire_download_instruction_rev4_superviser.pdf'));
        }
    }

    public function download_data()
    {
        $file = public_path() . "/site/excel/lastImportedExcel.xlsx";

        $headers = array(
            'Content-Type: application/xlsx',
        );

        return \Response::download($file, 'lastImportedExcel.xlsx', $headers);
    }

    public function get_data($path)
    {
        \Illuminate\Support\Facades\File::delete(public_path()."/site/excel/lastImportedExcel.xlsx");
        Excel::load($path, function ($file) {
            $allData = GeographicalAreas::query()
                ->select('id', 'area_name')
                ->where('bankCode', 1)
                ->orderBy('id')
                ->get();

            $sheet = $file->getActiveSheet(1);
            $count = 5;
            foreach ($allData as $key => $value) {
                while ($sheet->getCell('B' . $count) == ""){
                    $count++;
                }
                $sheet->setCellValue('A' . $count, $value->id);
                $count++;
            }
        })
            ->setFileName('lastImportedExcel')
            ->save('xlsx', public_path() . "/site/excel/");
        return true;
    }

    public function import_excel(Request $request)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'excel_file' => 'required'
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $message = $validator->messages();
                $message = json_decode($message);
                $message = $message->excel_file[0];
                return view('account.import_excel', ['messages' => $message]);
            }

            $file = $request->file('excel_file');
            $path = $file->getRealPath();
            $excel = Importer::make('Excel');

            $excel->load($path);
            $collection = $excel->getCollection();

            Coefficient::query()->truncate();

            foreach ($collection as $key => $collect) {
                if ($key == 1) {
                    foreach ($collect as $a => $code)
                        $ind_code[$a] = $code;
                }
            }

            GeographicalAreas::query()->truncate();
            $region_code = [];
            foreach ($collection as $key => $collect) {
                if($collect[1]){
                    if($collect[1]>0 && $collect[1]<150){
                        array_push($region_code, $collect[1]);
                    }
                }
                if ($key == 1) {
                    foreach ($collect as $a => $code)
                        $ind_code[$a] = $code;
                }
            }

            array_push($region_code, '0085', 7110, 7111, 1108, 7108, 7109, 8000);
            $region_code = array_values($region_code);
            $region_code_one = 0001;
            $region_description = 'Алтайский край';
            foreach ($collection as $key => $collect) {
                if (in_array($collect[1], $region_code) && $region_description != $collect[2]){
                    $region_code_one = $collect[1];
                    $region_description = $collect[2];
                }

                if ($collect[1]) {
                    $country = 'ru';
                    if ($collect[0] == 'BLR') {
                        $country = 'by';
                    }
                    for ($count = 1; $count < 11; $count++) {
                        GeographicalAreas::query()
                            ->insert([
                                'bankCode' => $count,
                                'area_code' => $collect[1],
                                'area_name' => $collect[2],
                                'progressiveRegionNumber' => $region_code_one,
                                'region_description' => $region_description,
                                'country' => GeographicalAreas::COUNTRY
                            ]);
                    }
                    foreach ($collect as $b => $coef) {
                        for ($i = 0; $i < count($ind_code); $i++) {
                            if ($b == $i && $ind_code[$i] != null) {
                                Coefficient::insert($ind_code[$b], $collect[1], $collect[2], $collect[$b], $collect[$b]);
                            }
                        }
                    }
                }
//                done insert
            }
//            Save excel file
            $this->get_data($path);
            return view('account.import_excel', ['true' => true]);
        }
        return view('account.import_excel');
    }

    public function account()
    {


        if (Auth::user()->role_id == Role::get_role_id_by_name(Role::ROLE_FINAL_CLIENT)) {


            $companies = CompanyList::query()->where('user_id', Auth::id())->paginate();
            if (count($companies)) {

                return view('account.search_company', ['companies' => $companies, 'results' => true]);

            }

            return view('account.search_company', ['companies' => null, 'results' => true]);

        } elseif (Auth::user()->role_id == Role::get_role_id_by_name(Role::ROLE_BANK_MINIMUM)) {

            $view = new CompanyController();
            return $view->print_questionnaire();
        }
        if (Auth::user()->role_id == Role::get_role_id_by_name(Role::ROLE_BANK_SUPERVISOR)){
            return redirect('/'.App::getLocale().'/manager');
        }

        $companies = CompanyList::query()->orderBy('created', 'desc')->limit(5)->get();
        $questionnaires = Questionnaire::get_all_questionnaire();

        return view('account.profile', ['companies' => $companies, 'questionnaires' => $questionnaires]);
    }

    public function new_to_old($bank, $lang, $id){
        $client = Questionnaire::query()
            ->where('id', '=', $id)
            ->first();

        if($client->status_code_id == 1){
            Questionnaire::query()
                ->where('id', '=', $id)
                ->update(['status_code_id' => 2]);
        }
    }

    public function logout()
    {
        Auth::logout();
        return Redirect::to('/' . App::getLocale() . '/login');
    }

    public function user_management()

    {
        $users = User::all();
        if (Auth::user()->role_id == Role::get_role_id_by_name(Role::ROLE_BANK_MANAGER)) {
            $users = User::query()->where('role_id', Role::get_role_id_by_name(Role::ROLE_FINAL_CLIENT))->get();
        }
        return view('account.user_management', ['users' => $users]);

    }

    public function add_new_user(Request $request)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'email' => 'required|email|unique:users,email',
                'username' => 'required|string|unique:users,username',
                'first_name' => 'string|nullable',
                'last_name' => 'string|nullable',
                'patronymic' => 'string|nullable',
                'password' => 'required|min:6|max:20',
                'confirm_password' => 'same:password',
                'role_id' => 'required|integer',
            ];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return $this->_response_body($validator->messages(),
                    StatusCode::HTTP_UNPROCESSABLE_ENTITY,
                    'Validation Error');
            }
            $user = User::add_new_user($request);
            if ($user) {
                return $this->_response_body($user);
            }

        }
        $roles = Role::get_all_roles();
        return view('account.new_user', ['roles' => $roles]);

    }

    public function delete_user($test, $lang, $id)
    {

        $user = User::query()->find($id);
        if ($user) {
            $user->delete();
            return $this->_response_body($user);


        }
        return false;
    }

    public function edit_user(Request $request, $test, $lang, $user_id)
    {

        $user = User::query()->find($user_id);
        if ($request->isMethod('post')) {
            $rules = [
                'email' => 'required|email|unique:users,email,' . $user_id,
                'username' => 'required|string|unique:users,username,' . $user_id,
                'password' => 'min:6|max:20|nullable',
                'first_name' => 'string|nullable',
                'last_name' => 'string|nullable',
                'patronymic' => 'string|nullable',
                'confirm_password' => 'same:password|nullable',
                'role_id' => 'required|integer',
            ];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return $this->_response_body($validator->messages(),
                    StatusCode::HTTP_UNPROCESSABLE_ENTITY,
                    'Validation Error');
            }
            $user = User::add_new_user($request);
            if ($user) {
                return $this->_response_body($user);
            }
        }
        $roles = Role::get_all_roles();
        return view('account.new_user', ['roles' => $roles, 'user' => $user]);


    }

    public function new_bank(Request $request)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'bank_name' => 'required|string|unique:banks_list,bank_name',

            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $message = $validator->messages();
                $message = json_decode($message);
                $message = $message->bank_name[0];
                return view('account.new_bank', ['messages' => $message]);

            }

            if (preg_match('/bank/', $request->input('bank_name'), $matches, PREG_OFFSET_CAPTURE)) {
                if (App::getLocale() == 'en') {
                    $message = 'You cannot use the word "Bank" in the subdomain.';
                } elseif (App::getLocale() == 'it') {

                    $message = 'Non si puà utilizzare la parola "Banca" nel sottodominio.';
                } else {
                    $message = 'Вы не можете использовать слово «Банк» в поддомене.';

                }
                return view('account.new_bank', ['messages' => $message]);

            }


            $company = Bank::insert($request);
            if ($company) {
                $name = str_replace(' ', '_', $request->input('bank_name'));
                SubdomainSetup::subDomainSetup($name);
                $path = '/var/www/' . $name;
                $document_root = $path . '/ecolo/public';
                $table_name = 'ecolo_' . $name;
                $server_name = $name . '.ecolo.rocketsystems.net';
                return $this->make_apache_confs($path, $server_name, $document_root, $table_name);

            }

        }


        return view('account.new_bank');

    }

    public function make_apache_confs($path, $server_name, $document_root, $table_name)
    {
        $output = shell_exec('sudo chown -R www-data:www-data /etc/apache2/sites-available && sudo shmod -R g+s /etc/apache2/sites-available');
        chdir('/var/www/ecolo');
        $server_name = escapeshellarg($server_name);
        $document_root = escapeshellarg($document_root);
        $a = escapeshellarg($path);
        $output = shell_exec("sh apache.sh  2>&1 $a $server_name $document_root $table_name");
        echo "<pre>" . $output;
    }

    public function checks(Request $request)
    {
        $studies = SoseStudy::query()->orderByRaw('study_code')->get();
        return view('account.check', ['studies' => $studies]);

    }

    public function study_checks($test, $lang, $study_id)
    {
        $studies = SoseStudy::query()->orderByRaw('study_code')->get();
        $study_code = SoseStudy::query()->where('id', $study_id)->select('study_code')->first();
        $study_checks = SoseCheck::query()->where('sose_study_id', $study_id)->get();


        return view('account.check', ['studies' => $studies,
            'study_id' => $study_id,
            'study_code' => $study_code->study_code,
            'study_checks' => $study_checks]);
    }

    public function delete_study_check($test, $lang, $study_id, $check_id)
    {
        SoseCheck::query()->where('id', $check_id)->delete();

        $study_check = SoseStudiesCheck::query()->where('sosestudy_id', $study_id)->where('sosecheck_id', $check_id)->first();

        $deleted = DB::delete('delete from sose_studies_checks WHERE `sosestudy_id` = ' . $study_id . ' AND sosecheck_id = ' . $check_id);

        return $this->_response_body(true);

    }

    public function add_study(Request $request, $test, $lang, $study_id)
    {

        if ($request->isMethod('post')) {

            $rules = [
                'pre_check_condition' => 'required|string',
                'validation_formula' => 'required|string',
                'message_ru' => 'string|nullable',
                'message_en' => 'string|nullable',
                'message_it' => 'string|nullable',


            ];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return $this->_response_body($validator->messages(),
                    StatusCode::HTTP_UNPROCESSABLE_ENTITY,
                    'Validation Error');
            }
            $user_id = Auth::id();
            $sose_check_id = SoseCheck::insert($request->input('pre_check_condition'), $request->input('validation_formula'), $user_id, $study_id);
            if ($sose_check_id) {
                $sose_check_validation = SoseCheckValidationMessage::insert($user_id, $sose_check_id->id,
                    $request->input('message_it'),
                    $request->input('message_en'),
                    $request->input('message_ru')
                );
                $sose_study_check = SoseStudiesCheck::insert($study_id, $sose_check_id->id);
                if ($sose_check_validation && $sose_study_check) {
                    return $study_id;
                }
            }
            return false;
        }
        $studies = SoseStudy::query()->orderByRaw('study_code')->get();
        $study_code = SoseStudy::query()->where('id', $study_id)->select('study_code')->first();
        return view('account.add_or_edit_study', ['studies' => $studies,
                'study_id' => $study_id,
                'study_code' => $study_code->study_code]
        );

    }

    public function edit_study(Request $request, $test, $lang, $study_id, $check_id)
    {

        if ($request->isMethod('post')) {

            $rules = [
                'pre_check_condition' => 'required|string',
                'validation_formula' => 'required|string',
                'message_it' => 'string|nullable',
                'message_ru' => 'string|nullable',
                'message_en' => 'string|nullable',


            ];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return $this->_response_body($validator->messages(),
                    StatusCode::HTTP_UNPROCESSABLE_ENTITY,
                    'Validation Error');
            }
            $user_id = Auth::id();
            $sose_check_id = SoseCheck::update_check($request->input('pre_check_condition'), $request->input('validation_formula'), $user_id, $check_id);
            if ($sose_check_id) {
                $sose_check_validation = SoseCheckValidationMessage::update_valid($user_id, $sose_check_id->id,
                    $request->input('message_it'),
                    $request->input('message_en'),
                    $request->input('message_ru')
                );
                if ($sose_check_validation) {
                    return $study_id;
                }
            }
            return false;
        }
        $studies = SoseStudy::query()->orderByRaw('study_code')->get();
        $study_code = SoseStudy::query()->where('id', $study_id)->select('study_code')->first();
        $check = SoseCheckValidationMessage::query()->where('sose_check_id', $check_id)->first();
        return view('account.add_or_edit_study', ['studies' => $studies,
                'study_id' => $study_id,
                'study_code' => $study_code->study_code, 'check' => $check]
        );

    }

    public function banks_list()

    {
        $banks = Bank::get();
        return view('account.bank_list', ['banks' => $banks]);

    }

    public function delete_bank($test, $lang, $id)
    {

        $user = Bank::query()->find($id);
        if ($user) {
            $user->delete();
            return $this->_response_body($user);


        }
        return false;
    }

    public function bank_settings()
    {
        $bank = BankSetting::query()->first();
        return view('account.bank_settings', ['bank' => $bank]);


    }

    public function delete_logo()
    {
        $bank = BankSetting::query()->first();
        $bank->logo_path = null;
        $bank->save();
        return $this->_response_body(true);


    }

    public function delete_phone_number()
    {
        $bank = BankSetting::query()->first();
        $bank->phone_number = null;
        $bank->save();
        return $this->_response_body(true);


    }

    public function add_logo(Request $request)
    {


        $rules = [
//            'logo' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg',
            'phone_number' => 'nullable|string',


        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return $this->_response_body($validator->messages(),
                StatusCode::HTTP_UNPROCESSABLE_ENTITY,
                'Validation Error');
        }

        $base64_image = $request->input('image');

        $imageName = Str::random(16) . '.png';


        if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {

            $data = substr($base64_image, strpos($base64_image, ',') + 1);

            $data = base64_decode($data);
            Storage::disk('public')->put($imageName, $data);

        }


        $bank = BankSetting::query()->first();
        if (!$bank) {
            $bank = new BankSetting;
        }
        if (isset($base64_image)) {
            $bank->logo_path = $imageName;

        }
        if ($request->input('phone_number')) {
            $bank->phone_number = $request->input('phone_number');

        }
        $bank->save();
        return $this->_response_body($bank);
    }


    public function import_excel_update(Request $request)
    {


        if ($request->isMethod('post')) {

            $rules = [
                'excel_file' => 'required',
                'language' => 'required|string'

            ];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $message = $validator->messages();
                $message = json_decode($message);
                $message = $message->excel_file[0];
                return view('account.import_excel', ['messages' => $message]);
            }
            $lang = $request->input('language');
            $path = $request->file('excel_file')->getRealPath();
            $excel = Importer::make('Excel');
            $excel->load($path);
            $collection = $excel->getCollection();

            foreach ($collection as $key => $collect) {

                if ($key == 0) {
                    continue;
                }


                try {
                    $code = str_replace(".", "", $collect[4]);
                    $ateco = SoseAteco::get_by_code($code);
                    if (!$ateco) {
                        continue;
                    }
//                    $tree_code = QuestionnaireTree::query()->where('sose_ateco_id', $ateco->id)->first();
//                    $tree = CodeTree::query()->where('code', $tree_code->code)->first();
                    $description = 'description_' . $lang;
//                    $tree->{$description} = $collect[5] ;


//                    $tree->save();

                    $ateco_description = 'description_' . $lang;
                    $ateco->{$ateco_description} = $collect[5] . '[' . $collect[0] . ']';
                    $ateco->name = $collect[3] . '[' . $collect[0] . ']';
                    $ateco->save();

//                    if ($lang == 'ru') {
//
//                        $ateco->description_ru = $collect[5]. '[' .$collect[0] .']';
//                        $ateco->save();
//                    } elseif ($lang == 'it') {
//
//                        $ateco->description_it = $collect[5]. '['.$collect[0] .']';
//                        $ateco->save();
//                    } else {
//
//                        $ateco->description_en = $collect[5] . '['.$collect[0] .']';
//                        $ateco->save();
//                    }
                } catch (\Exception $ex) {
                    echo '<pre>';
                    var_dump($ex->getMessage(), $collect, $code);
                    die;
                }


            }
            return view('account.excel_for_update', ['true' => true]);
        }
        return view('account.excel_for_update');

    }

    public function action_tools()
    {
        $variable = GlobalVariable::all();
        $usd_to_euro = null;
        $usd_to_ruble = null;
        $euro_to_ruble = null;

        if (count($variable)) {
            foreach ($variable as $var) {
                print_r($var->variable);
                if ($var->variable == 'usd_to_euro') {
                    $usd_to_euro = $var->value;
                };
                if ($var->variable == 'usd_to_ruble') {
                    $usd_to_ruble = $var->value;
                };
                if ($var->variable == 'euro_to_ruble') {
                    $euro_to_ruble = $var->value;
                };

            }
        }
        return view('account.action_tools', [
            'usd_to_euro' => $usd_to_euro,
            'usd_to_ruble' => $usd_to_ruble,
            'euro_to_ruble' => $euro_to_ruble
        ]);

    }

    public function add_or_edit_globals(Request $request)
    {
        $rules = [
            'usd_to_euro' => 'nullable|string',
            'usd_to_ruble' => 'nullable|string',
            'euro_to_ruble' => 'nullable|string'

        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return $this->_response_body($validator->messages(),
                StatusCode::HTTP_UNPROCESSABLE_ENTITY,
                'Validation Error');
        }

        foreach ($request->all() as $key => $req) {
            $query = GlobalVariable::insert($key, $req);

        }
        return $this->_response_body(true);


    }

    public function usersImportTest()
    {
        die;
        $file = public_path('tusers.xlsx');
        $excel = Importer::make('Excel');
        $excel->load($file);
        $allData = $excel->getCollection();

        $allData[0] = ['Arshakyan', 'Artyom', null, 'arshakyan.artyom+alfa@gmail.com', 4, 'asdasd111'];

        foreach ($allData as $key => $user) {
            $lastname = $user[0];
            $firstname = $user[1];
            $patronymic = $user[2];
            $email = trim($user[3]);
            $password = $user[5];

            $newUser = new User();
            $newUser->username = $email;
            $newUser->first_name = $firstname;
            $newUser->last_name = $lastname;
            $newUser->patronymic = $patronymic;
            $newUser->email = $email;
            $newUser->confirmation_token = Str::random(32);
            $newUser->password = bcrypt($password);
            $newUser->role_id = in_array($key, [111, 112, 113]) ? 3 : 4;
            $newUser->save();

            $data = [
                'first_name' => $firstname,
                'username' => $email,
                'password' => $password
            ];

            Mail::to($email)->send(new NewUsersEmail($data));

            echo "$key ok <br>";
        }
    }

}
