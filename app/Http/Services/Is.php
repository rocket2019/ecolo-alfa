<?php

namespace App\Http\Services;

use App\Http\Services\Constants;
use Illuminate\Support\Facades\Auth;
use App\Http\Models\Role;

class Is extends Constants
{
    /**
     * is dev
     * @return bool
     */
    public static function dev(): bool
    {
        return parent::bankName() === parent::DEV;
    }

    /**
     * is mtb
     * @return bool
     */
    public static function mtb(): bool
    {
        return parent::bankName() === parent::MTB;
    }

    /**
     * is kazan
     * @return bool
     */
    public static function kazan(): bool
    {
        return parent::bankName() === parent::KAZAN;
    }

    /**
     * is minbank
     * @return bool
     */
    public static function minbank(): bool
    {
        return parent::bankName() === parent::MINBANK;
    }

    /**
     * is sovcom
     * @return bool
     */
    public static function sovcom(): bool
    {
        return parent::bankName() === parent::SOVCOM;
    }

    /**
     * is alfa
     * @return bool
     */
    public static function alfa(): bool
    {
        return parent::bankName() === parent::ALFA;
    }

    /**
     * is supervisor
     * @return bool
     */
    public static function supervisor(): bool
    {
        return Auth::check() && Auth::user()->role_id == Role::get_role_id_by_name(Role::ROLE_BANK_SUPERVISOR);
    }

    /**
     * is bank manager
     * @return bool
     */
    public static function bankManager(): bool
    {
        return Auth::check() && Auth::user()->role_id == Role::get_role_id_by_name(Role::ROLE_BANK_MANAGER);
    }

    /**
     * is admin
     * @return bool
     */
    public static function admin(): bool
    {
        return Auth::check() && Auth::user()->role_id == Role::get_role_id_by_name(Role::ROLE_EVA_ADMIN);
    }
}
