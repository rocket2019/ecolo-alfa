<?php
namespace App\Http\Services;

class Constants
{
    const
        DEV = 'dev',
        ALFA = 'alfa',
        MTB = 'mtb',
        KAZAN = 'kazan',
        SOVCOM = 'sovcom',
        MINBANK = 'minbank',
        OTP = 'otp',
        PLACE = 'place',
        BK = 'bk'
    ;

    const PHONE = [
        self::DEV => '+7-495-997-33-30',
        self::MTB => '+7-985-991-20-87',
        self::KAZAN => '8-800-234-4764',
        self::SOVCOM => '8-800-234-4764',
        self::MINBANK => '8-800-234-4764',
        self::ALFA => '8-800-234-4764',
    ];


    const CONNECTIONS = [
        self::DEV => 'ecolo_dev',
        self::ALFA => 'mysql',
        self::MTB => 'ecolo_mtb',
        self::KAZAN => 'ecolo_kb',
        self::SOVCOM => 'ecolo_sovcom',
        self::MINBANK => 'ecolo_minbank',
        self::OTP => 'ecolo_otp',
        self::PLACE => 'ecolo_place',
    ];

    /**
     * Get bank name from env
     * @return string
     */
    public static function bankName(): string
    {
        return env('BANK_NAME','dev');
    }

    /**
     * Get this Bank phone number
     * @return string
     */
    public static function bankPhone(): string
    {
        return self::PHONE[self::bankName()];
    }
}
