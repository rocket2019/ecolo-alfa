<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
    protected $table = 'balances';

    public static function tableName()
    {
        return 'balances';
    }
}
