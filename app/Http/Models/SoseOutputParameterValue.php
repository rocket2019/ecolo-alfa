<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class SoseOutputParameterValue extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    protected $table = 'sose_output_parameter_value';

    public static function tableName()
    {
        return 'sose_output_parameter_value';
    }
}
