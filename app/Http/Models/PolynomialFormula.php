<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class PolynomialFormula extends Model
{
    protected $table = 'polynomial_formula';

    public static function tableName()
    {
        return 'polynomial_formula';
    }
}
