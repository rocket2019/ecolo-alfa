<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class GlobalVariable extends Model
{
    protected $table = 'global_variables';
    const UPDATED_AT = 'updated';
    const CREATED_AT = 'created';

    public static function tableName()
    {
        return 'global_variables';
    }

    public static function insert($key, $value)
    {
        $query = self::query()->where('variable', $key)->first();
        if (!$query) {
            $query = new self();
        }
        $query->variable = $key;
        $query->value = $value;
        $query->user_id = Auth::id();
        $query->save();

        if ($key == 'euro_to_ruble')
        {
            $ruble_to_euro_tax = self::query()->where('variable','ruble_to_euro_tax')->first();
            $ruble_to_euro_tax->value = number_format(1 / $value,3);
            $ruble_to_euro_tax->save();
        }

    }
}
