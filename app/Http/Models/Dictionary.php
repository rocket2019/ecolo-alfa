<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Dictionary extends Model
{
    protected $table = 'dictionary';

    public static function tableName()
    {
        return 'dictionary';
    }
}
