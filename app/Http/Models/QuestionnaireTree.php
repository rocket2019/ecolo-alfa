<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionnaireTree extends Model
{
    protected $table = 'questionnaire_tree';

    public static function tableName()
    {
        return 'questionnaire_tree';
    }
    protected $with = [
        'sose_ateco',
        'sose_okved',
    ];

    public static function get_by_code($code)
    {
        $tree = self::query()
            ->where('code', $code)
            ->with('sose_ateco')
            ->leftJoin('sose_study', 'questionnaire_tree.sose_study_id', '=', 'sose_study.id')
            ->get();
        return $tree;
    }


    public function sose_ateco()
    {
        return $this->hasOne('App\Http\Models\SoseAteco', 'id', 'sose_ateco_id');
    }

    public function sose_okved()
    {
        return $this->hasOne('App\Http\Models\SoseOkved', 'id', 'sose_okved_id');
    }
}
