<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Coefficient extends Model
{
    protected $table = 'coefficient_table';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    public static function tableName()
    {
        return 'coefficient_table';
    }

    public static function insert($code, $region_code, $region_name, $micro, $small)
    {

        $query = new self();
        $query->industry_group_code = $code;
        $query->region_code = $region_code;
        $query->region_name = $region_name;
        $query->micro = $micro;
        $query->small = $small;


        if ($query->save()) {
            return $query;
        }
    }
}
