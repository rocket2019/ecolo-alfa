<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class EditQuestionnaireLog extends Model
{
    protected $table = 'edit_questionnaire_log';

    public static function tableName()
    {
        return 'edit_questionnaire_log';
    }
}
