<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class DataLog extends Model
{
    protected $table = 'data_logs';

    public static function tableName()
    {
        return 'data_logs';
    }
}
