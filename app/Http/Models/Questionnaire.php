<?php


namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Questionnaire extends Model

{
    protected $table = 'sose_questionnaire';
    const UPDATED_AT = 'updated';
    const CREATED_AT = 'created';

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'sose_questionnaire';
    }



    protected $with = [
        'sose_ateco', 'sose_status', 'sose_study', 'user', 'company'
    ];

    public static function insert($request, $client_id = null)
    {
        $sose_study = SoseAtecoStudiesEquivalence::get_by_ateco_id($request->input('ateco_id'));
        $query = new self();
        $query->user_id = Auth::id();

        $query->id_company = $client_id ?? $request->input('company_id');

        $query->sose_okved_id = self::get_okved_id_by_ateco_id($request->input('ateco_id'));
        $query->sose_ateco_id = $request->input('ateco_id');
        $query->company_unit = $request->input('company_unit');
        $query->status_code_id = 1;
        $query->sose_study_id = $sose_study->sosestudy_id;
        $query->sose_questionnaire_status_id = SoseQuestionnaireStatus::IN_EDITING;
        $query->client_id = $client_id;
        if ($query->save()) {
            return $query->id;
        }
        return false;

    } public static function new_questionnaire($user_id,$company_id,$sose_ateco_id,$sose_study_id,$unit)
    {
        $query = new self();
        $query->user_id = $user_id;
        $query->id_company = $company_id;
        $query->sose_ateco_id = $sose_ateco_id;
        $query->sose_okved_id = self::get_okved_id_by_ateco_id($sose_ateco_id);
        $query->sose_study_id = $sose_study_id;
        $query->company_unit = $unit;
        $query->sose_questionnaire_status_id = SoseQuestionnaireStatus::SUBMITTED;
        if ($query->save()) {
            return $query->id;
        }
        return false;

    }

    public static function get_okved_id_by_ateco_id($ateco_id)
    {
        $okved_id = SoseOkvedAtecoEquivalence::query()->where('soseateco_id',$ateco_id)->first();
        return $okved_id ? $okved_id->soseokved_id : null;
    }

    public static function change_status($questionnaire_id, $status_id)
    {
        $query = self::query()->find($questionnaire_id);
        if ($query) {
            $query->sose_questionnaire_status_id = $status_id;
            $query->save();
            return true;
        }
        return false;
    }

    public static function get_company_questionnaires($company_id)
    {
        $questionnaire = self::query()->where('id_company', $company_id)->get();
        return $questionnaire;

    }

    public static function get_all_questionnaire()
    {

        $query = self::query()->get();
        return $query;

    }

    public static function get_by_id($id)
    {
        $query = Questionnaire::query()->where('id', $id)->first();
        return $query;
    }

    public function sose_study()
    {
        return $this->hasOne('App\Http\Models\SoseStudy', 'id', 'sose_study_id');
    }

    public function sose_ateco()
    {
        return $this->hasOne('App\Http\Models\SoseAteco', 'id', 'sose_ateco_id');
    }

    public function user()
    {
        return $this->hasOne('App\Http\Models\User', 'id', 'user_id');
    }

    public function company()
    {
        return $this->hasOne('App\Http\Models\CompanyList', 'ID_company', 'id_company');
    }

    public function sose_status()
    {
        return $this->hasOne('App\Http\Models\SoseQuestionnaireStatus', 'id', 'sose_questionnaire_status_id');
    }

    public function sose_okved()
    {
        return $this->hasOne('App\Http\Models\SoseOkved','id','sose_okved_id');
    }

}
