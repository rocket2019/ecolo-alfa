<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Comment extends Model
{
    protected $table = 'comments';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    public static function tableName()
    {
        return 'comments';
    }

    public static function insert($request)
    {
        $query = new self();
        $query->user_id = Auth::id();
        $query->text = $request->input('text');
        $query->ID_company = $request->input('company_id');
        if ($query->save())
        {
            return true;
        }
        return false;

    }

    public static function get($company_id)
    {
        $comments = self::query()->where('ID_company',$company_id)->with('user')->get();
        return $comments;
    }

    public function user()
    {
        return $this->hasOne('App\Http\Models\User', 'id', 'user_id');
    }
}
