<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class RevenueMargin extends Model
{
    protected $table = 'revenue_margin';

    public static function tableName()
    {
        return 'revenue_margin';
    }
}
