<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;


class User extends Authenticatable
{
    use Notifiable;

    const ENABLED_TRUE = 1;
    const ENABLED_FALSE = 0;


    protected $table = 'users';

    public static function tableName()
    {
        return 'users';
    }



    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'city', 'department','first_name','last_name','patronymic'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public static function insert_email($email)
    {
        $user = new self();
        $user->email = $email;
        $user->confirmation_token = Str::random(32);

        if ($user->save()) {

            return $user;
        }
        return false;

    }

    public static function insert_user($request)
    {
        $input = $request->all();
        $user = self::query()->where('confirmation_token', $request->input('verify_token'))->first();
        if ($user) {
            $user->fill($input);
            $user->password = bcrypt($request->input('password'));
            $user->role_id = Role::get_role_id_by_name(Role::ROLE_FINAL_CLIENT);
            if ($user->save()) {
                return $user;
            }

        }
        return false;

    }

    public static function last_login_time()
    {
        $user = self::query()->find(Auth::id());

        $user->last_login = Carbon::now()->format('Y-m-d H:i:s');
        $user->save();

        return true;
    }

    public static function add_new_user($request)
    {
        $input = $request->all();

        $user = self::query()->where('id', $request->input('user_id'))->first();
        if (!$user) {
            $user = new self();
        }
        $user->fill($input);
        if ($request->input('password')) {
            $user->password = bcrypt($request->input('password'));

        }
        $user->role_id = $request->input('role_id');
        $user->confirmation_token = Str::random(32);
        if ($user->save()) {
            return $user;
        }


    }

    public static function getUsersByRole($role)
    {
        return self::query()->where('role_id', '=', $role)->get()->toArray();
    }

}
