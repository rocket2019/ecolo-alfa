<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class SoseStudy extends Model
{
    protected $table = 'sose_study';

    public static function tableName()
    {
        return 'sose_study';
    }
}
