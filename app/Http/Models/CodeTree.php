<?php

namespace App\Http\Models;


use App\Scopes\DescriptionTranslationScope;
use Illuminate\Database\Eloquent\Model;

class CodeTree extends Model
{
    protected $table = 'code_tree';

    public $timestamps = false;

    public function __construct()
    {
        parent::__construct();
        static::addGlobalScope(new DescriptionTranslationScope());
    }

    public static function tableName()
    {
        return 'code_tree';
    }


    public static function get_by_code($code)
    {
        return self::query()->where('code', '=',$code)->first();
    }

}
