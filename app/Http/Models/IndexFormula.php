<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class IndexFormula extends Model
{
    protected $table = 'index_formula';

    public static function tableName()
    {
        return 'index_formula';
    }
}
