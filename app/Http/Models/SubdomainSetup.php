<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class SubdomainSetup extends Model
{
    public static function subDomainSetup($subdomain)
    {
        $database = [];
        $database_name = 'ecolo_' . $subdomain;
        if (DB::connection()->getDatabaseName() !== $database_name)
        {

            if (DB::connection()->getDatabaseName() != $database_name) {

                Artisan::call('make:database ' . $database_name . ' mysql');

//                $database['host'] = 'localhost';
//                $database['database'] = $database_name;
//                $database['username'] = env('DB_USERNAME');
//                $database['password'] = env('DB_PASSWORD');


//        $account_data = Account::where( 'subdomain', $subdomain )->where( 'status', 1 )->first();
//
//        if( ! $account_data ) {
//            // account not found, do something else
//        }

                // Account found and is active, Continue processing...

                // Get the database config details from wherever and however you want,
                // Could be stored in a database (as JSON), separate file, or stored in an array here

                // And then do something like this

//                Config::set('database.connections.mysql.host', $database['host']);
//                Config::set('database.connections.mysql.database', $database['database']);
//                Config::set('database.connections.mysql.username', $database['username']);
//                Config::set('database.connections.mysql.password', $database['password']);
//                DB::reconnect();
//
//
//                Artisan::call('migrate:install');
//                Artisan::call('migrate --force');
//                Artisan::call('db:seed');
            }
        }

    }
}
