<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Client extends Model
{
    const QUESTIONNAIRE_STATUS_EDITING = 1;
    const QUESTIONNAIRE_STATUS_EVALUATE = 2;
    const QUESTIONNAIRE_STATUS_EVALUATED = 3;


    protected $table = 'clients';

    protected $fillable = [
        'company_name',
        'email',
        'INN_number',
        'main_activity',
        'area_region',
        'area_city'
    ];

    protected $with = [
        'questionnaire'
    ];

    public static function tableName()
    {
        return 'clients';
    }

    /**
     * Get Clients By Manager ID
     * @param $data
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getManagerClients($data)
    {
        return self::query()
            ->where('company_name','LIKE','%'.$data.'%')
            ->where('manager_id',Auth::id())
            ->get();

    }

    public function questionnaire()
    {
        return $this->hasOne('App\Http\Models\Questionnaire','client_id','id');
    }
}
