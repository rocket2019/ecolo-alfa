<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class SoseAtecoStudiesEquivalence extends Model
{
    protected $table = 'sose_ateco_studies_equivalence';

    public static function tableName()
    {
        return 'sose_ateco_studies_equivalence';
    }

    public static function get_by_ateco_id($ateco_id)
    {
        $query = self::query()->where('soseateco_id',$ateco_id)->with('sose_study')->first();
        return $query;

    }

    public function sose_study()
    {
        return $this->hasOne('App\Http\Models\SoseStudy', 'id', 'sosestudy_id');
    }
}
