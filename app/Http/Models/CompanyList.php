<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class CompanyList extends Model
{
    protected $table = 'company_list';
    protected $primaryKey = 'ID_company';

    const UPDATED_AT = 'updated';
    const CREATED_AT = 'created';

    public static function tableName()
    {
        return 'company_list';
    }

    protected $fillable = [
        'company_name',
        'email_manager',
        'INN_number',
        'main_activity',
        'area_region',
        'area_city',
        'ateco_id',
    ];

    protected $with = [
        'user','legal_forms'
    ];

    /**
     * @param $ID_company
     * @return mixed
     */
    public static function getByCompanyId($ID_company)
    {
        return self::query()->where('ID_company', '=', $ID_company)->first();
    }

    public static function insert($request)
    {
        $company = new self();
        $company->user_id = $request->input('user_id');
        $company->company_name = $request->input('company_name');
        $company->INN_number = $request->input('id_number');
        $company->legal_form = $request->input('legal_form');
        $company->main_activity = $request->input('main_activity');
        $company->location_address = $request->input('address');
        $company->region_code = $request->input('area_city');
        $company->region = $request->input('area_region');
        if ($company->save()) {
            return $company;
        }
    }

    public static function update_company($request, $id)
    {

        $company = self::query()->where('ID_company',$id)->first();
        if ($company) {
            $company->user_id = $request->input('user_id');
            $company->company_name = $request->input('company_name');
            $company->INN_number = $request->input('id_number');
            $company->legal_form = $request->input('legal_form');
            $company->main_activity = $request->input('main_activity');
            $company->location_address = $request->input('address');
            $company->region_code = $request->input('area_city');
            $company->region = $request->input('area_region');
            if ($company->save()) {
                return $company;
            }
        }

    }
    public static function new_company($name,$user_id,$number,$form_id,$main_activity,$subject)
    {
        $company = new self();
        $company->user_id = $user_id;
        $company->company_name = $name;
        $company->INN_number = $number;
        $company->legal_form = $form_id;
        $company->main_activity = $main_activity;
        $company->region_code =$subject;
        if ($company->save()) {
            return $company;
        }
    }

    /**
     * @return array
     */
    public static function countOfQuests()
    {
        return [
            'send' => self::query()->where('questionnaire_status', Client::QUESTIONNAIRE_STATUS_EVALUATE)->count(),
            'end' => self::query()->where('questionnaire_status', Client::QUESTIONNAIRE_STATUS_EVALUATED)->count(),
        ];
    }

    /**
     * Get Clients By Manager ID
     * @param $data
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getManagerClients($data)
    {
        return self::query()
            ->where(function (Builder $query) use ($data) {
                $query
                    ->where('company_name', 'LIKE', '%'.$data.'%')
                    ->orWhere('INN_number', 'LIKE', '%'.$data.'%');
            })
            ->when(Auth::user()->role_id == Role::get_role_id_by_name(Role::ROLE_BANK_SUPERVISOR),
                function ($query) {
                    return $query->where('questionnaire_status', '!=', 1)->orderBy('updated', 'DESC');
                },
                function ($query) {
                    return $query->where('manager_id', Auth::id())->orderBy('created', 'DESC');
                }
            )
            ->with('questionnaire')
            ->get();
    }

    public static function change_status($company_id, $status_id)
    {
        $query = self::query()->find($company_id);
        if ($query) {
            $query->questionnaire_status = $status_id;
            $query->save();
            return true;
        }
        return false;
    }

    public function user()
    {
        return $this->hasOne('App\Http\Models\User', 'id', 'user_id');
    }
    public function legal_forms()
    {
        return $this->hasOne('App\Http\Models\LegalForm', 'id', 'legal_form');
    }

    public function questionnaire()
    {
        return $this->hasOne('App\Http\Models\Questionnaire','id_company','ID_company');
    }


}
