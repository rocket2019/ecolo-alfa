<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class SoseOkved extends Model
{
    protected $table = 'sose_okved';

    public static function tableName()
    {
        return 'sose_okved';
    }
}
