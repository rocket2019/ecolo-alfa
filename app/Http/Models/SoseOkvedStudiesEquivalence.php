<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class SoseOkvedStudiesEquivalence extends Model
{
    protected $table = 'sose_okved_studies_equivalence';

    public static function tableName()
    {
        return 'sose_okved_studies_equivalence';
    }
}
