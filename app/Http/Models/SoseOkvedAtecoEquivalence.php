<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class SoseOkvedAtecoEquivalence extends Model
{
    protected $table = 'sose_okved_ateco_equivalence';

    public static function tableName()
    {
        return 'sose_okved_ateco_equivalence';
    }
}
