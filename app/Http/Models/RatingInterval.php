<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class RatingInterval extends Model
{
    protected $table = 'rating_intervals';

    public static function tableName()
    {
        return 'rating_intervals';
    }
}
