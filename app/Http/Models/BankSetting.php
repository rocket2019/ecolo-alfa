<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 7/4/2019
 * Time: 3:20 PM
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class BankSetting extends Model
{
    protected $table = 'bank_settings';

    public static function tableName()
    {
        return 'bank_settings';
    }



    public static function get_logo()
    {
        $logo = self::query()->first();
        if ($logo)
        {
            return $logo->logo_path;

        }
        return false;
    }
    public static function get_phone()
    {
        $logo = self::query()->first();
        if ($logo)
        {
            return $logo->phone_number;

        }
        return false;
    }

}