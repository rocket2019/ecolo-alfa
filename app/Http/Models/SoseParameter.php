<?php

namespace App\Http\Models;

use App\Scopes\DescriptionAndPathTranslationScope;
use App\Scopes\DescriptionTranslationScope;
use App\Scopes\SoseParameterScope;
use App\Scopes\SoseParameterMessageTranslationScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class SoseParameter extends Model
{

    protected $table = 'sose_parameter';

    protected $primaryKey = 'id';


    public $timestamps = false;

    public static function tableName()
    {
        return 'sose_parameter';
    }

    protected static function boot()
    {
        parent::boot(); // TODO: Change the autogenerated stub

        static::addGlobalScope(new SoseParameterScope());
    }

    protected $fillable = [
        'parameter_code', 'description_en', 'description_ru',
        'description_it', 'value_type', 'path_en',
        'path_it', 'path_ru', 'visible', 'units', 'units_en', 'units_ru', 'units_it'
    ];


    public static function update_parameter($request, $id, $visible)
    {
        $input = $request->except('visible');


        $parameter = self::find($id);

        $unit = $request->input('units');

        $units = self::query()->where('units', $unit)->whereNotNull('units_en')->whereNotNull('units')->first();


        if (isset($parameter)) {
            if ($units) {
                $input['units_en'] = $units->units_en;
                $input['units_ru'] = $units->units_ru;
                $input['units_it'] = $units->units_it;
            } else {
                $input['units_en'] = null;
                $input['units_ru'] = null;
                $input['units_it'] = null;
            }
            $input['units'] = $unit;

            $input['visible'] = $visible;

            if (strpos($parameter->parameter_code,'F0') !== false)
            {
                $f_params = SoseParameter::query()->where('parameter_code','=',$parameter->parameter_code)->get();
                foreach ($f_params as $f)
                {
                    $f->description_ru = $input['description_ru'];
                    $f->description_en = $input['description_en'];
                    $f->description_it = $input['description_it'];
                    $f['visible'] = $visible;
                    $f->path_ru = $input['path_ru'];
                    $f->save();
                }
            }


            try {
                if (strpos($parameter->parameter_code,'B') !== false)
                {
                    //Get all sose parameters
                    $sose_study = SoseStudiesParameter::query()
                        ->where('soseparameter_id', $id)
                        ->first();

                    $get_all_parameters = SoseStudiesParameter::query()
                        ->where('sosestudy_id', $sose_study->sosestudy_id)
                        ->select('soseparameter_id')
                        ->get();
                    $all_params_ids = [];
                    foreach ($get_all_parameters as $param) {
                        array_push($all_params_ids, $param->soseparameter_id);
                    }

                    $find_all_sose_params = SoseParameter::query()
                        ->whereIn('id', $all_params_ids)
                        ->get();

                    foreach ($find_all_sose_params as $sose_param) {

                        if ($sose_param->description_ru == $parameter->description_ru) {

                            $sose_param->description_ru = $input['description_ru'];
                            $sose_param->description_en = $input['description_en'];
                            $sose_param->description_it = $input['description_it'];
                            $sose_param['visible'] = $visible;
                            $sose_param->path_ru = $input['path_ru'];
                            $sose_param->save();
                        }
                    }
                }




                $parameter->update($input);
            } catch (\Exception $e) {
                var_dump($e);
            }


            return $parameter->id;


        }
        return false;
    }


}
