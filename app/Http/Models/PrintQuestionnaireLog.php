<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class PrintQuestionnaireLog extends Model
{
    protected $table = 'print_questionnaire_log';

    public static function tableName()
    {
        return 'print_questionnaire_log';
    }
}
