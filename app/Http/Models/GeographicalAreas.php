<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 6/27/2019
 * Time: 3:27 PM
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class GeographicalAreas extends Model
{
    public $timestamps =false;

    protected $table = 'geographical_areas';

    const COUNTRY = 'ru';

    public static function tableName()
    {
        return 'geographical_areas';
    }

    public static function getByAreaCode($area_code)
    {
        return self::query()->where('area_code',$area_code)->first();
    }

}
