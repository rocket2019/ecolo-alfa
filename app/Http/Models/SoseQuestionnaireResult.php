<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class SoseQuestionnaireResult extends Model
{
    protected $table = 'sose_questionnaire_result';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    public static function tableName()
    {
        return 'sose_questionnaire_result';
    }
}
