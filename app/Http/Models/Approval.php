<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Approval extends Model
{
    protected $table = 'approvals';

    public static function tableName()
    {
        return 'approvals';
    }
}
