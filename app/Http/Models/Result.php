<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $table = 'results';

    public static function tableName()
    {
        return 'results';
    }
}
