<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class SoseStudiesOutputParameter extends Model
{
    protected $table = 'sose_studies_output_parameters';

    public static function tableName()
    {
        return 'sose_studies_output_parameters';
    }
}
