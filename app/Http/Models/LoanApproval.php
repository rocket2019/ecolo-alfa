<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class LoanApproval extends Model
{
    protected $table = 'loan_approval';

    public static function tableName()
    {
        return 'loan_approval';
    }
}
