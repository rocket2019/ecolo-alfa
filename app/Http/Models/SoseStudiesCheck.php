<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class SoseStudiesCheck extends Model
{
    protected $table = 'sose_studies_checks';

//    const UPDATED_AT = 'updated';
//    const CREATED_AT = 'created';

    public $timestamps = false;

    public static function tableName()
    {
        return 'sose_studies_checks';
    }

    protected $with = [
        'sosestudy',
        'sosecheckvalid',
        'sosecheck'
    ];

    public static function insert($sosestudy_id, $sosecheck_id)
    {
        $query = new self();
        $query->sosestudy_id = $sosestudy_id;
        $query->sosecheck_id = $sosecheck_id;
        if ($query->save()) {
            return $query;
        }
        return false;
    }

    public function sosecheck()
    {
        return $this->hasOne('App\Http\Models\SoseCheck', 'id', 'sosecheck_id');
    }

    public function sosestudy()
    {
        return $this->hasOne('App\Http\Models\SoseStudy', 'id', 'sosestudy_id');
    }

    public function sosecheckvalid()
    {
        return $this->hasOne('App\Http\Models\SoseCheckValidationMessage', 'sose_check_id', 'sosecheck_id');
    }
}
