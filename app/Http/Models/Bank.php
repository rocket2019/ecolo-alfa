<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 4/19/2019
 * Time: 6:50 PM
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $table = 'banks_list';

    public static function tableName()
    {
        return 'banks_list';
    }

    protected $with = [
        'user',
    ];

    public static function insert($request)
    {

        $company = new self();
        $company->bank_name = $request->input('bank_name');
        if ($company->save()) {
            return $company;
        }
    }

    public static function get()
    {
        $company = self::query()->with('user')->get();
        return $company;
    }


    public function user()
    {
        return $this->hasOne('App\Http\Models\User', 'id', 'user_id');
    }
}