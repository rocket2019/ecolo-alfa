<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class IndexesRatio extends Model
{
    protected $table = 'indexes_ratios';

    public static function tableName()
    {
        return 'indexes_ratios';
    }
}
