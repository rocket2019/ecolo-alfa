<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 4/2/2019
 * Time: 11:50 AM
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Role extends Model
{

    const ROLE_EVA_BACK_OFFICE = 'EvaBackoffice';
    const ROLE_BANK_MANAGER = 'BankManager';
    const ROLE_BANK_SUPERVISOR = 'Supervisor';
    const ROLE_EVA_ADMIN = 'Administrator';
    const ROLE_FINAL_CLIENT = 'Client';
    const ROLE_BANK_MINIMUM = 'BankMinimum';

    /**
     * @var string
     */
    protected $table = 'roles';

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'roles';
    }

    /**
     * @param $role
     * @return mixed
     */

    public static function get_all_roles()
    {
        if (App::getLocale() == 'en') {
            $roles = self::query()->select('id', 'name_en as name')->get();
        } elseif (App::getLocale() == 'it') {
            $roles = self::query()->select('id', 'name_it as name')->get();

        } else {
            $roles = self::query()->select('id', 'name_ru as name')->get();

        }
        return $roles;


    }


    public static function get_role_id_by_name($role)
    {
        $query = self::query()->where('name_en', $role)->first();
        return $query->id;
    }

    public static function get_role_name_by_id($id)
    {
        $query = self::query()->find($id);
        if ($query){
            if (App::getLocale() == 'en') {
                return $query->name_en;
            } elseif (App::getLocale() == 'it') {
                return $query->name_it;
            } else {
                return $query->name_ru;
            }
        }


    }

}