<?php

namespace App\Http\Models;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use App\Http\Services\Constants;
use App\Http\Services\Is;
use Illuminate\Support\Facades\Request;

class SoseCheck extends Model
{
    const SOSE_CODE_PATTERN = '/([A-Z]{1}[0-9]{5})/';

    protected $table = 'sose_check';

    public $timestamps = false;

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'sose_check';
    }

    /**
     * @return mixed
     */
    public function sosecheckvalid()
    {
        return $this->hasOne('App\Http\Models\SoseCheckValidationMessage', 'sose_check_id', 'id');
    }

    /**
     * @param null $data
     * @param $code
     * @param null $status_message
     * @return JsonResponse
     */
    protected function _response_body($data = null, $code = StatusCode::HTTP_OK, $status_message = null)
    {
        $status_message = (!empty($status_message))
            ? $status_message
            : 'HTTP response ' . $code;

        return response()->json([
            'statusMessage' => $status_message,
            'body' => $data
        ], $code ? $code : StatusCode::HTTP_OK);
    }

    /**
     * @param $sose_study_id
     * @return mixed
     */
    private static function getSoseStudiesParameters($sose_study_id)
    {
        return SoseStudiesParameter::query()
            ->where('sosestudy_id', $sose_study_id)
            ->pluck('soseparameter_id')
            ->toArray();
    }

    /**
     * @param $parameter_code
     * @param $sose_study_id
     * @return bool
     */
    private static function exceptionsForLike($parameter_code, $sose_study_id)
    {
        return (in_array($parameter_code, ['D04401', 'D03701']) && $sose_study_id == 241)
            || (in_array($parameter_code, ['B01501']) && $sose_study_id == 316)
            || (in_array($parameter_code, ['D02801']) && $sose_study_id == 303);
    }

    /**
     * @param $parameter_code
     * @param $sose_study_id
     * @return mixed
     */
    private static function allValidationRows($parameter_code, $sose_study_id)
    {
        return self::query()
            ->where('validation_formula', 'LIKE', $parameter_code . '%')
            ->where('sose_study_id', $sose_study_id)
            ->when(self::exceptionsForLike($parameter_code, $sose_study_id),
                function ($query) use ($parameter_code) {
                    return $query->where('pre_check_condition', 'LIKE', '%' . $parameter_code . '%');
                },
                function ($query) use ($parameter_code) {
                    return $query->where('pre_check_condition', 'NOT LIKE', '%' . $parameter_code . '%');
                }
            )->get();
    }

    /**
     * @param $fields
     * @return array
     */
    public static function validate($fields)
    {
        //Decode all codes
        $codes = json_decode($fields['codes'], true);
        $sose_study_id = $fields['sose_study_id'];
        $sose_parameter_ids = self::getSoseStudiesParameters($sose_study_id);
        $messages = [];
        $parameters = $fields['parameter_id'];

        foreach ($parameters as $key => $parameter) {
            $current_parameter_id = $key;
            $current_parameter_value = $parameter;
            $parameter_code = SoseParameter::find($current_parameter_id)->parameter_code;
            $all_validation_rows = self::allValidationRows($parameter_code, $sose_study_id);


                foreach ($all_validation_rows as $validation_rows) {
                        if (preg_match_all(self::SOSE_CODE_PATTERN, $validation_rows->pre_check_condition, $code) == 1) {
                            if (array_key_exists($code[0][0], $codes)) {
                                $values = SoseParameterValue::query()->where('parameter_id', '=', $codes[$code[0][0]])->where('client_id', $fields['client_id'])->get();
                                if (count($values)) {
                                    if ($values[0]->value == null) {
                                        continue;
                                    }
                                }

                            } else {
                                continue;
                            }
                        }

                        if (substr($validation_rows->validation_formula, 0, 6) === $parameter_code) {


                            $pre_check_validation = self::pre_check($current_parameter_value, $parameter_code, $validation_rows->pre_check_condition, $validation_rows->id, $parameters, $codes, $sose_parameter_ids, true, $sose_study_id);

                            if (is_bool($pre_check_validation)) {

                                if (!$pre_check_validation) {
                                    continue;
                                }

                                $validation_formula = self::pre_check($current_parameter_value, $parameter_code, $validation_rows->validation_formula, $validation_rows->id, $parameters, $codes, $sose_parameter_ids, false, $sose_study_id);

                                if (is_bool($validation_formula)) {
                                    continue;
                                } else {
                                    $messages[$parameter_code] = $validation_formula;
                                }

                            } else {
                                $messages[$parameter_code] = $pre_check_validation;

                            }

                        }

                }



        }

        return $messages;
    }

    /**
     * @param $condition_id
     * @param $sose_study_id
     * @return mixed
     */
    private static function soseCheckValidationMessage($condition_id, $sose_study_id)
    {
        return SoseCheckValidationMessage::query()
            ->where('sose_check_id', $condition_id)
            ->where('sose_study_id', $sose_study_id)
            ->first();
    }

    /**
     * @param $split_by_symbol
     * @param $sose_parameter_ids
     * @return mixed
     */
    private static function soseParameter($split_by_symbol, $sose_parameter_ids)
    {
        return SoseParameter::query()
            ->where('parameter_code', $split_by_symbol)
            ->whereIn('id', $sose_parameter_ids)
            ->first();
    }

    /**
     * @param $field
     * @param $code
     * @param $condition
     * @param $condition_id
     * @param $all_fields
     * @param $codes
     * @param $sose_parameter_ids
     * @param $is_pre_check
     * @param $sose_study_id
     * @return bool|string
     */
    public static function pre_check($field, $code, $condition, $condition_id, $all_fields, $codes, $sose_parameter_ids, $is_pre_check, $sose_study_id)
    {
            if ($condition === 'TRUE') {
                return true;
            }

            if ($condition) {
                $explode = explode(" ", $condition);

                //One condition
                if (count($explode) == 1 && self::count_symbols($explode[0])) {

                    $split_by_symbol = self::split_by_symbol($explode[0]);


                    // If pre check validation is true
                    if ($split_by_symbol[1] == 'TRUE') {

                        $str = eval('return ' . $field . ' == true;');
                        if ($str) {
                            return $is_pre_check ? true : false;

                        } else {
                            return self::soseCheckValidationMessage($condition_id, $sose_study_id)->russian;
                        }
                    }

                    //If pre check validation contains logical comparision
                    $parameter_id = self::soseParameter($split_by_symbol[0], $sose_parameter_ids);

                    if ($parameter_id) {
                        try {
                            if (isset($all_fields[$parameter_id->id])) {

                                $p_id = (strlen($all_fields[$parameter_id->id]) == 0) ? 'NULL' : $all_fields[$parameter_id->id];
                            } else {
                                return true;
                            }

                        } catch (\Exception $ex) {
                            return $ex->getMessage();
                        }

                    } else {
                        return self::soseCheckValidationMessage($condition_id, $sose_study_id)->russian;
                    }


                    try {
                        $new_conditions = [];
                        preg_match_all(self::SOSE_CODE_PATTERN, $split_by_symbol[1], $new_conditions);


                        foreach ($new_conditions[0] as $key => $value) {
                            $split_by_symbol[1] = str_replace($value, isset($codes[$value]) && isset($all_fields[$codes[$value]]) && !empty($all_fields[$codes[$value]]) ? $all_fields[$codes[$value]] : 'NULL', $split_by_symbol[1]);
                        }

                        $str = eval('return ' . $p_id . $split_by_symbol[2] . $split_by_symbol[1] . ';');


                    } catch (\Exception $ex) {
                        return $ex->getMessage();
                    }

                    if ($str) {
                        return true;
                    }

                    if (!$str && $is_pre_check) {
                        return false;
                    }

                    return SoseCheckValidationMessage::query()->where('sose_check_id', $condition_id)->first()->russian;
                }

                //If pre check validation contains multiple comparision
                $new = [];
                preg_match_all(self::SOSE_CODE_PATTERN, $condition, $new);
                if (isset($new)) {
                    foreach ($new[0] as $item) {
                        $condition = str_replace($item, isset($codes[$item]) && isset($all_fields[$codes[$item]]) && !empty($all_fields[$codes[$item]]) ? $all_fields[$codes[$item]] : 'NULL', $condition);
                    }
                }

                try {
                    $eval_string = eval('return ' . $condition . ';');
                    if ($eval_string) {
                        return true;
                    }

                    if ($is_pre_check) {
                        return false;
                    }

                } catch (\Exception $ex) {
                    return $ex->getMessage();
                }

                //Todo must check for translations
                return SoseCheckValidationMessage::query()->where('sose_check_id', $condition_id)->first()->russian;
            }

    }

    /**
     * @return string[]
     */
    public static function validation_symbols()
    {
        return ['!=', '==', '||', '&&', '<=', '=>', '+'];
    }

    /**
     * @param $string
     * @return array
     */
    public static function split_by_symbol($string)
    {
        $symbols = self::validation_symbols();
        foreach ($symbols as $key => $symbol) {
            if (strpos($string, $symbol) !== false) {
                return array_merge(explode($symbol, $string), [$symbol]);
            }
        }
    }

    /**
     * @param string $string
     * @return false|int
     */
    public static function count_symbols(string $string)
    {
        $symbols = self::validation_symbols();
        $count = 0;
        foreach ($symbols as $key => $symbol) {
            if (strpos($string, $symbol) !== false) {
                $count++;
                if ($count > 1) {
                    return false;
                }
            }
        }
        return $count;
    }

    /**
     * @param $pre_check_condition
     * @param $validation_formula
     * @param $user_id
     * @param $study_id
     * @return SoseCheck|false
     */
    public static function insert($pre_check_condition, $validation_formula, $user_id, $study_id)
    {
        $query = new self();
        $query->validation_formula = $validation_formula;
        $query->pre_check_condition = $pre_check_condition;
        $query->user_id = $user_id;
        $query->sose_study_id = $study_id;
        if ($query->save()) {
            return $query;
        }
        return false;
    }

    /**
     * @param $pre_check_condition
     * @param $validation_formula
     * @param $user_id
     * @param $check_id
     * @return false
     */
    public static function update_check($pre_check_condition, $validation_formula, $user_id, $check_id)
    {
        $query = self::query()->find($check_id);
        if ($query) {
            $query->validation_formula = $validation_formula;
            $query->pre_check_condition = $pre_check_condition;
            $query->user_id = $user_id;
            if ($query->save()) {
                return $query;
            }
        }
        return false;
    }

}
