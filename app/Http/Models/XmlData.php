<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class XmlData extends Model
{
    protected $fillable = [
        'questionnaire_id',
        'client_id',
        'coherence',
        'normality',
        'revenue',
        'profit'
    ];
}
