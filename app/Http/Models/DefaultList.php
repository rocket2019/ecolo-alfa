<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class DefaultList extends Model
{
    protected $table = 'default_lists';

    public static function tableName()
    {
        return 'default_list';
    }
}
