<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class SoseStudiesParameter extends Model
{
    protected $table = 'sose_studies_parameters';

    public $timestamps = false;

    public static function tableName()
    {
        return 'sose_studies_parameters';
    }
    public static function get_by_study_id($id)
    {
        $query = self::query()
            ->where('sosestudy_id',$id)
            ->with('sose_param')
            ->join('sose_parameter','sose_parameter.id','=','sose_studies_parameters.soseparameter_id')
            ->orderBy('sose_parameter.parameter_code')
            ->get();
        return $query;

    }
    public static function get_by_study_id_as_array($id)
    {
        $query = self::query()->where('sosestudy_id',$id)->with('sose_param')->orderBy('id', 'DESC')->get()->toArray();
        return $query;

    }
    public function sose_param()
    {
        return $this->hasOne('App\Http\Models\SoseParameter', 'id', 'soseparameter_id');
    }

    public function sose_parameter_message()
    {
        return $this->hasOne('App\Http\Models\SoseParameterMessage', 'sose_parameter_id', 'soseparameter_id');
    }
}
