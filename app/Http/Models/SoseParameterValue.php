<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class SoseParameterValue extends Model
{
    protected $table = 'sose_parameter_value';
    const UPDATED_AT = 'updated';
    const CREATED_AT = 'created';

    public static function tableName()
    {
        return 'sose_parameter_value';
    }

    public static function insert($parameter_id, $value, $company_id, $questionnaire_id, $client_id = null)
    {
        $self = self::query()
            ->where('parameter_id', $parameter_id)
            ->where('id_company', $company_id)
            ->where('sose_questionnaire_id', $questionnaire_id)
            ->first();
        if (!$self) {
            $self = new self();

        }
        $self->parameter_id = $parameter_id;
        $self->value = $value;
        $self->id_company = $company_id;
        $self->sose_questionnaire_id = $questionnaire_id;
        $self->user_id = Auth::id();
        $self->client_id = $client_id;

        if ($self->save()) {
            return $self;
        }
        return false;

    }

    public static function getQuestionnaireResults($questionnaire_id)
    {
        $query = self::query()
            ->where('sose_questionnaire_id',$questionnaire_id)
            ->with('param')
            ->get();

        return $query;
    }

    public function param()
    {
        return $this->hasOne('App\Http\Models\SoseParameter','id','parameter_id');
    }
}
