<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use PDO;

class SubdomainMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

//        $database = [];
//        $database_name = 'ecolo_' . $request->route()->subdomain;
//
//        $database['host'] = 'localhost';
//        $database['database'] = $database_name;
//        $database['username'] = 'root';
//        $database['password'] = 'password123456789';
//
//        if (DB::connection()->getDatabaseName() != $database_name) {
//
//
//            Artisan::call('make:database ' . $database_name . ' mysql');
//
//
//
//
//
//
////        $account_data = Account::where( 'subdomain', $subdomain )->where( 'status', 1 )->first();
////
////        if( ! $account_data ) {
////            // account not found, do something else
////        }
////
////            // Account found and is active, Continue processing...
////
////            // Get the database config details from wherever and however you want,
////            // Could be stored in a database (as JSON), separate file, or stored in an array here
////
////            // And then do something like this
////
//
//
//
//        }else{
//            DB::disconnect('mysql');
//            Config::set('database.connections.mysql.host', $database['host']);
//            Config::set('database.connections.mysql.database', $database['database']);
//            Config::set('database.connections.mysql.username', $database['username']);
//            Config::set('database.connections.mysql.password', $database['password']);
//
//            DB::reconnect();
//
//        }
        return $next($request);
    }
}
