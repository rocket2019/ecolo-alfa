<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 3/29/2019
 * Time: 5:40 PM
 */

namespace App\Http\Middleware;


use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class CheckAuth
{

    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            Auth::logout();
            return Redirect::to('/'.App::getLocale().'/login');
        }

        return $next($request);
    }
}