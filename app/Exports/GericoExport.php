<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class GericoExport implements FromView,WithTitle
{
    protected $valuesMap;
    protected $study_code;
    protected $companyIdStr;
    protected $rowsNum;
    protected $atecoCode;
    protected $dynamicHeader;
    protected $month;

    public function __construct($valuesMap,$study_code,$companyIdStr,$rowsNum,$atecoCode,$dynamicHeader,$month)
    {
        $this->valuesMap = $valuesMap;
        $this->study_code = $study_code;
        $this->companyIdStr = $companyIdStr;
        $this->rowsNum = $rowsNum;
        $this->atecoCode = $atecoCode;
        $this->dynamicHeader = $dynamicHeader;
        $this->month = $month;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
       return view('gerico.showTelematic',['valuesMap' =>$this->valuesMap,
           'study_code' => $this->study_code,
           'companyIdStr' => $this->companyIdStr,
           'rowsNum' => $this->rowsNum,
           'atecoCode' => $this->atecoCode,
           'dynamicHeader' => $this->dynamicHeader]);
    }

    public function title(): string
    {
        return $this->month;
    }
}
