<?php


namespace App\Exports;

use App\Http\Models\Coefficient;
use App\Http\Models\CompanyList;
use App\Http\Models\GeographicalAreas;
use App\Http\Models\Questionnaire;
use App\Http\Models\SoseAteco;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;


class GericoClient implements FromView, WithTitle
{

    protected $study_code;
    protected $companyIdStr;
    protected $atecoCode;
    protected $questionnaireId;

    public function __construct($study_code, $companyIdStr, $atecoCode, $questionnaireId)
    {
        $this->study_code = $study_code;
        $this->companyIdStr = $companyIdStr;
        $this->atecoCode = $atecoCode;
        $this->questionnaireId = $questionnaireId;
    }

    /**
     * @return View
     */
    public function view(): View
    {
        $company = CompanyList::query()->where('ID_company',$this->companyIdStr)->first();
        $questionnaire = Questionnaire::query()->find($this->questionnaireId);
        $ateco = SoseAteco::query()->where('ateco_code', $this->atecoCode)->first();
        $region = GeographicalAreas::query()->where('area_code', $company->region_code)->first();
        $ateco_description = explode("[", $ateco->description);
        $ateco_description = $ateco_description[0];
        if ($region) {

            $code = $region->area_name;
            $coeficent = Coefficient::query()->where('region_code', $region->area_code)->where('industry_group_code',$this->study_code)->first();
            if ($coeficent) {
                $coef = $coeficent->micro;
            }
            else{
                $coef = null;
            }
        } else {
            $code = null;
            $coef = null;
        }
        return view('gerico.client', ['company' => $company,
            'study_code' => $this->study_code,
            'ateco' => $ateco,
            'description' => $ateco_description,
            'region_code' => $code,
            'coef' => $coef,
            'questionnaire' => $questionnaire]);
    }

    public function title(): string
    {
        return 'Client';
    }


}
