<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class oldDataExport implements FromView,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $industry_group_code;
    protected $region_name;
    protected $gerico;
    public function __construct($industry_group_code, $region_name, $gerico)
    {
        $this->industry_group_code = $industry_group_code;
        $this->region_name = $region_name;
        $this->gerico = $gerico;
    }

    public function view(): View{
        return view('gerico.exportExcel', [
            'group_code' => $this->industry_group_code,
            'region_name' => $this->region_name,
            'gerico' => $this->gerico
        ]);
    }

//    public function collection()
//    {
//        return GeographicalAreas::query()
//            ->select('area_code', 'area_name', 'micro', 'small')
//            ->join('coefficient_table', 'area_code', 'coefficient_table.region_code')
//            ->where('bankCode', 1)
//            ->limit(2000)
//            ->get();
//
//    }
//    public function headings(): array{
//        $headingsArr = ['ID', 'Region Name'];
//        $headingsData = Coefficient::query()
//            ->select('industry_group_code')
//            ->limit(180)
//            ->get();
//        $headingsDataVal = array();
//        foreach ($headingsData as $key=>$value){
//            array_push($headingsDataVal, $value->industry_group_code, '');
//        }
//        $headingsArr = array_merge($headingsArr, $headingsDataVal);
//        $headingRowTwo = ['', ''];
//        for($count = 0; $count < 180; $count++){
//            if($count%2 == 0){
//                array_push($headingRowTwo, 'Without Gerico 2018');
//            }
//            else{
//                array_push($headingRowTwo, 'With Gerico 2018');
//            }
//        }
//        return [$headingsArr, $headingRowTwo, ['hello']];
//    }
//    public function startRow(): int{
//        return 3;
//    }

}
