<?php

namespace App\Exports;

use App\Http\Models\Questionnaire;
use App\Http\Models\SoseOutputParameter;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class RawResult implements FromView,WithTitle
{

    protected $rawResult;

    public function __construct($rawResult)
    {

        $this->rawResult = $rawResult;
    }

    public function view(): View
    {
        return view('gerico.raw_results', [

            'raws' => $this->rawResult]);
    }

    public function title(): string
    {
        return 'Raw Results';
    }
}
