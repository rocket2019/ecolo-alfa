<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\GericoExport;

class GericoSheets implements WithMultipleSheets
{
    use Exportable;

    protected $study_code;
    protected $companyIdStr;
    protected $atecoCode;
    protected $questionnaireId;
    protected $params;
    protected $values;
    protected $raw_results;

    public function __construct($study_code,$companyIdStr,$atecoCode,$questionnaireId,$params,$values,$raw_results)
    {
        $this->study_code = $study_code;
        $this->companyIdStr = $companyIdStr;
        $this->atecoCode = $atecoCode;
        $this->questionnaireId = $questionnaireId;
        $this->params = $params;
        $this->values = $values;
        $this->raw_results = $raw_results;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    /**
     * @return array
     */
    public function sheets(): array
    {

        $sheets = [];
        $sheets['Client'] = new GericoClient($this->study_code, $this->companyIdStr, $this->atecoCode, $this->questionnaireId);
        $sheets['Input data'] = new GericoInput($this->params,$this->values);
        $sheets['Who work'] = new GericoWhoWork($this->questionnaireId);
        $sheets['Raw Results'] = new RawResult($this->raw_results);
        $sheets['Coefficients'] = new Coefficients(true);

        return $sheets;
    }
}
