<?php

namespace App\Exports;
use App\Http\Models\Questionnaire;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class GericoWhoWork implements FromView,WithTitle
{

    protected $questionnaireId;

    public function __construct($questionnaireId)
    {

        $this->questionnaireId = $questionnaireId;
    }
    public function view(): View
    {
        $questionnaire = Questionnaire::query()->find($this->questionnaireId);

        return view('gerico.whoWork',[
            'questionnaire' =>$questionnaire]);
    }

    public function title(): string
    {
        return 'Who works';
    }

}
