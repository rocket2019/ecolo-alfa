<?php


namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class GericoInput implements FromView,WithTitle
{
    protected $params;
    protected $values;


    public function __construct($params,$values)
    {
        $this->params = $params;
        $this->values = $values;

    }

    public function view(): View
    {

        return view('gerico.inputData',['params'=>$this->params,'values'=>$this->values]);
    }

    public function title(): string
    {
        return 'Input Data';
    }

}
