<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithTitle;

class Coefficients implements FromView,WithTitle,WithCalculatedFormulas
{
    public function __construct($questionnaireId)
    {

    }
    public function view(): View
    {

        return view('gerico.coefficients');
    }

    public function title(): string
    {
        return 'Coefficients';
    }


}
