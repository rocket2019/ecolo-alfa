<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Route;

const GET = 'get';
const POST = 'post';
const PUT = 'put';
const PATCH = 'patch';
const DELETE = 'delete';
Route::get('/', function () {
    return redirect('ru/login');
});

//Route::get('test-users','ProfileController@usersImportTest');

Route::get('/{lang}/excel/{company_id}/{questionnaire_id}','GericoController@excel');
Route::get('/{lang}/client/generate-report','ClientController@generateReportFromExcelToPdf');

Route::post('/download-new-instruction','ProfileController@downloadNewInstruction')->name('downloadNewInstruction');

Route::group(['domain' => '{subdomain}.' . env('PAST_URL_NAME'), 'middleware' => 'subdomain'], function () {
        Route::prefix('{lang}')->group(function () {

            Route::prefix('client')->group(function (){
                Route::post('/send-email-link','ClientController@send_email_link');
                Route::get('/preview/{ateco_id}','ClientController@view_questionnaire');
                Route::get('/print/{client_id}/{questionnaire_id}/{ateco_id}','ClientController@print');
                Route::get('/product-codes/{ateco_id}','ClientController@product_codes');
                Route::get('/questionnaire/view/{client_id}/{questionnaire_id}', 'ClientController@fill_questionnaire');
                Route::post('/questionnaire/view/get-role', 'ClientController@get_user_role');
                Route::post('/fill-questionnaire/{client_id}/questionnaire/{questionnaire_id}', 'ClientController@fill_questionnaire');
                Route::post('/edit-questionnaire/{client_id}/questionnaire/{questionnaire_id}', 'ClientController@edit_company_list');
                Route::post('/fill-questionnaire/{client_id}/questionnaire/{questionnaire_id}/send-mail', 'ClientController@send_mail_to_admin');
                Route::post('/delete-questionnaire', 'ClientController@delete_questionnaire');
                Route::post('/get-area-city', 'ClientController@get_area_city');
            });
            Route::post('/company/fill-questionnaire-value/{company_id}/questionnaire/{questionnaire_id}', 'CompanyController@insert_questionnaire_value');

            Route::get('/seed-test','CompanyController@seed');

            Route::match([GET, POST], '/login', 'Auth\AuthController@login')->name('login');
//            Route::match([GET, POST], '/register', 'Auth\AuthController@register')->name('register');
            Route::match([GET, POST], '/confirm-register', 'Auth\AuthController@register_confirm');
            Route::match([GET, POST], '/reset', 'Auth\AuthController@reset_password');
            Route::match([GET, POST], '/verify_token/{verify_token}', 'Auth\AuthController@verify');
            Route::match([GET, POST], '/reset_token/{reset_token}', 'Auth\AuthController@update_password');
            Route::match([GET, POST], '/update-password/{reset_token}', 'Auth\AuthController@update_password');
            Route::group(['middleware' => 'check.auth'], function () {

                Route::get('/manager','ApplicationController@index');
                Route::match(['get','post'],'/client/{token}/register','ClientController@register');


                Route::get('/account', 'ProfileController@account');
                Route::post('/account/new-to-old/{id}', 'ProfileController@new_to_old');
                Route::get('/logout', 'ProfileController@logout');
                Route::prefix('/company')->group(function () {
                    Route::get('/viewPdf', 'CompanyController@viewPdf');
                    Route::get('/viewPdf2', 'CompanyController@viewPdf2');
                    Route::get('/search-company', 'CompanyController@search_company');
                    Route::get('/runEvaluation/{company_id}/{questionnaire_id}','GericoController@runEvaluation');
                    Route::get('/gerico-txt/{company_id}/{questionnaire_id}','GericoController@gerico_txt');
                    Route::get('/export-excel-file/{company_id}/{questionnaire_id}/{ateco_code}/{study_code}','GericoController@exportExcel');
                    Route::get('/to-bank/{questionnaire_id}','CompanyController@to_bank');

                    Route::match([GET, POST], '/new', 'CompanyController@new_company');
                    Route::get('/view/{company_id}', 'CompanyController@view_company');
                    Route::get('/delete/{id}', 'CompanyController@delete_company');
                    Route::get('/delete-questionnaire/{id}', 'CompanyController@delete_questionnaire');
                    Route::match([GET, POST], '/edit/{company_id}', 'CompanyController@edit_company');
                    Route::match([GET, POST], '/comment/{company_id}', 'CompanyController@comment');
                    Route::match([GET, POST], '/new-questionnaire', 'CompanyController@new_questionnaire');
                    Route::match([GET, POST], '/fill-questionnaire/{company_id}/questionnaire/{questionnaire_id}', 'CompanyController@fill_questionnaire');
                    Route::match([GET, POST], '/get-ateco', 'CompanyController@get_ateco');

                    Route::match([GET, POST], '/edit/{company_id}/{questionnaire_id}', 'CompanyController@edit_questionnaire');
                    Route::post('/edit/{company_id}/{questionnaire_id}/delete', 'CompanyController@delete_report');
                    Route::post('/edit/{company_id}/{questionnaire_id}/upload', 'CompanyController@upload_file');
                    Route::get('/print/{company_id}/{questionnaire_id}/{ateco_id}', 'CompanyController@print_filled_questionnaire');
                    Route::get('/report/{company_id}/{questionnaire_id}/{ateco_id}', 'CompanyController@report_filled_questionnaire');
                    Route::get('/send-to-evaluation/{questionnaire_id}', 'CompanyController@send_to_evaluation');

                    Route::match([GET, POST], '/print-questionnaire', 'CompanyController@print_questionnaire');
                    Route::get('/generate-pdf/{ateco_id}/{company_unit}', 'CompanyController@GeneratePDF');
                    Route::match([GET, POST], '/edit-parameter/{parameter_id}', 'CompanyController@edit_parameter');
                    Route::post('/get-area-city', 'CompanyController@get_area_city');
                    Route::match([GET, POST],'/import-pdf', 'CompanyController@import_pdf_view');
                    Route::get('/get-company-region', 'CompanyController@get_company_region');

                });


                Route::prefix('/admin')->group(function () {
                    Route::get('/user-management', 'ProfileController@user_management');
                    Route::match([GET, POST], '/add-new-user', 'ProfileController@add_new_user');
                    Route::get('/delete-user/{id}', 'ProfileController@delete_user');
                    Route::match([GET, POST], '/edit-user/{user_id}', 'ProfileController@edit_user');
                    Route::match([GET, POST], '/new-bank', 'ProfileController@new_bank');
                    Route::get('/banks-list', 'ProfileController@banks_list');
                    Route::get('/delete-bank/{id}', 'ProfileController@delete_bank');

                    Route::get('/study-checks/{id}', 'ProfileController@study_checks');
                    Route::get('/delete-study-check/{study_id}/{check_id}', 'ProfileController@delete_study_check');

                    Route::match([GET, POST], '/check', 'ProfileController@checks');
                    Route::match([GET, POST], '/add-study-check/{study_id}', 'ProfileController@add_study');
                    Route::match([GET, POST], '/{study_id}/edit-study-check/{check_id}', 'ProfileController@edit_study');
                    Route::match([GET, POST], '/bank-settings', 'ProfileController@bank_settings');
                    Route::post('/add-logo', 'ProfileController@add_logo');
                    Route::get('/delete-bank-logo', 'ProfileController@delete_logo');
                    Route::get('/delete-bank-number', 'ProfileController@delete_phone_number');

                    Route::get('/action-tools', 'ProfileController@action_tools');


                    Route::match([GET, POST], '/import-excel', 'ProfileController@import_excel');
                    Route::get('/import-excel/download', 'ProfileController@download_data');
                    Route::get('/import-excel/get-data', 'ProfileController@get_data')->name('getdata');
                    Route::get('/import-excel/create_excel', 'ProfileController@create_excel');
                    Route::match([GET, POST], '/import-excel-for-update', 'ProfileController@import_excel_update');
                    Route::get('/visible/{id}', 'CompanyController@visible');
                    Route::post('/add-or-edit-globals', 'ProfileController@add_or_edit_globals');

                });
            });
        });



    });

Route::get('/application', 'ApplicationController@index');
Route::get('/mail', 'ApplicationController@mail');
Route::get('/applications', 'AllApplicationsController@index');
Route::get('/blockA', 'BlockAController@index');
Route::get('/blockD', 'BlockDController@index');
Route::get('/entry', 'EntryFormController@index');


