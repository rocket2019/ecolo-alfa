<?php

use App\Http\Models\SoseStudy;
use App\Http\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersScraperSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ini_set('max_execution_time', 0);
        $live_db = DB::connection('ecolo_server');

        $table = $live_db->table('platform_user');
        $live_data = $table->get();



        foreach ($live_data as $user) {
            $exists = User::query()
                ->where('email',$user->email)
                ->orWhere('id',$user->id)
                ->exists();
            if($exists)
            {
                continue;
            }

            $member = new User();
            $member->id = $user->id;
            $member->username = $user->username;
            $member->username_canonical = $user->username_canonical;
            $member->email = $user->email;
            $member->email_canonical = $user->email_canonical;
            $member->enabled = $user->enabled;
            $member->salt = $user->salt;
            $member->password = $user->password;
            $member->last_login = $user->last_login;
            $member->locked = $user->locked;
            $member->expired = $user->expired;
            $member->expires_at = $user->expires_at;
            $member->credentials_expired = $user->credentials_expired;
            $member->credentials_expire_at = $user->credentials_expire_at;
            $member->created_at = $user->created;
            $member->updated_at = $user->updated;

            $member->save();
            echo $member->id.'<br>';


        }
    }
}
