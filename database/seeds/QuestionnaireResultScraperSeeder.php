<?php

use App\Http\Models\SoseQuestionnaireResult;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QuestionnaireResultScraperSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ini_set('max_execution_time', 0);
        $live_db = DB::connection('ecolo_server');

        $table = $live_db->table('sose_questionnaire_result');
        $live_data = $table->get();



        foreach ($live_data as $result) {
            $exists = SoseQuestionnaireResult::query()
                ->orWhere('id',$result->id)
                ->exists();
            if($exists)
            {
                continue;
            }

            $res = new SoseQuestionnaireResult();
            $res->id = $result->id;
            $res->sose_questionnaire_id = $result->sose_questionnaire_id;
            $res->regional_revenue_coefficient = $result->regional_revenue_coefficient;
            $res->user_id = $result->user_id;
            $res->created = $result->created;
            $res->updated = $result->updated;

            $res->save();
            echo $res->id.'<br>';



        }
    }
}
