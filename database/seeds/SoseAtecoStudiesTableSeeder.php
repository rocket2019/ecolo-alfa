<?php

use App\Http\Models\SoseAtecoStudiesEquivalence;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 7/11/2019
 * Time: 1:38 PM
 */
class SoseAtecoStudiesTableSeeder extends Seeder
{
    /**
     *
     */
    public function run()
    {
        ini_set('max_execution_time', 0);
        $live_db = DB::connection('ecolo_example');
        $table = $live_db->table('sose_ateco_studies_equivalence');
        $live_data = $table->get();

//        DB::table('sose_ateco_studies_equivalence')->truncate();


        foreach ($live_data as $member) {

            DB::statement('SET FOREIGN_KEY_CHECKS = 0');
            $check = new SoseAtecoStudiesEquivalence();
            $check->soseateco_id = $member->soseateco_id;
            $check->sosestudy_id = $member->sosestudy_id;
            $check->save();
            DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        }

    }


}