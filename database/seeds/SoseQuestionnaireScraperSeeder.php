<?php

use App\Http\Models\Questionnaire;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SoseQuestionnaireScraperSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ini_set('max_execution_time', 0);
        $live_db = DB::connection('ecolo_server');

        $table = $live_db->table('sose_questionnaire');
        $live_data = $table->get();

        foreach ($live_data as $questionnaire) {
            $exists = Questionnaire::query()
                ->where('id',$questionnaire->id)
                ->exists();
            if($exists)
            {
                continue;
            }

            $new_questionnaire = new Questionnaire();
            $new_questionnaire->id = $questionnaire->id;
            $new_questionnaire->id_company = $questionnaire->id_company;
            $new_questionnaire->sose_study_id = $questionnaire->sose_study_id;
            $new_questionnaire->user_id = $questionnaire->user_id;
            $new_questionnaire->balance_year = $questionnaire->balance_year;
            $new_questionnaire->valid = $questionnaire->valid;
            $new_questionnaire->created = $questionnaire->created;
            $new_questionnaire->updated = $questionnaire->updated;
            $new_questionnaire->sose_ateco_id = $questionnaire->sose_ateco_id;
            $new_questionnaire->sose_okved_id = $questionnaire->sose_okved_id;
            $new_questionnaire->sose_questionnaire_status_id = $questionnaire->sose_questionnaire_status_id;
            $new_questionnaire->updated_status_user_id = $questionnaire->updated_status_user_id;
            $new_questionnaire->updated_status = $questionnaire->updated_status;
            $new_questionnaire->backup = $questionnaire->backup;


            $new_questionnaire->save();

            echo $new_questionnaire->id.'<br>';
        }
    }
}
