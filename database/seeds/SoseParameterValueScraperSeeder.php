<?php

use App\Http\Models\SoseParameterValue;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SoseParameterValueScraperSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ini_set('max_execution_time', 0);
        $live_db = DB::connection('ecolo_server');

        $table = $live_db->table('sose_parameter_value');
        $live_data = $table->get();

        foreach ($live_data as $parameter) {
            $exists = SoseParameterValue::query()
                ->where('id',$parameter->id)
                ->exists();
            if($exists)
            {
                continue;
            }

            $param = new SoseParameterValue();
            $param->id = $parameter->id;
            $param->parameter_id = $parameter->parameter_id;
            $param->id_company = $parameter->id_company;
            $param->balance_year = $parameter->balance_year;
            $param->value = $parameter->value;
            $param->user_id = $parameter->user_id;
            $param->created = $parameter->created;
            $param->updated = $parameter->updated;
            $param->sose_questionnaire_id = $parameter->sose_questionnaire_id;

            $param->save();

            echo $param->id.'<br>';

        }
    }
}
