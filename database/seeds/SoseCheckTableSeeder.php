<?php

use App\Http\Models\SoseCheck;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SoseCheckTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ini_set('max_execution_time', 0);
        $live_db = DB::connection('ecolo_example');
        $table = $live_db->table('sose_check');
        $live_data = $table->get();
//        DB::table('sose_check')->truncate();

        foreach ($live_data as $member) {

            $check = new SoseCheck();
            $check->id = $member->id;
            $check->user_id = $member->user_id;
            $check->pre_check_condition = $member->pre_check_condition;
            $check->validation_formula = $member->validation_formula;
            $check->save();
        }

    }


}
