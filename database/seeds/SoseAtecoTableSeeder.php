<?php

use App\Http\Models\SoseAteco;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 7/11/2019
 * Time: 1:27 PM
 */

class SoseAtecoTableSeeder extends Seeder
{

    public function run()
    {
        ini_set('max_execution_time', 0);
        $live_db = DB::connection('ecolo_example');
        $table = $live_db->table('sose_ateco');
        $live_data = $table->get();
//        DB::table('sose_ateco')->truncate();


        foreach ($live_data as $member) {


            $check = new SoseAteco();
            $check->id = $member->id;
            $check->ateco_code = $member->ateco_code;
            $check->description_it = $member->description_it;
            $check->description_ru = $member->description_ru;
            $check->description_en = $member->description_en;
            $check->industry_group_code = $member->industry_group_code;
            $check->save();
        }

    }

}