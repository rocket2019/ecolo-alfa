<?php

use App\Http\Models\SoseStudiesOutputParameter;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SoseOutputStudySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ini_set('max_execution_time', 0);
        $live_db = DB::connection('ecolo_example');
        $table = $live_db->table('sose_studies_output_parameters');
        $live_data = $table->get();
//        DB::table('sose_studies_output_parameters')->truncate();



        foreach ($live_data as $member) {
            DB::statement('SET FOREIGN_KEY_CHECKS = 0');


            $check = new SoseStudiesOutputParameter();
            $check->sosestudy_id = $member->sosestudy_id;
            $check->soseoutputparameter_id = $member->soseoutputparameter_id;
            $check->save();
            DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        }

    }
}
