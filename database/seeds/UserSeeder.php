<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [ 'admin','admin@mail.com','1'] ,
            [ 'ecolomanager','ecoloemanager@mail.com','2'] ,
            [ 'supervisor','supervisor@mail.com','3'] ,
            [ 'bankmanager','bankmanager@mail.com','4'] ,
            [ 'client','client@mail.com','5'] ,
        ];


//        DB::table('users')->truncate();

        for ($i = 0; $i < count($roles); $i++) {
            DB::table('users')->insert(
                [
                    'username' => $roles[$i][0],
                    'email' => $roles[$i][1],
                    'role_id' => $roles[$i][2],
                    'password' =>bcrypt(123456),
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ]
            );
        }
    }
}
