<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesSeeder extends Seeder
{

    public function run()
    {
        $roles = [
          [ 'Administrator','Amministratore','Администратор'] ,
          [ 'EvaBackoffice','EvaBackoffice','ЕваБэкофис'] ,
          [ 'Supervisor','Supervisore','Супервайзор'] ,
          [ 'BankManager','ManagerDiBanca','МенеджерБанка'] ,
          [ 'Client','Cliente','Клиент'] ,
          [ 'BankMinimum','MinimoBanca','БанкМинимум'] ,

        ];

//        DB::table('roles')->truncate();

        for ($i = 0; $i < count($roles); $i++) {
            DB::table('roles')->insert(
                [
                    'name_en' => $roles[$i][0],
                    'name_it' => $roles[$i][1],
                    'name_ru' => $roles[$i][2],
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ]
            );
        }
    }
}
