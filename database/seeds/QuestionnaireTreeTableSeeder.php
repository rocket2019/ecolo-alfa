<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 7/11/2019
 * Time: 3:18 PM
 */

class QuestionnaireTreeTableSeeder extends Seeder
{


    public function run()
    {
        ini_set('max_execution_time', 0);
        $live_db = DB::connection('ecolo_example');
        $table = $live_db->table('questionnaire_tree');
        $live_data = $table->get();
//        DB::table('questionnaire_tree')->truncate();


        foreach ($live_data as $member) {
            DB::statement('SET FOREIGN_KEY_CHECKS = 0');


            $check = new \App\Http\Models\QuestionnaireTree();
            $check->id = $member->id;
            $check->sose_study_id = $member->sose_study_id;
            $check->sose_ateco_id = $member->sose_ateco_id;
            $check->sose_okved_id = $member->sose_okved_id;
            $check->code = $member->code;
            $check->sort_order = $member->sort_order;
            $check->save();
            DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        }

    }
}