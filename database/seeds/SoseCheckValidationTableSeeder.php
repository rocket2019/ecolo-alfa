<?php

use App\Http\Models\SoseCheckValidationMessage;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SoseCheckValidationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ini_set('max_execution_time', 0);
        $live_db = DB::connection('ecolo_example');
        $table = $live_db->table('sose_check_validation_message');
        $live_data = $table->get();

//        DB::table('sose_check_validation_message')->truncate();

        foreach ($live_data as $member) {
            DB::statement('SET FOREIGN_KEY_CHECKS = 0');
            $check = new SoseCheckValidationMessage();
            $check->id = $member->id;
            $check->user_id = $member->user_id;
            $check->sose_check_id = $member->sose_check_id;
            $check->message_en = $member->english;
            $check->message_ru = $member->russian;
            $check->message_it = $member->italian;
            $check->save();
            DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        }

    }
}
