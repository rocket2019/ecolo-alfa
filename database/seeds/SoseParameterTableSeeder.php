<?php

use App\Http\Models\SoseParameter;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 7/11/2019
 * Time: 1:40 PM
 */

class SoseParameterTableSeeder extends  Seeder
{
    /**
     *
     */
    public function run()
    {
        ini_set('max_execution_time', 0);
        $live_db = DB::connection('ecolo_example');
        $table = $live_db->table('sose_parameter');
        $live_data = $table->get();

//        DB::table('sose_parameter')->truncate();

        foreach ($live_data as $member) {


            $check = new SoseParameter();
            $check->id = $member->id;
            $check->parameter_code = $member->parameter_code;
            $check->description_en = $member->description_en;
            $check->description_it = $member->description_it;
            $check->description_ru = $member->description_ru;
            $check->units = $member->units;
            $check->units_en = $member->units_en;
            $check->units_ru = $member->units_ru;
            $check->units_it = $member->units_it;
            $check->value_type = $member->value_type;
            $check->path_en = $member->path_en;
            $check->path_it = $member->path_it;
            $check->path_ru = $member->path_ru;
            $check->panel_chars = $member->panel_chars;
            $check->parameter_dec_code = $member->parameter_dec_code;
            $check->visible = $member->visible;
            $check->save();
        }

    }

}