<?php

use App\Http\Models\SoseStudiesOutputParameter;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {


        $path = realpath("/home");
        $path = $path.'/ecolo_migration.sql';

        DB::unprepared(file_get_contents($path));
        $this->command->info('All migrations seeded!');



//         $this->call(RolesSeeder::class);
//         $this->call(LegalFormSeeder::class);
//         $this->call(QuestionnaireSeeder::class);
//         $this->call(QuestionnaireStatusSeeder::class);
//         $this->call(UserSeeder::class);
//        $this->call(SoseStudyTableSeeder::class);
//        $this->call(SoseAtecoTableSeeder::class);
//        $this->call(SoseOutputTableSeeder::class);
//        $this->call(SoseParameterTableSeeder::class);
//         $this->call(AreasTableSeeder::class);
//        $this->call(SoseCheckTableSeeder::class);
//
//
//        $this->call(SoseCheckValidationTableSeeder::class);
//        $this->call(SoseStudyCheckTableSeeder::class);
//        $this->call(SoseAtecoStudiesTableSeeder::class);
//        $this->call(SoseStudiesParameterTableSeeder::class);
//        $this->call(SoseOutputStudySeeder::class);
//        $this->call(QuestionnaireTreeTableSeeder::class);



    }
}
