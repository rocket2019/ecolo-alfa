<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 7/11/2019
 * Time: 2:53 PM
 */

class AreasTableSeeder extends Seeder
{
    public function run()
    {
        ini_set('max_execution_time', 0);
        $live_db = DB::connection('ecolo_example');
        $table = $live_db->table('geographical_areas');
        $live_data = $table->get();
//        DB::table('geographical_areas')->truncate();


        foreach ($live_data as $member) {


            $check = new \App\Http\Models\GeographicalAreas();
            $check->id = $member->id;
            $check->bankCode = $member->bankCode;
            $check->area_code = $member->area_code;
            $check->area_name = $member->area_name;
            $check->progressiveRegionNumber = $member->progressiveRegionNumber;
            $check->progressiveCityNumber = $member->progressiveCityNumber;
            $check->region_description = $member->region_description;
            $check->save();
        }

    }

}