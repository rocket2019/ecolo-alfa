<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QuestionnaireSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $roles = [
            [ '1','Trade','Commercio','Торговля','1','1'] ,
            [ '1.1','Retail trade','Commercio al dettaglio','Розничная торговля','1','2'],
            [ '1.1.1','Food commodities','Prodotti alimentari','продовольственные товары','1','3'],
            [ '1.1.2','Non-food','Non alimentari','непродовольственные','1','3'],
            [ '1.1.2.1','Clothes, footwear,  jewelry and leather products','Vestiti, calzature, gioielli e prodotti in pelle','Одежда, обувь, украшения и изделия из кожи','1','4'],
            [ '1.1.2.2','Electronics, Video, Clock, Computers, Photo','Elettronica, Video, Orologio, Computer, Foto','Электроника, видео, часы, компьютеры,фото','1','4'],
            [ '1.1.2.3','Furniture, goods for repair, goods for office and home, dish','Mobili, beni per la riparazione, beni per ufficio e casa, piatti','Мебель, товары для ремонта, товары для офиса и дома, посуда','1','4'],
            [ '1.1.2.4','Automobiles, motorcycles, equipments, boats','Automobili, motocicli, attrezzature, barche','Автомашины, мото, оборудование, лодки','1','4'],
            [ '1.1.2.5','Medicine, pharmacy, Goods for Children, Printed Products','Medicina, farmacie, prodotti per bambini, prodotti stampati','Медицина, аптеки, товары для детей, печатная продукция','1','4'],
            [ '1.1.2.6','Other','Altro','Другое','1','4'],
            [ '1.2','Wholesale','Vendita all\'ingrosso','Оптовая торговля','1','2'],
            [ '1.2.1','Food commodities','Prodotti alimentari','продовольственные товары','1','3'],
            [ '1.2.2','Non-food','Non alimentari','непродовольственные','1','3'],
            [ '1.2.2.1','Clothes, footwear,  jewelry and leather products','Vestiti, calzature, gioielli e prodotti in pelle','Одежда, обувь, украшения и изделия из кожи и меха','1','4'],
            [ '1.2.2.2','Electronics, Video, Clock, Computers, Photo','Elettronica, Video, Orologio, Computer, Foto','Электроника, видео, часы, компьютеры, фото','1','4'],
            [ '1.2.2.3','Furniture, goods for repair, goods for office and home, dish','Mobili, beni per la riparazione, beni per ufficio e casa, piatti','Мебель, товары для ремонта, товары для офиса и дома, посуда','1','4'],
            [ '1.2.2.4','Automobiles, motorcycles, equipments, boats','Automobili, motocicli, attrezzature, barche','Автомашины, мото, оборудование, лодки','1','4'],
            [ '1.2.2.5','Medicine, pharmacy, Goods for Children, Printed Products','Medicina, farmacie, prodotti per bambini, prodotti stampati','Медицина, аптеки, товары для детей, печатная продукция','1','4'],
            [ '1.2.2.6','Other','Altro','Другое','1','4'],
            [ '1.3','wholesale and retail trade','commercio all\'ingrosso e al dettaglio','Оптовая и розничная торговля','1','2'],
            [ '1.3.2','Non-food','Non alimentari','непродовольственные','1','3'],
            [ '1.4','Movable retail sales','Vendita al dettaglio mobile','Передвижная розничная торговля','1','2'],
            [ '1.4.1','Food commodities','Prodotti alimentari','продовольственные товары','1','3'],
            [ '1.4.2','Non-food','Non alimentari','непродовольственные','1','3'],

            [ '2','Services','Servizi','Услуги','2','1'] ,
            [ '2.1','Productive','Produttivo','Производственные','2','2'],
            [ '2.1.1','Structural renovation','Rinnovamento strutturale','Строительный ремонт','2','3'],
            [ '2.1.2','Construction','Costruzione','Строительство','2','3'],
            [ '2.1.3','Service and installation of equipment','Servizio   e installazione di attrezzature','Обслуживание и установка оборудования','2','3'],
            [ '2.1.4','Rent, leasing and real estate transactions, not connected with the construction','Affitto, leasing e transazioni immobiliari, non collegate alla costruzione','Аренда, лизинг и операции с недвижимостью, не связанные со строительством','2','3'],
            [ '2.1.5','Transportation, logistics and carriage','Trasporto, logistica e spostamento','Транспорт, логистика, перевозки','2','3'],
            [ '2.2','Professionals','Professionisti','Профессиональные','2','2'],
            [ '2.2.1','Insurance','di assicurazione','Страховые','2','3'],
            [ '2.2.3','Advertising','Pubblicitario','Рекламные','2','3'],
            [ '2.2.4','Consultancy (marketing, IT, etc.)','Consulenza (marketing, IT, ecc.)','Консультационные (маркетинг, ИТ и т.п.)','2','3'],
            [ '2.2.5','Organizational and management','Organizzazione e gestione','Организационные и управленческие','2','3'],
            [ '2.2.6','Information Technologies','Tecnologie informatiche','Информационные технологии','2','3'],
            [ '2.3','Community or consumer','Comunità o consumatore','Общественные и потребительские','2','2'],
            [ '2.3.1','Entertainment, leisure, tourism, radio and television','Intrattenimento, tempo libero, turismo, radio e televisione','Развлечения, досуг, туризм, радио и телевидение','2','3'],
            [ '2.3.2','Culture','Cultura','Культура','2','3'],
            [ '2.3.3','Education','Formazione ','Образование','2','3'],
            [ '2.3.4','Sport','Sport','Спорт','2','3'],
            [ '2.3.5','Hotels, restaurants, cafes, etc.','Alberghi, ristoranti, caffè, ecc.','Отели, рестораны, кафе и т.п.','2','3'],
            [ '2.3.6','Beauty and health','Bellezza e salute','Красота и здоровье','2','3'],
            [ '2.3.7','Repair of consumer goods','Riparazione di beni di consumo','Ремонт потреб. товаров','2','3'],
            [ '2.3.8','Maintenance of cars and other vehicle','Manutenzione di auto e altri veicoli','Обслуживание автомобилей и других ТС','2','3'],
            [ '2.4','Real estate agents, brokers, agents in the spheres','Agenti immobiliari, mediatori, agenti nei settori','Посредники, брокеры, агенты в сфере:','2','2'],
            [ '2.4.1','Sale of food products, beverage, tobacco products etc.','Vendita di prodotti alimentari, bevande, prodotti del tabacco ecc.','Продаж продуктов питания, напитков, табачных изделий и т.п.','2','3'],
            [ '2.4.2','Sale of clothes and footwear','Vendita di vestiti e calzature','Продаж одежды и обуви','2','3'],
            [ '2.4.3','Sale of paper products, stationery, print media etc.','Vendita di prodotti di carta, articoli di cancelleria, supporti di stampa ecc.','Продаж бумажных изделий, канцелярских товаров, печатных изданий и т.п.','2','3'],
            [ '2.4.4','Sale of furniture, household goods, domestic and electronic products, etc.','Vendita di mobili, articoli per la casa, prodotti domestici ed elettronici, ecc.','Продаж мебели, товаров для дома, бытовых и электронных изделий и т.п.','2','3'],
            [ '2.4.5','Sale of jewelry, metal, glass, optical, chemical production, fuel etc.','Vendita di gioielli, metallo, vetro, ottica, produzione chimica, carburante ecc.','Продаж ювелирных изделий, металлов, стекла, оптики, химической промышленности, топлива и т.п.','2','3'],
            [ '2.4.6','The sale of various types of machinery, equipment, construction materials, funds for repairs, farming, etc.','La vendita di vari tipi di macchinari, attrezzature, materiali da costruzione, fondi per riparazioni, agricoltura, ecc.','Продаж различных машин, оборудования, строительных материалов, средств для ремонта, сельского хозяйства и т.п.','2','3'],
            [ '2.4.7','Sale of medical / pharmaceutical products, hygiene care, etc.','Vendita di prodotti medico / farmaceutici, igiene, ecc.','Продаж медицинских/фармацевтических товаров, средств гигиены и т.п.','2','3'],
            [ '2.4.8','Sale of sports, children\'s goods and so on.','Vendita di articoli sportivi, articoli per bambini e così via.','Продаж спортивных, детских товаров и т.п.','2','3'],
            [ '2.4.9','Other mediation, broker and agent service','Altri servizi di mediazione, broker e agenti','Другая посредническая, брокерская и агентская деятельность','2','3'],
            [ '2.5','Other','Altro','Другие','2','2'],

            [ '3','Production','Produzione','Производство','3','1'] ,
            [ '3.1','Consumer goods','Beni di consumo','Потребительских товаров','3','2'],
            [ '3.1.1','Nutritional','Nutritivo','Продовольственных','3','3'],
            [ '3.1.2','Non-food','Non alimentari','Непродовольственных','3','3'],
            [ '3.1.2.1','Household/office goods and consumer electronics','Articoli per la casa / ufficio e prodotti elettronici di consumo','Товары для дома/офиса и бытовая электроника','3','4'],
            [ '3.1.2.2','clothes and footwear','vestiti e calzature','Одежда и обувь','3','4'],
            [ '3.1.2.3','Jewelry and decorations','Gioielli e decorazioni','Ювелирные изделия и украшения','3','4'],
            [ '3.1.2.4','Medical Supplies','Forniture mediche','Медицинские изделия','3','4'],
            [ '3.1.2.5','Typography','Tipografia','Типография','3','4'],
            [ '3.2','Non-consumer goods','Beni non-alimentari ','Непотребительских товаров','3','2'],
            [ '3.2.1','Electricity and different equipment','Elettricità e diverse attrezzature','Электроэнергетика и различное оборудование','3','3'],
            [ '3.2.2','Repair and maintenance of the machine','Riparazione e manutenzione della macchina','Ремонт и техобслуживание машин','3','3'],
            [ '3.2.3','Chemistry, petrochemical industry and manufacture of glass','Chimica, industria petrolchimica e produzione di vetro','Химическая, нефтехимическая промышленность и изделия из стекла','3','3'],
            [ '3.2.4','Machine building and metalworking','Costruzione di macchinari e lavorazione dei metalli','Машиностроение и металлообработка','3','3'],
            [ '3.2.5','Forest, wood-cutting and pulp-bearing fabrication','Fabbricazione forestale, per il taglio del legno e della pasta di legno','Лесная, деревообрабатывающая и целлюлозно-бумажная промышленность','3','3'],
            [ '3.2.6','Production of different materials and construction funds','Produzione di diversi materiali e fondi per la costruzione','Производство различных материалов и средств для строительства','3','3'],
            [ '3.3','Other','Altro','Другие','3','2'],
            [ '3.2.1.1','Manufacture of generators, cables and other electrical equipment','Produzione di generatori, cavi e altre apparecchiature elettriche','Изготовление генераторов, кабелей и другого электрооборудования','3','4'],
            [ '3.2.1.2','Manufacture of optical, measurable and various communication devices','Fabbricazione di comunicazioni ottiche, misurabili e varie dispositivi di comunicazione','Изготовление оптических, измерительных и различных коммуникационных приборов','3','4'],
            [ '3.2.1.3','Manufacture of motor vehicles, thermal, electronically and other complex equipment','Fabbricazione di autoveicoli, apparecchiature termiche, elettroniche e di altro tipo','Изготовление автомобильного, теплового, электронного и прочего сложного оборудованиянистратор','3','4'],
            [ '3.2.4.1','Steel processing and other elements of metalworking (not containing iron) and production of various types of products','Lavorazione dell\'acciaio e altri elementi della lavorazione dei metalli (non contenenti ferro) e produzione di vari tipi di prodotti','Обработка стали и других легких металлов (не содержащих железа) и изготовление оборудования и различных изделий из них','3','4'],
            [ '3.2.4.2','Repair of iron, pig-iron, diverse ferroalloys, and manufacturing of equipment and various products','Riparazione di ferro, ghisa, varie ferroleghe e fabbricazione di attrezzature e prodotti vari','Обработка железа, чугуна, различных ферросплавов и изготовление оборудования и различных изделий из них','3','4'],




        ];

//        DB::table('code_tree')->truncate();

        for ($i = 0; $i < count($roles); $i++) {
            DB::table('code_tree')->insert(
                [
                    'code' => $roles[$i][0],
                    'description_en' => $roles[$i][1],
                    'description_it' => $roles[$i][2],
                    'description_ru' => $roles[$i][3],
                    'group' =>$roles[$i][4],
                    'level' => $roles[$i][5],
                ]
            );
        }
    }
}
