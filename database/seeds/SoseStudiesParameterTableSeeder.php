<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 7/11/2019
 * Time: 1:43 PM
 */

class SoseStudiesParameterTableSeeder extends Seeder
{
    public function run()
    {
        ini_set('max_execution_time', 0);
        $live_db = DB::connection('ecolo_example');
        $table = $live_db->table('sose_studies_parameters');
        $live_data = $table->get();

//        DB::table('sose_studies_parameters')->truncate();


        foreach ($live_data as $member) {

            DB::statement('SET FOREIGN_KEY_CHECKS = 0');

            $check = new \App\Http\Models\SoseStudiesParameter();
            $check->sosestudy_id = $member->sosestudy_id;
            $check->soseparameter_id = $member->soseparameter_id;
            $check->save();
            DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        }

    }

}