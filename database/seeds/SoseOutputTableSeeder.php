<?php

use App\Http\Models\SoseOutputParameter;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SoseOutputTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ini_set('max_execution_time', 0);
        $live_db = DB::connection('ecolo_example');
        $table = $live_db->table('sose_output_parameter');
        $live_data = $table->get();

//        DB::table('sose_output_parameter')->truncate();

        foreach ($live_data as $member) {


            $check = new SoseOutputParameter();
            $check->id = $member->id;
            $check->parameter_code = $member->parameter_code;
            $check->description_en = $member->description_en;
            $check->description_it = $member->description_it;
            $check->description_ru= $member->description_ru;
            $check->units = $member->units;
            $check->value_type = $member->value_type;
            $check->path = $member->path;
            $check->panel_chars = $member->panel_chars;
            $check->normalization = $member->normalization;
            $check->save();
        }

    }
}
