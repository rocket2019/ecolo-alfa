<?php

use App\Http\Models\CompanyList;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompanyScraperSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ini_set('max_execution_time', 0);
        $live_db = DB::connection('ecolo_server');

        $table = $live_db->table('company_list');
        $live_data = $table->get();

        foreach ($live_data as $company) {
            $exists = CompanyList::query()
                ->where('company_name',$company->company_name)
                ->exists();
            if($exists)
            {
                continue;
            }

            $new_company = new CompanyList();
            $new_company->ID_company = $company->ID_company;
            $new_company->company_name = $company->company_name;
            $new_company->OKPO_number = $company->OKPO_number;
            $new_company->INN_number = $company->INN_number;
            $new_company->email_manager = $company->email_manager;
            $new_company->location_address = $company->location_address;
            $new_company->OKVED_main_code = $company->OKVED_main_code;
            $new_company->main_activity = $company->main_activity;
            $new_company->OKOPF_OKFS_code = $company->OKOPF_OKFS_code;
            $new_company->OKEI_code = $company->OKEI_code;
            $new_company->legacy_id = $company->legacy_id;
            $new_company->created = $company->created;
            $new_company->updated = $company->updated;
            $new_company->user_id = $company->user_id;
            $new_company->region_code = $company->region_code;
            $new_company->client_id = $company->client_id;
            $new_company->email_client = $company->email_client;
            $new_company->save();

            echo $new_company.'<br>';
        }
    }
}
