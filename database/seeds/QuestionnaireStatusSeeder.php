<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QuestionnaireStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $forms = [
            ['IN_EDITING', 'IN PREPARATION BY THE CLIENT', 'IN PREPARAZIONE DAL CLIENTE', 'ПОДГОТОВКА КЛИЕНТОМ', '3'],
            ['SUBMITTED', 'SUBMITTED TO EB BACKOFFICE', 'PRESENTATO A BACKOFFICE EB', 'ПРЕДСТАВЛЕНО EB BACKOFFICE', '1'],
            ['REGISTERED_IN_BACKOFFICE', 'REGISTERED BY EB BACKOFFICE', 'REGISTRATO DA BACKOFFICE', 'Зарегистрировано по EB BACKOFFICE', '5'],
            ['IN_BACKOFFICE_ELABORATION', 'UNDER ELABORATION IN EB BACKOFFICE', 'SOTTO L\'ELABORAZIONE IN EB BACKOFFICE', 'ПОД РАЗРАБОТКОЙ В EB BACKOFFICE', '2'],
            ['SENT_TO_BANK', 'SENT TO BANK WITH RECOMENDATION', 'INVIATO ALLA BANCA CON RACCOMANDAZIONE', 'ОТПРАВЛЕН В БАНК С РЕКОМЕНДАЦИЕЙ', '4'],


        ];

//        DB::table('sose_questionnaire_status')->truncate();

        for ($i = 0; $i < count($forms); $i++) {
            DB::table('sose_questionnaire_status')->insert(
                [
                    'code' => $forms[$i][0],
                    'description_en' => $forms[$i][1],
                    'description_it' => $forms[$i][2],
                    'description_ru' => $forms[$i][3],
                    'order' => $forms[$i][4],
                ]
            );
        }
    }
}
