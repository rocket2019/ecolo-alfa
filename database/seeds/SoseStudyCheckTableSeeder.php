<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SoseStudyCheckTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ini_set('max_execution_time', 0);
        $live_db = DB::connection('ecolo_example');
        $table = $live_db->table('sose_studies_checks');
        $live_data = $table->get();
//        DB::table('sose_studies_checks')->truncate();



        foreach ($live_data as $member) {

            DB::statement('SET FOREIGN_KEY_CHECKS = 0');
            $check = new \App\Http\Models\SoseStudiesCheck();
            $check->sosestudy_id = $member->sosestudy_id;
            $check->sosecheck_id = $member->sosecheck_id;
            $check->save();
            DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        }

    }
}
