<?php

use App\Http\Models\SoseStudy;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 7/11/2019
 * Time: 1:45 PM
 */

class SoseStudyTableSeeder extends \Illuminate\Database\Seeder
{


    /**
     *
     */
    public function run()
    {
        ini_set('max_execution_time', 0);
        $live_db = DB::connection('ecolo_example');
        $table = $live_db->table('sose_study');
        $live_data = $table->get();
//        DB::table('sose_study')->truncate();


        foreach ($live_data as $member) {


            $check = new SoseStudy();
            $check->id = $member->id;
            $check->study_code = $member->study_code;
            $check->description = $member->description;
            $check->save();
        }

    }
}