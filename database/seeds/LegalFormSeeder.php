<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LegalFormSeeder extends Seeder
{
    public function run()
    {
        $forms = [
            ['Limited responsability company', 'Azienda a responsabilità limitata', 'ООО [Общество с ограниченной ответственностью]'],
            ['Company with additional responsability', 'Azienda con responsabilità aggiuntiva', 'ОДО [Общество с дополнительной ответственностью]'],
            ['Individual/Freelances', 'Individuale/Freelancer', 'ИП [Индивидуальный предприниматель]'],
            ['Opened Joint Stock Company', 'Opened Joint Stock Company', 'ПАО [Публичное акционерное общество]'],
            ['Closed Joint Stock Company', 'Closed Joint Stock Company', 'НПАО [Непубличное акционерное общество]'],
            ['A general partnership', 'Una società in nome collettivo', 'Полное товарищество'],
            ['A limited partnership', 'Una società in accomandita', 'Товарищество на вере [коммандитное товарищество]'],
            ['Production Cooperative', 'Cooperativa di produzione', 'Производственный кооператив [артель]'],
            ['The agricultural cooperative', 'La cooperativa agricola', 'Сельскохозяйственная артель [колхоз]'],
            ['Fishing cooperative', 'Cooperativa di pesca', 'Рыболовецкий артель'],
            ['Cooperative organization', 'Organizzazione Cooperativa', 'Кооперативное хозяйство [коопхоз]'],
            ['Production cooperative (excluding agricultaral cooperatives)', 'Cooperativa di produzione (escluso cooperative agricole)', 'Производственный кооператив [кроме сельскохозяйственных производственных кооперативов]'],

        ];

//        DB::table('legal_forms')->truncate();

        for ($i = 0; $i < count($forms); $i++) {
            DB::table('legal_forms')->insert(
                [
                    'name_en' => $forms[$i][0],
                    'name_it' => $forms[$i][1],
                    'name_ru' => $forms[$i][2],
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ]
            );
        }
    }
}
