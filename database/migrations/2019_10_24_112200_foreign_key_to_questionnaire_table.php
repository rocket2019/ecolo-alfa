<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignKeyToQuestionnaireTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(\App\Http\Models\Questionnaire::tableName(), function (Blueprint $table) {

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('company_id')
                ->references('id')->on('companies_list')
                ->onDelete('cascade');
            $table->foreign('code_id')
                ->references('id')->on('code_tree')
                ->onDelete('cascade');
            $table->foreign('sose_ateco_id')
                ->references('id')->on('sose_ateco')
                ->onDelete('cascade');
            $table->foreign('sose_study_id')
                ->references('id')->on('sose_study')
                ->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('questionnaire', function (Blueprint $table) {
            //
        });
    }
}
