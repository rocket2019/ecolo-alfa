<?php

use App\Http\Models\SoseQuestionnaire;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnNamesOnSoseQuestionnaireTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(SoseQuestionnaire::tableName(), function (Blueprint $table) {
            $table->renameColumn('company_id','id_company');
            $table->renameColumn('created_at','created');
            $table->renameColumn('updated_at','updated');
            $table->integer('sose_questionnaire_status_id')->nullable();
            $table->integer('updated_status_user_id')->nullable();
            $table->timestamp('updated_status')->nullable();
            $table->boolean('backup')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(SoseQuestionnaire::tableName(), function (Blueprint $table) {
            $table->renameColumn('id_company','company_id');
            $table->renameColumn('created_at','created');
            $table->renameColumn('updated_at','updated');
            $table->dropColumn('updated_status');
            $table->dropColumn('backup');
            $table->dropColumn('sose_questionnaire_status_id');
            $table->dropColumn('updated_status_user_id');

        });
    }
}
