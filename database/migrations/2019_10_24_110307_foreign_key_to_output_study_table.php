<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignKeyToOutputStudyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(\App\Http\Models\SoseStudiesOutputParameter::tableName(), function (Blueprint $table) {

            $table->foreign('sosestudy_id')
                ->references('id')->on('sose_study')
                ->onDelete('cascade');
            $table->foreign('soseoutputparameter_id')
                ->references('id')->on('sose_output_parameter')
                ->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('output_study', function (Blueprint $table) {
            //
        });
    }
}
