<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanApprovalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_approval', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('company_id')->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->date('evaluation_year')->nullable();
            $table->float('prob_default')->nullable();
            $table->float('prime_rate')->nullable();
            $table->float('spread')->nullable();
            $table->float('interest_rate')->nullable();
            $table->float('x7_projected')->nullable();
            $table->bigInteger('amount')->nullable();
            $table->bigInteger('loan_cost')->nullable();
            $table->integer('months')->nullable();
            $table->string('approvation_level')->nullable();
            $table->string('report')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_approval');
    }
}
