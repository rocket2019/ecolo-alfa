<?php

use App\Http\Models\Questionnaire;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnsToSoseQuestionnaireTableAndRename extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Questionnaire::tableName(), function (Blueprint $table) {

            $table->date('balance_year')->after('user_id')->nullable();
            $table->boolean('valid')->after('balance_year')->nullable();
            $table->integer('sose_okved_id')->after('sose_ateco_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sose_questionnaire_table_and_rename', function (Blueprint $table) {
            //
        });
    }
}
