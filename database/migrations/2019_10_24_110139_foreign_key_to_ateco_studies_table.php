<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignKeyToAtecoStudiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(\App\Http\Models\SoseAtecoStudiesEquivalence::tableName(), function (Blueprint $table) {

            $table->foreign('sosestudy_id')
                ->references('id')->on('sose_study')
                ->onDelete('cascade');
            $table->foreign('soseateco_id')
                ->references('id')->on('sose_ateco')
                ->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ateco_studies', function (Blueprint $table) {
            //
        });
    }
}
