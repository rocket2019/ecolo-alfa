<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoseParameterMessageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sose_parameter_message', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('sose_parameter_id')->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->longText('english')->nullable();
            $table->longText('russian')->nullable();
            $table->longText('italian')->nullable();
            $table->longText('path_english')->nullable();
            $table->longText('path_russian')->nullable();
            $table->longText('path_italian')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sose_parameter_message');
    }
}
