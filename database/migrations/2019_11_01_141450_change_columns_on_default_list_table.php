<?php

use App\Http\Models\DefaultList;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnsOnDefaultListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(DefaultList::tableName(), function (Blueprint $table) {
            $table->renameColumn('company_ID','ID_company');
            $table->renameColumn('created_at','created');
            $table->renameColumn('updated_at','updated');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(DefaultList::tableName(), function (Blueprint $table) {
            $table->renameColumn('ID_company','company_ID');
            $table->renameColumn('created_at','created');
            $table->renameColumn('updated_at','updated');

        });
    }
}
