<?php

use App\Http\Models\SoseParameterValue;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnNamesOnSoseParameterValueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(SoseParameterValue::tableName(), function (Blueprint $table) {
            $table->renameColumn('company_id','id_company');
            $table->renameColumn('created_at','created');
            $table->renameColumn('updated_at','updated');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(SoseParameterValue::tableName(), function (Blueprint $table) {
            $table->renameColumn('id_company','company_id');
            $table->renameColumn('created','created_at');
            $table->renameColumn('updated','updated_at');

        });
    }
}
