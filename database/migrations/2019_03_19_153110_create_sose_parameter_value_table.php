<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoseParameterValueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sose_parameter_value', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('parameter_id')->unsigned()->nullable();
            $table->bigInteger('company_id')->unsigned()->nullable();
            $table->date('balance_year')->nullable();
            $table->float('value')->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->bigInteger('sose_questionnaire_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sose_parameter_value');
    }
}
