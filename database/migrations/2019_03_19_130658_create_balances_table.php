<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->date('balance');
            $table->integer('employees')->nullable();
            $table->bigInteger('as_1110')->nullable();
            $table->bigInteger('as_1120')->nullable();
            $table->bigInteger('as_1130')->nullable();
            $table->bigInteger('as_1140')->nullable();
            $table->bigInteger('as_1150')->nullable();
            $table->bigInteger('as_1160')->nullable();
            $table->bigInteger('as_1170')->nullable();
            $table->bigInteger('as_1180')->nullable();
            $table->bigInteger('as_1190')->nullable();
            $table->bigInteger('as_1210')->nullable();
            $table->bigInteger('as_1220')->nullable();
            $table->bigInteger('as_1230')->nullable();
            $table->bigInteger('as_1240')->nullable();
            $table->bigInteger('as_1250')->nullable();
            $table->bigInteger('as_1260')->nullable();

            $table->bigInteger('el_1310')->nullable();
            $table->bigInteger('el_1320')->nullable();
            $table->bigInteger('el_1340')->nullable();
            $table->bigInteger('el_1350')->nullable();
            $table->bigInteger('el_1360')->nullable();
            $table->bigInteger('el_1370')->nullable();
            $table->bigInteger('el_1410')->nullable();
            $table->bigInteger('el_1420')->nullable();
            $table->bigInteger('el_1430')->nullable();
            $table->bigInteger('el_1450')->nullable();
            $table->bigInteger('el_1510')->nullable();
            $table->bigInteger('el_1520')->nullable();
            $table->bigInteger('el_1530')->nullable();
            $table->bigInteger('el_1540')->nullable();
            $table->bigInteger('el_1550')->nullable();

            $table->bigInteger('pl_2110')->nullable();
            $table->bigInteger('pl_2120')->nullable();
            $table->bigInteger('pl_2210')->nullable();
            $table->bigInteger('pl_2220')->nullable();
            $table->bigInteger('pl_2310')->nullable();
            $table->bigInteger('pl_2320')->nullable();
            $table->bigInteger('pl_2330')->nullable();
            $table->bigInteger('pl_2340')->nullable();
            $table->bigInteger('pl_2350')->nullable();
            $table->bigInteger('pl_2410')->nullable();
            $table->bigInteger('pl_2421')->nullable();
            $table->bigInteger('pl_2430')->nullable();
            $table->bigInteger('pl_2450')->nullable();
            $table->bigInteger('pl_2460')->nullable();

            $table->bigInteger('id_company')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('balances');
    }
}
