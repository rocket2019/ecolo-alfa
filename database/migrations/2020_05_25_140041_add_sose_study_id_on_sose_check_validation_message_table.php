<?php

use App\Http\Models\SoseCheckValidationMessage;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoseStudyIdOnSoseCheckValidationMessageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(SoseCheckValidationMessage::tableName(), function (Blueprint $table) {
            $table->integer('sose_study_id')->after('user_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(SoseCheckValidationMessage::tableName(), function (Blueprint $table) {
            $table->dropColumn('sose_study_id');
        });
    }
}
