<?php

use App\Http\Models\CompanyList;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQuestionnaireStatusColumnToCompanyListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(CompanyList::tableName(), function (Blueprint $table) {
            $table->integer('questionnaire_status')->after('client_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(CompanyList::tableName(), function (Blueprint $table) {
            $table->dropColumn('questionnaire_status');
        });
    }
}
