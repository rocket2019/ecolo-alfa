<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndexesRatiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indexes_ratios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('balance_year');
            $table->bigInteger('company_id')->nullable();
            $table->bigInteger('user_id')->nullable();


            $table->bigInteger('II')->nullable();
            $table->bigInteger('IM')->nullable();
            $table->bigInteger('IFI')->nullable();
            $table->bigInteger('I')->nullable();
            $table->bigInteger('DE')->nullable();
            $table->bigInteger('DF')->nullable();
            $table->bigInteger('DL')->nullable();
            $table->bigInteger('AC')->nullable();
            $table->bigInteger('PN')->nullable();
            $table->bigInteger('PL')->nullable();
            $table->bigInteger('PC')->nullable();
            $table->bigInteger('VP')->nullable();
            $table->bigInteger('CO')->nullable();
            $table->bigInteger('VA')->nullable();
            $table->bigInteger('CP')->nullable();
            $table->bigInteger('CV')->nullable();
            $table->bigInteger('CA')->nullable();
            $table->bigInteger('MOL')->nullable();
            $table->bigInteger('AA')->nullable();
            $table->bigInteger('RO')->nullable();
            $table->bigInteger('RGF')->nullable();
            $table->bigInteger('RGS')->nullable();
            $table->bigInteger('RL')->nullable();
            $table->bigInteger('IMP')->nullable();
            $table->bigInteger('RN')->nullable();
            $table->bigInteger('LI')->nullable();
            $table->bigInteger('LD')->nullable();
            $table->bigInteger('AFN')->nullable();
            $table->bigInteger('AT')->nullable();
            $table->bigInteger('PF')->nullable();
            $table->bigInteger('DT')->nullable();
            $table->bigInteger('MP')->nullable();
            $table->bigInteger('PT')->nullable();
            $table->bigInteger('CCN')->nullable();
            $table->bigInteger('CL')->nullable();
            $table->bigInteger('RI')->nullable();
            $table->bigInteger('OF')->nullable();
            $table->bigInteger('ES')->nullable();
            $table->bigInteger('COP')->nullable();
            $table->bigInteger('FCR')->nullable();
            $table->bigInteger('PTP')->nullable();
            $table->bigInteger('PFL')->nullable();

            $table->float('x_1')->nullable();
            $table->float('x_2')->nullable();
            $table->float('x_3')->nullable();
            $table->float('x_4')->nullable();
            $table->float('x_5')->nullable();
            $table->float('x_6')->nullable();
            $table->float('x_7')->nullable();
            $table->float('x_8')->nullable();
            $table->float('x_9')->nullable();
            $table->float('x_10')->nullable();
            $table->float('x_11')->nullable();
            $table->float('x_12')->nullable();
            $table->float('x_13')->nullable();
            $table->float('x_14')->nullable();
            $table->float('x_15')->nullable();
            $table->float('x_16')->nullable();
            $table->float('x_17')->nullable();
            $table->float('x_18')->nullable();
            $table->float('x_19')->nullable();
            $table->float('x_20')->nullable();
            $table->float('x_21')->nullable();
            $table->float('x_22')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indexes_ratios');
    }
}
