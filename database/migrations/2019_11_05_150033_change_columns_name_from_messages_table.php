<?php

use App\Http\Models\SoseParameterMessage;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnsNameFromMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(SoseParameterMessage::tableName(), function (Blueprint $table) {
            $table->renameColumn('path_english','pathEnglish');
            $table->renameColumn('path_russian','pathRussian');
            $table->renameColumn('path_italian','pathItalian');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(SoseParameterMessage::tableName(), function (Blueprint $table) {
            $table->renameColumn('pathEnglish','path_english');
            $table->renameColumn('pathRussian','path_russian');
            $table->renameColumn('pathItalian','path_italian');
        });
    }
}
