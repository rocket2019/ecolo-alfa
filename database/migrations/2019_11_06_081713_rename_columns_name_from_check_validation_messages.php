<?php

use App\Http\Models\SoseCheckValidationMessage;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnsNameFromCheckValidationMessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(SoseCheckValidationMessage::tableName(), function (Blueprint $table) {
            $table->renameColumn('message_en','english');
            $table->renameColumn('message_ru','russian');
            $table->renameColumn('message_it','italian');
            $table->renameColumn('created_at','created');
            $table->renameColumn('updated_at','updated');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('check_validation_messages', function (Blueprint $table) {
            //
        });
    }
}
