<?php

use App\Http\Models\CompanyList;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnNamesOnCompanyListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(CompanyList::tableName(), function (Blueprint $table) {
            $table->renameColumn('id','ID_company');
            $table->renameColumn('legal_form_id','legal_form');
            $table->renameColumn('created_at','created');
            $table->renameColumn('updated_at','updated');
            $table->string('email_manager')->after('company_name')->nullable();
            $table->string('email_client')->after('email_manager')->nullable();
            $table->integer('client_id')->after('email_client')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(CompanyList::tableName(), function (Blueprint $table) {
            $table->renameColumn('ID_company','id');
            $table->renameColumn('legal_form','legal_form_id');
            $table->renameColumn('created','created_at');
            $table->renameColumn('updated','updated_at');
            $table->dropColumn('email_manager');
            $table->dropColumn('email_client');
            $table->dropColumn('client_id');

        });
    }
}
