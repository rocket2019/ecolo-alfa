<?php

use App\Http\Models\SoseOutputParameter;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAndAddColumnsToSoseStudyOutputParameter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(SoseOutputParameter::tableName(), function (Blueprint $table) {
            $table->string('description_it')->after('parameter_code')->nullable();
            $table->string('description_ru')->after('description_it')->nullable();
            $table->renameColumn('description','description_en');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sose_study_output_parameter', function (Blueprint $table) {
            //
        });
    }
}
