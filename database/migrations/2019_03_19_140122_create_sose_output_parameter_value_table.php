<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoseOutputParameterValueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sose_output_parameter_value', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('parameter_id')->nullable();
            $table->bigInteger('company_id')->unsigned()->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->date('balance_year')->nullable();
            $table->float('value')->nullable();
            $table->bigInteger('sose_questionnaire_result_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sose_output_parameter_value');
    }
}
