<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignKeyToStudyParamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(\App\Http\Models\SoseStudiesParameter::tableName(), function (Blueprint $table) {
            $table->foreign('sosestudy_id')
                ->references('id')->on('sose_study')
                ->onDelete('cascade');
            $table->foreign('soseparameter_id')
                ->references('id')->on('sose_parameter')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('study_param', function (Blueprint $table) {
            //
        });
    }
}
