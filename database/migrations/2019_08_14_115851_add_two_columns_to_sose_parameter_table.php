<?php

use App\Http\Models\SoseParameter;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTwoColumnsToSoseParameterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(SoseParameter::tableName(), function (Blueprint $table) {
            $table->string('units_en')->after('units')->nullable();
            $table->string('units_ru')->after('units_en')->nullable();
            $table->string('units_it')->after('units_ru')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sose_parameter', function (Blueprint $table) {
            //
        });
    }
}
