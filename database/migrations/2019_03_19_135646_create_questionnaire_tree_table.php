<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionnaireTreeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questionnaire_tree', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('sose_study_id')->unsigned()->nullable();
            $table->bigInteger('sose_ateco_id')->unsigned()->nullable();
            $table->bigInteger('sose_okved_id')->nullable();
            $table->string('code');
            $table->integer('sort_order');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questionnaire_tree');
    }
}
