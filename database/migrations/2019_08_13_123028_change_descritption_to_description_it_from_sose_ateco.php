<?php

use App\Http\Models\SoseAteco;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDescritptionToDescriptionItFromSoseAteco extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(SoseAteco::tableName(), function (Blueprint $table) {
            $table->renameColumn('description','description_it');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('description_it_from_sose_ateco', function (Blueprint $table) {
            //
        });
    }
}
