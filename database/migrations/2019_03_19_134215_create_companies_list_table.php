<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies_list', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->text('company_name');
            $table->longText('OKPO_number')->nullable();
            $table->longText('INN_number')->nullable();
            $table->bigInteger('legal_form_id')->unsigned();
            $table->longText('location_address')->nullable();
            $table->longText('OKVED_main_code')->nullable();
            $table->longText('main_activity')->nullable();
            $table->longText('OKOPF_OKFS_code')->nullable();
            $table->longText('OKEI_code')->nullable();
            $table->text('legacy_id')->nullable();
            $table->text('region_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies_list');
    }
}
