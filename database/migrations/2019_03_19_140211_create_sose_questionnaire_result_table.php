<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoseQuestionnaireResultTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sose_questionnaire_result', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('sose_questionnaire_id')->unsigned()->nullable();
            $table->float('regional_revenue_coefficient')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sose_questionnaire_result');
    }
}
