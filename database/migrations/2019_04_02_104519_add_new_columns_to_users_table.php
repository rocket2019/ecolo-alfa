<?php

use App\Http\Models\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(User::tableName(), function (Blueprint $table) {
            $table->string('city')->nullable()->after('email');
            $table->string('department')->nullable()->after('city');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(User::tableName(), function (Blueprint $table) {
            $table->dropColumn('city');
            $table->dropColumn('department');
        });
    }
}
