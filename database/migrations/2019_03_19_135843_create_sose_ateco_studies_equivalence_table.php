<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoseAtecoStudiesEquivalenceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sose_ateco_studies_equivalence', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('soseateco_id')->unsigned();
            $table->bigInteger('sosestudy_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sose_ateco_studies_equivalence');
    }
}
