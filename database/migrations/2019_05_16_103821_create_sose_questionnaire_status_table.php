<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoseQuestionnaireStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sose_questionnaire_status', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('description_en');
            $table->string('description_it');
            $table->string('description_ru');
            $table->integer('order');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sose_questionnaire_status');
    }
}
