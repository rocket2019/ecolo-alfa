<?php

use App\Http\Models\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username')->nullable();
            $table->string('username_canonical')->nullable();
            $table->string('email');
            $table->string('email_canonical')->nullable();
            $table->boolean('enabled')->default(User::ENABLED_TRUE);
            $table->string('salt')->nullable();
            $table->string('password')->nullable();
            $table->dateTime('last_login')->nullable();
            $table->boolean('locked')->default(0);
            $table->boolean('expired')->nullable();
            $table->dateTime('expires_at')->nullable();
            $table->dateTime('password_requested_at')->nullable();
            $table->string('confirmation_token')->nullable();
            $table->bigInteger('role_id')->unsigned()->nullable();
            $table->boolean('credentials_expired')->nullable();
            $table->dateTime('credentials_expire_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
