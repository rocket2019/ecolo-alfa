<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoseOutputParameterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sose_output_parameter', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('parameter_code')->nullable();
            $table->string('description')->nullable();
            $table->string('units')->nullable();
            $table->string('value_type')->nullable();
            $table->longText('path')->nullable();
            $table->longText('normalization')->nullable();
            $table->string('panel_chars')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sose_output_parameter');
    }
}
