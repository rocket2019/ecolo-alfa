<?php

use App\Http\Models\GlobalVariable;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnNamesOnGlobalVariablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(GlobalVariable::tableName(), function (Blueprint $table) {
            $table->renameColumn('created_at','created');
            $table->renameColumn('updated_at','updated');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(GlobalVariable::tableName(), function (Blueprint $table) {
            $table->renameColumn('created','created_at');
            $table->renameColumn('updated','updated_at');
        });
    }
}
