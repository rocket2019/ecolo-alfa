<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignKeyToStudyCheckTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(\App\Http\Models\SoseStudiesCheck::tableName(), function (Blueprint $table) {

            $table->foreign('sosestudy_id')
                ->references('id')->on('sose_study')
                ->onDelete('cascade');
            $table->foreign('sosecheck_id')
                ->references('id')->on('sose_check')
                ->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('study_check', function (Blueprint $table) {
            //
        });
    }
}
