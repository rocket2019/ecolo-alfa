<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRevenueMarginTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revenue_margin', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('sose_questionnaire_result_id')->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->float('min_revenue_expert')->nullable();
            $table->float('max_revenue_expert')->nullable();
            $table->float('min_margin_expert')->nullable();
            $table->float('max_margin_expert')->nullable();
            $table->float('min_revenue_bank')->nullable();
            $table->float('max_revenue_bank')->nullable();
            $table->float('min_margin_bank')->nullable();
            $table->float('max_margin_bank')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('revenue_margin');
    }
}
