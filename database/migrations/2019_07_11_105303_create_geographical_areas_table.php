<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeographicalAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(\App\Http\Models\GeographicalAreas::tableName(), function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('bankCode');
            $table->string('area_code');
            $table->string('area_name');
            $table->integer('progressiveRegionNumber');
            $table->integer('progressiveCityNumber');
            $table->string('region_description');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geographical_areas');
    }
}
