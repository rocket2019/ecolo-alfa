<?php

use App\Http\Models\SoseCheckValidationMessage;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeValidationMessagesOnSoseCheckValidationMessageTable extends Migration
{
    public function up()
    {
        Schema::table(SoseCheckValidationMessage::tableName(), function (Blueprint $table) {
            $table->renameColumn('english','message_en');
            $table->renameColumn('russian','message_ru');
            $table->renameColumn('italian','message_it');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(SoseCheckValidationMessage::tableName(), function (Blueprint $table) {
            $table->renameColumn('message_en','english');
            $table->renameColumn('message_ru','russian');
            $table->renameColumn('message_it','italian');
        });
    }
}
