<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoseStudiesChecksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sose_studies_checks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('sosestudy_id')->unsigned();
            $table->bigInteger('sosecheck_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sose_studies_checks');
    }
}
