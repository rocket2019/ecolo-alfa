<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignKeyToQuestionnaireTreeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(\App\Http\Models\QuestionnaireTree::tableName(), function (Blueprint $table) {

            $table->foreign('sose_study_id')
                ->references('id')->on('sose_study')
                ->onDelete('cascade');
            $table->foreign('sose_ateco_id')
                ->references('id')->on('sose_ateco')
                ->onDelete('cascade');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('questionnaire_tree', function (Blueprint $table) {
            //
        });
    }
}
