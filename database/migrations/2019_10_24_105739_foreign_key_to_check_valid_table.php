<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignKeyToCheckValidTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(\App\Http\Models\SoseCheckValidationMessage::tableName(), function (Blueprint $table) {

            $table->foreign('sose_check_id')
                ->references('id')->on('sose_check')
                ->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('check_valid', function (Blueprint $table) {
            //
        });
    }
}
