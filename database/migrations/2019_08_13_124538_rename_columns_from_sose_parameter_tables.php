<?php

use App\Http\Models\SoseParameter;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnsFromSoseParameterTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(SoseParameter::tableName(), function (Blueprint $table) {
            $table->renameColumn('description','description_en');
            $table->renameColumn('path','path_en');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sose_parameter_tables', function (Blueprint $table) {
            //
        });
    }
}
