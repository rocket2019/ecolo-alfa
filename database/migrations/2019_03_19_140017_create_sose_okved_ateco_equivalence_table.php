<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoseOkvedAtecoEquivalenceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sose_okved_ateco_equivalence', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('soseokved_id');
            $table->integer('soseateco_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sose_okved_ateco_equivalence');
    }
}
