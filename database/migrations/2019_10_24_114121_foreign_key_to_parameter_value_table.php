<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignKeyToParameterValueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(\App\Http\Models\SoseParameterValue::tableName(), function (Blueprint $table) {


            $table->foreign('parameter_id')
                ->references('id')->on('sose_parameter')
                ->onDelete('cascade');
            $table->foreign('company_id')
                ->references('id')->on('companies_list')
                ->onDelete('cascade');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parameter_value', function (Blueprint $table) {
            //
        });
    }
}
