<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoseOkvedStudiesEquivalenceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sose_okved_studies_equivalence', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('soseokved_id');
            $table->integer('sosestudy_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sose_okved_studies_equivalence');
    }
}
