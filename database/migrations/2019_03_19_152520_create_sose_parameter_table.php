<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoseParameterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sose_parameter', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('parameter_code')->nullable();
            $table->string('description')->nullable();
            $table->string('description_it')->nullable();
            $table->string('description_ru')->nullable();
            $table->string('units')->nullable();
            $table->string('value_type')->nullable();
            $table->string('path')->nullable();
            $table->string('path_ru')->nullable();
            $table->string('path_it')->nullable();
            $table->string('panel_chars')->nullable();
            $table->string('parameter_dec_code')->nullable();
            $table->boolean('visible')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sose_parameter');
    }
}
