@extends('account.layout')
@section('content')
    <div class="row-fluid">
        <div class="alert alert-success alert_logo" role="alert" style="display: none">
            @lang('global.logo_success')
        </div>
        <div class="span12">
            <form id="bank_logo_form" role="form" method="POST" class="form-horizontal well"
                  enctype="multipart/form-data">

                <h3 class="heading">
                    @lang('global.settings')</h3>
                <div class="demo-wrap upload-demo">
                    <div class="container">
                        <div class="grid">
                            <div class="col-1-2">


                                <div class="actions">
                                    <a class="btn file-btn">
                                        <input type="file" id="upload" value="Choose a file" accept="image/*" />
                                    </a>
                                    <button type="button" class="upload-result">Result</button>
                                </div>
                            </div>
                            <div class="col-1-2">

                                <div class="upload-demo-wrap">
                                    <div id="upload-demo"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="images-demo" style="display: inline-block;
    position: relative;">
                @if(\App\Http\Models\BankSetting::get_logo())
                    <img class="bank-logo"
                        src="{{asset('/storage/'.\App\Http\Models\BankSetting::get_logo())}}"
                        style="height: 150px; width: 150px;border-right: 1px solid darkgrey;">
                <span class="delete_logo" style="cursor: pointer; position: absolute; top: 5px; right: 8px;" >
                    <i class="fa fa-times fa-2x" aria-hidden="true"></i></span>
                @endif
                </div>
                <div class="formSep control-group ">
                    <label class="control-label required">
                        @lang('inputs.add_new_logo')
                        <span class="help-block error-logo"></span>
                    </label>
                    <div class="controls">
                        <input type="hidden" name="image" required="required" id='bank_logo'/>
                    </div>

                </div>

                <div class="formSep control-group ">
                    <label class="control-label required">
                        @lang('inputs.add_new_phone_number')
                        <span class="help-block error-phone_number"></span>
                    </label>
                    <div class="controls">
                        <input style="vertical-align: top;" type="text" name="phone_number" required="required"
                               @if($bank) value="{{$bank->phone_number}}" @endif/>
                        <span class="delete_number" style="cursor: pointer;margin-left: 10px"><i class="fa fa-times fa-2x" aria-hidden="true"></i></span>

                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span6">

                    </div>

                    <div class="span6" style="text-align: right">
                        <button class="btn btn-inverse add_logo" type="button">
                            @lang('buttons.save') <i class="icon-ok icon-white"></i>
                        </button>
                    </div>
                </div>

            </form>

        </div>


    </div>


    <div class="modal" id="delete_logo" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    Are you sure you want delete bank logo ?
                    <span class="error delete-error"></span>
                </div>
                <input type="hidden" name="user_id" value="">
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary delete-logo" data-action="" data-id="">Delete
                    </button>
                </div>

            </div>
        </div>
    </div>
    <div class="modal" id="delete_number" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    Are you sure you want delete bank phone number ?
                    <span class="error delete-error"></span>
                </div>
                <input type="hidden" name="user_id" value="">
                <div class="modal-footer">
                    <button type="button" class="" data-dismiss="modal">Cancel</button>
                    <button type="button" class="delete-number" data-action="" data-id="">Delete
                    </button>
                </div>

            </div>
        </div>
    </div>


@endsection
