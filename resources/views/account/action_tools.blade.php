@extends('account.layout')
@section('content')
    <div class="row-fluid">
        <div class="alert alert-success alert_logo" role="alert" style="display: none">
            @lang('global.logo_success')
        </div>
        <div class="container">
            <div class="row">
                <h3 class="heading">
                    @lang('global.action')
                </h3>
                <br>
                <form class="valuate">
                    <div class="form-group col-md-12">

                            <label>USD => Euro</label>
                            <input type="text" class=" form-control input" name="usd_to_euro"
                                   @if($usd_to_euro) value="{{$usd_to_euro}}" @endif
                                   style="margin-right: 20px"/>
                        <label>USD => Ruble</label>

                        <input type="text" class=" form-control input" name="usd_to_ruble"
                                   @if($usd_to_ruble) value="{{$usd_to_ruble}}" @endif

                                   style="margin-right: 20px"/>
                        <label>Euro => Ruble</label>

                        <input type="text" class=" form-control input" name="euro_to_ruble"
                                   @if($euro_to_ruble) value="{{$euro_to_ruble}}" @endif
                                   style="margin-right: 20px"/>


                        <span style="font-size: 20px;margin-left: 20px;color: #555555; padding: 10px"
                              class="rate-result">

                        </span>

                    </div>
                </form>
                <br>
                <div class="devices_filter_box col-lg-12">
                    <button type="button" id="change" class="btn btn-primary">@lang('buttons.save')</button>
                </div>
            </div>

        </div>

    </div>
@endsection
