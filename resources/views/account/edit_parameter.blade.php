@extends('account.layout')
@section('content')
    <style>
         input, textarea {
            width: 500px;
        }
    </style>

    <div class="row-fluid">
        <div class="span12">
            <form id="parameter_form" class="form-horizontal well " method="post">
                <input value="{{$parameter->id}}" type="hidden" name="parameter_id">
                <h3 class="heading">@lang('labels.edit_parameter')</h3>
                <div id="company">
                    <div class="formSep control-group ">
                        <label class="control-label required" for="evabeta_user_username">
                            @lang('labels.parameter_code')
                            <span class="help-block error-parameter_code"></span>
                        </label>
                        <div class="controls">
                            <input type="text" name="parameter_code" required="required"
                                   value="{{$parameter->parameter_code}}"/>
                        </div>
                    </div>

                    <div class="formSep control-group ">
                        <label class="control-label required" for="evabeta_user_username">
                            @lang('labels.units_cases') {{$parameter->units}}
                            <span class="help-block error-units"></span>
                        </label>
                        <div class="controls">
                            <select name="units" required="required">
                                <option @if($parameter->units == 'Number of paid days') selected
                                        @endif value="Number of paid days">Days
                                </option>
                                <option @if($parameter->units == 'Number') selected @endif value="Number">Number
                                </option>
                                <option @if($parameter->units == 'Percentage of days worked by this type of employee from the number of working days') selected
                                        @endif value="Percentage of days worked by this type of employee from the number of working days">
                                    Percentage of days worked by this type of employee from the number of working days
                                </option>
                                <option @if($parameter->units == 'Total number') selected @endif value="Total number">Number
                                </option>
                                <option @if($parameter->units == '') selected @endif value=""></option>
                                <option @if($parameter->units == 'Square meters') selected @endif value="Square meters">m2
                                </option>
                                <option @if($parameter->units == 'Cubic meters') selected @endif value="Cubic meters">m3
                                </option>
                                <option @if($parameter->units == 'Linear meters') selected @endif value="Linear meters">Linear
                                    meters
                                </option>
                                <option @if($parameter->units == '%') selected @endif value="%">%</option>
                                <option @if($parameter->units == 'Cases') selected @endif value="Cases">Cases</option>
                                <option @if($parameter->units == 'Rubles') selected @endif value="Rubles">Euro/Year
                                </option>
                                <option @if($parameter->units == 'Overall capacity in cubic meters') selected
                                        @endif value="Overall capacity in cubic meters">m3
                                </option>
                                <option @if($parameter->units == 'Overall capacity in Kg') selected
                                        @endif value="Overall capacity in Kg">Kg
                                </option>
                                <option @if($parameter->units == 'Capacity in quintals') selected @endif value="Capacity in quintals">
                                    Quintals
                                </option>
                                <option @if($parameter->units == 'Kw') selected @endif value="Kw">Kw</option>
                                <option @if($parameter->units == 'Tonnellate') selected @endif value="Tonnellate">Tons
                                </option>
                                <option @if($parameter->units == 'Giornate retribuite') selected @endif value="Giornate retribuite">
                                    Days
                                </option>
                                <option @if($parameter->units == 'Numero giornate complessive') selected
                                        @endif value="Numero giornate complessive">Days
                                </option>
                                <option @if($parameter->units == 'Capacità in metri cubi') selected
                                        @endif value="Capacità in metri cubi">m3
                                </option>
                                <option @if($parameter->units == 'Liters') selected @endif value="Liters">Liters</option>
                                <option @if($parameter->units == 'Quintals') selected @endif value="Quintals">Quintals
                                </option>
                                <option @if($parameter->units == 'Kg') selected @endif value="Kg">Kg</option>
                                <option @if($parameter->units == 'Kwh') selected @endif value="Kwh">Kw hour</option>
                                <option @if($parameter->units == 'Kilowatt-hour') selected @endif value="Kilowatt-hour">
                                    Кw-hour
                                </option>
                                <option @if($parameter->units == 'Kg.') selected @endif value="Kg.">Kg</option>
                                <option @if($parameter->units == 'Number of items purchased during the year') selected
                                        @endif value="Number of items purchased during the year">Number
                                </option>
                                <option @if($parameter->units == 'Percentage of revenues') selected
                                        @endif value="Percentage of revenues">%
                                </option>
                                <option @if($parameter->units == 'Grammi') selected @endif value="Grammi">Gramm</option>
                                <option @if($parameter->units == 'Euro/m2') selected @endif value="Euro/m2">Euro/m2
                                </option>
                                <option @if($parameter->units == 'Altezza massima raggiungibile in metri') selected
                                        @endif value="Altezza massima raggiungibile in metri">meters
                                </option>
                                <option @if($parameter->units == 'Numero incarichi scritti') selected
                                        @endif value="Numero incarichi scritti">Number
                                </option>
                                <option @if($parameter->units == 'Numero incarichi verbali') selected
                                        @endif value="Numero incarichi verbali">Number
                                </option>
                                <option @if($parameter->units == 'Numero trattative concluse') selected
                                        @endif value="Numero trattative concluse">Number
                                </option>
                                <option @if($parameter->units == 'numero') selected @endif value="numero">number</option>
                                <option @if($parameter->units == 'Euro per unità') selected @endif value="Euro per unità">Euro
                                </option>
                                <option @if($parameter->units == 'Portata passeggeri (numero)') selected
                                        @endif value="Portata passeggeri (numero)">Number
                                </option>
                                <option @if($parameter->units == 'KM percorsi') selected @endif value="KM percorsi">Km
                                </option>
                                <option @if($parameter->units == 'Km') selected @endif value="Km">Km</option>
                                <option @if($parameter->units == 'Persone/ora') selected @endif value="Persone/ora">Men/Hour
                                </option>
                                <option @if($parameter->units == 'Numero degli incarichi') selected
                                        @endif value="Numero degli incarichi">Number
                                </option>
                                <option @if($parameter->units == 'Numero eventi') selected @endif value="Numero eventi">Number
                                </option>
                                <option @if($parameter->units == 'Potenza') selected @endif value="Potenza">Power</option>
                                <option @if($parameter->units == 'metri cubi') selected @endif value="metri cubi">m3</option>
                                <option @if($parameter->units == 'Totale KW"') selected @endif value="Totale KW">Kw</option>
                                <option @if($parameter->units == 'Numero incarichi') selected @endif value="Numero incarichi">Number</option>
                                <option @if($parameter->units == 'Numero annuo di pratiche') selected @endif value="Numero annuo di pratiche">Number</option>
                                <option @if($parameter->units == 'Numero annuo') selected @endif value="Numero annuo">Number</option>
                                <option @if($parameter->units == 'Totale m3') selected @endif value="Totale m3">Total m3</option>
                                <option @if($parameter->units == 'Capacità totale m3') selected @endif value="Capacità totale m3">m3</option>
                                <option @if($parameter->units == 'Numero al chiuso') selected @endif value="Numero al chiuso">Number</option>
                                <option @if($parameter->units == 'Numero all’aperto') selected @endif value="Numero all’aperto">Number</option>
                                <option @if($parameter->units == 'Capacity number of chickens') selected @endif value="Capacity number of chickens">Number of chickens</option>
                                <option @if($parameter->units == 'Capacity number of trays 40X60') selected @endif value="Capacity number of trays 40X60">Number of trays 40X60</option>
                                <option @if($parameter->units == 'di cui Numero autonegozi') selected @endif value="di cui Numero autonegozi">number</option>
                                <option @if($parameter->units == 'Numero vaschette esposte') selected @endif value="Numero vaschette esposte">Number</option>
                                <option @if($parameter->units == 'Numero gruppi caffà') selected @endif value="Numero gruppi caffà">Number</option>
                                <option @if($parameter->units == 'Numero (di cui autonegozi)') selected @endif value="Numero (di cui autonegozi)">Number</option>
                                <option @if($parameter->units == 'Capacità complessiva in numero teglie') selected @endif value="Capacità complessiva in numero teglie">Number of trays</option>
                                <option @if($parameter->units == 'N. giornate retribuite') selected @endif value="N. giornate retribuite">Days</option>
                                <option @if($parameter->units == 'Numero ore') selected @endif value="Numero ore">Hours</option>
                                <option @if($parameter->units == 'di cui attrezzati per soccorso stradale (escluse le autogrà)') selected @endif value="di cui attrezzati per soccorso stradale (escluse le autogrà)">number
                                </option>
                                <option @if($parameter->units == 'Anno di immatricolazione dell’automezzo più nuovo') selected @endif value="Anno di immatricolazione dell’automezzo più nuovo">Year</option>
                                <option @if($parameter->units == 'Giornate lavorate') selected @endif value="Giornate lavorate">Days</option>
                                <option @if($parameter->units == 'Capacità di carico in Kg') selected @endif value="Capacità di carico in Kg">Kg</option>
                                <option @if($parameter->units == 'Kg vapore per ora') selected @endif value="Kg vapore per ora">Kg of vapour/hour</option>
                                <option @if($parameter->units == 'Unità sterili') selected @endif value="Unità sterili">Number</option>
                                <option @if($parameter->units == 'Numero ore lavorate nel periodo d`imposta') selected @endif value="Numero ore lavorate nel periodo d`imposta">Hours of work</option>
                                <option @if($parameter->units == 'Numero di ore lavorate nel periodo d`imposta') selected @endif value="Numero di ore lavorate nel periodo d`imposta">Hours of work</option>
                                <option @if($parameter->units == 'Metri quadrati') selected @endif value="Metri quadrati">m2</option>
                                <option @if($parameter->units == 'Mc') selected @endif value="Mc">m3</option>
                                <option @if($parameter->units == 'Tonnellate/24 ore') selected @endif value="Tonnellate/24 ore">Tons/24 hours</option>
                                <option @if($parameter->units == 'Numero di giornate retribuite') selected @endif value="Numero di giornate retribuite">Days</option>
                                <option @if($parameter->units == 'di cui commercializzati nell`anno') selected @endif value="di cui commercializzati nell`anno">number</option>
                                <option @if($parameter->units == 'Lt') selected @endif value="Lt">Liters</option>
                            </select>
                        </div>
                    </div>
                    <div class="formSep control-group ">
                        <label class="control-label required" for="evabeta_user_username">
                            @lang('labels.field_type')
                            <span class="help-block error-value_type"></span>
                        </label>
                        <div class="controls">
                            <select name="value_type" required="required">
                                <option value="N1" @if($parameter->value_type == 'N1') selected @endif>N1</option>
                                <option value="N2" @if($parameter->value_type == 'N2') selected @endif>N2</option>
                                <option value="N3" @if($parameter->value_type == 'N3') selected @endif>N3</option>
                                <option value="N4" @if($parameter->value_type == 'N4') selected @endif>N4</option>
                                <option value="N5" @if($parameter->value_type == 'N5') selected @endif>N5</option>
                                <option value="N10" @if($parameter->value_type == 'N10') selected @endif>N10</option>
                                <option value="N11" @if($parameter->value_type == 'N11') selected @endif>N11</option>
                                <option value="N12" @if($parameter->value_type == 'N12') selected @endif>N12</option>
                                <option value="N13" @if($parameter->value_type == 'N13') selected @endif>N13</option>
                                <option value="CB" @if($parameter->value_type == 'CB') selected @endif>CB</option>
                                <option value="CBN" @if($parameter->value_type == 'CBN') selected @endif>CBN</option>
                                <option value="CAT" @if($parameter->value_type == 'CAT') selected @endif>CAT</option>
                                <option value="PC" @if($parameter->value_type == 'PC') selected @endif>PC</option>
                                <option value="VAL" @if($parameter->value_type == 'VAL') selected @endif>VAL</option>
                            </select>
                        </div>
                    </div>
                    <div class="formSep control-group ">
                        <label class="control-label required" for="evabeta_user_username">
                            @lang('labels.english')
                            <span class="help-block error-description"></span>
                        </label>
                        <div class="controls">
                            <textarea name="description_en">{{$parameter->description_en}}</textarea>
                        </div>
                    </div>
                    <div class="formSep control-group ">
                        <label class="control-label required" for="evabeta_user_username">
                            @lang('labels.italian')
                            <span class="help-block error-description_it"></span>
                        </label>
                        <div class="controls">
                            <textarea name="description_it">{{$parameter->description_it}}</textarea>
                        </div>
                    </div>
                    <div class="formSep control-group ">
                        <label class="control-label required" for="evabeta_user_username">
                            @lang('labels.russian')
                            <span class="help-block error-description_ru"></span>
                        </label>
                        <div class="controls">
                            <textarea name="description_ru">{{$parameter->description_ru}}</textarea>
                        </div>
                    </div>
                    <div class="formSep control-group ">
                        <label class="control-label required" for="evabeta_user_username">
                            @lang('labels.path_english')
                            <span class="help-block error-path"></span>
                        </label>
                        <div class="controls">
                            <textarea name="path_en">{{$parameter->path_en}}</textarea>
                        </div>
                    </div>
                    <div class="formSep control-group ">
                        <label class="control-label required" for="evabeta_user_username">
                            @lang('labels.path_russian')
                            <span class="help-block error-path_ru"></span>
                        </label>
                        <div class="controls">
                            <textarea name="path_ru">{{$parameter->path_ru}}</textarea>
                        </div>
                    </div>
                    <div class="formSep control-group ">
                        <label class="control-label required" for="evabeta_user_username">
                            @lang('labels.path_italian')
                            <span class="help-block error-path_it"></span>
                        </label>
                        <div class="controls">
                            <textarea name="path_it">{{$parameter->path_it}}</textarea>
                        </div>
                    </div>
                    <div class="formSep control-group ">
                        <label class="control-label required" for="evabeta_user_username">
                            @lang('labels.visible')
                            <span class="help-block error-visible"></span>
                        </label>
                        <div class="controls">
                            <input type="checkbox" name="visible" @if($parameter->visible == 1) checked @endif>
                        </div>
                    </div>


                </div>
            </form>
        </div>


    </div>
    <div class="row-fluid">
        <div class="span6">

        </div>

        <div class="span6" style="text-align: right">
            <button class="btn btn-inverse update_parameter">
                @lang('buttons.save') <i class="icon-ok icon-white"></i>
            </button>
        </div>
    </div>




@endsection
