<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8"/>
    <title>Document</title>
</head>
<style>
    body {
        font-family: DejaVu Sans !important;
    }
    table, tr, th, td{
        border: 1px solid black;
    }
    table{
        border-collapse: collapse;
    }
</style>
<body>

<table>
    <tr>
        <th>Region Name</th>
        <th>Region Code</th>
        <th>Industry Code</th>
        <th>Micro</th>
        <th>Small</th>
    </tr>
    @foreach($data as $key=>$value)
        <tr>
            <td>{{ $value->region_name }}</td>
            <td>{{ $value->region_code }}</td>
            <td>{{ $value->industry_group_code }}</td>
            <td>{{ $value->micro }}</td>
            <td>{{ $value->small }}</td>
        </tr>
    @endforeach
</table>

</body>
</html>
