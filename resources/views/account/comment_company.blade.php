@extends('account.layout')
@section('content')
    <div class="row-fluid">
        <div class="span12">
            <form class="form-horizontal new_comment">

                <h3 class="heading">
                    @lang('labels.company') {{$company->company_name}}:
                  @lang('global.add_new_comment') </h3>


                <div id="comment">
                    <div class="formSep control-group ">
                        <label class="control-label required" for="comment_text">@lang('labels.text')<span
                                    class="help-block"></span>
                            <span class="help-block error-text"></span>
                        </label>


                        <div class="controls">
                            <textarea name="text" class="span10" rows="7"></textarea>
                        </div>
                    </div>
                    <input type="hidden" name="company_id" value="{{$company->ID_company}}"/></div>

                <div class="row-fluid">
                    <div class="span6">
                        <a href="/{{App::getLocale()}}/company/view/{{$company->ID_company}}" class="btn button-prev">
                            <i class="icon-chevron-left icon-white"></i> @lang('buttons.exit') </a>
                        <span class="loader button-next"><i class="loader-icon"></i></span>
                    </div>

                    <div class="span6">
                        <button class="btn btn-inverse save_comment"  type="button" style="float: right">
                            @lang('buttons.save') <i class="icon-ok icon-white"></i>
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>



@endsection