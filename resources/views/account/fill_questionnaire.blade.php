@extends('account.layout')
@section('content')
    <style>
        .table th, .table td {
            border-top: none !important;
            border-bottom: 1px solid #ddd;
        }
        .upload_file_list > tr, th, td{
            border-top: none !important;
            border-bottom: 1px solid #ddd;
            text-align: center;
        }
        .upload_file_list table{
            width: 100%;
            height: 100%;

        }
        .upload_file_list tr, td{
            font-size: 16px;
            font-weight: 500;
        }
        .files{
            margin-top: 30px;
            width: 70%;
            display: flex;
            justify-content: space-between;
        }
        .files .upload_file_list{
            width: 40%;
        }
    </style>
    @php
        $checkUser = \Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_SUPERVISOR);
    @endphp
    <div id="loading_layer" style="display:none"><img src="/site/img/ajax_loader.gif"/></div>
    <div class="row-fluid">
        <div class="span12">
            <h3 class="heading" style="display: inline">@lang('labels.edit_questionnaire')</h3>
            {{--            <div class="alert alert-danger" role="alert" style="display: none">--}}

            {{--            </div>--}}
            <a href="/{{\Illuminate\Support\Facades\App::getLocale()}}/account" style="color: black; font-size: 26px; float: right"><i class="fas fa-times"></i></a>
            <div class="well" style="margin-top: 20px">

                <div class="row-fluid">
                    <div class="span6">
                        <a href="/{{App::getLocale()}}/company/view/{{$questionnaire->company->ID_company}}"
                           class="btn button-prev ">
                            <i class="icon-chevron-left icon-white"></i> @lang('buttons.exit') </a>

                    </div>
                    <div class="span6">
                        <div class="btn-group pull-right">

                            @if(!$checkUser)
                                <button class="btn fill_questionnaire" type="button">
                                    <i class="icon-pencil"></i>
                                    @lang('buttons.ready')
                                </button>
                            @else
                                <a href="/{{App::getLocale()}}/company/print/{{$questionnaire->company->ID_company}}/{{$questionnaire->id}}/{{$questionnaire->sose_ateco->id}}" class="btn fill_questionnaire" type="button">
                                    <i class="icon-pencil"></i>
                                    @lang('buttons.download_pdf')
                                </a>
                            @endif



                        </div>
                    </div>

                </div>
                @php
                    $ateco_description = explode("[", $questionnaire->sose_ateco->name)
                @endphp
                <table id="questionnaireTable" class="table" style="margin-top: 20px">

                    <tr>
                        <th>@lang('labels.company_name'):</th>
                        <td>{{$questionnaire->company->company_name}}</td>
                    </tr>
                    <tr>
                        <th>@lang('labels.credit_request_id'):</th>
                        <td>{{$questionnaire->company->INN_number}}</td>
                    </tr>
                    <tr>
                        <th>@lang('labels.ateco_code'):</th>
                        <td>{{$questionnaire->sose_ateco->ateco_code}} - {{$ateco_description[0]}} -
                            ({{$questionnaire->sose_study->study_code}})
                        </td>
                    </tr>
                    <tr>
                        <th>@lang('labels.questionnaire_status'):</th>
                        <td>@if(!$questionnaire->status_code_id)
                                - @else {{\App\Http\Models\SoseQuestionnaireStatus::get_by_id($questionnaire->status_code_id)->description}} @endif</td>
                    </tr>
                </table>

                <div class="tabbable" style="margin-top: 0px;">

                    <ul id="tabParameters" class="nav nav-tabs" style="margin:0px;">
                        @if(array_key_exists('O', $parameters) && \Illuminate\Support\Facades\Auth::user()->role_id != \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_FINAL_CLIENT))
                            <li data-id='tab0' class="active"><a id="error-tab0" href="#">@lang('labels.group') 0</a>
                            </li> @endif
                        @if(array_key_exists('A', $parameters))
                            <li data-id='tabA'
                                @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_FINAL_CLIENT)) class="active" @endif>
                                <a href="#" id="error-tabA">@lang('labels.group') A</a></li> @endif
                        @if(count($b_units))
                            <li data-id='tabB'><a id="error-tabB" href="#">@lang('labels.group') B</a></li> @endif
                        @if(array_key_exists('C', $parameters) )
                            <li data-id='tabC'><a id="error-tabC" href="#">@lang('labels.group') C</a></li> @endif
                        @if(array_key_exists('D', $parameters) )
                            <li data-id='tabD'><a id="error-tabD" href="#">@lang('labels.group') D</a></li> @endif
                        @if(array_key_exists('E', $parameters) )
                            <li data-id='tabE'><a id="error-tabE" href="#">@lang('labels.group') E</a></li> @endif
                        @if(array_key_exists('F', $parameters) )
                            <li data-id='tabF'><a id="error-tabF" href="#">@lang('labels.group') F</a></li> @endif
                        @if(array_key_exists('L', $parameters) )
                            <li data-id='tabL'><a id="error-tabL" href="#">@lang('labels.group') L</a></li> @endif
                        @if(array_key_exists('Z', $parameters) )
                            <li data-id='tabZ'><a id="error-tabZ" href="#">@lang('labels.group') Z</a></li> @endif


                    </ul>
                    <form id="questionnaire_form">
                        <input type="hidden" name="sose_study_id" value="{{$questionnaire->sose_study->id}}">

                        <div id="tab-content" class="tab-content">
                            @if(\Illuminate\Support\Facades\Auth::user()->role_id != \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_FINAL_CLIENT))
                                <div class="tab-pane tab-pane-sose active" id="tab0">
                                    <table id="questionnaireTable" class="table" style="margin-top: 20px">
                                        @if( array_key_exists('O', $parameters) )

                                            @foreach($parameters['O'] as $a => $b)
                                                @if($loop->first)
                                                    <tr>
                                                        <td colspan="4" style="border: none!important;">
                                                            <h2>{{$b[0]->sose_param->param_path}}</h2></td>
                                                    </tr>
                                                @endif
                                                @php

                                                    $count = 0;
                                                @endphp
                                                @foreach($b as $parameter)


                                                    @php $count++; @endphp
                                                    @if(strpos($a, 'nosub') === false  && $count < 2)
                                                        <tr>
                                                            <td colspan="4" style="border: none!important;">
                                                                <h3 class="heading"><b>{{$a}}</b></h3>
                                                            </td>
                                                        </tr>


                                                    @endif

                                                    @if((\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_BACK_OFFICE) || \Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN)) ||  $parameter->sose_param->visible == 1)

                                                        <tr>
                                                            <td style="width: 60%">{{$parameter->sose_param->desc}}</td>
                                                            <td>{{$parameter->sose_param->parameter_code}}</td>

                                                            <td style="line-height: 30px !important">
                                                                <input class="txtNumber delimetr_number"
                                                                       @if($parameter->sose_param->value_type == 'CB') type="checkbox"
                                                                       @else type="text"
                                                                       @endif name="parameter_id[{{$parameter->sose_param->id}}]"
                                                                       @if(isset($values))
                                                                       @foreach($values as $value) @if($value->parameter_id == $parameter->sose_param->id ) value="{{$value->value}}"
                                                                       @endif @endforeach
                                                                       @endif
                                                                       data-id="{{$parameter->sose_param->id}}"
                                                                       data-code="{{$parameter->sose_param->parameter_code}}"
                                                                       onblur="myFunction(this);"
                                                                       @if($checkUser) disabled @endif
                                                                ><span
                                                                    class="help-inline">{{$parameter->sose_param->unit}}</span>
                                                                <br>
                                                                <span style="max-width: 200px; display:none"
                                                                      class="alert alert-danger validation-error-span validation-error-{{$parameter->sose_param->parameter_code}}"></span>

                                                            </td>
                                                            <td>
                                                                @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_BACK_OFFICE) || \Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN))
                                                                    <a target="_blank"
                                                                       href="/{{App::getLocale()}}/company/edit-parameter/{{$parameter->sose_param->id}}"
                                                                       class="btn btn-mini"><i class="icon-pencil"></i></a>
                                                                    <input type="checkbox" class="visible"
                                                                           value="{{$parameter->sose_param->id}}"
                                                                           class="visible"
                                                                           value="{{$parameter->sose_param->id}}"
                                                                           class="visible"
                                                                           value="{{$parameter->sose_param->id}}"
                                                                           @if($parameter->sose_param->visible==1 ) checked @endif
                                                                           @if($checkUser) disabled @endif>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @endforeach
                                        @endif

                                    </table>
                                </div>
                            @endif
                            <div
                                class="tab-pane tab-pane-sose @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_FINAL_CLIENT))  active @endif"
                                id="tabA">

                                <table id="questionnaireTable" class="table" style="margin-top: 20px">
                                    @if(array_key_exists('A', $parameters))

                                        @foreach($parameters['A'] as $a => $b)
                                            @php
                                                $path =explode("|", $b[0]->sose_param->param_path)
                                            @endphp
                                            @if($loop->first)
                                                <tr>
                                                    <td colspan="4" style="border: none!important;">
                                                        <h2>{{$path[0]}}</h2></td>
                                                </tr>
                                            @endif
                                            @php

                                                $count = 0;
                                            @endphp
                                            @foreach($b as $parameter)


                                                @php $count++; @endphp
                                                @if(strpos($a, 'nosub') === false  && $count < 2)
                                                    <tr>
                                                        <td colspan="4" style="border: none!important;">
                                                            <h3 class="heading"><b>{{$a}}</b></h3>
                                                        </td>
                                                    </tr>


                                                @endif
                                                @if((\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_BACK_OFFICE) || \Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN)) || $parameter->sose_param->visible == 1)

                                                    <tr>
                                                        <td style="width: 60%">{{$parameter->sose_param->desc}}</td>
                                                        <td>{{$parameter->sose_param->parameter_code}}</td>
                                                        <td>
                                                            <input class="delimetr_number"
                                                                   @if($parameter->sose_param->value_type == 'CB') type="checkbox"
                                                                   @else type="text" @endif style="width: 50%"
                                                                   name="parameter_id[{{$parameter->sose_param->id}}]"
                                                                   @if(isset($values)) @foreach($values as $value) @if($value->parameter_id == $parameter->sose_param->id ) value="{{$value->value}}"
                                                                   @endif @endforeach @endif
                                                                   data-id="{{$parameter->sose_param->id}}"
                                                                   data-code="{{$parameter->sose_param->parameter_code}}"
                                                                   onblur="myFunction(this);"
                                                                   @if($checkUser) disabled @endif
                                                            > <span
                                                                class="help-inline">{{$parameter->sose_param->units == '%' ? '%' : $parameter->sose_param->unit}}</span>
                                                            <br>
                                                            <span style="max-width: 200px; display:none"
                                                                  class="alert alert-danger validation-error-span validation-error-{{$parameter->sose_param->parameter_code}}"></span>

                                                        </td>
                                                        <td style="width: 7%">
                                                            @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_BACK_OFFICE) || \Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN))
                                                                <a target="_blank"
                                                                   href="/{{App::getLocale()}}/company/edit-parameter/{{$parameter->sose_param->id}}"
                                                                   class="btn btn-mini"><i class="icon-pencil"></i></a>
                                                                <input type="checkbox" class="visible"
                                                                       value="{{$parameter->sose_param->id}}"
                                                                       class="visible"
                                                                       value="{{$parameter->sose_param->id}}"
                                                                       class="visible"
                                                                       value="{{$parameter->sose_param->id}}"
                                                                       @if($parameter->sose_param->visible==1 ) checked @endif
                                                                       @if($checkUser) disabled @endif>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    @endif

                                </table>
                            </div>


                            <div class="tab-pane tab-pane-sose " id="tabB">
                                <table id="questionnaireTable" class="table" style="margin-top: 20px">
                                    @if(count($b_units))

                                        <h2>{{$first->sose_param->param_path}}</h2>
                                        @if((\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_BACK_OFFICE) || \Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN)) || $parameter->sose_param->visible == 1)
                                            <tr>

                                                <td style="width: 60%">{{$first->sose_param->desc}}</td>
                                                <td>{{$first->sose_param->parameter_code}}</td>
                                                <td>
                                                    <input class="delimetr_number"
                                                           id="{{$first->sose_param->parameter_code}}"
                                                           @if($parameter->sose_param->value_type == 'CB') type="checkbox"
                                                           @else type="text" @endif
                                                           style="width: 50%"
                                                           name="parameter_id[{{$first->sose_param->id}}]"
                                                           @if(count($b_units) > 1) value="{{$questionnaire->company_unit}}" @else value="" @endif
                                                           data-id="{{$first->sose_param->id}}"
                                                           data-code="{{$first->sose_param->parameter_code}}"
                                                           onblur="myFunction(this);"
                                                           @if($checkUser) disabled @endif>
                                                    <span
                                                        class="help-inline">{{$first->sose_param->unit}}</span>
                                                    <br>
                                                    <span style="max-width: 200px; display:none"
                                                          class="alert alert-danger validation-error-span validation-error-{{$first->sose_param->parameter_code}}"></span>

                                                </td>
                                                <td style="width: 7%;">
                                                    @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_BACK_OFFICE) || \Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN))
                                                        <a target="_blank"
                                                           href="/{{App::getLocale()}}/company/edit-parameter/{{$first->sose_param->id}}"
                                                           class="btn btn-mini"><i
                                                                class="icon-pencil"></i></a>
                                                        <input type="checkbox" class="visible"
                                                               value="{{$first->sose_param->id}}"
                                                               class="visible"
                                                               value="{{$first->sose_param->id}}"
                                                               class="visible"
                                                               value="{{$first->sose_param->id}}"
                                                               @if($first->sose_param->visible==1 ) checked @endif
                                                               @if($checkUser) disabled @endif>
                                                    @endif
                                                </td>
                                            </tr>
                                            @php
                                                $key=0;
                                                $b_count = 0;
                                                $previous_subheading = 'nosub';
                                            @endphp
                                            @foreach($b_units as $parameter)

                                                @if($parameter->sose_param->value_type== "CAT" )
                                                    @php $key++; $b_count = 0; $previous_subheading = 'nosub';@endphp
                                                @endif

                                                @if($key<=$questionnaire->company_unit)

                                                    @if ($parameter->sose_param->value_type== "CAT")

                                                        <tr>
                                                            <td style="width:70%;font-size:16px;border: none!important;">
                                                                <span style="font-weight: bold">@lang('global.structural_unit') N. {{$key}}</span>
                                                            </td>

                                                        </tr>
                                                    @endif

                                                    @php

                                                        $find_subheading = explode("|", $parameter->sose_param->param_path);
                                                        if (isset($find_subheading[1]) && (strlen($find_subheading[1]) > 0 && strlen(trim($find_subheading[1])) != 0))
                                                        {
                                                            $find_subheading = $find_subheading[1];
                                                            if ($find_subheading != $previous_subheading)
                                                                {
                                                                    $b_count = 0;
                                                                }else{
                                                                     $b_count++;
                                                                }


                                                        }else{

                                                            $find_subheading = 'nosub';
                                                        }
                                                    @endphp



                                                    @if((strpos($find_subheading, 'nosub') === false && $b_count < 1) || ($b_count == 1 && $find_subheading != $previous_subheading) )

                                                        <tr>
                                                            <td colspan="4" style="border: none!important;">
                                                                <h3 class="heading"><b>{{$find_subheading}} </b></h3>
                                                            </td>
                                                        </tr>
                                                        @php $previous_subheading = $find_subheading @endphp


                                                    @endif



                                                    @if((\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_BACK_OFFICE) || \Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN)) || $parameter->sose_param->visible == 1)

                                                        <tr>

                                                            <td style="width: 60%">{{$parameter->sose_param->desc}}</td>
                                                            <td>{{$parameter->sose_param->parameter_code}}</td>
                                                            <td>
                                                                <input class="delimetr_number"
                                                                       @if($parameter->sose_param->value_type == 'CB') type="checkbox"
                                                                       @else type="text"
                                                                       @endif style="width: 50%"
                                                                       name="parameter_id[{{$parameter->sose_param->id}}]"
                                                                       @if(isset($values))
                                                                       @foreach($values as $value)
                                                                       @if($value->parameter_id == $parameter->sose_param->id ) value="{{$value->value}}"
                                                                       @endif @endforeach @endif
                                                                       data-id="{{$parameter->sose_param->id}}"
                                                                       data-code="{{$parameter->sose_param->parameter_code}}"
                                                                       onblur="myFunction(this);"
                                                                       @if($checkUser) disabled @endif><span
                                                                    class="help-inline">{{$parameter->sose_param->unit}}</span>
                                                                <br>
                                                                <span style="max-width: 200px; display:none"
                                                                      class="alert alert-danger validation-error-span validation-error-{{$parameter->sose_param->parameter_code}}"></span>

                                                            </td>
                                                            <td style="width: 7%;">
                                                                @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_BACK_OFFICE) || \Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN))
                                                                    <a target="_blank"
                                                                       href="/{{App::getLocale()}}/company/edit-parameter/{{$parameter->sose_param->id}}"
                                                                       class="btn btn-mini"><i
                                                                            class="icon-pencil"></i></a>
                                                                    <input type="checkbox" class="visible"
                                                                           value="{{$parameter->sose_param->id}}"
                                                                           class="visible"
                                                                           value="{{$parameter->sose_param->id}}"
                                                                           class="visible"
                                                                           value="{{$parameter->sose_param->id}}"
                                                                           @if($parameter->sose_param->visible==1 ) checked @endif
                                                                           @if($checkUser) disabled @endif>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endif
                                            @endforeach
                                        @endif
                                    @endif

                                </table>
                            </div>


                            <div class="tab-pane tab-pane-sose " id="tabC">

                                <table id="questionnaireTable" class="table" style="margin-top: 20px">
                                    @if(array_key_exists('C', $parameters))

                                        @foreach($parameters['C'] as $a => $b)
                                            @php
                                                $path =explode("|", $b[0]->sose_param->param_path)
                                            @endphp
                                            @if($loop->first)
                                                <tr>
                                                    <td colspan="4" style="border: none!important;">
                                                        <h2>{{$path[0]}}</h2></td>
                                                </tr>
                                            @endif
                                            @php

                                                $c = 0;
                                            @endphp
                                            @foreach($b as $parameter)


                                                @php $c++; @endphp
                                                @if(strpos($a, 'nosub') === false  && $c < 2)
                                                    <tr>
                                                        <td colspan="4" style="border: none!important;">
                                                            <h3 class="heading"><b>{{$a}}</b></h3>
                                                        </td>
                                                    </tr>


                                                @endif
                                                @if((\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_BACK_OFFICE) || \Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN)) || $parameter->sose_param->visible == 1)
                                                    <tr>
                                                        <td style="width: 60%">{{$parameter->sose_param->desc}}</td>
                                                        <td>{{$parameter->sose_param->parameter_code}}</td>
                                                        <td>
                                                            <input class="delimetr_number"
                                                                   @if($parameter->sose_param->value_type == 'CB') type="checkbox"
                                                                   @else type="text" @endif style="width: 50%"
                                                                   name="parameter_id[{{$parameter->sose_param->id}}]"
                                                                   @if(isset($values)) @foreach($values as $value) @if($value->parameter_id == $parameter->sose_param->id ) value="{{$value->value}}"
                                                                   @endif @endforeach @endif
                                                                   data-id="{{$parameter->sose_param->id}}"
                                                                   data-code="{{$parameter->sose_param->parameter_code}}"
                                                                   onblur="myFunction(this);"
                                                                   @if($checkUser) disabled @endif
                                                            >
                                                            <span
                                                                class="help-inline">{{$parameter->sose_param->unit}}</span>
                                                            <br>
                                                            <span style="max-width: 200px; display:none"
                                                                  class="alert alert-danger validation-error-span validation-error-{{$parameter->sose_param->parameter_code}}"></span>

                                                        </td>
                                                        <td style="width: 7%;">
                                                            @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_BACK_OFFICE) || \Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN))
                                                                <a target="_blank"
                                                                   href="/{{App::getLocale()}}/company/edit-parameter/{{$parameter->sose_param->id}}"
                                                                   class="btn btn-mini"><i class="icon-pencil"></i></a>
                                                                <input type="checkbox" class="visible"
                                                                       value="{{$parameter->sose_param->id}}"
                                                                       class="visible"
                                                                       value="{{$parameter->sose_param->id}}"
                                                                       class="visible"
                                                                       value="{{$parameter->sose_param->id}}"
                                                                       @if($parameter->sose_param->visible==1 ) checked @endif
                                                                       @if($checkUser) disabled @endif>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    @endif
                                </table>
                            </div>
                            <div class="tab-pane tab-pane-sose " id="tabD">

                                <table id="questionnaireTable" class="table" style="margin-top: 20px">
                                    @if(array_key_exists('D', $parameters))

                                        @foreach($parameters['D'] as $a => $b)
                                            @php
                                                $path = explode("|", $b[0]->sose_param->param_path)
                                            @endphp
                                            @if($loop->first)
                                                <tr>
                                                    <td colspan="4" style="border: none!important;">
                                                        <h2>{{$path[0]}}</h2></td>
                                                </tr>
                                            @endif
                                            @php

                                                $d = 0;
                                            @endphp
                                            @foreach($b as $parameter)


                                                @php $d++; @endphp
                                                @if(strpos($a, 'nosub') === false  && $d < 2)
                                                    <tr>
                                                        <td colspan="4" style="border: none!important;">
                                                            <h3 class="heading"><b>{{$a}}</b></h3>
                                                        </td>
                                                    </tr>


                                                @endif

                                                <tr>


                                                    <td style="width: 60%">{{$parameter->sose_param->desc}}</td>
                                                    <td>{{$parameter->sose_param->parameter_code}}</td>
                                                    <td>
                                                        <input class="delimetr_number"
                                                               @if($parameter->sose_param->value_type == 'CB') type="checkbox"
                                                               @else type="text" @endif style="width: 50%"
                                                               name="parameter_id[{{$parameter->sose_param->id}}]"
                                                               @if(isset($values)) @foreach($values as $value) @if($value->parameter_id == $parameter->sose_param->id )
                                                               value="{{$value->value}}" @endif @endforeach @endif
                                                               data-id="{{$parameter->sose_param->id}}"
                                                               data-code="{{$parameter->sose_param->parameter_code}}"
                                                               onblur="myFunction(this);"
                                                               @if($checkUser) disabled @endif
                                                        >
                                                        <span
                                                            class="help-inline">{{$parameter->sose_param->unit}}</span>
                                                        <br>
                                                        <span style="max-width: 200px; display:none"
                                                              class="alert alert-danger validation-error-span validation-error-{{$parameter->sose_param->parameter_code}}"></span>

                                                    </td>
                                                    <td style="width: 7%;">
                                                        @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_BACK_OFFICE) || \Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN))
                                                            <a target="_blank"
                                                               href="/{{App::getLocale()}}/company/edit-parameter/{{$parameter->sose_param->id}}"
                                                               class="btn btn-mini"><i class="icon-pencil"></i></a>
                                                            <input type="checkbox" class="visible"
                                                                   value="{{$parameter->sose_param->id}}"
                                                                   class="visible"
                                                                   value="{{$parameter->sose_param->id}}"
                                                                   class="visible"
                                                                   value="{{$parameter->sose_param->id}}"
                                                                   @if($parameter->sose_param->visible==1 ) checked @endif
                                                                   @if($checkUser) disabled @endif>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endforeach
                                    @endif

                                </table>
                            </div>
                            <div class="tab-pane tab-pane-sose " id="tabE">

                                <table id="questionnaireTable" class="table" style="margin-top: 20px">
                                    @if(array_key_exists('E', $parameters))

                                        @foreach($parameters['E'] as $a => $b)
                                            @php
                                                $path =explode("|", $b[0]->sose_param->param_path)
                                            @endphp
                                            @if($loop->first)
                                                <tr>
                                                    <td colspan="4" style="border: none!important;">
                                                        <h2>{{$path[0]}}</h2></td>
                                                </tr>
                                            @endif
                                            @php

                                                $count = 0;
                                            @endphp
                                            @foreach($b as $parameter)


                                                @php $count++; @endphp
                                                @if(strpos($a, 'nosub') === false && $count < 2)
                                                    <tr>
                                                        <td colspan="4" style="border: none!important;">
                                                            <h3 class="heading"><b>{{$a}}</b></h3>
                                                        </td>
                                                    </tr>


                                                @endif
                                                @if((\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_BACK_OFFICE) || \Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN)) || $parameter->sose_param->visible == 1)

                                                    <tr>
                                                        <td style="width: 60%">{{$parameter->sose_param->desc}}</td>
                                                        <td>{{$parameter->sose_param->parameter_code}}</td>
                                                        <td>
                                                            <input class="delimetr_number"
                                                                   @if($parameter->sose_param->value_type == 'CB') type="checkbox"
                                                                   @else type="text" @endif style="width: 50%"
                                                                   name="parameter_id[{{$parameter->sose_param->id}}]"
                                                                   @if(isset($values)) @foreach($values as $value) @if($value->parameter_id == $parameter->sose_param->id ) value="{{$value->value}}"
                                                                   @endif @endforeach @endif
                                                                   data-id="{{$parameter->sose_param->id}}"
                                                                   data-code="{{$parameter->sose_param->parameter_code}}"
                                                                   onblur="myFunction(this);"
                                                                   @if($checkUser) disabled @endif
                                                            >
                                                            <span
                                                                class="help-inline">{{$parameter->sose_param->unit}}</span>
                                                            <br>
                                                            <span style="max-width: 200px; display:none"
                                                                  class="alert alert-danger validation-error-span validation-error-{{$parameter->sose_param->parameter_code}}"></span>

                                                        </td>
                                                        <td style="width: 7%;">
                                                            @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_BACK_OFFICE) || \Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN))
                                                                <a target="_blank"
                                                                   href="/{{App::getLocale()}}/company/edit-parameter/{{$parameter->sose_param->id}}"
                                                                   class="btn btn-mini"><i class="icon-pencil"></i></a>
                                                                <input type="checkbox" class="visible"
                                                                       value="{{$parameter->sose_param->id}}"
                                                                       class="visible"
                                                                       value="{{$parameter->sose_param->id}}"
                                                                       class="visible"
                                                                       value="{{$parameter->sose_param->id}}"
                                                                       @if($parameter->sose_param->visible==1 ) checked @endif
                                                                       @if($checkUser) disabled @endif>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    @endif

                                </table>
                            </div>
                            <div class="tab-pane tab-pane-sose" id="tabF">

                                <table id="questionnaireTable" class="table" style="margin-top: 20px">
                                    @if(array_key_exists('F', $parameters))

                                        @foreach($parameters['F'] as $a => $b)
                                            @php
                                                $path =explode("|", $b[0]->sose_param->param_path)
                                            @endphp
                                            @if($loop->first)
                                                <tr>
                                                    <td colspan="4" style="border: none!important;">
                                                        <h2>@if(!strpos($path[0], 'БЛОК F') && Config::get('app.locale') == 'ru')БЛОК F - @endif{{$path[0]}}</h2></td>
                                                </tr>
                                            @endif
                                            @php

                                                $count = 0;
                                            @endphp
                                            @foreach($b as $parameter)


                                                @php $count++; @endphp
                                                @if(strpos($a, 'nosub') === false && $count < 2)
                                                    <tr>
                                                        <td colspan="4" style="border: none!important;">
                                                            <h3 class="heading"><b>{{$a}}</b></h3>
                                                        </td>
                                                    </tr>


                                                @endif
                                                @if((\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_BACK_OFFICE) || \Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN)) || $parameter->sose_param->visible == 1)

                                                    <tr>
                                                        <td style="width: 60%">{{$parameter->sose_param->desc}}</td>
                                                        <td>{{$parameter->sose_param->parameter_code}}</td>
                                                        <td>
                                                            <input class="delimetr_number f_block"
                                                                   @if($parameter->sose_param->value_type == 'CB') type="checkbox"
                                                                   @else type="text" @endif style="width: 50%"
                                                                   name="parameter_id[{{$parameter->sose_param->id}}]"
                                                                   @if(isset($values)) @foreach($values as $value) @if($value->parameter_id == $parameter->sose_param->id ) value="{{$value->value}}"
                                                                   @endif @endforeach @endif
                                                                   data-id="{{$parameter->sose_param->id}}"
                                                                   id="{{$parameter->sose_param->parameter_code}}"
                                                                   data-code="{{$parameter->sose_param->parameter_code}}"
                                                                   onblur="f_block_function(this);"
                                                                   @if($parameter->sose_param->parameter_code=='F02801') disabled @endif
                                                                   @if($checkUser) disabled @endif>
                                                            <span
                                                                class="help-inline">{{$parameter->sose_param->unit}}</span>
                                                            <br>
                                                            <span style="max-width: 200px; display:none"
                                                                  class="alert alert-danger validation-error-span validation-error-{{$parameter->sose_param->parameter_code}}"></span>

                                                        </td>
                                                        <td style="width: 7%;">
                                                            @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_BACK_OFFICE) || \Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN))
                                                                <a target="_blank"
                                                                   href="/{{App::getLocale()}}/company/edit-parameter/{{$parameter->sose_param->id}}"
                                                                   class="btn btn-mini"><i class="icon-pencil"></i></a>
                                                                <input type="checkbox" class="visible"
                                                                       value="{{$parameter->sose_param->id}}"
                                                                       class="visible"
                                                                       value="{{$parameter->sose_param->id}}"
                                                                       class="visible"
                                                                       value="{{$parameter->sose_param->id}}"
                                                                       @if($parameter->sose_param->visible==1 ) checked @endif
                                                                       @if($checkUser) disabled @endif>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    @endif

                                </table>
                            </div>
                            <div class="tab-pane tab-pane-sose " id="tabL">

                                <table id="questionnaireTable" class="table" style="margin-top: 20px">
                                    @if(array_key_exists('L', $parameters))

                                        @foreach($parameters['L'] as $a => $b)
                                            @php
                                                $path =explode("|", $b[0]->sose_param->param_path)
                                            @endphp
                                            @if($loop->first)
                                                <tr>
                                                    <td colspan="4" style="border: none!important;">
                                                        <h2>{{$path[0]}}</h2></td>
                                                </tr>
                                            @endif
                                            @php

                                                $count = 0;
                                            @endphp
                                            @foreach($b as $parameter)


                                                @php $count++; @endphp
                                                @if(strpos($a, 'nosub') === false  && $count < 2)
                                                    <tr>
                                                        <td colspan="4" style="border: none!important;">
                                                            <h3 class="heading"><b>{{$a}}</b></h3>
                                                        </td>
                                                    </tr>


                                                @endif
                                                @if((\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_BACK_OFFICE) || \Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN)) || $parameter->sose_param->visible == 1)

                                                    <tr>
                                                        <td style="width: 60%">{{$parameter->sose_param->desc}}</td>
                                                        <td>{{$parameter->sose_param->parameter_code}}</td>
                                                        <td>
                                                            <input class="delimetr_number"
                                                                   @if($parameter->sose_param->value_type == 'CB') type="checkbox"
                                                                   @else type="text" @endif style="width: 50%"
                                                                   name="parameter_id[{{$parameter->sose_param->id}}]"
                                                                   @if(isset($values)) @foreach($values as $value) @if($value->parameter_id == $parameter->sose_param->id ) value="{{$value->value}}"
                                                                   @endif @endforeach @endif
                                                                   data-id="{{$parameter->sose_param->id}}"
                                                                   data-code="{{$parameter->sose_param->parameter_code}}"
                                                                   onblur="myFunction(this);"
                                                                   @if($checkUser) disabled @endif
                                                            >
                                                            <span
                                                                class="help-inline">{{$parameter->sose_param->unit}}</span>
                                                            <br>
                                                            <span style="max-width: 200px; display:none"
                                                                  class="alert alert-danger validation-error-span validation-error-{{$parameter->sose_param->parameter_code}}"></span>

                                                        </td>
                                                        <td style="width: 7%;">
                                                            @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_BACK_OFFICE) || \Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN))
                                                                <a target="_blank"
                                                                   href="/{{App::getLocale()}}/company/edit-parameter/{{$parameter->sose_param->id}}"
                                                                   class="btn btn-mini"><i class="icon-pencil"></i></a>
                                                                <input type="checkbox" class="visible"
                                                                       value="{{$parameter->sose_param->id}}"
                                                                       class="visible"
                                                                       value="{{$parameter->sose_param->id}}"
                                                                       class="visible"
                                                                       value="{{$parameter->sose_param->id}}"
                                                                       @if($parameter->sose_param->visible==1 ) checked @endif
                                                                       @if($checkUser) disabled @endif>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    @endif
                                </table>
                            </div>
                            <div class="tab-pane tab-pane-sose " id="tabZ">
                                <table id="questionnaireTable" class="table" style="margin-top: 20px">
                                    @if(array_key_exists('Z', $parameters))

                                        @foreach($parameters['Z'] as $a => $b)
                                            @php
                                                $path =explode("|", $b[0]->sose_param->param_path)
                                            @endphp
                                            @if($loop->first)
                                                <tr>
                                                    <td colspan="4" style="border: none!important;">
                                                        <h2>{{$path[0]}}</h2></td>
                                                </tr>
                                            @endif
                                            @php

                                                $count = 0;
                                            @endphp
                                            @foreach($b as $parameter)


                                                @php $count++; @endphp
                                                @if(strpos($a, 'nosub') === false && $count < 2)
                                                    <tr>
                                                        <td colspan="4" style="border: none!important;">
                                                            <h3 class="heading"><b>{{$a}}</b></h3>
                                                        </td>
                                                    </tr>


                                                @endif
                                                @if((\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_BACK_OFFICE) || \Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN)) || $parameter->sose_param->visible == 1)

                                                    <tr>
                                                        <td style="width: 60%">{{$parameter->sose_param->desc}}</td>
                                                        <td>{{$parameter->sose_param->parameter_code}}</td>
                                                        <td>
                                                            <input
                                                                @if($parameter->sose_param->value_type == 'CB') type="checkbox"
                                                                @else type="text" @endif style="width: 50%"
                                                                name="parameter_id[{{$parameter->sose_param->id}}]"
                                                                @if(isset($values)) @foreach($values as $value) @if($value->parameter_id == $parameter->sose_param->id ) value="{{$value->value}}"
                                                                @endif @endforeach @endif
                                                                data-id="{{$parameter->sose_param->id}}"
                                                                data-code="{{$parameter->sose_param->parameter_code}}"
                                                                onblur="myFunction(this);"
                                                                @if($checkUser) disabled @endif
                                                            >
                                                            <span
                                                                class="help-inline">{{$parameter->sose_param->unit}}</span>
                                                            <br>
                                                            <span style="max-width: 200px; display:none"
                                                                  class="alert alert-danger validation-error-span validation-error-{{$parameter->sose_param->parameter_code}}"></span>

                                                        </td>
                                                        <td style="width: 7%;">
                                                            @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_BACK_OFFICE) || \Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN))
                                                                <a target="_blank"
                                                                   href="/{{App::getLocale()}}/company/edit-parameter/{{$parameter->sose_param->id}}"
                                                                   class="btn btn-mini"><i class="icon-pencil"></i></a>
                                                                <input type="checkbox" class="visible"
                                                                       value="{{$parameter->sose_param->id}}"
                                                                       class="visible"
                                                                       value="{{$parameter->sose_param->id}}"
                                                                       class="visible"
                                                                       value="{{$parameter->sose_param->id}}"
                                                                       @if($parameter->sose_param->visible==1 ) checked @endif
                                                                       @if($checkUser) disabled @endif>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    @endif

                                </table>
                            </div>


                            <div class="btn-group pull-right">

                                @if(!$checkUser)
                                    <button id="btnsave" class="btn fill_questionnaire" type="button">
                                        <i class="icon-pencil"></i>
                                        @lang('buttons.ready')
                                    </button>
                                @endif


                            </div>
                        </div>

                    </form>
                    <input type="hidden" value="{{$questionnaire->company->ID_company}}" name="company_id">
                    <input type="hidden" value="{{$questionnaire->id}}" name="questionnaire_id">
                </div>
{{--                <div class="files">--}}
{{--                    <div class="upload_file">--}}
{{--                        <form--}}
{{--                            action="/{{App::getLocale()}}/company/edit/{{$questionnaire->company->ID_company}}/{{$questionnaire->id}}/upload"--}}
{{--                            method="POST" enctype="multipart/form-data">--}}
{{--                            {{ csrf_field() }}--}}
{{--                            <label for="report" style="font-weight: 500 !important;font-size: 18px;">Upload report file(.pdf--}}
{{--                                or .xlsx or .xls)</label>--}}
{{--                            <input type="file" class="custom-file-input" name="report" id="report"--}}
{{--                                   style="display: block !important;margin-top: 15px !important;">--}}
{{--                            <span class="validation_success" style="display: block;font-weight: bold !important; color: green;">--}}
{{--                            @if(session('successMsg'))--}}
{{--                                    {{ session('successMsg') }}--}}
{{--                                @endif--}}
{{--                        </span>--}}
{{--                            <span class="validation_error" style="display: block;font-weight: bold !important; color: red;">--}}
{{--                            @if(session('errorMsg'))--}}
{{--                                    {{ session('errorMsg') }}--}}
{{--                                @endif--}}
{{--                        </span>--}}
{{--                            <input type="text" style="visibility: hidden" name="comp_name" value="{{$questionnaire->company->company_name}}">--}}
{{--                            <button type="submit" id="save_report" class="btn save_file" style="display: block;margin-top: 15px"><i--}}
{{--                                    class="fas fa-check"></i>Save--}}
{{--                            </button>--}}
{{--                        </form>--}}
{{--                    </div>--}}
{{--                    <div class="upload_file_list">--}}
{{--                        <table>--}}
{{--                            @php--}}
{{--                            if (file_exists(base_path('public/site/reports/pdf/'.strtolower($questionnaire->company->company_name).'.pdf'))--}}
{{--                                || file_exists(base_path('public/site/reports/excel/'.strtolower($questionnaire->company->company_name).'.xlsx'))--}}
{{--                                || file_exists(base_path('public/site/reports/excel/'.strtolower($questionnaire->company->company_name).'.xls'))){--}}

{{--                            @endphp--}}
{{--                                <tr>--}}
{{--                                    <th>Type</th>--}}
{{--                                    <th>Name</th>--}}
{{--                                    <th>Delete</th>--}}
{{--                                </tr>--}}
{{--                            @php--}}
{{--                                }--}}
{{--                            @endphp--}}

{{--                            @php--}}
{{--                                if(file_exists(base_path('public/site/reports/pdf/'.strtolower($questionnaire->company->company_name).'.pdf'))){--}}

{{--                            @endphp--}}
{{--                            <tr>--}}
{{--                                <td>PDF</td>--}}
{{--                                <td>{{strtolower($questionnaire->company->company_name).'.pdf'}}</td>--}}
{{--                                <td>--}}
{{--                                    <button type="button" class="btn delete_file" name="{{strtolower($questionnaire->company->company_name).'.pdf'}}/{{$questionnaire->company->ID_company}}/{{$questionnaire->id}}">--}}
{{--                                        <i class="fas fa-trash-alt"></i>--}}
{{--                                    </button>--}}
{{--                                </td>--}}
{{--                            </tr>--}}

{{--                            @php--}}
{{--                                }--}}
{{--                                if (file_exists(base_path('public/site/reports/excel/'.strtolower($questionnaire->company->company_name).'.xlsx'))){--}}

{{--                            @endphp--}}
{{--                            <tr>--}}
{{--                                <td>XLSX</td>--}}
{{--                                <td>{{strtolower($questionnaire->company->company_name).'.xlsx'}}</td>--}}
{{--                                <td>--}}
{{--                                    <button type="button" class="btn delete_file" name="{{strtolower($questionnaire->company->company_name).'.xlsx'}}/{{$questionnaire->company->ID_company}}/{{$questionnaire->id}}"><i class="fas fa-trash-alt"></i></button>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                            @php--}}
{{--                                }--}}
{{--                                if (file_exists(base_path('public/site/reports/excel/'.strtolower($questionnaire->company->company_name).'.xls'))){--}}

{{--                            @endphp--}}
{{--                            <tr>--}}
{{--                                <td>XLS</td>--}}
{{--                                <td>{{strtolower($questionnaire->company->company_name).'.xls'}}</td>--}}
{{--                                <td>--}}
{{--                                    <button type="button" class="btn delete_file" name="{{strtolower($questionnaire->company->company_name).'.xls'}}/{{$questionnaire->company->ID_company}}/{{$questionnaire->id}}"><i class="fas fa-trash-alt"></i></button>--}}
{{--                                </td>--}}
{{--                            </tr>--}}

{{--                            @php--}}
{{--                                };--}}
{{--                            @endphp--}}
{{--                        </table>--}}
{{--                    </div>--}}
{{--                </div>--}}



            </div>
        </div>
    </div>

@endsection
