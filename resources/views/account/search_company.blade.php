@extends('account.layout')
@section('content')
    @if(count($companies))
    <div class="row-fluid">

        <div class="search_page">

            <div class="span12">
                @if(isset($result))
                    <h3 class="heading">
                        <small>@lang('global.search_results_for')</small>

                        {{$result}}
                    </h3>
                @endif
                <div class="well clearfix">
                    <div class="row-fluid">
                        <span style="display: inline-block"> @lang('global.showing') {{$companies->currentPage()}} @lang('global.of') {{$companies->lastPage()}} @lang('global.results')</span>

                        <div id="pagination_numbers">{{$companies->links() }}</div>
                    </div>
                </div>
                @if(count($companies))

                    <div class="search_panel clearfix">
                        @foreach($companies as $key=>$company)
                            <div class="search_item clearfix">
                                <span class="searchNb">{{$key+1}}</span>
                                <div class="search_content" style="padding-left: 10px">
                                    <h4>

                                        <a class="sepV_a"
                                           href="/{{App::getLocale()}}/company/view/{{$company->ID_company}}">{{$company->company_name}}</a>

                                    </h4>

                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif


            </div>


        </div>

    </div>
    @else
        <h2 style="width:40%;margin: auto">@lang('global.no_results_available')</h2>
    @endif
@endsection