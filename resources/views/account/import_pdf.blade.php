@extends('account.layout')
@section('content')
    <div class="row-fluid">
        <div class="alert alert-success alert_logo" role="alert" style="display: none">
            @lang('global.logo_success')
        </div>
        <div class="span12">
            <form  method="POST" class="form-horizontal well" action="/{{App::getLocale()}}/company/import-pdf" enctype="multipart/form-data">
                @csrf
                <h3 class="heading">
                    @lang('inputs.import_pdf')</h3>

                <div class="formSep control-group ">
                    <label class="control-label required">
                        @lang('inputs.choose_pdf_file')
                        <span class="help-block error-bank_name">
                               @if(isset($messages))
                                {{$messages}}
                            @endif

                        </span>
                    </label>
                    <div class="controls">
                        <input type="file" name="pdf" required="required" id='pdf_file'/>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span6">

                    </div>

                    <div class="span6" style="text-align: right">
                        <button class="btn btn-inverse" type="submit">
                            @lang('buttons.save') <i class="icon-ok icon-white"></i>
                        </button>
                    </div>
                </div>

            </form>

        </div>



    </div>



@endsection