<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="{{asset('/site/css/pdfView.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">
    <title>Document</title>
</head>
<body>
<div class="header_logo">
    <div class="logo_first">
        <img src="/site/img/6358224_preview.png" alt="">
    </div>
    <div class="logo_second">
        <img src="/site/img/443059386.png" alt="">
    </div>
</div>
<div class="caption">
    <h1>ОТЧЕТ ДЛЯ КРЕДИТНОГО КОМИТЕТА БАНКА</h1>
</div>
<div class="content">
    <div class="general">
        <h3>Общая информация</h3>
        <p>ИП, Волков, Омская область, г. Омск</p>
        <p>Специализация: Ремонт механической части автомобилей</p>
    </div>
    <div class="check">
        <h3>Проверка входных данных заполненной клиентом анкеты</h3>
        <table class="check_table">
            <tr>
                <th></th>
                <th></th>
                <th class="level">мин. уровень Банка</th>
            </tr>
            <tr>
                <th>
                    <h4>НОРМАЛЬНОСТЬ</h4>
                    <p>
                        Согласованность введенных в анкете данных между собой
                        (доля нормальных показателей)
                    </p>
                </th>
                <th class="procent green_pro">83%</th>
                <th class="procent">75%</th>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <th>
                    <h4>КОГЕРЕНТНОСТЬ</h4>
                    <p>
                        Соответствие введенных данных сегментному и территориальному
                        кластеру компании
                    </p>
                    <p>(совокупная степень попадания в проверочные интервалы)</p>
                </th>
                <th class="procent green_pro">98%</th>
                <th class="procent">75%</th>
            </tr>
        </table>
    </div>
    <div class="quality">
        <div class="attention_logo">
            <img src="/site/img/success.png" alt="">
        </div>
        <div class="attention_text">
            <p>
                Качество входных данных не соответствует установленному Банком уровню – расчет предполагаемого годового дохода и прибыли не произведен
            </p>
        </div>
    </div>
    <div class="chart">
        <div class="chart_caption">
            <h2>Годовой доход и прибыль, соответствующие текущей структуре бизнеса:</h2>
        </div>
        <div class="chart_colors">
            <ul>
                <li class="color color_blue"></li>
                <li>Доход</li>
                <li class="color color_gray"></li>
                <li>Прибыль</li>
            </ul>
        </div>
        <div class="chart_blocks">
            <div class=" block chart_bottom">
                <div class="chart">
                    <canvas id="successChart1" style="max-width: 500px;"></canvas>
                </div>
                <div class="chart_description">
                    <p>Расчет ECOLO</p>
                    <p>(нижняя граница)</p>
                </div>
            </div>
            <div class="block chart_top">
                <div class="chart">
                    <canvas id="successChart2" style="max-width: 500px;"></canvas>
                </div>
                <div class="chart_description">
                    <p>Расчет ECOLO</p>
                    <p>(верхняя граница)</p>
                </div>
            </div>
            <div class="block chart_client">
                <div class="chart">
                    <canvas id="successChart3" style="max-width: 500px;"></canvas>
                </div>
                <div class="chart_description">
                    <p>По заявлению клиента на</p>
                    <p>26/12/2019</p>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/js/mdb.min.js"></script>

<script src="{{asset('/site/js/chart2.js')}}"></script>
</body>
</html>
