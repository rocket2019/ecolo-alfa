@extends('account.layout')
@section('content')
    <div class="row-fluid">
        <div class="span12">
            <h3 class="heading"> @lang('global.ecolo_check_management')</h3>

            <div class="span12">
                <div class="span4">@lang('global.select_a_study')</div>
                <div class="span8">
                    <select style="width: 90%" onchange="location = this.value;" id="study_checks">
                        <option> @lang('global.select_a_study')</option>
                        @foreach($studies as $study)
                            <option value="/{{App::getLocale()}}/admin/study-checks/{{$study->id}}"  @if(isset($study_id) && $study->id == $study_id) selected @endif>{{$study->study_code}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        @if(isset($study_id))
            <input type="hidden" name="study_id" value="{{$study_id}}">
            <div class="span12" style="margin-top: 30px!important;">
                <a style="margin-bottom:10px" href="/{{App::getLocale()}}/admin/add-study-check/{{$study_id}}"
                   class="pull-right btn btn-info">@lang('buttons.add_new_check_for_this_study')</a>
                <h3 class="heading"> @lang('global.checks_define_for_study') <b>{{$study_code}}</b></h3>
                @if(!count($study_checks))
                    <h2 class="info muted" style="text-align: center">@lang('global.no_data')</h2>
                @else
                    <table class="table table-bordered table-striped table_vam" id="dt_gal">
                        <thead>
                        <tr>
                            <th>@lang('labels.pre_check_condition')</th>
                            <th>@lang('labels.validation_formula')</th>
                            <th>@lang('labels.translations')</th>
                            <th>@lang('global.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($study_checks as $study_check)
                            <tr>
                                <td>{{$study_check->pre_check_condition}}</td>
                                <td>{{$study_check->validation_formula}}</td>
                                <td>
                                    @if(isset($study_check->sosecheckvalid->english))
                                        <span class="pull-left label label-success tip"
                                              title="if A01002!=NULL than A01001!=NULL"
                                              placeholder="right center">EN</span>
                                    @endif
                                    @if(isset($study_check->sosecheckvalid->russian))
                                        <span class="pull-left label label-info  tip"
                                              title="if A01002!=NULL than A01001!=NULL"
                                              placeholder="right center">RU</span>
                                    @endif
                                    @if(isset($study_check->sosecheckvalid->italian))
                                        <span class="pull-left label label-warning tip"
                                              title="if A01002!=NULL than A01001!=NULL"
                                              placeholder="right center">IT</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="/{{App::getLocale()}}/admin/{{$study_id}}/edit-study-check/{{$study_check->id}}" class="sepV_a" title="Edit"><i class="icon-pencil"></i></a>
                                    <a href="#" class="sepV_a delete_study_check" title="Edit" data-id = "{{$study_check->id}}"><i class="icon-trash"></i></a>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        @endif

    </div>
    <div class="modal" id="delete_study_check" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    Are you sure you want delete this item ?
                    <span class="error delete-error"></span>
                </div>
                <input type="hidden" name="user_id" value="">
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary delete-study-check" data-action="" data-id="">Delete
                    </button>
                </div>

            </div>
        </div>
    </div>


@endsection
