@extends('account.layout')
@section('content')


    <div class="row-fluid">
        <div class="span12">
            <h3 class="heading">  @if(isset($check)) @lang('labels.edit_check') @else @lang('labels.add_e_new_check')  @endif<b>{{$study_code}}</b></h3>
        </div>

        <div class="span12">
            <form class="well study_check">
                <p class="f_legend">@if(isset($check)) @lang('labels.edit_check') @else @lang('labels.add_e_new_check')  @endif</p>
                <input type="hidden" name="study_id" value="{{$study_id}}">
                @if(isset($check)) <input type="hidden" name="check_id" value="{{$check->sose_check_id}}">@endif
                <div id="sose_check">
                    <div class="formSep control-group"><label class="control-label required">@lang('labels.pre_check_condition')
                            <span class="help">@lang('labels.check_will_be_performed')</span></label>
                        <div class="controls">
                            <input type="text" name="pre_check_condition"
                                   @if(isset($check)) value="{{$check->sosecheck->pre_check_condition}}" @endif><br>
                            <span class="error error-pre_check_condition"></span>

                        </div>
                    </div>
                    <div class="formSep control-group">
                        <label class="control-label required">@lang('labels.validation_formula')
                            <span class="help">@lang('labels.check_will_be_ok')</span></label>
                        <div class="controls">
                            <input type="text" name="validation_formula"
                                   @if(isset($check)) value="{{$check->sosecheck->validation_formula}}" @endif><br>
                            <span class="error error-validation_formula"></span>
                        </div>
                    </div>
                    <div class="formSep control-group ">
                        <label class="control-label required">@lang('labels.error_translate'):
                            <span class="help">@lang('labels.translated_message_prompted')</span></label>
                        <div class="controls">
                            <div id="sose_check_validation_message">
                                <div class="formSep control-group ">
                                    <label class="control-label">@lang('labels.russian_text')<span class="help"></span></label>
                                    <div class="controls">
                                        <textarea name="message_ru">@if(isset($check)){{$check->russian}}@endif</textarea>
                                    </div>
                                </div>
                                <div class="formSep control-group ">
                                    <label class="control-label">@lang('labels.english_text')<span class="help"></span></label>
                                    <div class="controls">
                                        <textarea name="message_en">@if(isset($check)){{$check->english}}@endif</textarea>
                                    </div>
                                </div>
                                <div class="formSep control-group ">
                                    <label class="control-label">@lang('labels.italian_text')<span class="help"></span></label>
                                    <div class="controls">
                                        <textarea name="message_it">@if(isset($check)){{$check->italian}}@endif</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row-fluid">
                    <div class="span6">
                        <a href="/{{App::getLocale()}}/admin/study-checks/{{$study_id}}" class="btn button-prev">
                            <i class="icon-chevron-left icon-white"></i> @lang('buttons.cancel') </a>
                        <span class="loader button-next"><i class="loader-icon"></i></span>
                    </div>

                    <div class="span6">
                        <button style="float: right"class="btn btn-inverse @if(isset($check)) edit_study_check @else add_study_check @endif"
                                type="button">
                            @lang('buttons.save') <i class="icon-ok icon-white"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>


    </div>





@endsection