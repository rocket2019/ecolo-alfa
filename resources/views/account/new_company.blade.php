@extends('account.layout')
@section('content')
    <div class="row-fluid">
        <div class="span12">
            <form id="new_company_form" class="form-horizontal well" method="post">
                @if(!empty($company))
                    <input value="{{$company->ID_company}}" type="hidden" name="company_id">
                @endif
                <h3 class="heading">
                    @lang('global.add_new_company')</h3>


                <div id="company">
                    @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_FINAL_CLIENT))
                        <input value="{{\Illuminate\Support\Facades\Auth::user()->id}}" name="user_id" type="hidden">
                    @else
                        <div class="formSep control-group ">
                            <label class="control-label required" for="evabeta_user_username">
                                @lang('labels.client_email')
                                <span class="help-block error-user_id"></span>
                            </label>
                            <div class="controls">
                                <select name="user_id" >
                                    @foreach($users as $user)
                                        <option @if(!empty($company) && $company->user_id == $user->id) selected
                                                @endif value="{{$user->id}}">{{$user->email}}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>
                    @endif
                    <div class="formSep control-group ">
                        <label class="control-label required" for="evabeta_user_username">
                            @lang('labels.company_name')
                            <span class="help-block error-company_name"></span>
                        </label>
                        <div class="controls">
                            <input type="text" name="company_name"
                                   @if(!empty($company)) value="{{$company->company_name}}" @endif />
                        </div>
                    </div>
                        <div class="formSep control-group ">
                            <label class="control-label required" for="evabeta_user_username">
                                @lang('labels.main_activity')
                                <span class="help-block error-main_activity"></span>
                            </label>
                            <div class="controls">
                                <input type="text" name="main_activity"
                                       @if(!empty($company)) value="{{$company->main_activity}}" @endif />
                            </div>
                        </div>
                    <div class="formSep control-group ">
                        <label class="control-label required" for="evabeta_user_username">
                            @lang('labels.id_number')
                            <span class="help-block error-id_number"></span>
                        </label>
                        <div class="controls">
                            <input type="text" name="id_number" @if(!empty($company)) value="{{$company->INN_number}}"
                                   @endif />
                        </div>
                    </div>
                    <div class="formSep control-group ">
                        <label class="control-label required" for="evabeta_user_username">
                            @lang('labels.legal_form')
                            <span class="help-block error-legal_form"></span>
                        </label>
                        <div class="controls">
                            <select name="legal_form" >
                                @foreach($legal_forms as $legal_form)
                                    <option @if(!empty($company) &&  $company->legal_form == $legal_form->id) selected
                                            @endif value="{{$legal_form->id}}">{{$legal_form->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="formSep control-group ">
                        <label class="control-label required" for="evabeta_user_username">
                            @lang('labels.address')
                            <span class="help-block error-address"></span>
                        </label>
                        <div class="controls">
                            <input type="text" name="address"
                                   @if(!empty($company)) value="{{$company->location_address}}"
                                   @endif />
                        </div>
                    </div>
                    <div class="formSep control-group ">
                        <label class="control-label required" for="evabeta_user_username">

                            @lang('labels.area_code_region')
                            <span class="help-block error-area_region"></span>
                            <span class="help-block error-legal_form"></span>
                        </label>
                        <div class="controls">
                            <select name="area_region">
                                <option value="">@lang('labels.select_region')</option>
                                @foreach($regions as $region)
                                    <option
                                            @if(!empty($company) && $company->region == $region->area_code) selected
                                            @endif
                                            value="{{$region->area_code}}">{{$region->region_description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                        <div class="formSep control-group ">
                            <label class="control-label required" for="evabeta_user_username">

                                @lang('labels.area_code_city')
                                <span class="help-block error-area_city"></span>
                            </label>
                            <div class="controls">
                                <select name="area_city">
                                    <option value="">@lang('labels.select_city')</option>

                                @if(!empty($company_region))
                                        <option value="{{$company_region->area_code}}">{{$company_region->area_name}}</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                </div>
            </form>
        </div>


    </div>
    <div class="row-fluid">
        <div class="span6">
            <a href="{{URL::previous()}}" class="btn button-prev">
                <i class="icon-chevron-left icon-white"></i> @lang('buttons.cancel') </a>
            <span class="loader button-next"><i class="loader-icon"></i></span>
        </div>

        <div class="span6">
            <button style="float: right" class="btn btn-inverse @if(!empty($company)) update_company @else save_company @endif ">
                @lang('buttons.save') <i class="icon-ok icon-white"></i>
            </button>
        </div>
    </div>




@endsection
