@extends('account.layout')
<style>
    .dropup,
    .dropright,
    .dropdown,
    .dropleft {
        position: relative;
    }

    .dropdown-toggle {
        white-space: nowrap;
    }

    .dropdown-toggle::after {
        display: inline-block;
        margin-left: 0.255em;
        vertical-align: 0.255em;
        border-top: 0.3em solid;
        border-right: 0.3em solid transparent;
        border-bottom: 0;
        border-left: 0.3em solid transparent;
    }

    .dropdown-toggle:empty::after {
        margin-left: 0;
    }

    .dropdown-menu {
        position: absolute;
        top: 100%;
        left: 0;
        z-index: 1000;
        display: none;
        float: left;
        min-width: 10rem;
        padding: 0.5rem 0;
        margin: 0.125rem 0 0;
        font-size: 1rem;
        color: #212529;
        text-align: left;
        list-style: none;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid rgba(0, 0, 0, 0.15);
        border-radius: 0.25rem;
    }

    .dropdown-menu-left {
        right: auto;
        left: 0;
    }

    .dropdown-menu-right {
        right: 0;
        left: auto;
    }

    @media (min-width: 576px) {
        .dropdown-menu-sm-left {
            right: auto;
            left: 0;
        }

        .dropdown-menu-sm-right {
            right: 0;
            left: auto;
        }
    }

    @media (min-width: 768px) {
        .dropdown-menu-md-left {
            right: auto;
            left: 0;
        }

        .dropdown-menu-md-right {
            right: 0;
            left: auto;
        }
    }

    @media (min-width: 992px) {
        .dropdown-menu-lg-left {
            right: auto;
            left: 0;
        }

        .dropdown-menu-lg-right {
            right: 0;
            left: auto;
        }
    }

    @media (min-width: 1200px) {
        .dropdown-menu-xl-left {
            right: auto;
            left: 0;
        }

        .dropdown-menu-xl-right {
            right: 0;
            left: auto;
        }
    }

    .dropup .dropdown-menu {
        top: auto;
        bottom: 100%;
        margin-top: 0;
        margin-bottom: 0.125rem;
    }

    .dropup .dropdown-toggle::after {
        display: inline-block;
        margin-left: 0.255em;
        vertical-align: 0.255em;
        border-top: 0;
        border-right: 0.3em solid transparent;
        border-bottom: 0.3em solid;
        border-left: 0.3em solid transparent;
    }

    .dropup .dropdown-toggle:empty::after {
        margin-left: 0;
    }

    .dropright .dropdown-menu {
        top: 0;
        right: auto;
        left: 100%;
        margin-top: 0;
        margin-left: 0.125rem;
    }

    .dropright .dropdown-toggle::after {
        display: inline-block;
        margin-left: 0.255em;
        vertical-align: 0.255em;
        border-top: 0.3em solid transparent;
        border-right: 0;
        border-bottom: 0.3em solid transparent;
        border-left: 0.3em solid;
    }

    .dropright .dropdown-toggle:empty::after {
        margin-left: 0;
    }

    .dropright .dropdown-toggle::after {
        vertical-align: 0;
    }

    .dropleft .dropdown-menu {
        top: 0;
        right: 100%;
        left: auto;
        margin-top: 0;
        margin-right: 0.125rem;
    }

    .dropleft .dropdown-toggle::after {
        display: inline-block;
        margin-left: 0.255em;
        vertical-align: 0.255em;
    }

    .dropleft .dropdown-toggle::after {
        display: none;
    }

    .dropleft .dropdown-toggle::before {
        display: inline-block;
        margin-right: 0.255em;
        vertical-align: 0.255em;
        border-top: 0.3em solid transparent;
        border-right: 0.3em solid;
        border-bottom: 0.3em solid transparent;
    }

    .dropleft .dropdown-toggle:empty::after {
        margin-left: 0;
    }

    .dropleft .dropdown-toggle::before {
        vertical-align: 0;
    }

    .dropdown-menu[x-placement^="top"], .dropdown-menu[x-placement^="right"], .dropdown-menu[x-placement^="bottom"], .dropdown-menu[x-placement^="left"] {
        right: auto;
        bottom: auto;
    }

    .dropdown-divider {
        height: 0;
        margin: 0.5rem 0;
        overflow: hidden;
        border-top: 1px solid #e9ecef;
    }

    .dropdown-item {
        display: block;
        padding: 0.25rem 1.5rem;
        clear: both;
        font-weight: 400;
        color: #212529;
        text-align: inherit;
        white-space: nowrap;
        background-color: transparent;
        border: 0;
    }

    .dropdown-item:hover, .dropdown-item:focus {
        color: #16181b;
        text-decoration: none;
        background-color: #f8f9fa;
    }

    .dropdown-item.active, .dropdown-item:active {
        color: #fff;
        text-decoration: none;
        background-color: #007bff;
    }

    .dropdown-item.disabled, .dropdown-item:disabled {
        color: #6c757d;
        pointer-events: none;
        background-color: transparent;
    }

    .dropdown-menu.show {
        display: block;
    }
</style>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
@section('content')
    @if(\Illuminate\Support\Facades\Auth::user()->role_id != \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_MINIMUM))
        <div class="row-fluid">
            <div class="span8">
                <div class="span12">
                    <form class="well form-inline" action="/{{App::getLocale()}}/company/search-company" method="GET">
                        <p class="f_legend">
                            @lang('global.search_company_by_name_or')</p>
                        <input type="text" placeholder="@lang('global.type_company_name_or')"
                               class="span8" name="company_name">

                        <button type="submit" class="btn btn-inverse">
                            <i class="icon-search icon-white"></i>
                            @lang('buttons.search')
                        </button>
                    </form>
                </div>


            </div>

            <div class="span4">


                <h3 class="heading">@lang('global.last_visited_companies')</h3>

                @if(count($companies))
                    <ul class="list user_list">
                        @foreach( $companies as $company)
                            <li>
                                <a href="/{{App::getLocale()}}/company/view/{{$company->ID_company}}"
                                   class="sl_name">{{$company->company_name}}</a><br>
                            </li>
                        @endforeach
                    </ul>

                @endif
            </div>

        </div>
    @endif
    <div class="row-fluid">
        <div class="span12">
            @if(session('successMsg'))
                <div class="alert alert-success" role="alert">
                    {{ session('successMsg') }}
                </div>
            @endif

            @if(session('errorMsg'))
                <div class="alert alert-danger" role="alert">
                    {{ session('errorMsg') }}
                </div>
            @endif
            <div class="w-box">
                <div class="w-box-header">
                    @lang('global.the_list_of_all_questionnaires')
                </div>


                <div class="w-box-content company_sose_wdg">


                    <div id="questionnairesTable_wrapper" class="dataTables_wrapper no-footer">
                        <table width="100%" class="table table-bordered dataTable no-footer"
                               id="questionnairesTable" role="grid" style="width: 100%; border-collapse: collapse !important;">
                            <thead>
                            <tr role="row">
                                <td class="sorting" tabindex="0" aria-controls="questionnairesTable" rowspan="1"
                                    colspan="1" aria-label="N анкеты: activate to sort column ascending"
                                    style="width: 43px;">@lang('global.questionnaire_id')
                                </td>
                                <td class="sorting" tabindex="0" aria-controls="questionnairesTable" rowspan="1"
                                    colspan="1"
                                    aria-label="Статус анкеты  и мейл/роль изменившего стату : activate to sort column ascending"
                                    style="width: 107px;">@lang('global.questionnaire_and_user')
                                </td>
                                <td class="sorting" tabindex="0" aria-controls="questionnairesTable" rowspan="1"
                                    colspan="1"
                                    aria-label="Дата изменения статуса: activate to sort column ascending"
                                    style="width: 66px;">@lang('global.last_updated_status')
                                </td>
                                <td class="sorting" tabindex="0" aria-controls="questionnairesTable" rowspan="1"
                                    colspan="1"
                                    aria-label="Название компании и флаг ошибок (клиент с &amp;gt;1 компанией-красный): activate to sort column ascending"
                                    style="width: 79px;">@lang('global.company_name_and_error')
                                </td>

                                <td class="sorting" tabindex="0" aria-controls="questionnairesTable" rowspan="1"
                                    colspan="1" aria-label="Возможные действия: activate to sort column ascending"
                                    style="width: 87px;">@lang('global.possible_actions')
                                </td>
                            </tr>
                            </thead>

                            <tbody>
                            @if(count($questionnaires))
                                @php
                                    //this propertis ($tdBorder, $count) are just to add or remove table td's border-top value
                                    $tdBorder = false;
                                    $count    = 0;
                                @endphp
                                @foreach($questionnaires as $questionnaire)
                                    @if (isset($questionnaire->company->ID_company))
                                        @if ($questionnaire->sose_questionnaire_status_id == 6 || $count > 0)
                                            @php
                                                $tdBorder = true;
                                                $count++;
                                            @endphp
                                        @endif
                                        <tr role="row" style="@if($questionnaire->sose_questionnaire_status_id == 6) border:1px solid red; @endif">
                                            <td class="sorting" tabindex="0" aria-controls="questionnairesTable"
                                                rowspan="1"
                                                colspan="1" aria-label="N анкеты: activate to sort column ascending"
                                                data-id="{{$questionnaire->company->questionnaire_status}}"
                                                style="@if(in_array($questionnaire->company->questionnaire_status, array(2,6)) && $questionnaire->company->OKEI_code == null)color: orangered;@endif
                                                    width: 43px;@if($tdBorder) border-top:0 !important; @endif">{{$questionnaire->id}}
                                            </td>
                                            <td class="sorting" tabindex="0" aria-controls="questionnairesTable"
                                                rowspan="1"
                                                colspan="1" aria-label="N анкеты: activate to sort column ascending"
                                                style="width: 43px;@if($tdBorder) border-top:0 !important; @endif">
                                                @if(($questionnaire->sose_status))
                                                    {{$questionnaire->sose_status->description}}
                                                @endif
                                                /@if(isset($questionnaire->user)){{$questionnaire->user->email}}@endif
                                            </td>
                                            <td class="sorting" tabindex="0" aria-controls="questionnairesTable"
                                                rowspan="1"
                                                colspan="1" aria-label="N анкеты: activate to sort column ascending"
                                                style="width: 43px;@if($tdBorder) border-top:0 !important; @endif"> {{$questionnaire->updated}}
                                            </td>
                                            <td class="sorting" tabindex="0" aria-controls="questionnairesTable"
                                                rowspan="1"
                                                colspan="1" aria-label="N анкеты: activate to sort column ascending"
                                                style="width: 43px;@if($tdBorder) border-top:0 !important; @endif"><a
                                                    href="/{{App::getLocale()}}/company/view/{{$questionnaire->company->ID_company}}">{{$questionnaire->company->company_name}}</a>
                                            </td>
                                            <td class="sorting" tabindex="0" aria-controls="questionnairesTable"
                                                rowspan="1"
                                                colspan="1" aria-label="N анкеты: activate to sort column ascending"
                                                style="width: 43px;@if($tdBorder) border-top:0 !important; @endif">
                                                @if(\Illuminate\Support\Facades\Auth::user()->role_id != \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_SUPERVISOR) && \Illuminate\Support\Facades\Auth::user()->role_id != \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_MINIMUM))
                                                    @if($questionnaire->sose_questionnaire_status_id == \App\Http\Models\SoseQuestionnaireStatus::IN_EDITING )
                                                        <a class="btn edit-status"
                                                           href="/{{App::getLocale()}}/company/edit/{{$questionnaire->company->ID_company}}/{{$questionnaire->id}}" data-id="{{$questionnaire->id}}"><i
                                                                class="icon-edit"></i> @lang('buttons.edit')</a>
                                                    @elseif($questionnaire->sose_questionnaire_status_id == \App\Http\Models\SoseQuestionnaireStatus::SUBMITTED  )
                                                        <a class="btn edit-status"
                                                           href="/{{App::getLocale()}}/company/edit/{{$questionnaire->company->ID_company}}/{{$questionnaire->id}}" data-id="{{$questionnaire->id}}"><i
                                                                class="icon-edit"></i> @lang('buttons.edit')</a>
                                                        <a class="btn send_to_eval" data-id="{{$questionnaire->id}}"><i
                                                                class="icon-refresh"></i> @lang('buttons.send')
                                                        </a>
                                                    @elseif($questionnaire->sose_questionnaire_status_id == \App\Http\Models\SoseQuestionnaireStatus::REGISTERED_IN_BACKOFFICE
                                                    && (\Illuminate\Support\Facades\Auth::user()->role_id ==\App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_BACK_OFFICE) || \Illuminate\Support\Facades\Auth::user()->role_id ==\App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN)))
                                                        <a class="btn btn-evaluate"
                                                           data-url="/{{App::getLocale()}}/company/runEvaluation/{{$questionnaire->company->ID_company}}/{{$questionnaire->id}}"><i
                                                                class="icon-refresh"></i> @lang('buttons.evaluate')
                                                        </a>
                                                    @elseif($questionnaire->sose_questionnaire_status_id == \App\Http\Models\SoseQuestionnaireStatus::IN_BACKOFFICE_ELABORATION
                                                                                                    && (\Illuminate\Support\Facades\Auth::user()->role_id ==\App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN) || \Illuminate\Support\Facades\Auth::user()->role_id ==\App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_BACK_OFFICE)))

                                                        <a class="btn to_bank" data-id="{{$questionnaire->id}}"><i
                                                                class="icon-circle-arrow-right"></i> @lang('buttons.to_bank')
                                                        </a>
                                                    @endif
                                                @endif
                                                @if(\Illuminate\Support\Facades\Auth::user()->role_id ==\App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN) && $questionnaire->sose_questionnaire_status_id > \App\Http\Models\SoseQuestionnaireStatus::SUBMITTED)
                                                    <a class="btn edit-status"
                                                       href="/{{App::getLocale()}}/company/edit/{{$questionnaire->company->ID_company}}/{{$questionnaire->id}}" data-id="{{$questionnaire->id}}"><i
                                                            class="icon-edit"></i> @lang('buttons.edit')</a>
                                                @endif

                                                @if(\Illuminate\Support\Facades\Auth::user()->role_id ==\App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN) && $questionnaire->sose_questionnaire_status_id > \App\Http\Models\SoseQuestionnaireStatus::REGISTERED_IN_BACKOFFICE)

                                                    <a class="btn btn-evaluate"
                                                       data-url="/{{App::getLocale()}}/company/runEvaluation/{{$questionnaire->company->ID_company}}/{{$questionnaire->id}}"><i
                                                            class="icon-refresh"></i> @lang('buttons.evaluate')
                                                    </a>
                                                @endif

                                                    @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_SUPERVISOR))
{{--                                                        <a class="btn "--}}
{{--                                                           href="/{{App::getLocale()}}/company/edit/{{$questionnaire->company->ID_company}}/{{$questionnaire->id}}"><i--}}
{{--                                                                class="icon-edit"></i> @lang('buttons.questionnaire')--}}
{{--                                                        </a>--}}
                                                    @else
                                                        <a href="/{{App::getLocale()}}/company/print/{{$questionnaire->company->ID_company}}/{{$questionnaire->id}}/{{$questionnaire->sose_ateco->id}}"
                                                           target="_blank"
                                                           class="btn">@lang('buttons.print')</a>
                                                    @endif
                                                @if(\Illuminate\Support\Facades\Auth::user()->role_id !=\App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_SUPERVISOR))
                                                    <button type="button" class="btn file_upload"
                                                            data-idComp="{{$questionnaire->ID_company}}"
                                                            data-name="{{$questionnaire->company->ID_company}}"
                                                            data-id="{{$questionnaire->id}}"
                                                            data-company="{{$questionnaire->company->company_name}}"
                                                            data-toggle="modal"
                                                            data-target="#myModal">@lang('buttons.report')</button>
                                                @endif
                                                    @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_SUPERVISOR))
                                                        @if(file_exists(base_path('public/site/reports/pdf/'.strtolower($questionnaire->company->company_name).$questionnaire->id_company.'.pdf')))
                                                            <a class="btn" disabled=""><i
                                                                    class="icon-edit"></i> @lang('buttons.questionnaire')
                                                            </a>
                                                            <div class="btn-group dropleft" style="margin-bottom: 28px">
                                                                <button class="report_btn btn dropdown-toggle"
                                                                        type="button"
                                                                        id="dropdownMenuButton"
                                                                        data-toggle="dropdown" aria-haspopup="true"
                                                                        aria-expanded="false"
                                                                        style="height: 30px; padding: 0 .75rem; color: black;">
                                                                    Отчет
                                                                </button>
                                                                <div class="dropdown-menu"
                                                                     aria-labelledby="dropdownMenuButton">
                                                                    {{--                            @php--}}
                                                                    {{--                                }--}}
                                                                    {{--                            @endphp--}}
                                                                    @php
                                                                        if(file_exists(base_path('public/site/reports/pdf/'.strtolower($questionnaire->company->company_name).$questionnaire->id_company.'.pdf'))){

                                                                    @endphp
                                                                    <a class="dropdown-item"
                                                                       href="{{asset('/site/reports/pdf/'.strtolower($questionnaire->company->company_name).$questionnaire->id_company.'.pdf')}}"
                                                                       target="_blank">PDF</a>
                                                                    @php
                                                                        }
                                                                        if (file_exists(base_path('public/site/reports/excel/'.strtolower($questionnaire->company->company_name).$questionnaire->id_company.'.xlsx'))){

                                                                    @endphp
                                                                    <a class="dropdown-item"
                                                                       href="{{asset('/site/reports/excel/'.strtolower($questionnaire->company->company_name).$questionnaire->id_company.'.xlsx')}}"
                                                                       target="_blank" style="z-index: 999">EXCEL</a>
                                                                </div>
                                                            </div>
                                                            @php
                                                                }
                                                            @endphp
                                                        @else
                                                            <a class="btn "
                                                               href="/{{App::getLocale()}}/company/edit/{{$questionnaire->company->ID_company}}/{{$questionnaire->id}}"><i
                                                                    class="icon-edit"></i> @lang('buttons.questionnaire')
                                                            </a>
                                                        @endif
                                                    @endif

                                                    <a class="btn" target="_blank" href="/{{App::getLocale()}}/client/questionnaire/view/{{$questionnaire->company->ID_company}}/{{$questionnaire->id}}">Анкета</a>

                                            </td>
                                            @if ($count >= 2)
                                                @php
                                                    $tdBorder = false;
                                                    $count = 0;
                                                @endphp
                                            @endif
                                        </tr>
                                    @endif
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>


                </div>

            </div>
        </div>
    </div>
    <div class="container">
        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <div class="upload_file">
                            <form
                                action=""
                                method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <label for="report" style="font-weight: 500 !important;font-size: 18px;">Upload report
                                    file(.pdf)</label>
                                <input type="file" class="custom-file-input" name="report" id="report"
                                       style="display: block !important;margin-top: 15px !important;" required>
                                <input type="text" class="company_name" style="display: none" name="comp_name" value="">
                                <button type="submit" id="save_report" class="btn save_file"
                                        style="display: block;margin-top: 15px"><i
                                        class="fas fa-check"></i>Save
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

    </div>
@endsection

