@extends('account.layout')
@section('content')
    <div class="row-fluid">
        <div class="span12">
            <form id="new_user_form" class="form-horizontal well">
                @if(!empty($user))
                    <input value="{{$user->id}}" type="hidden" name="user_id">
                @endif
                <h3 class="heading">@lang('buttons.add_new_user')</h3>


                <div id="evabeta_user">
                    <div class="formSep control-group ">
                        <label class="control-label required" for="evabeta_user_username">
                            @lang('inputs.username')
                            <span class="help-block error-username"></span>
                        </label>
                        <div class="controls">
                            <input type="text" name="username" @if(!empty($user)) value="{{$user->username}}"
                                   @endif />
                        </div>
                    </div>
                    <div class="formSep control-group ">
                        <label class="control-label required" for="evabeta_user_username">
                            @lang('labels.first_name')
                            <span class="help-block error-first_name"></span>
                        </label>
                        <div class="controls">
                            <input type="text" name="first_name" @if(!empty($user)) value="{{$user->first_name}}"
                                   @endif />
                        </div>
                    </div>
                    <div class="formSep control-group ">
                        <label class="control-label required" for="evabeta_user_username">
                           @lang('labels.last_name')
                            <span class="help-block error-last_name"></span>
                        </label>
                        <div class="controls">
                            <input type="text" name="last_name" @if(!empty($user)) value="{{$user->last_name}}"
                                   @endif />
                        </div>
                    </div>
                    <div class="formSep control-group ">
                        <label class="control-label required" for="evabeta_user_username">
                            @lang('labels.patronymic') <span class="help-block error-patronymic"></span>
                        </label>
                        <div class="controls">
                            <input type="text" name="patronymic" @if(!empty($user)) value="{{$user->patronymic}}"
                                   @endif />
                        </div>
                    </div>

                    <div class="formSep control-group ">
                        <label class="control-label required" for="evabeta_user_username">
                            @lang('inputs.email')
                            <span class="help-block error-email"></span>
                        </label>
                        <div class="controls">
                            <input type="email" name="email" @if(!empty($user)) value="{{$user->email}}"
                                   @endif />
                        </div>
                    </div>
                    <div class="formSep control-group ">
                        <label class="control-label required" for="evabeta_user_username">
                            @lang('inputs.password')
                            <span class="help-block error-password"></span>
                        </label>
                        <div class="controls">
                            <input type="password" name="password" />
                        </div>
                    </div>
                    <div class="formSep control-group ">
                        <label class="control-label required" for="evabeta_user_username">
                            @lang('inputs.confirm_password')
                            <span class="help-block error-confirm_password"></span>
                        </label>
                        <div class="controls">
                            <input type="password" name="confirm_password" />
                        </div>
                    </div>
                    @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_MANAGER))

                  <input type="hidden" name="role_id" value="{{\App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_FINAL_CLIENT)}}"/>

                    @else
                        <div class="formSep control-group ">
                            <label class="control-label required" for="evabeta_user_role">@lang('labels.role')
                                <span class="help-block error-role_id"></span></label>
                            <div class="controls">
                                <select name="role_id" >
                                    @foreach($roles as $role)
                                        <option @if(!empty($user) && $user->role_id == $role->id) selected
                                                @endif value="{{$role->id}}">{{$role->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif

                    <div class="row-fluid">
                        <div class="span6">
                            <a href="/{{App::getLocale()}}/admin/user-management" class="btn button-prev">
                                <i class="icon-chevron-left icon-white"></i> @lang('buttons.cancel')</a>
                            <span class="loader button-next"><i class="loader-icon"></i></span>
                        </div>

                        <div class="span6">
                            <button style="float: right" class="btn btn-inverse @if(!empty($user)) update_user @else save_user @endif"
                                    type="button">
                                @lang('buttons.save') <i class="icon-ok icon-white"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection