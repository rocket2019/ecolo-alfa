@extends('account.layout')
@section('content')
    <div id="loading_layer" style="display:none"><img src="/site/img/ajax_loader.gif"/></div>

    <div class="row-fluid">
        <div class="alert alert-success alert_logo" role="alert" style="display: none">
            @lang('global.logo_success')
        </div>
        <div class="alert alert-success" role="alert" @if(isset($true)) style="display: block"
             @else style="display: none" @endif>
            @lang('global.excel_success')
        </div>
        <div class="span12">
{{--            action="/{{App::getLocale()}}/admin/import-excel"--}}
            <form id="excel_data" method="POST" class="form-horizontal well"
                  enctype="multipart/form-data">
                {{ csrf_field() }}
                <h3 class="heading">
                    @lang('inputs.import_excel')</h3>

                <div class="formSep control-group ">
                    <label class="control-label required">
                        @lang('inputs.choose_excel_file')
                        <span class="help-block error-bank_name">
                               @if(isset($messages))
                                {{$messages}}
                            @endif

                        </span>
                    </label>
                    <div class="controls">
                        <input type="file" name="excel_file" required="required" id='pdf_file'/>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span6">

                    </div>

                    <div class="span6" style="text-align: right">
                        <button class="btn btn-inverse">
                            @lang('buttons.save') <i class="icon-ok icon-white"></i>
                        </button>
{{--                        Created by Suren--}}
                    </div>
                </div>

            </form>
            <a href="/{{App::getLocale()}}/admin/import-excel/download" class="btn get-all-data">
                Older Data
            </a>

        </div>


    </div>



@endsection
