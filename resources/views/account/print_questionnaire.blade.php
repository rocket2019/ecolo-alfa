@extends('account.layout')
@section('content')
    <style>
        .dropbtn {
            background-color: #4CAF50;
            color: white;
            padding: 16px;
            font-size: 16px;
            border: none;
            cursor: pointer;
        }

        .dropbtn:hover, .dropbtn:focus {
            background-color: #3e8e41;
        }

        #myInput {
            box-sizing: border-box;
            background-position: 14px 12px;
            background-repeat: no-repeat;
            font-size: 16px;
            padding: 18px 20px 12px 10px;
            border: none;
            border-bottom: 1px solid #ddd;
        }

        #myInput:focus {
            outline: 3px solid #ddd;
        }


        .dropdown-content {
            background-color: #f6f6f6;
            min-width: 230px;
            overflow: auto;
            border: 1px solid #ddd;
            z-index: 1;
        }

        .dropdown-content a {
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
        }

        .dropdown1 {
            height: 300px;
        }

        .dropdown1 a:hover {
            background-color: #ddd;
            cursor: pointer;
        }
        #selected{
            background: #DDE;

        }


    </style>

    <div class="alert alert-danger" role="alert" style="display: none">
        @lang('global.select_specialisation')
    </div>
    <div class="row-fluid">

        <h3 class="heading">@lang('global.select_questionnaire')</h3>
        <ul style="border:1px dashed grey;margin: 0 0 20px 0" class="file-tree">

            <li><a href="#">1 {{\App\Http\Models\CodeTree::get_by_code('1')->description}}</a>
                <ul>

                    <li><a href="#">1.1 {{\App\Http\Models\CodeTree::get_by_code('1.1')->description}}</a>
                        <ul>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('1.1.1')->id}}">
                                1.1.1 {{\App\Http\Models\CodeTree::get_by_code('1.1.1')->description}}</li>
                            <li>
                                <a href="#">1.1.2 {{\App\Http\Models\CodeTree::get_by_code('1.1.2')->description}}</a>
                                <ul>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('1.1.2.1')->id}}">
                                        1.1.2.1 {{\App\Http\Models\CodeTree::get_by_code('1.1.2.1')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('1.1.2.2')->id}}">
                                        1.1.2.2 {{\App\Http\Models\CodeTree::get_by_code('1.1.2.2')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('1.1.2.3')->id}}">
                                        1.1.2.3 {{\App\Http\Models\CodeTree::get_by_code('1.1.2.3')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('1.1.2.4')->id}}">
                                        1.1.2.4 {{\App\Http\Models\CodeTree::get_by_code('1.1.2.4')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('1.1.2.5')->id}}">
                                        1.1.2.5 {{\App\Http\Models\CodeTree::get_by_code('1.1.2.5')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('1.1.2.6')->id}}">
                                        1.1.2.6 {{\App\Http\Models\CodeTree::get_by_code('1.1.2.6')->description}}</li>

                                </ul>
                            </li>

                        </ul>
                    </li>
                    <li><a href="#">1.2 {{\App\Http\Models\CodeTree::get_by_code('1.2')->description}}</a>
                        <ul>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('1.2.1')->id}}">
                                1.2.1 {{\App\Http\Models\CodeTree::get_by_code('1.2.1')->description}}</li>
                            <li>
                                <a href="#">1.2.2 {{\App\Http\Models\CodeTree::get_by_code('1.2.2')->description}}</a>
                                <ul>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('1.2.2.1')->id}}">
                                        1.2.2.1 {{\App\Http\Models\CodeTree::get_by_code('1.2.2.1')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('1.2.2.2')->id}}">
                                        1.2.2.2 {{\App\Http\Models\CodeTree::get_by_code('1.2.2.2')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('1.2.2.3')->id}}">
                                        1.2.2.3 {{\App\Http\Models\CodeTree::get_by_code('1.2.2.3')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('1.2.2.4')->id}}">
                                        1.2.2.4 {{\App\Http\Models\CodeTree::get_by_code('1.2.2.4')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('1.2.2.5')->id}}">
                                        1.2.2.5 {{\App\Http\Models\CodeTree::get_by_code('1.2.2.5')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('1.2.2.6')->id}}">
                                        1.2.2.6 {{\App\Http\Models\CodeTree::get_by_code('1.2.2.6')->description}}</li>

                                </ul>
                            </li>

                        </ul>
                    </li>
                    <li><a href="#">1.3 {{\App\Http\Models\CodeTree::get_by_code('1.3')->description}}</a>
                        <ul>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('1.3.2')->id}}">
                                1.3.2 {{\App\Http\Models\CodeTree::get_by_code('1.3.2')->description}}</li>

                        </ul>
                    </li>
                    <li><a href="#">1.4 {{\App\Http\Models\CodeTree::get_by_code('1.4')->description}}</a>
                        <ul>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('1.4.1')->id}}">
                                1.4.1 {{\App\Http\Models\CodeTree::get_by_code('1.4.1')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('1.4.2')->id}}">
                                1.4.2 {{\App\Http\Models\CodeTree::get_by_code('1.4.2')->description}}</li>

                        </ul>
                    </li>

                </ul>
            </li>
            <li><a href="#">2 {{\App\Http\Models\CodeTree::get_by_code('2')->description}}</a>
                <ul>
                    <li><a href="#">2.1 {{\App\Http\Models\CodeTree::get_by_code('2.1')->description}}</a>
                        <ul>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.1.1')->id}}">
                                2.1.1 {{\App\Http\Models\CodeTree::get_by_code('2.1.1')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.1.2')->id}}">
                                2.1.2 {{\App\Http\Models\CodeTree::get_by_code('2.1.2')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.1.3')->id}}">
                                2.1.3 {{\App\Http\Models\CodeTree::get_by_code('2.1.3')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.1.4')->id}}">
                                2.1.4 {{\App\Http\Models\CodeTree::get_by_code('2.1.4')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.1.5')->id}}">
                                2.1.5 {{\App\Http\Models\CodeTree::get_by_code('2.1.5')->description}}</li>

                        </ul>
                    </li>
                    <li><a href="#">2.2 {{\App\Http\Models\CodeTree::get_by_code('2.2')->description}}</a>
                        <ul>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.2.1')->id}}">
                                2.2.1 {{\App\Http\Models\CodeTree::get_by_code('2.2.1')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.2.3')->id}}">
                                2.2.3 {{\App\Http\Models\CodeTree::get_by_code('2.2.3')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.2.4')->id}}">
                                2.2.4 {{\App\Http\Models\CodeTree::get_by_code('2.2.4')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.2.5')->id}}">
                                2.2.5 {{\App\Http\Models\CodeTree::get_by_code('2.2.5')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.2.6')->id}}">
                                2.2.6 {{\App\Http\Models\CodeTree::get_by_code('2.2.6')->description}}</li>

                        </ul>
                    </li>
                    <li><a href="#">2.3 {{\App\Http\Models\CodeTree::get_by_code('2.3')->description}}</a>
                        <ul>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.3.1')->id}}">
                                2.3.1 {{\App\Http\Models\CodeTree::get_by_code('2.3.1')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.3.2')->id}}">
                                2.3.2 {{\App\Http\Models\CodeTree::get_by_code('2.3.2')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.3.3')->id}}">
                                2.3.3 {{\App\Http\Models\CodeTree::get_by_code('2.3.3')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.3.4')->id}}">
                                2.3.4 {{\App\Http\Models\CodeTree::get_by_code('2.3.4')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.3.5')->id}}">
                                2.3.5 {{\App\Http\Models\CodeTree::get_by_code('2.3.5')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.6')->id}}">
                                2.3.6 {{\App\Http\Models\CodeTree::get_by_code('2.3.6')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.3.7')->id}}">
                                2.3.7 {{\App\Http\Models\CodeTree::get_by_code('2.3.7')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.3.8')->id}}">
                                2.3.8 {{\App\Http\Models\CodeTree::get_by_code('2.3.8')->description}}</li>

                        </ul>
                    </li>
                    <li><a href="#">2.4 {{\App\Http\Models\CodeTree::get_by_code('2.4')->description}}</a>
                        <ul>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.4.1')->id}}">
                                2.4.1 {{\App\Http\Models\CodeTree::get_by_code('2.4.1')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.4.2')->id}}">
                                2.4.2 {{\App\Http\Models\CodeTree::get_by_code('2.4.2')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.4.3')->id}}">
                                2.4.3 {{\App\Http\Models\CodeTree::get_by_code('2.4.3')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.4.4')->id}}">
                                2.4.4 {{\App\Http\Models\CodeTree::get_by_code('2.4.4')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.4.5')->id}}">
                                2.4.5 {{\App\Http\Models\CodeTree::get_by_code('2.4.5')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.4.6')->id}}">
                                2.4.6 {{\App\Http\Models\CodeTree::get_by_code('2.4.6')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.4.7')->id}}">
                                2.4.7 {{\App\Http\Models\CodeTree::get_by_code('2.4.7')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.4.8')->id}}">
                                2.4.8 {{\App\Http\Models\CodeTree::get_by_code('2.4.8')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.4.9')->id}}">
                                2.4.9 {{\App\Http\Models\CodeTree::get_by_code('2.4.9')->description}}</li>

                        </ul>
                    </li>
                    <li class="code_id" data-id="{{\App\Http\Models\CodeTree::get_by_code('2.5')->id}}">
                        2.5 {{\App\Http\Models\CodeTree::get_by_code('2.5')->description}}</li>

                </ul>
            </li>
            <li><a href="#">3 {{\App\Http\Models\CodeTree::get_by_code('3')->description}}</a>
                <ul>
                    <li><a href="#">3.1 {{\App\Http\Models\CodeTree::get_by_code('3.1')->description}}</a>
                        <ul>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('3.1.1')->id}}">
                                3.1.1 {{\App\Http\Models\CodeTree::get_by_code('3.1.1')->description}}</li>
                            <li>
                                <a href="#">3.1.2 {{\App\Http\Models\CodeTree::get_by_code('3.1.2')->description}}</a>
                                <ul>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('3.1.2.1')->id}}">
                                        3.1.2.1 {{\App\Http\Models\CodeTree::get_by_code('3.1.2.1')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('3.1.2.2')->id}}">
                                        3.1.2.2 {{\App\Http\Models\CodeTree::get_by_code('3.1.2.2')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('3.1.2.3')->id}}">
                                        3.1.2.3 {{\App\Http\Models\CodeTree::get_by_code('3.1.2.3')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('3.1.2.4')->id}}">
                                        3.1.2.4 {{\App\Http\Models\CodeTree::get_by_code('3.1.2.4')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('3.1.2.5')->id}}">
                                        3.1.2.5 {{\App\Http\Models\CodeTree::get_by_code('3.1.2.5')->description}}</li>

                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a href="#">3.2 {{\App\Http\Models\CodeTree::get_by_code('3.2')->description}}</a>
                        <ul>
                            <li>
                                <a href="#">3.2.1 {{\App\Http\Models\CodeTree::get_by_code('3.2.1')->description}}</a>
                                <ul>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.1.1')->id}}">
                                        3.2.1.1 {{\App\Http\Models\CodeTree::get_by_code('3.2.1.1')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.1.2')->id}}">
                                        3.2.1.2 {{\App\Http\Models\CodeTree::get_by_code('3.2.1.2')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.1.3')->id}}">
                                        3.2.1.3 {{\App\Http\Models\CodeTree::get_by_code('3.2.1.3')->description}}</li>

                                </ul>
                            </li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.2')->id}}">
                                3.2.2 {{\App\Http\Models\CodeTree::get_by_code('3.2.2')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.3')->id}}">
                                3.2.3 {{\App\Http\Models\CodeTree::get_by_code('3.2.3')->description}}</li>

                            <li>
                                <a href="#">3.2.4 {{\App\Http\Models\CodeTree::get_by_code('3.2.4')->description}}</a>
                                <ul>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.4.1')->id}}">
                                        3.2.4.1 {{\App\Http\Models\CodeTree::get_by_code('3.2.4.1')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.4.2')->id}}">
                                        3.2.4.2 {{\App\Http\Models\CodeTree::get_by_code('3.2.4.2')->description}}</li>

                                </ul>
                            </li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.5')->id}}">
                                3.2.5 {{\App\Http\Models\CodeTree::get_by_code('3.2.5')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.6')->id}}">
                                3.2.6 {{\App\Http\Models\CodeTree::get_by_code('3.2.6')->description}}</li>


                        </ul>
                    </li>

                </ul>
            </li>
        </ul>

        <div class="row-fluid">
            <div class="span3">@lang('global.select_specialisation')</div>
            <div class="span9">
                <div id="myDropdown"  class="dropdown-content">
                    <input type="text" placeholder="@lang('buttons.search').." onclick="open_sose_company()" id="myInput" style="width: 94%"
                           onkeyup="filterFunction()" autocomplete="off">
                    <button class="btn clear_ateco" style="vertical-align: top;display: inline-block"><i
                                class="icon-remove icon-white"></i>
                    </button>
                    <div class="dropdown1" id='sose_okveds_select' style="overflow: scroll;display: none">
                        @foreach($ateco as $key=>$at)
                            @php
                                $description = $at->sose_ateco->description;
                                $pieces = explode("[", $description);

                            @endphp

                            <a class="ateco_id_function" @if($loop->first) id="#selected" @endif onclick="callmodifyAtecoImputValueAfterAnyMiliseconds(this)"
                               data-id="{{$at->sose_ateco->id}}">{{$at->sose_okved->okved_code.' - '.$at->sose_ateco->ateco_code.'-'.$pieces[0] }}
                                <span class="key-for-search" style="display: none">@if(isset($pieces[1])){{$pieces[1]}}@endif</span>
                            </a>

                        @endforeach
                    </div>
                </div>
            </div>

        </div>
        <div class="row-fluid" style="margin-top: 20px">
            <div class="span3">@lang('global.the_number_of_company_units')</div>
            <div class="span9">
                <select style="width: 100%" id="company_unit">
                    @for ($i = 1; $i <= 10; $i++)
                        <option value="{{ $i }}">{{ $i }}</option>
                    @endfor

                </select>

            </div>

        </div>

        <div style="float: right; display: flex" id="for_link">
            <a style="display:none;color: aliceblue;font-weight: 500;background-color: #258db4;text-align: center;padding:6px 14px; border-radius: 4px; font-size: 13px; text-decoration: none; margin-right: 8px;" target="_blank" class="profile_btn product_codes" ><i style="margin-right: 6px" class="fas fa-arrow-circle-down"></i>Таблица с кодами товаров</a>
            <button id="buttonPrint" class="btn btn-inverse" disabled="disabled">
                @lang('buttons.download_pdf')
                <i class="icon-ok icon-white"></i>
                <span class="loader button-next"><i class="loader-icon"></i></span>
            </button>
            <a id="url" rel="external" href="https://google.com" target="_blank" style="display: none">dd</a>

        </div>

    </div>
@endsection
