@extends('account.layout')
@section('content')
    <div class="row-fluid">
        <div class="span12">

            <form class="form-horizontal well" method="post" action="/{{App::getLocale()}}/admin/new-bank">
                @csrf
                <h3 class="heading">
                    @lang('buttons.add_new_bank')</h3>
                <div class="formSep control-group ">
                    <label class="control-label required">
                        @lang('inputs.bank_name')
                        <span class="help-block error-bank_name">
                               @if(isset($messages))
                                {{$messages}}
                            @endif

                        </span>
                    </label>
                    <div class="controls">
                        <input type="text" name="bank_name"  id='bank_name'/>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span6">
                        <a href="{{URL::previous()}}" class="btn button-prev">
                            <i class="icon-chevron-left icon-white"></i> @lang('buttons.cancel') </a>
                        <span class="loader button-next"><i class="loader-icon"></i></span>
                    </div>

                    <div class="span6">
                        <button style="float: right" class="btn btn-inverse" type="submit">
                            @lang('buttons.save') <i class="icon-ok icon-white"></i>
                        </button>
                    </div>
                </div>

            </form>
        </div>


    </div>



@endsection