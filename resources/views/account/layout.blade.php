@php
    use Illuminate\Support\Facades\Request;
    $url1 = explode( '/', Request::path() );
 array_shift($url1);
 $url =  implode("/",$url1);

use App\Http\Models\Role;

@endphp

        <!DOCTYPE html>
<html lang="en" class="login_page">
<link rel="shortcut icon" type="image/x-icon" href="{{asset('/site/img/favicon_ecolo.png')}}"/>
<div id="loading_layer" style="display:none"><img src="/site/img/ajax_loader.gif"/></div>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>Alfa-Ecolo</title>

    <!-- Bootstrap framework -->
    <link rel="stylesheet" href="{{asset('/site/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/site/bootstrap/css/bootstrap-responsive.min.css')}}">
{{--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">--}}
{{--<link rel="stylesheet" type="text/css"--}}
{{--href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">--}}
<!-- Font Awesome (http://fortawesome.github.com/Font-Awesome) -->
    <link rel="stylesheet" href="{{asset('/site/css/font-awesome/css/font-awesome.min.css')}}">
    <!--[if IE 7]>
    <link rel="stylesheet" href="{{asset('/site/css/font-awesome/css/font-awesome-ie7.min.css')}}">
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.18/datatables.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- theme color-->

    <link rel="stylesheet" href="{{asset('/site/css/blue.css')}}">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
    <!-- tooltip -->
    <link rel="stylesheet" href="{{asset('/site/lib/qtip2/jquery.qtip.min.css')}}">
    <!-- flags -->
    <link rel="stylesheet" href="{{asset('/site/img/flags/flags.css')}}"/>
    <!-- main styles -->
    <link rel="stylesheet" href="{{asset('/site/css/style.css')}}">


    <!-- custom styles -->
    <link rel="stylesheet" href="{{asset('/site/css/eb_custom.css')}}">
    <link rel="stylesheet" href="{{asset('/site/css/file-explore.css')}}">


    <!-- Favicon -->

    <link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="{{asset('/site/js/Croppie/croppie.css')}}">

<!--[if lte IE 8]>
    <script src="{{asset('/site/js/ie/html5.js')}}"></script>
    <script src="{{asset('/site/js/ie/respond.min.js')}}"></script>
    <![endif]-->

    <style>
        .evaluate-response-div{
            height: 270px;
            overflow-y: scroll;
            font-size: 15px;
            line-height: 30px;
            padding: 4px;
        }

    </style>

</head>
<body>


<header>

    <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container-fluid">

                <a class="brand" href="/{{App::getLocale()}}/account">
                    <i class="icon-home icon-white"></i> ECOLO (Efficient COrporate LOans) 2.1
                </a>

                @if (Auth::user()->role_id == Role::get_role_id_by_name(Role::ROLE_BANK_MANAGER) || Auth::user()->role_id == Role::get_role_id_by_name(Role::ROLE_BANK_SUPERVISOR))
                <a class="brand" href="/{{App::getLocale()}}/manager">
                    <i class="icon-home icon-white"></i> Кабинет
                </a>
                @endif



                <ul class="nav user_menu pull-right">

                    <li class="divider-vertical hidden-phone hidden-tablet"></li>
                    <li class="dropdown">
                        <a onclick="switchLan()" style="cursor: pointer;">
                            @if(App::getLocale() == 'en')
                                <i class="flag-gb"></i>
                            @elseif(App::getLocale() == 'ru')
                                <i class="flag-ru"></i>

                            @else
                                <i class="flag-it"></i>

                            @endif

                            <b style="border-top-color: #fff" class="caret"></b></a>
                        <ul id="menuLanguage" class="dropdown-menu">
                            <li>
                                <a href="/en/{{$url}}">
                                    <i class="flag-gb"></i>
                                    English
                                </a>
                            </li>
                            <li>
                                <a href="/it/{{$url}}">
                                    <i class="flag-it"></i>
                                    Italiano
                                </a>
                            </li>
                            <li>
                                <a href="/ru/{{$url}}">
                                    <i class="flag-ru"></i>
                                    Pусский
                                </a>
                            </li>
                        </ul>

                        <script type="application/javascript">

                            var open = false;

                            function switchLan() {

                                if (open) {
                                    open = false;
                                    $('#menuLanguage').hide();
                                } else {
                                    open = true;
                                    $('#menuLanguage').show();
                                }

                            }

                        </script>
                    </li>

                    <li class="divider-vertical hidden-phone hidden-tablet"></li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img
                                    src="/site/img/user_avatar.png"
                                    alt=""
                                    class="user_avatar"> {{\Illuminate\Support\Facades\Auth::user()->username}}
                            <b class="caret"></b></a>
                        <ul class="dropdown-menu">

                            <!--
                            <li><a href="javascript:void(0)" onclick="smoke.alert('Not available in demo version');return false;">My Profile</a></li>
                            <li class="divider"></li>-->

                            <li><a href="/{{App::getLocale()}}/logout">@lang('links.logout')</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <a data-target=".nav-collapse" data-toggle="collapse" class="btn_menu">
                    <span class="icon-align-justify icon-white"></span>
                </a>


            </div>
        </div>
    </div>
</header>

<div id="wrapper" @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_MINIMUM) || \Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_FINAL_CLIENT)) style="padding-left: 0px" @endif>
@if(\Illuminate\Support\Facades\Auth::user()->role_id != \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_FINAL_CLIENT) && \Illuminate\Support\Facades\Auth::user()->role_id != \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_MINIMUM) )

    <!-- Sidebar -->
        <div id="sidebar-wrapper" class="sidebar">

            <form action="/{{App::getLocale()}}/company/search-company" class="input-append" method="GET">
                <input autocomplete="off" name="company_name" class="search_query input-medium" size="16"
                       type="text"
                      style="margin-left: -10px" placeholder="@lang('links.search_company') ..."/>
                <button type="submit" class="btn">
                    <i class="icon-search"></i>
                </button>
                <a href="#menu-toggle" class="btn btn-default " id="menu-toggle" >
                    <i class="fa fa-bars"></i> </a>
            </form>

            <div id="side_accordion" class="accordion">

                <div class="accordion-group">

                    <div class="accordion-heading">
                        <a href="#collapse_1" data-parent="#side_accordion"
                           data-toggle="collapse"
                           class=" accordion-toggle">
                            <i class="icon-folder-close"></i> @lang('global.companies')
                        </a>
                    </div>
                    <div class="accordion-body collapse @if(strpos(URL::current(),'company') !== false ) in @endif"
                         id="collapse_1">
                        <div class="accordion-inner">
                            <ul class="nav nav-list">
                                @if(\Illuminate\Support\Facades\Auth::user()->role_id != \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_SUPERVISOR) && \Illuminate\Support\Facades\Auth::user()->role_id != \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_MINIMUM))
                                <li>
                                    <a
                                       href="/{{App::getLocale()}}/company/new">
                                        @lang('links.new_company')
                                    </a>
                                </li>
                                @endif
                                <li class="">
                                    <a class=""
                                       href="/{{App::getLocale()}}/company/print-questionnaire">
                                        @lang('links.print_questionnaire')
                                    </a>
                                </li>
                                @if(\Illuminate\Support\Facades\Auth::user()->role_id <= \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_BACK_OFFICE))

                                    <li class="">
                                        <a @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_SUPERVISOR) && \Illuminate\Support\Facades\Auth::user()->role_id != \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_MINIMUM))
                                           class="disabled" @endif
                                           href="/{{App::getLocale()}}/company/import-pdf">
                                            @lang('links.import_pdf')
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                @if(\Illuminate\Support\Facades\Auth::user()->role_id <= \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_BACK_OFFICE))


                    <div class="accordion-group">

                        <div class="accordion-heading">
                            <a href="#collapse_3" data-parent="#side_accordion"
                               data-toggle="collapse"
                               class=" accordion-toggle ">
                                <i class="icon-briefcase"></i> @lang('links.administration')
                            </a>
                        </div>
                        <div class="accordion-body collapse  @if(strpos(URL::current(),'admin') !== false ) in @endif"
                             id="collapse_3">
                            <div class="accordion-inner ">
                                <ul class="nav nav-list">

                                    <li class="">
                                        <a @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_SUPERVISOR))
                                           class="disabled" @endif
                                           href="/{{App::getLocale()}}/admin/import-excel">
                                            @lang('links.data_import')                                            </a>
                                    </li>

                                    <li class="">
                                        <a @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_SUPERVISOR))
                                           class="disabled" @endif
                                           href="/{{App::getLocale()}}/admin/import-excel-for-update">
                                            @lang('links.data_import_update')                                            </a>
                                    </li>


                                    <li class="">
                                        <a @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_SUPERVISOR))
                                           class="disabled" @endif
                                           href="/{{App::getLocale()}}/admin/check">
                                            @lang('links.checks')                                            </a>
                                        </a>
                                    </li>


                                    <li class="">
                                        <a @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_SUPERVISOR))
                                           class="disabled" @endif
                                           href="/{{App::getLocale()}}/admin/user-management">
                                            @lang('links.user_management')                                            </a>

                                        </a>
                                    </li>
                                    <li class="">
                                        <a @if(\Illuminate\Support\Facades\Auth::user()->role_id != \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN))
                                           class="disabled" @endif
                                           href="/{{App::getLocale()}}/admin/banks-list">
                                            @lang('global.banks_list')
                                        </a>
                                    </li>
                                    <li class="">
                                        <a @if(\Illuminate\Support\Facades\Auth::user()->role_id != \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN))
                                           class="disabled" @endif
                                           href="/{{App::getLocale()}}/admin/bank-settings">
                                        {{Route::input('subdomain')}}
                                            </a>
                                    </li>

                                    <li class="">
                                        <a @if(\Illuminate\Support\Facades\Auth::user()->role_id != \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN))
                                           class="disabled" @endif
                                           href="/{{App::getLocale()}}/admin/action-tools">
                                            @lang('global.action')
                                        </a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="push"></div>
            </div>

            @if(\Illuminate\Support\Facades\Auth::user()->role_id <= 2)

                <div class="sidebar_info">
                    <ul class="unstyled">
                        <li>
                            <span class="act act-success"> {{count(\App\Http\Models\CompanyList::all())}} </span>
                            <strong>@lang('global.companies')</strong>
                        </li>
                        <li>
                            <span class="act act-danger">0</span>
                            <strong>@lang('global.balances')</strong>
                        </li>
                        <li>
                            <span class="act act-danger">0</span>
                            <strong>@lang('global.rated_companies')</strong>
                        </li>
                    </ul>
                </div>
            @endif


        </div>
    @endif
    <div id="page-content-wrapper">
        <div class="container-fluid">

            @yield('content')

        </div>
    </div>
    <div class="modal" id="send_to_eval" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body" style="text-align: center">
                   <h2>@lang('global.after_sending')</h2>
                    <span class="error delete-error"></span>
                </div>
                <input type="hidden" name="user_id" value="">
                <input type="hidden" name="company_id" value="">
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('buttons.cancel')</button>
                    <button type="button" class="btn btn-primary send-to-eval" data-action="" data-id="">@lang('buttons.send')
                    </button>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="modal" id="evaluate-div" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;height: 65%;text-align: center;overflow: hidden" >
    <div class="modal-dialog modal-dialog-centered" role="document">
        <h2 style="margin-top: 40px;color: darkgreen">@lang('global.eval_success')</h2>
        <div class="evaluate-response-div"></div>
        <button type="button" onclick="window.location.reload()" class="btn btn-secondary" data-dismiss="modal" style="padding: 5px 30px;">OK</button>

    </div>
</div>
<div class="modal" id="evaluate-div-error" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;height: 40%;text-align: center;overflow: hidden" >
    <div class="modal-dialog modal-dialog-centered" role="document">
        <h2 style="margin-top: 80px;color: red">
            @lang('global.eval_error')

            </h2>
        <button type="button" onclick="window.location.reload()" class="btn btn-secondary" data-dismiss="modal" style="padding: 5px 30px;margin-top: 80px">OK</button>

    </div>
</div>

<!-- /#sidebar-wrapper -->

<!-- Page Content -->

<!-- /#page-content-wrapper -->
<script>
    if (typeof(lang) == "undefined") {
        var lang = window.location.pathname;
        lang.indexOf(1);

        lang.toLowerCase();

        lang = lang.split("/")[1];

        lang = '/' + lang;
    }

</script>

<script src="{{asset('/site/js/jquery.min.js')}}"></script>
<script src="{{asset('/site/js/jquery.actual.min.js')}}"></script>
<script src="{{asset('/site/lib/validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('/site/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('/site/js/number-divider.min.js')}}"></script>
<script src="{{asset('/site/js/eb_rating_common.js')}}"></script>
<script src="{{asset('/site/js/file-explore.js')}}"></script>
<script src="{{asset('/site/js/user.js')}}"></script>
@if(strpos(Request::path(),'/admin/bank-settings') === false)
<script src="{{asset('/site/js/company.js?x=dsssssss')}}"></script>

@endif
<script src="{{asset('/site/js/bank.js?x=xwwwqq')}}"></script>
<script src="{{asset('/site/js/study.js?x=p2d22')}}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/js/all.min.js"></script>
<script type="text/javascript" charset="utf8"
        src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
<script type="text/javascript" charset="utf8"
        src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script type="text/javascript" charset="utf8"
        src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.js"></script>

<script src="{{asset('/site/js/action-tools.js')}}"></script>
<script src="{{asset('/site/js/dataTable.js')}}"></script>

<script type="application/javascript">

    var open = false;

    function switchLan() {

        if (open) {
            open = false;
            $('#menuLanguage').hide();
        } else {
            open = true;
            $('#menuLanguage').show();
        }

    }

</script>


<script src="{{asset('/site/js/auth/login.js')}}"></script>

<script src="{{asset('/site/js/auth/register.js')}}"></script>
<script src="{{asset('/site/js/auth/reset_password.js')}}"></script>

<script src="{{asset('/site/js/Croppie/croppie.js')}}"></script>

<script>
    $(document).ready(function () {
        $('.sidebar_switch').click(function () {
            let alertLogin = $('.sidebar_switch');
            alertLogin.toggleClass('on_switch');
            alertLogin.toggleClass('off_switch');
            $('.sidebar').toggle()
            var main_content = $(".main_content")
            main_content.toggleClass('isOut')
            var isOut = main_content.hasClass('isOut')
            main_content.animate({marginLeft: isOut ? '0px' : '240px'})

        });
        let table = $('#example').DataTable({
            responsive: true,
            "searching": true
        });

        $("#menu-toggle").click(function (e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });

    });
</script>

</body>

</html>
