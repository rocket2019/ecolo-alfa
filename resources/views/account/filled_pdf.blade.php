<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title>{{$study_code}} | {{$ateco_code}} - {{$ateco_description}}</title>
    <style>

        body {
            font-family: Verdana !important;
        }


        table td:nth-child(4) {

            text-align: center;
        }

        .input_short {
            width: 20px;
            background: #b3c7e6;
            border-radius: 0px;
            border-color: #a3bde6;
        }

        .input_long {
            width: 70px;
            background: #b3c7e6;
            border-radius: 0px;
            border-color: #a3bde6;
        }


        tr.blue {
            background: #f0f1f2;
            border: 1px solid #f0f1f2;
        }

        .heading {
            font-size: 15px;
            font-style: normal;
            font-weight: bold;
            margin-bottom: 10px;
            margin-top: 10px;
            display: block;
            /*border-bottom: 1px solid black;*/

        }

        input[type=checkbox] {
            /* Double-sized Checkboxes */
            -ms-transform: scale(2); /* IE */
            -moz-transform: scale(2); /* FF */
            -webkit-transform: scale(2); /* Safari and Chrome */
            -o-transform: scale(2); /* Opera */
            transform: scale(2);
            padding: 40px;
        }


        .company_div {
            border: 1px solid darkgrey;
            position: absolute;
            background: white;
            text-align: center;
            padding: 5px 8px;
            margin: -44px 0px 0px 15px;
            width: 90px;
            font-weight: bold;
            z-index: 9999999999;
        }

        .company_table {
            width: 100%;
        }

        .company_table tr td:nth-child(1) {
            width: 50%;
        }

        .company_input {
            width: 300px;
            background: #b3c7e6;
            border-radius: 0px;
            border-color: #a3bde6;

        }

        .company_table tr td input {
            width: 100px;
            background: #b3c7e6;
            border-radius: 0px;
            border-color: #a3bde6;
        }

        .questionnaie_table table {
            height: 100%;
        }

        .questionnaie_table table td {
            padding: 5px 15px;


        }

    </style>
</head>
<body>


<div style="width: 100%;margin:30px auto">
    <table style="width: 100%;">
        <tr>
            <td> @if(\App\Http\Models\BankSetting::get_logo() &&  file_exists(public_path('/storage/'.\App\Http\Models\BankSetting::get_logo())))
                    <img
                        src="{{asset('/storage/'.\App\Http\Models\BankSetting::get_logo())}}"
                        style="height: 110px; width: 110px;border-right: 1px solid darkgrey;padding-right: 15px">
                @endif
                <img src="{{asset('/site/img/evabeta.png')}}" style="height: 70px; width: 130px;padding-left: 10px;padding-bottom: 18px;">


            </td>

            <td style="text-align: right">
                <table>
                    <tr>
                        <td style="display: inline">
                            <img src="{{asset('/site/img/support.png')}}" style="height: 50px; width: 50px;"></td>

                        <td style="display: inline"> @lang('global.feedback')
                            <br>{{\App\Http\Models\BankSetting::get_phone()}}</td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>


    <form>
        @csrf

        <div style="width: 95%;margin:20px auto;color: black;">
            <span style="font-weight: bold;font-size: 15px">@lang('labels.code_ecolo'):</span><br>
            <span
                style="display: block;font-size: 15px;font-weight: bold">{{$study_code}} | {{$ateco_code}} - {{$ateco_description}} </span>
            <div style="border: 1px solid darkgrey;padding:20px 10px 10px 10px;margin-top: 38px;">
                <div class="company_div">
                    @lang('labels.company')
                </div>
                <table class="company_table">
                    <tr>
                        <td>@lang('labels.company_name')</td>
                        <td><input type="text" name="company_name" class="company_input"
                                   value="{{$company->company_name}}">
                        </td>
                    </tr>

                    <tr>
                        <td>@lang('labels.main_activity')</td>
                        <td><input type="text" name="main_activity" class="company_input"
                                   value="{{$company->main_activity}}">
                        </td>
                    </tr>
                    <tr>

                        <td>@lang('labels.legal_form')</td>
                        <td><input name="legal_form" type="text" class="company_input"
                                   value="{{$company->legal_forms ? $company->legal_forms->name : ''}}">
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('labels.subject')</td>
                        <td><input name="subject" type="text" class="company_input"
                                   value="{{$area_name}}">
                        </td>
                    </tr>

                </table>
                <table class="company_table" style="margin-top: 15px">
                    <tr>
                        <td>@lang('labels.id_number')</td>
                        <td><input name="id_number" type="text" class="company_input"
                                   value="{{$company->INN_number}}">
                        </td>
                    </tr>

                    <tr>
                        <td>@lang('labels.mail_bank_employee')<br><span
                                style="color: grey;font-style: italic">(@lang('global.fill_by_bank'))</span>
                        </td>
                        <td style="vertical-align: top"><input name="bank_email" type="text" class="company_input"
                                                               @if(isset($company->user)) value="{{$company->user->email}}" @endif>
                        </td>
                    </tr>
                </table>


            </div>
        </div>

        <div style="width: 100%;margin:30px auto" class="questionnaie_table">

            @if(array_key_exists('A', $parameters))
                <span class="heading">
                     @php
                         $path = explode("|", $parameters['A_path']->param_path)
                     @endphp

                    {{$path[0]}}
                </span>

                <table style="border-top: 1px solid #3264A3 ;padding: 30px 0">
                    @php
                        $count = 0;
                    @endphp

                    @foreach($parameters['A'] as $item=>$b)
                        @php
                            $count_a = 0;
                        @endphp

                        @if(strpos($item, 'nosub') === false)
                            <tr>
                                <td colspan="4">
                                    <h3 class="heading" style="font-size: 20px"><b>{{$item}}</b></h3>
                                </td>
                            </tr>
                        @endif

                        @foreach($b as $key=>$parameter)
                            @if ($parameter->sose_param->visible == 1)
                                @php $count_a++;$count++; @endphp

                                <tr @if($count % 2 == 0) class="blue" @endif>
                                    <td style="width:70%;font-size:16px">
                                        {{$parameter->sose_param->desc}}
                                    </td>
                                    <td>{{$parameter->sose_param->parameter_code}}</td>
                                    <td style="text-align:center"><input
                                            @if($parameter->sose_param->value_type == 'CB') class="input_short"
                                            type="checkbox" value="0"
                                            @else   class="input_long" type="text"
                                            @endif
                                            @foreach($values as $value) @if($value->parameter_id == $parameter->sose_param->id )@if($parameter->sose_param->value_type == 'CB' && $value->value==1) checked="checked"
                                            @elseif($parameter->sose_param->value_type != 'CB') value="{{$value->value}}"
                                            @endif
                                            @endif @endforeach
                                            name="parameter_id_{{$parameter->sose_param->id}}">
                                    </td>
                                    <td>{{$parameter->sose_param->unit}}</td>
                                </tr>
                            @endif
                        @endforeach
                    @endforeach
                </table>
            @endif

            @if(count($b_units))
                <span class="heading">
                    {{$first->sose_param->param_path}}
                </span>
                <table style="border-top: 1px solid #3264A3 ;padding: 30px 0">
                    <tr>
                        <td style="width:70%;font-size:16px">
                            @lang('global.total_numbers')
                        </td>
                        <td></td>
                        <td style="text-align:center">{{$questionnaire->company_unit}}
                        </td>
                        <td></td>

                    </tr>

                    @php
                        $key=0;
                        $previous_subheading = 'nosub';
                        $b_count = 0;
                    @endphp

                    @foreach($b_units as $parameter)
                        @if($parameter->sose_param->value_type== "CAT" )
                            @php $key++; $b_count = 0;@endphp
                        @endif
                        @if($key<=$questionnaire->company_unit)

                            @if ($parameter->sose_param->value_type== "CAT")

                                <tr>
                                    <td style="width:70%;font-size:16px;border: none!important;">
                                        <span
                                            style="font-weight: bold">@lang('global.structural_unit') N. {{$key}}</span>
                                    </td>

                                </tr>
                            @endif
                            @php

                                $find_subheading = explode("|", $parameter->sose_param->param_path);
                                if (isset($find_subheading[1]) && (strlen($find_subheading[1]) > 0 && strlen(trim($find_subheading[1])) != 0))
                                {
                                    $find_subheading = $find_subheading[1];
                                    if ($find_subheading != $previous_subheading)
                                        {
                                            $b_count = 1;
                                        }else{
                                             $b_count++;
                                        }


                                }else{

                                    $find_subheading = 'nosub';
                                }
                            @endphp



                            @if(strpos($find_subheading, 'nosub') === false && $b_count < 2)

                                <tr>
                                    <td colspan="4" style="border: none!important;">
                                        <h3 class="heading"><b>{{$find_subheading}}</b></h3>
                                    </td>
                                </tr>
                                @php $previous_subheading = $find_subheading @endphp


                            @endif
                            @if ($parameter->sose_param->visible == 1)

                                @php $count++; @endphp
                                <tr @if($count %2 != 0) class="blue" @endif>
                                    <td style="width:70%;font-size:16px">
                                        {{$parameter->sose_param->desc}}
                                    </td>
                                    <td>{{$parameter->sose_param->parameter_code}}</td>
                                    <td style="text-align:center"><input
                                            @if($parameter->sose_param->value_type == 'CB') class="input_short"
                                            type="checkbox" value="0"
                                            @else   class="input_long" type="text"
                                            @endif
                                            @foreach($values as $value) @if($value->parameter_id == $parameter->sose_param->id )@if($parameter->sose_param->value_type == 'CB' && $value->value==1) checked="checked"
                                            @elseif($parameter->sose_param->value_type != 'CB') value="{{$value->value}}"
                                            @endif
                                            @endif @endforeach
                                            name="parameter_id_{{$parameter->sose_param->id}}">
                                    </td>
                                    <td>{{$parameter->sose_param->unit}}</td>

                                </tr>
                            @endif
                        @endif
                    @endforeach


                </table>

            @endif
            @if(array_key_exists('C', $parameters))
                <span class="heading">
                     @php
                         $path =explode("|", $parameters['C_path']->param_path)
                     @endphp
                    {{$path[0]}}
                </span>

                <table style="border-top: 1px solid #3264A3 ;padding: 30px 0">
                    @php
                        $count = 0;
                    @endphp

                    @foreach($parameters['C'] as $item=>$b)
                        @if(strpos($item, 'nosub') === false)
                            <tr>
                                <td colspan="4">
                                    <h3 class="heading" style="font-size: 20px"><b>{{$item}}</b></h3>
                                </td>
                            </tr>
                        @endif
                        @foreach($b as $key=>$parameter)
                            @if ($parameter->sose_param->visible == 1)
                                @php
                                    $count_c = 0;
                                @endphp

                                @php $count_c++;$count++ @endphp

                                <tr @if($count %2 != 0) class="blue" @endif>
                                    <td style="width:70%;font-size:16px">
                                        {{$parameter->sose_param->desc}}
                                    </td>
                                    <td>{{$parameter->sose_param->parameter_code}}</td>
                                    <td style="text-align:center"><input
                                            @if($parameter->sose_param->value_type == 'CB') class="input_short"
                                            type="checkbox" value="0"
                                            @else   class="input_long" type="text"
                                            @endif
                                            @foreach($values as $value) @if($value->parameter_id == $parameter->sose_param->id )@if($parameter->sose_param->value_type == 'CB' && $value->value==1) checked="checked"
                                            @elseif($parameter->sose_param->value_type != 'CB') value="{{$value->value}}"
                                            @endif
                                            @endif @endforeach
                                            name="parameter_id_{{$parameter->sose_param->id}}">
                                    </td>
                                    <td>{{$parameter->sose_param->unit}}</td>

                                </tr>
                            @endif
                        @endforeach
                    @endforeach
                </table>

            @endif
            @if(array_key_exists('D', $parameters))
                <span class="heading">
                     @php
                         $path =explode("|", $parameters['D_path']->param_path)
                     @endphp
                    {{$path[0]}}
                </span>
                <table style="border-top: 1px solid #3264A3 ;padding: 30px 0">
                    @php
                        $count = 0;
                    @endphp
                    @foreach($parameters['D'] as $item=>$b)
                        @php
                            $count_d = 0;
                        @endphp
                        @if(strpos($item, 'nosub') === false)
                            <tr>
                                <td colspan="4">
                                    <h3 class="heading" style="font-size: 20px"><b>{{$item}}</b></h3>
                                </td>
                            </tr>
                        @endif
                        @foreach($b as $key=>$parameter)
                            @if ($parameter->sose_param->visible == 1)
                                @php $count_d++;$count++; @endphp

                                <tr @if($count %2 != 0) class="blue" @endif>
                                    <td style="width:70%;font-size:16px">
                                        {{$parameter->sose_param->desc}}
                                    </td>
                                    <td>{{$parameter->sose_param->parameter_code}}</td>
                                    <td style="text-align:center"><input
                                            @if($parameter->sose_param->value_type == 'CB') class="input_short"
                                            type="checkbox" value="0"
                                            @else   class="input_long" type="text"
                                            @endif
                                            @foreach($values as $value) @if($value->parameter_id == $parameter->sose_param->id )@if($parameter->sose_param->value_type == 'CB' && $value->value==1) checked="checked"
                                            @elseif($parameter->sose_param->value_type != 'CB') value="{{$value->value}}"
                                            @endif
                                            @endif @endforeach
                                            name="parameter_id_{{$parameter->sose_param->id}}"
                                        >
                                    </td>
                                    <td>{{$parameter->sose_param->unit}}</td>
                                </tr>
                            @endif
                        @endforeach
                    @endforeach
                </table>

            @endif
            @if(array_key_exists('E', $parameters))
                <span class="heading">
                     @php
                         $path =explode("|", $parameters['E_path']->param_path)
                     @endphp
                    {{$path[0]}}
                </span>
                <table style="border-top: 1px solid #3264A3 ;padding: 30px 0">
                    @php
                        $count = 0;
                    @endphp

                    @foreach($parameters['E'] as $item=>$b)
                        @php
                            $count_e = 0;
                        @endphp

                        @if(strpos($item, 'nosub') === false)
                            <tr>
                                <td colspan="4">
                                    <h3 class="heading" style="font-size: 20px"><b>{{$item}}</b></h3>
                                </td>
                            </tr>
                        @endif

                        @foreach($b as $key=>$parameter)
                            @if ($parameter->sose_param->visible == 1)


                                @php $count_e++;$count++ @endphp

                                <tr @if($count %2 != 0) class="blue" @endif>
                                    <td style="width:70%;font-size:16px">
                                        {{$parameter->sose_param->desc}}
                                    </td>
                                    <td>{{$parameter->sose_param->parameter_code}}</td>
                                    <td style="text-align:center"><input
                                            @if($parameter->sose_param->value_type == 'CB') class="input_short"
                                            type="checkbox" value="0"
                                            @else   class="input_long" type="text"
                                            @endif
                                            @foreach($values as $value) @if($value->parameter_id == $parameter->sose_param->id )@if($parameter->sose_param->value_type == 'CB' && $value->value==1) checked="checked"
                                            @elseif($parameter->sose_param->value_type != 'CB') value="{{$value->value}}"
                                            @endif
                                            @endif @endforeach
                                            name="parameter_id_{{$parameter->sose_param->id}}">
                                    </td>
                                    <td>{{$parameter->sose_param->unit}}</td>

                                </tr>
                            @endif
                        @endforeach
                    @endforeach


                </table>

            @endif
            @if(array_key_exists('F', $parameters))
                <span class="heading">
                     @php
                         $path =explode("|", $parameters['F_path']->param_path)
                     @endphp
                    @if(!strpos($item, 'БЛОК F') && Config::get('app.locale') == 'ru')БЛОК F - @endif{{$path[0]}}
                </span>
                <table style="border-top: 1px solid #3264A3 ;padding: 30px 0">
                    @php
                        $count = 0;
                    @endphp

                    @foreach($parameters['F'] as $item=>$b)
                        @php
                            $count_f = 0;
                        @endphp

                        @if(strpos($item, 'nosub') === false)
                            <tr>
                                <td colspan="4">
                                    <h3 class="heading" style="font-size: 20px"><b>{{$item}}</b></h3>
                                </td>
                            </tr>
                        @endif

                        @foreach($b as $key=>$parameter)
                            @if ($parameter->sose_param->visible == 1)
                                @php $count_f++;$count++; @endphp

                                <tr @if($count %2 != 0) class="blue" @endif>
                                    <td style="width:70%;font-size:16px">
                                        {{$parameter->sose_param->desc}}
                                    </td>
                                    <td>{{$parameter->sose_param->parameter_code}}</td>
                                    <td style="text-align:center"><input
                                            @if($parameter->sose_param->value_type == 'CB') class="input_short"
                                            type="checkbox" value="0"
                                            @else   class="input_long" type="text"
                                            @endif
                                            @foreach($values as $value) @if($value->parameter_id == $parameter->sose_param->id ) @if($parameter->sose_param->value_type == 'CB' && $value->value==1) checked="checked"
                                            @elseif($parameter->sose_param->value_type != 'CB') value="{{$value->value}}"
                                            @endif
                                            @endif @endforeach

                                            name="parameter_id_{{$parameter->sose_param->id}}">
                                    </td>
                                    <td>{{$parameter->sose_param->unit}}</td>

                                </tr>
                            @endif
                        @endforeach
                    @endforeach


                </table>

            @endif
            @if(array_key_exists('L', $parameters))
                <span class="heading">
                     @php
                         $path =explode("|", $parameters['L_path']->param_path)
                     @endphp
                    {{$path[0]}}
                </span>
                <table style="border-top: 1px solid #3264A3 ;padding: 30px 0">
                    @php
                        $count = 0;
                    @endphp

                    @foreach($parameters['L'] as $item=>$b)
                        @php
                            $count_l = 0;
                        @endphp

                        @if(strpos($item, 'nosub') === false)
                            <tr>
                                <td colspan="4">
                                    <h3 class="heading" style="font-size: 20px"><b>{{$item}}</b></h3>
                                </td>
                            </tr>
                        @endif

                        @foreach($b as $key=>$parameter)
                            @if ($parameter->sose_param->visible == 1)
                                @php $count_l++;$count++; @endphp

                                <tr @if($count %2 != 0) class="blue" @endif>
                                    <td style="width:70%;font-size:16px">
                                        {{$parameter->sose_param->desc}}
                                    </td>
                                    <td>{{$parameter->sose_param->parameter_code}}</td>
                                    <td style="text-align:center"><input
                                            @if($parameter->sose_param->value_type == 'CB') class="input_short"
                                            type="checkbox" value="0"
                                            @else   class="input_long" type="text"
                                            @endif
                                            @foreach($values as $value) @if($value->parameter_id == $parameter->sose_param->id )@if($parameter->sose_param->value_type == 'CB' && $value->value==1) checked="checked"
                                            @elseif($parameter->sose_param->value_type != 'CB') value="{{$value->value}}"
                                            @endif
                                            @endif @endforeach
                                            name="parameter_id_{{$parameter->sose_param->id}}">
                                    </td>
                                    <td>{{$parameter->sose_param->unit}}</td>

                                </tr>
                            @endif
                        @endforeach
                    @endforeach


                </table>

            @endif
            @if(array_key_exists('Z', $parameters))
                <span class="heading">
                     @php
                         $path =explode("|", $parameters['Z_path']->param_path)
                     @endphp
                    {{$path[0]}}
                </span>
                <table style="border-top: 1px solid #3264A3 ;padding: 30px 0">
                    @php
                        $count = 0;
                    @endphp

                    @foreach($parameters['Z'] as $item=>$b)
                        @php
                            $count_z = 0;
                        @endphp

                        @if(strpos($item, 'nosub') === false && $count_z < 2)
                            <tr>
                                <td colspan="4">
                                    <h3 class="heading" style="font-size: 20px"><b>{{$item}}</b></h3>
                                </td>
                            </tr>
                        @endif

                        @foreach($b as $key=>$parameter)
                            @if ($parameter->sose_param->visible == 1)
                                @php $count_z++;$count++; @endphp

                                <tr @if($count %2 != 0) class="blue" @endif>
                                    <td style="width:70%;font-size:16px">
                                        {{$parameter->sose_param->desc}}
                                    </td>
                                    <td>{{$parameter->sose_param->parameter_code}}</td>
                                    <td style="text-align:center"><input
                                            @if($parameter->sose_param->value_type == 'CB') class="input_short"
                                            type="checkbox" value="checked"
                                            @else   class="input_long" type="text"
                                            @endif
                                            @foreach($values as $value) @if($value->parameter_id == $parameter->sose_param->id )@if($parameter->sose_param->value_type == 'CB' && $value->value==1) checked="checked"
                                            @elseif($parameter->sose_param->value_type != 'CB') value="{{$value->value}}"
                                            @endif
                                            @endif @endforeach
                                            name="parameter_id_{{$parameter->sose_param->id}}">
                                    </td>
                                    <td>{{$parameter->sose_param->unit}}</td>

                                </tr>
                            @endif
                        @endforeach
                    @endforeach
                </table>
            @endif
        </div>
    </form>
</div>
</body>

<script>
    var aInputs = document.querySelectorAll("input[type='checkbox']");
    for (var i = 0; i < aInputs.length; i++) {
        if (aInputs[i].value == 1) {
            aInputs[i].checked = true;
        }
    }
</script>
</html>
