@extends('account.layout')
@section('content')
    <div class="row-fluid">
        <div class="span12">
            <h3 class="heading">ECOLO @lang('global.banks_list')</h3>

            <div class="span12">
                <a style="margin-bottom:10px" href="/{{App::getLocale()}}/admin/new-bank" class="pull-right btn btn-info">@lang('buttons.add_new_bank')</a>
                <table class="table table-bordered table-striped table_vam" id="dt_gal">
                    <thead>
                    <tr>
                        <th>@lang('inputs.bank_name')</th>

                        <th>@lang('global.actions')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($banks))
                        @foreach($banks as $bank)
                            <tr>
                                <td>{{$bank->bank_name}}</td>

                                <td>

                                    <a href="#" class="btn btn-mini delete_bank" data-id="{{$bank->id}}"  title="@lang('buttons.remove_bank')"><i
                                                class="icon-trash" ></i></a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal" id="delete_bank" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        Are you sure you want delete this item ?
                        <span class="error delete-error"></span>
                    </div>
                    <input type="hidden" name="user_id" value="">
                    <div class="modal-footer">
                        <button type="button" class="" data-dismiss="modal">Cancel</button>
                        <button type="button" class="delete-bank" data-action="" data-id="">Delete</button>
                    </div>

                </div>
            </div>
        </div>
    </div>



@endsection
