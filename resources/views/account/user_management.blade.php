@extends('account.layout')
@section('content')
    <div class="row-fluid">
        <div class="span12">
            <h3 class="heading"> @lang('global.ECOLO_users_management')</h3>

            <div class="span12">
                <a style="margin-bottom:10px" href="/{{App::getLocale()}}/admin/add-new-user" class="pull-right btn btn-info">@lang('buttons.add_new_user')</a>
                <table class="table table-bordered table-striped table_vam" id="dt_gal">
                    <thead>
                    <tr>
                        <th>@lang('inputs.username')</th>
                        <th>@lang('inputs.email')</th>
                        <th>@lang('global.last_login')</th>
                        <th>@lang('global.roles')</th>
                        <th>@lang('global.actions')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($users))
                        @foreach($users as $user)
                    <tr>
                        <td>{{$user->username}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                           {{$user->last_login}}
                        </td>
                        <td>
                            {{\App\Http\Models\Role::get_role_name_by_id($user->role_id)}}
                        </td>
                        <td>
                            <a href="/{{App::getLocale()}}/admin/edit-user/{{$user->id}}" class="btn btn-mini" title="@lang('buttons.edit_user')"><i
                                        class="icon-pencil"></i></a>
                            <a href="#" class="btn btn-mini delete" data-id="{{$user->id}}" title="@lang('buttons.remove_user')"><i
                                        class="icon-trash" ></i></a>
                        </td>
                    </tr>
                    @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal" id="delete" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        Are you sure you want delete this item ?
                        <span class="error delete-error"></span>
                    </div>
                    <input type="hidden" name="user_id" value="">
                    <div class="modal-footer">
                        <button type="button" class="" data-dismiss="modal">Cancel</button>
                        <button type="button" class="delete-user" data-action="" data-id="">Delete</button>
                    </div>

                </div>
            </div>
        </div>
    </div>



@endsection
