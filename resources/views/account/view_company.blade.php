@extends('account.layout')
@section('content')
    <style>
        #evaluate-div .modal-content ul li {
            list-style: none;
            text-decoration: none;

        }

        .dropbtn {
            background-color: #4CAF50;
            color: white;
            padding: 16px;
            font-size: 16px;
            border: none;
            cursor: pointer;
        }

        .dropbtn:hover, .dropbtn:focus {
            background-color: #3e8e41;
        }

        #myInput {
            box-sizing: border-box;
            background-position: 14px 12px;
            background-repeat: no-repeat;
            font-size: 16px;
            padding: 18px 20px 12px 10px;
            border: none;
            border-bottom: 1px solid #ddd;
        }

        #myInput:focus {
            outline: 3px solid #ddd;
        }


        .dropdown-content {
            background-color: #f6f6f6;
            min-width: 230px;
            overflow: auto;
            border: 1px solid #ddd;
            z-index: 1;
        }

        .dropdown-content a {
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
        }

        .dropdown1 {
            height: 300px;
        }

        .dropdown1 a:hover {
            cursor: pointer;
            background-color: #ddd;
        }

        #selected {
            background: #DDE;

        }

        td.gerico-error {
            border:1px solid red;
        }

        td.gerico-error.first-td{
            border-right:1px solid #ddd;
        }

        td.gerico-error.second-td{
            border-left:1px solid #ddd;
            border-right:1px solid #ddd;
        }

        td.gerico-error.third-td{
            border-left: 1px solid #ddd;
        }


    </style>

    <h3 class="heading">
        <b>
            @if(count($company->user)) @lang('global.client')  {{$company->user->username}} . @endif
            @lang('global.company') {{$company->company_name}}    </b>
    </h3>

    <div class="row-fluid">
        <div class="span8">
            <div class="w-box">
                <div class="w-box-header">
                    @lang('global.company_details')
                    @if(\Illuminate\Support\Facades\Auth::user()->role_id <= \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_MANAGER) && \Illuminate\Support\Facades\Auth::user()->role_id != \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_SUPERVISOR))

                        <div class="pull-right">
                            <a href="/{{App::getLocale()}}/company/edit/{{$company->ID_company}}"
                               class="btn btn-mini tip_left"
                               title="">
                                <i class="icon-edit"></i>
                            </a>

                            <a class="btn btn-mini delete_company" href="#"
                               data-id="{{$company->ID_company}}"
                               title="">
                                <i class="icon-trash"></i>
                            </a>

                        </div>
                    @endif
                </div>
                <div class="w-box-content company_details_wdg">
                    <div class="row-fluid" style="margin-top: 0px">
                        <div class="span6">
                            <b>@lang('labels.company_name'):</b> {{$company->company_name}}
                        </div>

                        <div class="span6">
                            <b>@lang('labels.main_activity'):</b> {{$company->main_activity}}
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span6">
                            <b>@lang('labels.id_number'):</b> {{$company->INN_number}}
                        </div>

                        <div class="span6">
                            <b>@lang('labels.legal_form')
                                :</b>
                            @if($company->legal_form)
                            {{$company->legal_forms->name}}
                            @endif
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span6">
{{--                            @lang('labels.address')--}}
                            <b>Регион и муниципальное образование:</b> {{$region.' / '.$city}}
                        </div>
                        <div class="span6">
                            <b>@lang('labels.client_email'):</b>
                            @if(count($company->user))    {{$company->user->email}} @else No user @endif
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="span4">


            <div class="w-box">
                <div class="w-box-header">
                    @lang('global.comments')
                    @if(\Illuminate\Support\Facades\Auth::user()->role_id != \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_SUPERVISOR) && \Illuminate\Support\Facades\Auth::user()->role_id != \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_MINIMUM))

                        <div class="pull-right">
                            <a href="/{{App::getLocale()}}/company/comment/{{$company->ID_company}}"
                               class="btn btn-mini tip_left"
                               title=""><i class="icon-pencil"></i></a>
                        </div>
                    @endif
                </div>
            </div>

            <div class="w-box-content company_comments_wdg">
                @if(!count($comments))
                    <span class="info muted">@lang('global.no_comments')</span>
                @else
                    <ul>
                        @foreach($comments as $comment)
                            <li>
                                <span class="text">{{$comment->text}}</span>
                                <span class="subtext">@lang('labels.posted_by') {{$comment->user->username}} / {{\Carbon\Carbon::parse( $comment->created_at)->format('m-d-Y')}}    </span>
                            </li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>

    </div>
    <div class="row-fluid">

        <div class="span12">
            <div class="w-box">
                <div class="w-box-header">
                    @lang('global.press_new_or_work')
                    @if(\Illuminate\Support\Facades\Auth::user()->role_id <= \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_MANAGER) && \Illuminate\Support\Facades\Auth::user()->role_id != \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_SUPERVISOR))

                        <div class="pull-right">
                            <a href="#"
                               class="btn btn-mini tip_left new_questionnaire"><i class="icon-plus"></i></a>
                        </div>
                    @endif
                </div>

                <div id="sose_panel" class="w-box-content company_sose_wdg">
                    @if(!count($questionnaires))
                        <span class="info muted">@lang('global.no_data')</span>
                    @else
                        <table class="table table-bordered table-striped questionnaires-table" id="smpl_tbl" style="border-collapse: collapse !important;">
                            <thead>
                            <tr>
                                <td>@lang('labels.saved_questionnaires')</td>
                                <td>@lang('labels.available_results')</td>
                                <td>@lang('global.actions')</td>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                //this propertis ($tdBorder, $count) are just to add or remove table td's border-top value
                                $tdBorder = false;
                            @endphp
                            @foreach($questionnaires  as $questionnaire)
                                @if ($questionnaire->sose_questionnaire_status_id == 6)
                                    @php $tdBorder = true; @endphp
                                @endif
                                <tr>
                                    <td @if($tdBorder) class="gerico-error first-td" @endif>
                                        <div class="pull-left">
                                            <b>ATECO:{{$questionnaire->sose_ateco->ateco_code}}
                                                ID:{{$questionnaire->id}}({{$questionnaire->sose_study->study_code}}
                                                )@if(!$questionnaire->valid) <i class="icon-warning-sign ttip_r"></i> @endif</b><br>
                                            <small>@if(isset($questionnaire->user)){{$questionnaire->user->username}} @endif
                                                / {{$questionnaire->created}} /
                                                {{$questionnaire->sose_status->description}}
                                            </small>
                                        </div>
                                        @if(\Illuminate\Support\Facades\Auth::user()->role_id != \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_SUPERVISOR) && \Illuminate\Support\Facades\Auth::user()->role_id != \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_MINIMUM))

                                            <div class="pull-right">
                                                @if($questionnaire->sose_questionnaire_status_id == \App\Http\Models\SoseQuestionnaireStatus::IN_EDITING)
                                                    <a class="btn "
                                                       href="/{{App::getLocale()}}/company/edit/{{$questionnaire->company->ID_company}}/{{$questionnaire->id}}"><i
                                                                class="icon-edit"></i> @lang('buttons.edit')</a>
                                                @elseif($questionnaire->sose_questionnaire_status_id == \App\Http\Models\SoseQuestionnaireStatus::SUBMITTED)
                                                    <a class="btn "
                                                       href="/{{App::getLocale()}}/company/edit/{{$questionnaire->company->ID_company}}/{{$questionnaire->id}}"><i
                                                                class="icon-edit"></i> @lang('buttons.edit')</a>
                                                    <a class="btn send_to_eval" data-id="{{$questionnaire->id}}"><i
                                                                class="icon-refresh"></i> @lang('buttons.send')
                                                    </a>
                                                @elseif($questionnaire->sose_questionnaire_status_id == \App\Http\Models\SoseQuestionnaireStatus::REGISTERED_IN_BACKOFFICE
                                                && (\Illuminate\Support\Facades\Auth::user()->role_id ==\App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_BACK_OFFICE) || \Illuminate\Support\Facades\Auth::user()->role_id ==\App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN)))
                                                    <a class="btn btn-evaluate"
                                                       data-url="/{{App::getLocale()}}/company/runEvaluation/{{$questionnaire->company->ID_company}}/{{$questionnaire->id}}"><i
                                                                class="icon-refresh"></i> @lang('buttons.evaluate')
                                                    </a>

                                                @elseif($questionnaire->sose_questionnaire_status_id == \App\Http\Models\SoseQuestionnaireStatus::IN_BACKOFFICE_ELABORATION
                                                                                                && (\Illuminate\Support\Facades\Auth::user()->role_id ==\App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN) || \Illuminate\Support\Facades\Auth::user()->role_id ==\App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_BACK_OFFICE)))

                                                    <a class="btn to_bank" data-id="{{$questionnaire->id}}"><i
                                                                class="icon-circle-arrow-right"></i> @lang('buttons.to_bank')
                                                    </a>
                                                @endif
                                                @if(\Illuminate\Support\Facades\Auth::user()->role_id ==\App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN) && $questionnaire->sose_questionnaire_status_id > \App\Http\Models\SoseQuestionnaireStatus::SUBMITTED)

                                                    <a class="btn "
                                                       href="/{{App::getLocale()}}/company/edit/{{$questionnaire->company->ID_company}}/{{$questionnaire->id}}"><i
                                                                class="icon-edit"></i> @lang('buttons.edit')</a>

                                                    @endif
                                                    @if(\Illuminate\Support\Facades\Auth::user()->role_id ==\App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_ADMIN) && $questionnaire->sose_questionnaire_status_id > \App\Http\Models\SoseQuestionnaireStatus::REGISTERED_IN_BACKOFFICE)

                                                        <a class="btn btn-evaluate"
                                                           data-url="/{{App::getLocale()}}/company/runEvaluation/{{$questionnaire->company->ID_company}}/{{$questionnaire->id}}"><i
                                                                    class="icon-refresh"></i> @lang('buttons.evaluate')
                                                        </a>

                                                    @endif
                                            </div>
                                        @endif
                                        @if(\Illuminate\Support\Facades\Auth::user()->role_id != \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_FINAL_CLIENT))
                                            <div class="pull-right">
                                                <a href="/{{App::getLocale()}}/company/print/{{$questionnaire->company->ID_company}}/{{$questionnaire->id}}/{{$questionnaire->sose_ateco->id}}"
                                                   target="_blank" class="btn">@lang('buttons.print')</a>
                                            </div>
                                        @endif

                                    </td>
                                    <td @if($tdBorder) class="gerico-error second-td" @endif>
                                        @if(\Illuminate\Support\Facades\Auth::user()->role_id <= \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_EVA_BACK_OFFICE))


                                            @if($questionnaire->sose_questionnaire_status_id == \App\Http\Models\SoseQuestionnaireStatus::REGISTERED_IN_BACKOFFICE || $questionnaire->sose_questionnaire_status_id == \App\Http\Models\SoseQuestionnaireStatus::IN_EDITING || $questionnaire->sose_questionnaire_status_id == \App\Http\Models\SoseQuestionnaireStatus::SUBMITTED)

                                                @lang('global.no_results_available')
                                    </td>
                                    @else
                                        @if($questionnaire->valid)
                                        <a href="/{{App::getLocale()}}/company/export-excel-file/{{$questionnaire->company->ID_company}}/{{$questionnaire->id}}/{{$questionnaire->sose_ateco->ateco_code}}/{{$questionnaire->sose_study->study_code}}"
                                           class="btn"><i class="icon-share"></i> @lang('buttons.export')</a>
                                        <a class="btn exportGericoTxt"
                                           href="/{{App::getLocale()}}/company/gerico-txt/{{$questionnaire->company->ID_company}}/{{$questionnaire->id}}"><i
                                                    class="icon-share"></i>GericoTxt</a>
                                        @endif
                                    @endif
                                    @endif
                                    <td @if($tdBorder) class="gerico-error third-td" @endif>
                                        @if(\Illuminate\Support\Facades\Auth::user()->role_id != \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_SUPERVISOR) && \Illuminate\Support\Facades\Auth::user()->role_id != \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_MINIMUM))

                                            <a href="#" class="btn remove-questionnaire"
                                               data-id="{{$questionnaire->id}}"><i class="icon-trash"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                @php $tdBorder = false; @endphp
                            @endforeach
                            </tbody>

                        </table>
                    @endif

                </div>

            </div>
        </div>
    </div>

    <div class="modal" id="delete_company" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    Are you sure you want delete this item ?
                    <span class="error delete-error"></span>
                </div>
                <input type="hidden" name="user_id" value="">
                <div class="modal-footer">
                    <button data-dismiss="modal">Cancel</button>
                    <button class="delete-company" data-action="" data-id="">Delete
                    </button>
                </div>

            </div>
        </div>
    </div>
    <div class="modal" id="delete_questionnaire" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    Are you sure you want delete this item ?
                    <span class="error delete-error"></span>
                </div>
                <input type="hidden" name="user_id" value="">
                <div class="modal-footer">
                    <button type="button" class="" data-dismiss="modal">Cancel</button>
                    <button type="button" class=" delete-questionnaire" data-action="" data-id="">Delete
                    </button>
                </div>

            </div>
        </div>
    </div>


    <div id="questionnaire-div" style="">
        <a href="#" class="btn btn-mini close_question" style="float: right"><i class="icon-remove"></i></a>
        <h3 class="heading">@lang('global.select_questionnaire')</h3>
        <ul style="border:1px dashed grey;margin: 0 0 20px 0" class="file-tree">
            <li><a href="#">1 {{\App\Http\Models\CodeTree::get_by_code('1')->description}}</a>
                <ul>

                    <li><a href="#">1.1 {{\App\Http\Models\CodeTree::get_by_code('1.1')->description}}</a>
                        <ul>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('1.1.1')->id}}">
                                1.1.1 {{\App\Http\Models\CodeTree::get_by_code('1.1.1')->description}}</li>
                            <li>
                                <a href="#">1.1.2 {{\App\Http\Models\CodeTree::get_by_code('1.1.2')->description}}</a>
                                <ul>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('1.1.2.1')->id}}">
                                        1.1.2.1 {{\App\Http\Models\CodeTree::get_by_code('1.1.2.1')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('1.1.2.2')->id}}">
                                        1.1.2.2 {{\App\Http\Models\CodeTree::get_by_code('1.1.2.2')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('1.1.2.3')->id}}">
                                        1.1.2.3 {{\App\Http\Models\CodeTree::get_by_code('1.1.2.3')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('1.1.2.4')->id}}">
                                        1.1.2.4 {{\App\Http\Models\CodeTree::get_by_code('1.1.2.4')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('1.1.2.5')->id}}">
                                        1.1.2.5 {{\App\Http\Models\CodeTree::get_by_code('1.1.2.5')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('1.1.2.6')->id}}">
                                        1.1.2.6 {{\App\Http\Models\CodeTree::get_by_code('1.1.2.6')->description}}</li>

                                </ul>
                            </li>

                        </ul>
                    </li>
                    <li><a href="#">1.2 {{\App\Http\Models\CodeTree::get_by_code('1.2')->description}}</a>
                        <ul>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('1.2.1')->id}}">
                                1.2.1 {{\App\Http\Models\CodeTree::get_by_code('1.2.1')->description}}</li>
                            <li>
                                <a href="#">1.2.2 {{\App\Http\Models\CodeTree::get_by_code('1.2.2')->description}}</a>
                                <ul>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('1.2.2.1')->id}}">
                                        1.2.2.1 {{\App\Http\Models\CodeTree::get_by_code('1.2.2.1')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('1.2.2.2')->id}}">
                                        1.2.2.2 {{\App\Http\Models\CodeTree::get_by_code('1.2.2.2')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('1.2.2.3')->id}}">
                                        1.2.2.3 {{\App\Http\Models\CodeTree::get_by_code('1.2.2.3')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('1.2.2.4')->id}}">
                                        1.2.2.4 {{\App\Http\Models\CodeTree::get_by_code('1.2.2.4')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('1.2.2.5')->id}}">
                                        1.2.2.5 {{\App\Http\Models\CodeTree::get_by_code('1.2.2.5')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('1.2.2.6')->id}}">
                                        1.2.2.6 {{\App\Http\Models\CodeTree::get_by_code('1.2.2.6')->description}}</li>

                                </ul>
                            </li>

                        </ul>
                    </li>
                    <li><a href="#">1.3 {{\App\Http\Models\CodeTree::get_by_code('1.3')->description}}</a>
                        <ul>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('1.3.2')->id}}">
                                1.3.2 {{\App\Http\Models\CodeTree::get_by_code('1.3.2')->description}}</li>

                        </ul>
                    </li>
                    <li><a href="#">1.4 {{\App\Http\Models\CodeTree::get_by_code('1.4')->description}}</a>
                        <ul>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('1.4.1')->id}}">
                                1.4.1 {{\App\Http\Models\CodeTree::get_by_code('1.4.1')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('1.4.2')->id}}">
                                1.4.2 {{\App\Http\Models\CodeTree::get_by_code('1.4.2')->description}}</li>

                        </ul>
                    </li>

                </ul>
            </li>
            <li><a href="#">2 {{\App\Http\Models\CodeTree::get_by_code('2')->description}}</a>
                <ul>
                    <li><a href="#">2.1 {{\App\Http\Models\CodeTree::get_by_code('2.1')->description}}</a>
                        <ul>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.1.1')->id}}">
                                2.1.1 {{\App\Http\Models\CodeTree::get_by_code('2.1.1')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.1.2')->id}}">
                                2.1.2 {{\App\Http\Models\CodeTree::get_by_code('2.1.2')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.1.3')->id}}">
                                2.1.3 {{\App\Http\Models\CodeTree::get_by_code('2.1.3')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.1.4')->id}}">
                                2.1.4 {{\App\Http\Models\CodeTree::get_by_code('2.1.4')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.1.5')->id}}">
                                2.1.5 {{\App\Http\Models\CodeTree::get_by_code('2.1.5')->description}}</li>

                        </ul>
                    </li>
                    <li><a href="#">2.2 {{\App\Http\Models\CodeTree::get_by_code('2.2')->description}}</a>
                        <ul>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.2.1')->id}}">
                                2.2.1 {{\App\Http\Models\CodeTree::get_by_code('2.2.1')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.2.3')->id}}">
                                2.2.3 {{\App\Http\Models\CodeTree::get_by_code('2.2.3')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.2.4')->id}}">
                                2.2.4 {{\App\Http\Models\CodeTree::get_by_code('2.2.4')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.2.5')->id}}">
                                2.2.5 {{\App\Http\Models\CodeTree::get_by_code('2.2.5')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.2.6')->id}}">
                                2.2.6 {{\App\Http\Models\CodeTree::get_by_code('2.2.6')->description}}</li>

                        </ul>
                    </li>
                    <li><a href="#">2.3 {{\App\Http\Models\CodeTree::get_by_code('2.3')->description}}</a>
                        <ul>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.3.1')->id}}">
                                2.3.1 {{\App\Http\Models\CodeTree::get_by_code('2.3.1')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.3.2')->id}}">
                                2.3.2 {{\App\Http\Models\CodeTree::get_by_code('2.3.2')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.3.3')->id}}">
                                2.3.3 {{\App\Http\Models\CodeTree::get_by_code('2.3.3')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.3.4')->id}}">
                                2.3.4 {{\App\Http\Models\CodeTree::get_by_code('2.3.4')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.3.5')->id}}">
                                2.3.5 {{\App\Http\Models\CodeTree::get_by_code('2.3.5')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.6')->id}}">
                                2.3.6 {{\App\Http\Models\CodeTree::get_by_code('2.3.6')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.3.7')->id}}">
                                2.3.7 {{\App\Http\Models\CodeTree::get_by_code('2.3.7')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.3.8')->id}}">
                                2.3.8 {{\App\Http\Models\CodeTree::get_by_code('2.3.8')->description}}</li>

                        </ul>
                    </li>
                    <li><a href="#">2.4 {{\App\Http\Models\CodeTree::get_by_code('2.4')->description}}</a>
                        <ul>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.4.1')->id}}">
                                2.4.1 {{\App\Http\Models\CodeTree::get_by_code('2.4.1')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.4.2')->id}}">
                                2.4.2 {{\App\Http\Models\CodeTree::get_by_code('2.4.2')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.4.3')->id}}">
                                2.4.3 {{\App\Http\Models\CodeTree::get_by_code('2.4.3')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.4.4')->id}}">
                                2.4.4 {{\App\Http\Models\CodeTree::get_by_code('2.4.4')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.4.5')->id}}">
                                2.4.5 {{\App\Http\Models\CodeTree::get_by_code('2.4.5')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.4.6')->id}}">
                                2.4.6 {{\App\Http\Models\CodeTree::get_by_code('2.4.6')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.4.7')->id}}">
                                2.4.7 {{\App\Http\Models\CodeTree::get_by_code('2.4.7')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.4.8')->id}}">
                                2.4.8 {{\App\Http\Models\CodeTree::get_by_code('2.4.8')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('2.4.9')->id}}">
                                2.4.9 {{\App\Http\Models\CodeTree::get_by_code('2.4.9')->description}}</li>

                        </ul>
                    </li>
                    <li class="code_id" data-id="{{\App\Http\Models\CodeTree::get_by_code('2.5')->id}}">
                        2.5 {{\App\Http\Models\CodeTree::get_by_code('2.5')->description}}</li>

                </ul>
            </li>
            <li><a href="#">3 {{\App\Http\Models\CodeTree::get_by_code('3')->description}}</a>
                <ul>
                    <li><a href="#">3.1 {{\App\Http\Models\CodeTree::get_by_code('3.1')->description}}</a>
                        <ul>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('3.1.1')->id}}">
                                3.1.1 {{\App\Http\Models\CodeTree::get_by_code('3.1.1')->description}}</li>
                            <li>
                                <a href="#">3.1.2 {{\App\Http\Models\CodeTree::get_by_code('3.1.2')->description}}</a>
                                <ul>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('3.1.2.1')->id}}">
                                        3.1.2.1 {{\App\Http\Models\CodeTree::get_by_code('3.1.2.1')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('3.1.2.2')->id}}">
                                        3.1.2.2 {{\App\Http\Models\CodeTree::get_by_code('3.1.2.2')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('3.1.2.3')->id}}">
                                        3.1.2.3 {{\App\Http\Models\CodeTree::get_by_code('3.1.2.3')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('3.1.2.4')->id}}">
                                        3.1.2.4 {{\App\Http\Models\CodeTree::get_by_code('3.1.2.4')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('3.1.2.5')->id}}">
                                        3.1.2.5 {{\App\Http\Models\CodeTree::get_by_code('3.1.2.5')->description}}</li>

                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a href="#">3.2 {{\App\Http\Models\CodeTree::get_by_code('3.2')->description}}</a>
                        <ul>
                            <li>
                                <a href="#">3.2.1 {{\App\Http\Models\CodeTree::get_by_code('3.2.1')->description}}</a>
                                <ul>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.1.1')->id}}">
                                        3.2.1.1 {{\App\Http\Models\CodeTree::get_by_code('3.2.1.1')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.1.2')->id}}">
                                        3.2.1.2 {{\App\Http\Models\CodeTree::get_by_code('3.2.1.2')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.1.3')->id}}">
                                        3.2.1.3 {{\App\Http\Models\CodeTree::get_by_code('3.2.1.3')->description}}</li>

                                </ul>
                            </li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.2')->id}}">
                                3.2.2 {{\App\Http\Models\CodeTree::get_by_code('3.2.2')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.3')->id}}">
                                3.2.3 {{\App\Http\Models\CodeTree::get_by_code('3.2.3')->description}}</li>

                            <li>
                                <a href="#">3.2.4 {{\App\Http\Models\CodeTree::get_by_code('3.2.4')->description}}</a>
                                <ul>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.4.1')->id}}">
                                        3.2.4.1 {{\App\Http\Models\CodeTree::get_by_code('3.2.4.1')->description}}</li>
                                    <li class="code_id"
                                        data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.4.2')->id}}">
                                        3.2.4.2 {{\App\Http\Models\CodeTree::get_by_code('3.2.4.2')->description}}</li>

                                </ul>
                            </li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.5')->id}}">
                                3.2.5 {{\App\Http\Models\CodeTree::get_by_code('3.2.5')->description}}</li>
                            <li class="code_id"
                                data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.6')->id}}">
                                3.2.6 {{\App\Http\Models\CodeTree::get_by_code('3.2.6')->description}}</li>


                        </ul>
                    </li>

                </ul>
            </li>
        </ul>
        <input id="code_id" type="hidden">
        <input id="company_id" type="hidden" value="{{$company->ID_company}}">
        <div class="row-fluid">
            <div class="span3">@lang('global.select_specialisation')</div>
            <div class="span9">
                <div id="myDropdown" class="dropdown-content">
                    <input type="text" placeholder="@lang('buttons.search').." onclick="open_sose()" id="myInput"
                           style="width: 93%" autocomplete="off"
                           onkeyup="filterFunction()">
                    <button class="btn clear_ateco" style="vertical-align: top;display: inline-block"><i
                                class="icon-remove icon-white"></i>
                    </button>
                    <div class="dropdown1" id='sose_okveds_select' style="overflow: scroll;display: none">
                        @foreach($ateco as $at)
                            @php
                                $description = $at->sose_ateco->description;
                                $pieces = explode("[", $description);

                            @endphp

                            <a class="ateco_id_function" onclick="callmodifyAtecoImputValueAfterAnyMiliseconds(this)"
                               data-id="{{$at->sose_ateco->id}}">{{$at->sose_okved->okved_code.' - '.$at->sose_ateco->ateco_code.'-'.$pieces[0] }}
                                <span class="key-for-search" style="display: none">@if(isset($pieces[1])){{$pieces[1]}}@endif</span></a>

                        @endforeach
                    </div>
                </div>

            </div>

        </div>
        <div class="row-fluid" style="margin-top: 20px">
            <div class="span3">@lang('global.the_number_of_company_units')</div>
            <div class="span9">
                <select style="width: 100%" id="company_unit">
                    @for ($i = 1; $i <= 10; $i++)
                        <option value="{{ $i }}">{{ $i }}</option>
                    @endfor

                </select>

            </div>

        </div>

        <div style="float: right; display: flex;">
            <a style="display:none; color: aliceblue; font-weight: 500; background-color: rgb(37, 141, 180); text-align: center; padding: 6px 14px; border-radius: 4px; font-size: 13px; text-decoration: none; margin-right: 8px;" target="_blank" class="profile_btn product_codes"><i style="margin-right: 6px" class="fas fa-arrow-circle-down"></i>Таблица с кодами товаров</a>
            <button id="add-questionnaire" class="btn btn-inverse" disabled="true">
                @lang('buttons.new_questionnaire')
                <i class="icon-ok icon-white"></i>
            </button>

        </div>
    </div>


@endsection
