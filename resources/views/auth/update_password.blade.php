@extends('auth.layout')
@section('content')


    <form id="update_password">

        <div class="top_b">ECOLO (Efficient COrporate LOans) 2.1: .@lang('buttons.login')</div>
        <div class="alert alert-error alert-login" style="display: none">
            @lang('global.check_email')
        </div>

        <div class="cnt_b">

            <div class="formRow">
                <div class="input-prepend">
                    <span class="add-on">
                        <i class="icon-lock"></i>
                    </span>
                    <input type="password" name="password" placeholder="@lang('inputs.password'):"
                           value=""/>
                </div>
            </div>
            <div class="formRow">
                <div class="input-prepend">
                    <span class="add-on">
                        <i class="icon-lock"></i>
                    </span>
                    <input type="password" name="confirm_password" placeholder="@lang('inputs.confirm_password'):"
                           value=""/>
                </div>
            </div>
            <input type="hidden" name="reset_token" value="{{$token}}" id="reset_token">

        </div>
        <div class="btm_b clearfix">

            @lang('buttons.choose_language')
            <span class="navbar dropdown">
                    <a onClick="switchLan()" style="cursor: pointer;"> @if(App::getLocale() == 'en')
                            <i class="flag-gb"></i>
                        @elseif(App::getLocale() == 'ru')
                            <i class="flag-ru"></i>

                        @else
                            <i class="flag-it"></i>

                        @endif <b
                                style="border-top-color: #fff" class="caret"></b></a>
                            </span>
            <ul id="menuLanguage" class="dropdown-menu">
                <li>
                    <a href="/en/reset_token/{{$token}}">
                        <i class="flag-gb"></i>
                        English
                    </a>
                </li>
                <li>
                    <a href="/it/reset_token/{{$token}}">
                        <i class="flag-it"></i>
                        Italiano
                    </a>
                </li>
                <li>
                    <a href="/ru/reset_token/{{$token}}">
                        <i class="flag-ru"></i>
                        Pусский
                    </a>
                </li>
            </ul>


            <button class="btn btn-inverse update_password" type="button">
                <i class="icon-signin icon-white"></i>
                @lang('buttons.reset_password')
            </button>

        </div>
        <div class="btm_b clearfix">
            <a href="/{{App::getLocale()}}/reset">
                @lang('links.forgot_password') </a>
        </div>
    </form>
    <script type="application/javascript">

        var open = false;

        function switchLan() {
            if (open) {
                open = false;
                $('#menuLanguage').hide();
            } else {
                open = true;
                $('#menuLanguage').show();
            }

        }


    </script>
@endsection
