@extends('auth.layout')
@section('content')

    <form class="register_form2" id="register_form2">

        <div class="top_b">
            @lang('global.system') ECOLO 2.19: @lang('buttons.register')
        </div>

        <div class="alert alert-error alert-login" style="display: none">

        </div>


        <div class="cnt_b">


            <div class="input-prepend">
                <span class="add-on"><i class="icon-user"></i></span>
                <input type="text" id="fos_user_registration_form_username" name="username"
                       maxlength="255" pattern=".{2,}" placeholder="@lang('inputs.username'):"/>
            </div>
            <div class="input-prepend">
                <span class="add-on"><i class="icon-plane"></i></span>
                <input type="text" id="fos_user_registration_form_plainPassword_first" name="city"
                       placeholder="@lang('inputs.city'):"/>
            </div>
            <div class="input-prepend">
                <span class="add-on"><i class="icon-info-sign"></i></span>
                <input type="text" id="fos_user_registration_form_plainPassword_first" name="department"
                        placeholder="@lang('inputs.department'):"/>
            </div>

            <div class="input-prepend">
                <span class="add-on"><i class="icon-lock"></i></span>
                <input type="password" id="fos_user_registration_form_plainPassword_first" name="password"
                       placeholder="@lang('inputs.password'):"/>
            </div>

            <div class="input-prepend">
                <span class="add-on"><i class="icon-lock"></i></span>
                <input type="password" id="fos_user_registration_form_plainPassword_second" name="confirm_password"
                       placeholder="@lang('inputs.confirm_password'):"/>
            </div>

            <input type="hidden" name="verify_token" id="verify_token" value="{{$verify_token}}"/>

        </div>

        <div class="btm_b clearfix">
            @lang('buttons.choose_language') <span class="navbar dropdown">
                <a onClick="switchLan()" style="cursor: pointer;"> @if(App::getLocale() == 'en')
                        <i class="flag-gb"></i>
                    @elseif(App::getLocale() == 'ru')
                        <i class="flag-ru"></i>

                    @else
                        <i class="flag-it"></i>

                    @endif <b
                            style="border-top-color: #fff" class="caret"></b></a> </span>
<ul id="menuLanguage" class="dropdown-menu">
            <li>
            <a href="/en/verify_token/{{$verify_token}}">
                <i class="flag-gb"></i>
                English
            </a>
        </li>
            <li>
            <a href="/it/verify_token/{{$verify_token}}">
                <i class="flag-it"></i>
                Italiano
            </a>
        </li>
            <li>
            <a href="/ru/verify_token/{{$verify_token}}">
                <i class="flag-ru"></i>
                Pусский
            </a>
        </li>
    </ul>



        <script type="application/javascript">

            var open = false;

            function switchLan() {

                if (open) {
                    open = false;
                    $('#menuLanguage').hide();
                } else {
                    open = true;
                    $('#menuLanguage').show();
                }

            }

        </script>
        </span>
        <a href="/{{App::getLocale()}}/login" class="btn ">
            @lang('buttons.exit')
        </a>
        <button class="btn btn-inverse register2" type="button">
            <i class="icon-signin icon-white"></i>
            @lang('buttons.register')
        </button>

        </div>

    </form>

@endsection