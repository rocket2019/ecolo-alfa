@php
    use Illuminate\Support\Facades\Request;
@endphp
        <!DOCTYPE html>
<html lang="en" class="login_page">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>Welcome!</title>

    <!-- Bootstrap framework -->
    <link rel="stylesheet" href="{{asset('/site/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/site/bootstrap/css/bootstrap-responsive.min.css')}}">

    <!-- Font Awesome (http://fortawesome.github.com/Font-Awesome) -->
    <link rel="stylesheet" href="{{asset('/site/css/font-awesome/css/font-awesome.min.css')}}">
<!--[if IE 7]>
    <link rel="stylesheet" href="{{asset('/site/css/font-awesome/css/font-awesome-ie7.min.css')}}">
    <![endif]-->

    <!-- theme color-->
    <link rel="stylesheet" href="{{asset('/site/css/blue.css')}}">

    <!-- tooltip -->
    <link rel="stylesheet" href="{{asset('/site/lib/qtip2/jquery.qtip.min.css')}}">
    <!-- flags -->
    <link rel="stylesheet" href="{{asset('/site/img/flags/flags.css')}}"/>
    <!-- main styles -->
    <link rel="stylesheet" href="{{asset('/site/css/style.css')}}">

    <link rel="stylesheet" href="{{asset('/site/css/public_style.css')}}">

    <!-- custom styles -->
    <link rel="stylesheet" href="{{asset('/site/css/eb_custom.css')}}">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('/site/img/favicon_ecolo.png')}}" type="image/x-icon"/>

    <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>

<!--[if lte IE 8]>
    <script src="{{asset('/site/js/ie/html5.js')}}"></script>
    <script src="{{asset('/site/js/ie/respond.min.js')}}"></script>
    <![endif]-->


</head>
<body style="background: #126398">
<div id="loading_layer" style="display:none"><img src="/site/img/ajax_loader.gif" /></div>

<div class="login-container">
    {{--    <div id="splash_eb_logo"></div>--}}
    <div style="text-align: center; margin-bottom: 40px">
        <h1 style="
        color: #fff;
font-style: normal;
font-weight: bold;
font-size: 35px;
line-height: 22px;">
            ВХОД В СИСТЕМУ ECOLO
        </h1>
    </div>

    <div class="login_box">
        @yield('content')
    </div>

    <div style="text-align: center; margin-top: 47px">
        <img style="width: 165px" src="/site/img/evabeta_fff.png" alt="logo">
    </div>

</div>

<script src="{{asset('/site/js/jquery.min.js')}}"></script>
<script src="{{asset('/site/js/jquery.actual.min.js')}}"></script>
<script src="{{asset('/site/lib/validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('/site/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('/site/js/eb_rating_common.js')}}"></script>

<script>
    if (typeof(lang) == "undefined") {
        var lang = window.location.pathname;
        lang.indexOf(1);

        lang.toLowerCase();

        lang = lang.split("/")[1];

        lang = '/' + lang;
    }

</script>

<script src="{{asset('/site/js/auth/login.js')}}"></script>

<script src="{{asset('/site/js/auth/register.js')}}"></script>
<script src="{{asset('/site/js/auth/reset_password.js')}}"></script>


<script>
    $(document).ready(function () {

        //* boxes animation
//            form_wrapper = $('.login_box');
//            function boxHeight() {
//                form_wrapper.animate({ marginTop : ( - ( form_wrapper.height() / 2) - 24) },400);
//            };
//            form_wrapper.css({ marginTop : ( - ( form_wrapper.height() / 2) - 24) });
//            $('.linkform a,.link_reg a').on('click',function(e){
//                var target	= $(this).attr('href'),
//                        target_height = $(target).actual('height');
//                $(form_wrapper).css({
//                    'height'		: form_wrapper.height()
//                });
//                $(form_wrapper.find('form:visible')).fadeOut(400,function(){
//                    form_wrapper.stop().animate({
//                        height	 : target_height,
//                        marginTop: ( - (target_height/2) - 24)
//                    },500,function(){
//                        $(target).fadeIn(400);
//                        $('.links_btm .linkform').toggle();
//                        $(form_wrapper).css({
//                            'height'		: ''
//                        });
//                    });
//                });
//                e.preventDefault();
//            });

        //* validation
        // $('#login_form').validate({
        //     onkeyup: false,
        //     errorClass: 'error',
        //     validClass: 'valid',
        //     rules: {
        //         username: { required: true, minlength: 3 },
        //         password: { required: true, minlength: 3 }
        //     },
        //     highlight: function(element) {
        //         $(element).closest('div').addClass("f_error");
        //         setTimeout(function() {
        //             boxHeight()
        //         }, 200)
        //     },
        //     unhighlight: function(element) {
        //         $(element).closest('div').removeClass("f_error");
        //         setTimeout(function() {
        //             boxHeight()
        //         }, 200)
        //     },
        //     errorPlacement: function(error, element) {
        //         $(element).closest('div').append(error);
        //     }
        // });
    });
</script>

</body>

</html>
