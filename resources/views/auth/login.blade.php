@extends('auth.layout')
@section('content')


    <form action="/login" id="login_form" class="login_form">

        {{--        <div class="top_b" style="background: #fff; border: none">ECOLO (Efficient COrporate LOans) 2.1: .@lang('buttons.login')</div>--}}
        <div class="alert-login" style="
        display: none;
        background: #FF5244;
    color: #fff;
    padding: 10px;
    border-radius: 5px; text-align: center">

        </div>

        <div class="cnt_b">
            <div class="formRow">
                <div class="input-prepend">

                    <label style="font-size: 16px; font-weight: bold;">@lang('inputs.username')</label>
                    <span class="add-on" style="background: #eee;
    border: none;
    color: #C4C4C4;
    position: absolute;
    z-index: 999;
        margin: 3px 0 0 3px;">
                        <i class="icon-user"></i>
                    </span>

                    <input class="login_input" type="text" id="username" style="
                    background: #EEE;
border: none;border-radius: 5px;padding: 8px 8px 8px 40px;"
                           name="username" placeholder="vika@example.ru"/>

                </div>
            </div>
            <div class="formRow">
                <div class="input-prepend" style="margin-top: 10px;">
                    <label style="font-size: 16px; font-weight: bold;">@lang('inputs.password')</label>
                    <span class="add-on" style="background: #eee;
    border: none;
    color: #C4C4C4;
    position: absolute;
    z-index: 999;
        margin: 3px 0 0 3px;">
                        <i class="icon-lock"></i>
                    </span>
                    <input style="
                    background: #EEE;
                    border: none;border-radius: 5px;padding: 8px 8px 8px 40px;" type="password" id="password" name="password" placeholder="*********"
                           value=""/>
                </div>
            </div>
            {{--            <div class="formRow clearfix">--}}
            {{--                <label class="checkbox"><input type="checkbox" name="remember_me"/> @lang('inputs.remember_me')</label>--}}
            {{--            </div>--}}
        </div>
        <div class="btm_b clearfix" style="border: none; background: #fff;text-align: right;
    position: relative;
    bottom: 35px;
    margin-right: 40px;">
            <a href="/{{App::getLocale()}}/reset" style="color: #29AC6F;">
                @lang('links.forgot_password') </a>
        </div>
        <div class="btm_b clearfix" style="border: none; background: #fff">

            {{--            @lang('buttons.choose_language')--}}
            {{--            <span class="navbar dropdown">--}}
            {{--                    <a onClick="switchLan()" style="cursor: pointer;">--}}
            {{--                         @if(App::getLocale() == 'en')--}}
            {{--                            <i class="flag-gb"></i>--}}
            {{--                        @elseif(App::getLocale() == 'ru')--}}
            {{--                            <i class="flag-ru"></i>--}}

            {{--                        @else--}}
            {{--                            <i class="flag-it"></i>--}}

            {{--                        @endif--}}
            {{--                        <b style="border-top-color: #fff" class="caret"></b></a>--}}
            {{--                            </span>--}}
            {{--            <ul id="menuLanguage" class="dropdown-menu" style="top: 80%!important;">--}}
            {{--                <li>--}}
            {{--                    <a href="/en/login">--}}
            {{--                        <i class="flag-gb"></i>--}}
            {{--                        English--}}
            {{--                    </a>--}}
            {{--                </li>--}}
            {{--                <li>--}}
            {{--                    <a href="/it/login">--}}
            {{--                        <i class="flag-it"></i>--}}
            {{--                        Italiano--}}
            {{--                    </a>--}}
            {{--                </li>--}}
            {{--                <li>--}}
            {{--                    <a href="/ru/login">--}}
            {{--                        <i class="flag-ru"></i>--}}
            {{--                        Pусский--}}
            {{--                    </a>--}}
            {{--                </li>--}}
            {{--            </ul>--}}

            {{--            <a class="btn" href="/{{App::getLocale()}}/register">--}}
            {{--                @lang('buttons.register')--}}
            {{--            </a>--}}

            <button class="btn-inverse login"
                    style="
    margin-left: 120px;width: 105px;
    height: 35px;
    border: none;
    background: #47AB6B;
    color: #fff;
    border-radius: 5px;position: relative;
    bottom: 15px;" type="button">
                ВОЙТИ
                {{--                <i class="icon-signin icon-white"></i>--}}
                {{--                @lang('buttons.login')--}}
            </button>

        </div>

    </form>
    <script type="application/javascript">

        var open = false;

        function switchLan() {
            if (open) {
                open = false;
                $('#menuLanguage').hide();
            } else {
                open = true;
                $('#menuLanguage').show();
            }

        }


    </script>
@endsection
