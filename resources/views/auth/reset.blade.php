@extends('auth.layout')
@section('content')

    <form id="reset_password">

        {{--        <div class="top_b">--}}
        {{--            @lang('global.system') ECOLO 2.19: @lang('buttons.reset_password')--}}

        {{--        </div>--}}

        <div class="alert-login" style="
        display: none;
    background: #FF5244;
    color: #fff;
    padding: 10px;
    border-radius: 5px; text-align: center">

        </div>

        <div class="cnt_b">

            <label for="username" style="font-size: 16px; font-weight: bold;">@lang('inputs.email'):</label>

            <div class="input-prepend">
                <span class="add-on" style="background: #eee;
    border: none;
    color: #C4C4C4;
    position: absolute;
    z-index: 999;
        margin: 3px 0 0 3px;"><i class="icon-user"></i></span>

                <input style="background: #EEE;
border: none;border-radius: 5px;padding: 8px 8px 8px 40px;" type="email"  name="email"  placeholder="vika@example.ru" />
            </div>

        </div>

        <div class="btm_b clearfix" style="border: none;
    background: none; text-align: center">
            {{--            @lang('buttons.choose_language')          <span class="navbar dropdown">--}}
            {{--                <a onClick="switchLan()" style="cursor: pointer;"> @if(App::getLocale() == 'en')--}}
            {{--                        <i class="flag-gb"></i>--}}
            {{--                    @elseif(App::getLocale() == 'ru')--}}
            {{--                        <i class="flag-ru"></i>--}}

            {{--                    @else--}}
            {{--                        <i class="flag-it"></i>--}}

            {{--                    @endif <b style="border-top-color: #fff" class="caret"></b></a>--}}
            {{--<ul id="menuLanguage" class="dropdown-menu">--}}
            {{--            <li>--}}
            {{--            <a href="/en/reset">--}}
            {{--                <i class="flag-gb"></i>--}}
            {{--                English--}}
            {{--            </a>--}}
            {{--        </li>--}}
            {{--            <li>--}}
            {{--            <a href="/it/reset">--}}
            {{--                <i class="flag-it"></i>--}}
            {{--                Italiano--}}
            {{--            </a>--}}
            {{--        </li>--}}
            {{--            <li>--}}
            {{--            <a href="/ru/reset">--}}
            {{--                <i class="flag-ru"></i>--}}
            {{--                Pусский--}}
            {{--            </a>--}}
            {{--        </li>--}}
            {{--    </ul>--}}

            </span>

            <button class=" reset_password"  type="button" style="width: 157px;
    height: 35px;
    border: none;
    background: #47AB6B;
    color: #fff;
    border-radius: 5px;position: relative;
    bottom: 15px; text-transform: uppercase">
                {{--                <i class="icon-signin icon-white"></i>--}}
                @lang('buttons.reset_password')
            </button>

        </div>

        <div style="margin: 0 0 10px 60px">
            <a style="color: #29AC6F" href="/{{App::getLocale()}}/login">
                <span style="font-size: 20px">&#8592;</span> Назад на страницу входа</a>
        </div>

    </form>

    <script type="application/javascript">

        var open = false;
        function switchLan(){

            if(open){
                open = false;
                $('#menuLanguage').hide();
            }else{
                open = true;
                $('#menuLanguage').show();
            }

        }

    </script>

@endsection
