@extends('auth.layout')
@section('content')

    <form class="register_form" id="register_form">

        <div class="top_b">
            @lang('global.system') ECOLO 2.19:  @lang('buttons.register')
        </div>

        <div class="alert alert-error alert-login" style="display: none">

        </div>


        <div class="cnt_b">
            <div class="input-prepend">
                <span class="add-on"><i class="icon-envelope"></i></span>
                <input type="email" id="fos_user_registration_form_email" name="email"
                       placeholder="@lang('inputs.email'):"/>
            </div>

        </div>

        <div class="btm_b clearfix">
            @lang('buttons.choose_language') <span class="navbar dropdown">
                <a onClick="switchLan()" style="cursor: pointer;"> @if(App::getLocale() == 'en')
                        <i class="flag-gb"></i>
                    @elseif(App::getLocale() == 'ru')
                        <i class="flag-ru"></i>

                    @else
                        <i class="flag-it"></i>

                    @endif <b
                            style="border-top-color: #fff" class="caret"></b></a>
<ul id="menuLanguage" class="dropdown-menu">
            <li>
            <a href="/en/register">
                <i class="flag-gb"></i>
                English
            </a>
        </li>
            <li>
            <a href="/it/register">
                <i class="flag-it"></i>
                Italiano
            </a>
        </li>
            <li>
            <a href="/ru/register">
                <i class="flag-ru"></i>
                Pусский
            </a>
        </li>
    </ul>

<script type="application/javascript">

    var open = false;

    function switchLan() {

        if (open) {
            open = false;
            $('#menuLanguage').hide();
        } else {
            open = true;
            $('#menuLanguage').show();
        }

    }

</script>            </span>
            <a href="/{{App::getLocale()}}/login" class="btn ">
                @lang('buttons.exit')
            </a>
            <button class="btn btn-inverse register" type="button">
                <i class="icon-signin icon-white"></i>
                @lang('buttons.register')
            </button>

        </div>

    </form>

@endsection