@php
    use App\Http\Models\Role;
@endphp
@include('client.header')

@php
    if (\Illuminate\Support\Facades\Auth::check()){
        $checkUser = (\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_SUPERVISOR));
    }
    else{
        $checkUser = false;
    }
@endphp
@if(!$preview_mode)
    <style>
        td{
            vertical-align: middle !important;
        }
        input[type=checkbox] {
            height: 24px!important;
            margin-left: 18%;
        }
        .code-td {
            text-align: right;
        }
        .help-inline {
            margin-left: 10%!important;
        }
        .a-td-input {
            width: 263px!important;
        }
        .b-td-input {
            width: 77%!important;
        }
        .c-td-input {
            width: 280px!important;
        }
        .d-td-input {
            width: 290px!important;
        }
        .e-td-input, .z-td-input  {
            width: 255px!important;
        }
        .f-td-input {
            width: 265px!important;
        }
        label.beautyCheckbox {
            position: relative;
            left: 110px;
        }
        label.beautyCheckbox > input[type="checkbox"] {
            display: none;
        }
        label.beautyCheckbox > input[type="checkbox"] + *::before {
            content: "";
            width: 24px;
            height: 24px;
            border-radius: 2px;
            border: 1px solid #47b39c;
            flex-shrink: 0;
        }
        label.beautyCheckbox > input.is_checked + *::before {
            content: "✓";
            color: white;
            text-align: center;
            background: #47b39c;
            border-color: #47b39c;
        }
        label.beautyCheckbox > input[type="checkbox"] + * {
            display: inline-flex;
        }
    </style>
    <section class="form_section">
        <div class="container">
            @if (Auth::check())
                <span class="prf_close" style="font-weight: normal; text-transform: uppercase; right: 20px!important;" data-client="@if(Auth::user()->role_id == Role::get_role_id_by_name(Role::ROLE_BANK_SUPERVISOR)) 1 @else 0 @endif">
            Закрыть анкету
            <img src="{{asset('/site/img/close_cross.svg')}}" style="position: relative; top: -1px; right: -4px;" alt="">
        </span>
            @endif
            <form class="row client-questionnaire">
                <div class="col-lg-7">
                    <div class="form_field">
                        <label>Клиент</label>
                        {{--                    <input type="text" name="client" placeholder="ИП Иванов Иван Иванович" value="{{$client->company_name}}" @if($checkUser) disabled @endif>--}}
                        <textarea class="autoresizing def-resizing ui-resizing" name="client" rows="1" type="text"  placeholder="ИП Иванов Иван Иванович" value="{{$client->company_name}}" @if($checkUser) disabled @endif>{{$client->company_name}}</textarea>
                    </div>
                    <div class="form_field">
                        <label>Email Клиента</label>
                        <input type="text" name="client_mail" placeholder="ivavivan@yandex.ru" value="{{$client->email_manager}}" @if($checkUser) disabled @endif>
                    </div>
                    <div class="form_field">
                        <label>ИНН</label>
                        <input type="text" name="inn" placeholder="789449657914"  value="{{$client->INN_number}}" @if($checkUser) disabled @endif>
                    </div>
                    <div class="form_field">
                        <label>Вид Деятельности</label>
                        {{--                    <input type="text" name="activity" placeholder="Торговля рыбными консервами" value="{{$client->main_activity}}" @if($checkUser) disabled @endif>--}}
                        <textarea class="autoresizing def-resizing ui-resizing" name="activity" rows="1" type="text"  placeholder="Торговля рыбными консервами" value="{{$client->main_activity}}" @if($checkUser) disabled @endif>{{$client->main_activity}}</textarea>
                    </div>
                    <div class="form_field">
                        <label>РЕГИОН/ОБЛАСТЬ</label>
                        <select name="area_region" @if($checkUser) disabled @endif>
                            <option value="">@lang('labels.select_region')</option>
                            @foreach($regions as $region)
                                <option
                                        @if($client->region == $region->area_code) selected @endif
                                value="{{$region->area_code}}">{{$region->region_description}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form_field last-field">
                        <label>ГОРОД/РАЙОН</label>
                        <select name="area_city" data-id="{{$client->region_code}}" @if($checkUser) disabled @endif>
                            <option value="">@lang('labels.select_city')</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-4 offset-xl-1" style="margin-left: 0!important;">
                    <div class="profile_info">
                        <div class="profile_number">
                            <p class="title" style="font-size: 18px;">Анкета</p>
                            <p class="number">{{$questionnaire->sose_study->description}}</p>
                        </div>
                        <div class="profile_doc">
                            <p class="number">{{$questionnaire->sose_ateco->ateco_code}}</p>
                            <a class="doc_download" href="{{env('APP_URL_NAME').'/'.App::getLocale()}}/client/print/{{$client->ID_company}}/{{$questionnaire->id}}/{{$questionnaire->sose_ateco_id}}" download>Скачать
                                PDF</a>
                        </div>
                    </div>
                    <p class="profile_text">{{explode('[', $questionnaire->sose_ateco->name)[0]}}</p>
                    <div class="btn_container dynamic_top">
                        @if(!$checkUser)
                            <button type="button" data-action="evaluate" class="link_btn save_questionnaire hover_btn quest_submit_btn" style="text-transform: uppercase;margin-right: 20px; background-color: #47AB6B; border: none; color: white;">Отправить на оценку</button>
                            <button type="button" data-action="save" class="profile_btn save_questionnaire link_btn_grey" style="text-transform: uppercase;background-color: white;padding: 0 18px;  color: black; border-radius: 5px;">Сохранить</button>
                        @endif
                    </div>
                    {{--                <div class="mt-2">--}}
                    {{--                    @php--}}
                    {{--                        if(file_exists(base_path('public/site/reports/pdf/'.strtolower($questionnaire->company->company_name).'.pdf'))--}}
                    {{--                            ||file_exists(base_path('public/site/reports/excel/'.strtolower($questionnaire->company->company_name).'.xlsx'))){--}}
                    {{--                    @endphp--}}
                    {{--                    <h3 style="margin-top: 18px">Доклады:</h3>--}}
                    {{--                    @php--}}
                    {{--                            }--}}
                    {{--                    @endphp--}}
                    {{--                    @php--}}
                    {{--                        if(file_exists(base_path('public/site/reports/pdf/'.strtolower($questionnaire->company->company_name).'.pdf'))){--}}

                    {{--                    @endphp--}}
                    {{--                    <button type="button" class="report_btn" style="border-radius: 2px">--}}
                    {{--                        <a href="{{asset('/site/reports/pdf/'.strtolower($questionnaire->company->company_name).'.pdf')}}" target="_blank" style="color: white; font-weight: 200;">PDF</a>--}}
                    {{--                    </button>--}}
                    {{--                    @php--}}
                    {{--                        }--}}
                    {{--                        if (file_exists(base_path('public/site/reports/excel/'.strtolower($questionnaire->company->company_name).'.xlsx'))){--}}

                    {{--                    @endphp--}}
                    {{--                    <button type="button" class="report_btn" style="border-radius: 2px">--}}
                    {{--                        <a href="{{asset('/site/reports/excel/'.strtolower($questionnaire->company->company_name).'.xlsx')}}" target="_blank" style="color: white; font-weight: 200;">EXCEL (.xlsx)</a>--}}
                    {{--                    </button>--}}
                    {{--                    @php--}}
                    {{--                        }--}}
                    {{--                        if (file_exists(base_path('public/site/reports/excel/'.strtolower($questionnaire->company->company_name).'.xls'))){--}}

                    {{--                    @endphp--}}
                    {{--                    <button type="button" class="report_btn" style="border-radius: 2px">--}}
                    {{--                        <a href="{{asset('/site/reports/excel/'.strtolower($questionnaire->company->company_name).'.xls')}}" target="_blank" style="color: white; font-weight: 200;">EXCEL (.xls)</a>--}}
                    {{--                    </button>--}}

                    {{--                    @php--}}
                    {{--                        };--}}
                    {{--                    @endphp--}}
                    {{--                </div>--}}
                </div>
            </form>
        </div>
    </section>
@else
    <h4>@lang('labels.code_ecolo'):</h4>
    <h4>{{$ateco_code[0].' | '.$ateco_name}}</h4>
@endif
{{--Block O Starts --}}
{{--    @if(array_key_exists('O', $parameters))--}}

{{--    <section class="block blockA">--}}
{{--        <div class="container">--}}
{{--            <ul class="block_header">--}}
{{--                <li class="block_title circle">--}}
{{--                    <span>O</span>--}}
{{--                </li>--}}
{{--                <li class="static">--}}
{{--                    <span>Блок</span>--}}
{{--                </li>--}}
{{--                <li class="circle"></li>--}}
{{--                <li class="circle"></li>--}}
{{--                <li class="circle"></li>--}}
{{--                <li class="circle"></li>--}}
{{--                <li class="circle"></li>--}}
{{--                <li class="circle"></li>--}}
{{--            </ul>--}}

{{--            <form class="block_content">--}}

{{--                --}}{{--                    <p class="block_content_subtitle">Сумма полей от A00101 до A00102 должна быть <=100%</p>--}}
{{--                <div class="table">--}}
{{--                    <table>--}}
{{--                        @foreach($parameters['O'] as $a => $b)--}}
{{--                            @php--}}
{{--                                $path =explode("|", $b[0]->sose_param->param_path)--}}
{{--                            @endphp--}}
{{--                            @if($loop->first)--}}
{{--                                <tr>--}}
{{--                                    <td colspan="4" style="border: none!important;">--}}
{{--                                        <h2>{{$path[0]}}</h2></td>--}}
{{--                                </tr>--}}
{{--                            @endif--}}
{{--                            @php--}}

{{--                                $count = 0;--}}
{{--                            @endphp--}}
{{--                            @foreach($b as $parameter)--}}
{{--                                @php $count++; @endphp--}}
{{--                                @if(strpos($a, 'nosub') === false  && $count < 2)--}}
{{--                                    <tr>--}}
{{--                                        <td colspan="4" style="border: none!important;">--}}
{{--                                            <h3 class="heading"><b>{{$a}}</b></h3>--}}
{{--                                        </td>--}}
{{--                                    </tr>--}}


{{--                                @endif--}}
{{--                                <tr>--}}
{{--                                    <td style="width: 60%">{{$parameter->sose_param->desc}}</td>--}}
{{--                                    <td>{{$parameter->sose_param->parameter_code}}</td>--}}
{{--                                    <td>--}}
{{--                                        <input class="delimetr_number"--}}
{{--                                               @if($parameter->sose_param->value_type == 'CB') type="checkbox"--}}
{{--                                               @else type="text" @endif style="width: 50%"--}}
{{--                                               name="parameter_id[{{$parameter->sose_param->id}}]"--}}
{{--                                               @if(isset($values)) @foreach($values as $value) @if($value->parameter_id == $parameter->sose_param->id ) value="{{$value->value}}"--}}
{{--                                               @endif @endforeach @endif--}}
{{--                                               data-id="{{$parameter->sose_param->id}}"--}}
{{--                                               data-code="{{$parameter->sose_param->parameter_code}}"--}}
{{--                                               onblur="saveQuestionnaire(this);"--}}
{{--                                        > <span--}}
{{--                                            class="help-inline">{{$parameter->sose_param->unit}}</span>--}}
{{--                                        <br>--}}
{{--                                        <span style="max-width: 200px; display:none"--}}
{{--                                              class="alert alert-danger validation-error-span validation-error-{{$parameter->sose_param->parameter_code}}"></span>--}}

{{--                                    </td>--}}

{{--                                </tr>--}}
{{--                            @endforeach--}}
{{--                        @endforeach--}}


{{--                    </table>--}}
{{--                </div>--}}
{{--            </form>--}}
{{--        </div>--}}

{{--    </section>--}}
{{--@endif--}}
{{--Block O Ends--}}
<form action="" id="questionnaire-form">
    @if (!$preview_mode)
        <input type="hidden" name="client_id" value="{{$client->ID_company}}">
        <input type="hidden" name="questionnaire_id" value="{{$questionnaire->id}}">
        <input type="hidden" name="company_id" value="{{$client->ID_company}}">
        <input type="hidden" name="sose_study_id" value="{{$questionnaire->sose_study_id}}">
    @endif


    <div id="final-validation" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="icon-box">
                        {{--                            <i class="material-icons">&#xE5CD;</i>--}}
                        <img src="https://icon-library.com/images/important-icon-png/important-icon-png-26.jpg">
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <p id="final-validation-text" class="error-text"></p>
                </div>
            </div>
        </div>
    </div>

    <div id="validation-failed-notify" class="modal fade">
        <div class="modal-dialog modal-confirm modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="icon-box" style="background: #FF5244;width: 103px !important; height: 103px !important;align-content: center;border-radius: 50%;align-items: center;display: flex;">
                        {{--                            <i class="material-icons">&#xE5CD;</i>--}}
                        <img src="{{asset('/site/img/!_symbol.png')}}" style="width: 17px !important;height: auto;display: flex;margin: 0 auto;">
                    </div>
                </div>
                <div class="modal-body">
                    <p id="final-validation-text" class="error-text">@lang("global.validation_error")</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container mt-5">

        {{--            <div class="container">--}}
        {{--                <span class="final-validation alert alert-danger mb-2" style="display: none; width: 600px; margin: 0 auto;"></span>--}}
        {{--            </div>--}}
        <ul class="nav">
            @if(array_key_exists('A', $parameters) )
                <li class="A nav-item circle" @if($preview_mode) style="background-color: #F9B232;" @endif>
                    <a class="nav-link nav-link-A active" data-toggle="tab" href="#tabA">
                        <span class="block-text-A" style="position: relative; font-size: 50px; font-weight: bold; top: -24px; left: -8px">A</span>
                    </a>
                </li>
                <span class="circle-text circle-A" @if($preview_mode) style="display: block;" @endif>БЛОК</span>
            @endif

            @if(count($b_units))
                <li class="B nav-item circle">
                    <a class="nav-link nav-link-B" data-toggle="tab" href="#tabB">
                        <span class="block-text-B" style="position: relative; font-size: 50px; font-weight: bold; top: -24px; left: -8px">B</span>
                    </a>
                </li>
                <span class="circle-text circle-B">БЛОК</span>
            @endif

            @if(array_key_exists('C', $parameters) )
                <li class="C nav-item circle">
                    <a class="nav-link nav-link-C" data-toggle="tab" href="#tabC">
                        <span class="block-text-C" style="position: relative; font-size: 50px; font-weight: bold; top: -24px; left: -8px">C</span>
                    </a>
                </li>
                <span class="circle-text circle-C">БЛОК</span>
            @endif

            @if(array_key_exists('D', $parameters) )
                <li class="D nav-item circle">
                    <a class="nav-link nav-link-D" data-toggle="tab" href="#tabD">
                        <span class="block-text-D" style="position: relative; font-size: 50px; font-weight: bold; top: -24px; left: -8px">D</span>
                    </a>
                </li>
                <span class="circle-text circle-D">БЛОК</span>
            @endif
            @if(array_key_exists('E', $parameters) )
                <li class="E nav-item circle">
                    <a class="nav-link nav-link-E" data-toggle="tab" href="#tabE">
                        <span class="block-text-E" style="position: relative; font-size: 50px; font-weight: bold; top: -24px; left: -8px">E</span>
                    </a>
                </li>
                <span class="circle-text circle-E">БЛОК</span>
            @endif

            @if(array_key_exists('F', $parameters) )
                <li class="F nav-item circle">
                    <a class="nav-link nav-link-F" data-toggle="tab" href="#tabF">
                        <span class="block-text-F" style="position: relative; font-size: 50px; font-weight: bold; top: -24px; left: -8px">F</span>
                    </a>
                </li>
                <span class="circle-text circle-F">БЛОК</span>
            @endif
            @if(array_key_exists('L', $parameters) )
                <li class="L nav-item circle">
                    <a class="nav-link nav-link-L" data-toggle="tab" href="#tabL">
                        <span class="block-text-L" style="position: relative; font-size: 50px; font-weight: bold; top: -24px; left: -8px">L</span>
                    </a>
                </li>
                <span class="circle-text circle-L">БЛОК</span>
            @endif

            @if(array_key_exists('Z', $parameters) )
                <li class="Z nav-item circle">
                    <a class="nav-link nav-link-Z" data-toggle="tab" href="#tabZ">
                        <span class="block-text-Z" style="position: relative; font-size: 50px; font-weight: bold; top: -24px; left: -8px">Z</span>
                    </a>
                </li>
                <span class="circle-text circle-Z">БЛОК</span>
            @endif
        </ul>
    </div>

    <div class="tab-content">
        @if(array_key_exists('A', $parameters))
            <div id="tabA" class="tab-pane fade show active">
                <section class="block blockA">
                    <div class="container">

                        <div class="block_content">

                            {{--                    <p class="block_content_subtitle">Сумма полей от A00101 до A00102 должна быть <=100%</p>--}}
                            <div class="table">
                                <table>
                                    @foreach($parameters['A'] as $a => $b)
                                        @php
                                            $path =explode("|", $b[0]->sose_param->param_path)
                                        @endphp
                                        @if($loop->first)
                                            <tr>
                                                <td colspan="4" style="border: none!important; background-color: white; padding: 20px;">
                                                    <h2 style="font-size: 23px;font-weight: bold;">{{substr_replace($path[0], '', 0, 12)}}</h2></td>
                                            </tr>
                                        @endif
                                        @php

                                            $count = 0;
                                        @endphp
                                        @foreach($b as $parameter)
                                            @php $count++; @endphp
                                            @if(strpos($a, 'nosub') === false  && $count < 2)
                                                <tr>
                                                    <td colspan="4" style="border: none!important; background-color: white; padding: 20px;">
                                                        <h3 class="heading" style="font-size: 18px;font-weight: bold;"><b>{{$a}}</b></h3>
                                                    </td>
                                                </tr>


                                            @endif
                                            <tr>


                                                <td style="width: 60%; padding: 20px;" class="{{$parameter->sose_param->parameter_code}}">{{$parameter->sose_param->desc}}</td>
                                                <td style="padding: 20px 0;" class="{{$parameter->sose_param->parameter_code}}" data-target="{{$parameter->sose_param->parameter_code}}">{{$parameter->sose_param->parameter_code}}</td>
                                                <td style="padding: 20px 0;" class="{{$parameter->sose_param->parameter_code}}">
                                                        <span style="max-width: 100%; display:none; background-color: #FF5244 !important;color: white;"
                                                              class="alert alert-danger validation-error-span validation-error-{{$parameter->sose_param->parameter_code}}"></span>

                                                    <input class="delimetr_number"
                                                           @if($parameter->sose_param->value_type == 'CB') type="checkbox" style="width: 50%; height: 15px;"
                                                           @else type="text" style="width: 100px; height: 34px; border-radius: 5px;margin-left: 15%;"@endif
                                                           name="parameter_id[{{$parameter->sose_param->id}}]"
                                                           @if(isset($values)) @foreach($values as $value) @if($value->parameter_id == $parameter->sose_param->id ) value="{{$value->value}}"
                                                           @endif @endforeach @endif
                                                           data-id="{{$parameter->sose_param->id}}"
                                                           data-code="{{$parameter->sose_param->parameter_code}}"
                                                           onblur="saveQuestionnaire(this);"
                                                           @if($checkUser) disabled @endif
                                                    > <span
                                                            style="margin-left: 20%;"
                                                            class="help-inline">{{$parameter->sose_param->units == '%' ? '%' : $parameter->sose_param->unit}}</span>
                                                    <br>

                                                </td>

                                            </tr>
                                        @endforeach
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        @endif
        @if(count($b_units) > 0)
            <div id="tabB" class="tab-pane fade">
                <section class="block blockA">
                    <div class="container">
                        <div class="block_content">

                            {{--                    <p class="block_content_subtitle">Сумма полей от A00101 до A00102 должна быть <=100%</p>--}}
                            <div class="table">
                                <table>
                                    @php
                                        if ($preview_mode)
                                                    {
                                                        $company_unit = 1;
                                                    }else{
                                                        $company_unit = $questionnaire->company_unit;
                                                    }
                                    @endphp
                                    <h2 style="padding: 20px;font-size: 23px;font-weight: bold;">{{substr_replace($first->sose_param->param_path, '', 0, 12)}}</h2>
{{--                                    @if($parameter->sose_param->parameter_code == $first->sose_param->parameter_code)--}}
                                    <tr>

                                        <td style="width: 60%; padding: 20px;" class="{{$parameter->sose_param->parameter_code}}">{{$first->sose_param->desc}}</td>
                                        <td style="padding: 20px 20px;" class="{{$parameter->sose_param->parameter_code}}" data-target="{{$parameter->sose_param->parameter_code}}">{{$first->sose_param->parameter_code}}</td>
                                        <td style="width: 100%; padding: 20px 0;" class="{{$parameter->sose_param->parameter_code}}">
                                            <span style="width: 100%; max-width: 100%; display:none; background-color: #FF5244 !important;color: white;"
                                                  class="alert alert-danger validation-error-span validation-error-{{$first->sose_param->parameter_code}}"></span>

                                            <input class="delimetr_number"
                                                   id="{{$first->sose_param->parameter_code}}"
                                                   @if($parameter->sose_param->value_type == 'CB') type="checkbox" style="width: 50%;height: 20px;" @else type="text" style="width: 100px; height: 34px; border-radius: 5px;margin-left: 13%;" @endif
                                                   name="parameter_id[{{$first->sose_param->id}}]"
                                                   @if(count($b_units) > 1) value="{{$company_unit}}" @else value="" @endif
                                                   value="{{$company_unit}}"
                                                   data-id="{{$first->sose_param->id}}"
                                                   data-code="{{$first->sose_param->parameter_code}}"
                                                   onblur="saveQuestionnaire(this);"
                                                   @if($checkUser) disabled @endif>
                                            <span
                                                    style="margin-left: 20%;"
                                                    class="help-inline">{{$first->sose_param->unit}}</span>
                                            <br>

                                        </td>
                                        <td style="width: 7%;"></td>
                                    </tr>
{{--                                    @endif--}}
                                    @php
                                        $key=0;
                                        $b_count = 0;
                                        $previous_subheading = 'nosub';
                                        $test = 0;
                                    @endphp

                                    @foreach($b_units as $parameter)
                                        @php
                                            $test++;
                                        @endphp

                                        @if($parameter->sose_param->value_type== "CAT")
                                            @php $key++; $b_count = 0; $previous_subheading = 'nosub';@endphp
                                        @endif
                                        @if($key<=  $company_unit)
                                            @if ($parameter->sose_param->value_type == "CAT")

                                                <tr>
                                                    <td style="width:70%;font-size:16px;border: none!important; background-color: white; padding: 20px;">
                                                        <span style="font-size: 18px;font-weight: bold;">@lang('global.structural_unit') N. {{$key}}</span>
                                                    </td>

                                                </tr>
                                            @endif

                                            @php
                                                $find_subheading = explode("|", $parameter->sose_param->param_path);
                                                if (isset($find_subheading[1]) && (strlen($find_subheading[1]) > 0 && strlen(trim($find_subheading[1])) != 0))
                                                {
                                                    $find_subheading = $find_subheading[1];
                                                    if ($find_subheading != $previous_subheading)
                                                        {
                                                            $b_count = 0;
                                                        }else{
                                                             $b_count++;
                                                        }


                                                }else{

                                                    $find_subheading = 'nosub';
                                                }
                                            @endphp



                                            @if((strpos($find_subheading, 'nosub') === false && $b_count < 1) || (strpos($find_subheading, 'nosub') === false && $b_count == 1 && $find_subheading != $previous_subheading) )

                                                <tr>
                                                    <td colspan="4" style="border: none!important; background-color: white; padding: 20px;">
                                                        <h3 class="heading" style="font-size: 18px;font-weight: bold;"><b>{{$find_subheading}} </b></h3>
                                                    </td>
                                                </tr>
                                                @php $previous_subheading = $find_subheading @endphp


                                            @endif

                                            @if($parameter->sose_param->visible)
                                                <tr style="width: 100%;">

                                                    <td style="width: 60%; padding: 20px;" class="{{$parameter->sose_param->parameter_code}}">{{$parameter->sose_param->desc}}</td>
                                                    <td style="padding: 20px 20px;" class="{{$parameter->sose_param->parameter_code}}" data-target="{{$parameter->sose_param->parameter_code}}">{{$parameter->sose_param->parameter_code}}</td>
                                                    <td style="width: 100%; padding: 20px 0;" class="{{$parameter->sose_param->parameter_code}}">
                                                    <span style="width: 100%; max-width: 100%; display:none; background-color: #FF5244 !important;color: white;"
                                                          class="alert alert-danger validation-error-span validation-error-{{$parameter->sose_param->parameter_code}}"></span>

                                                        <input class="delimetr_number"
                                                               @if($parameter->sose_param->value_type == 'CB') type="checkbox"  style="width: 50%; height: 15px;"
                                                               @else type="text" style="width: 100px; height: 34px; border-radius: 5px;margin-left: 13%;"
                                                               @endif
                                                               name="parameter_id[{{$parameter->sose_param->id}}]"
                                                               @if(isset($values))
                                                               @foreach($values as $value)
                                                               @if($value->parameter_id == $parameter->sose_param->id ) value="{{$value->value}}"
                                                               @endif @endforeach @endif
                                                               data-id="{{$parameter->sose_param->id}}"
                                                               data-code="{{$parameter->sose_param->parameter_code}}"
                                                               onblur="saveQuestionnaire(this);"
                                                               @if($checkUser) disabled @endif><span
                                                                style="margin-left: 20%;"
                                                                class="help-inline">{{$parameter->sose_param->unit}}</span>
                                                        <br>

                                                    </td>
                                                    <td style="width: 7%;"></td>
                                                </tr>
                                            @endif

                                        @endif
                                    @endforeach

                                </table>
                            </div>
                        </div>
                    </div>

                </section>
            </div>
        @endif
        @if(array_key_exists('C', $parameters))
            <div id="tabC" class="tab-pane fade">
                <section class="block blockC">
                    <div class="container">
                        <div class="block_content">

                            <div class="table">
                                <table>
                                    @foreach($parameters['C'] as $a => $b)
                                        @php
                                            $path =explode("|", $b[0]->sose_param->param_path)
                                        @endphp
                                        @if($loop->first)
                                            <tr>
                                                <td colspan="4" style="border: none!important; background-color: white; padding: 20px;">
                                                    <h2 style="font-size: 23px;font-weight: bold;">{{substr_replace($path[0], '', 0, 12)}}</h2></td>
                                            </tr>
                                        @endif
                                        @php

                                            $c = 0;
                                        @endphp
                                        @foreach($b as $parameter)
                                            @php $c++; @endphp
                                            @if(strpos($a, 'nosub') === false  && $c < 2)
                                                <tr>
                                                    <td colspan="4" style="border: none!important; background-color: white; padding: 20px;">
                                                        <h3 class="heading" style="font-size: 18px;font-weight: bold;"><b>{{$a}}</b></h3>
                                                    </td>
                                                </tr>


                                            @endif
                                            <tr>
                                                <td style="width: 60%; padding: 20px;" class="{{$parameter->sose_param->parameter_code}}">{{$parameter->sose_param->desc}}</td>
                                                <td style="padding: 20px 0;" class="{{$parameter->sose_param->parameter_code}}" data-target="{{$parameter->sose_param->parameter_code}}">{{$parameter->sose_param->parameter_code}}</td>
                                                <td style="padding: 20px 0;" class="{{$parameter->sose_param->parameter_code}}">
                                                            <span style="max-width: 100%; display:none; background-color: #FF5244 !important;color: white;"
                                                                  class="alert alert-danger validation-error-span validation-error-{{$parameter->sose_param->parameter_code}}"></span>

                                                    <input class="delimetr_number"
                                                           @if($parameter->sose_param->value_type == 'CB') type="checkbox" style="width: 50%; height: 15px;"
                                                           @else type="text" style="width: 100px; height: 34px; border-radius: 5px;margin-left: 21%;" @endif
                                                           name="parameter_id[{{$parameter->sose_param->id}}]"
                                                           @if(isset($values)) @foreach($values as $value) @if($value->parameter_id == $parameter->sose_param->id ) value="{{$value->value}}"
                                                           @endif @endforeach @endif
                                                           data-id="{{$parameter->sose_param->id}}"
                                                           data-code="{{$parameter->sose_param->parameter_code}}"
                                                           onblur="saveQuestionnaire(this);"
                                                           @if($checkUser) disabled @endif
                                                    > <span
                                                            style="margin-left: 20%;"
                                                            class="help-inline">{{$parameter->sose_param->unit}}</span>
                                                    <br>

                                                </td>

                                            </tr>
                                        @endforeach
                                    @endforeach


                                </table>
                            </div>
                        </div>
                    </div>

                </section>
            </div>
        @endif
        @if(array_key_exists('D', $parameters))
            <div id="tabD" class="tab-pane fade">
                <section class="block blockD">
                    <div class="container">
                        <div class="block_content">
                            @if (isset($product_code) && $product_code)
                                <div class="row block_row">
                                    <div class="col">
                                        <p class="subtitle"></p>
                                    </div>
                                    @if(!$preview_mode)
                                        <div class="col-auto">
                                            <a class="profile_btn" target="_blank" href="{{(env('APP_URL_NAME').'/'.$product_code)}}">Коды товаров</a>
                                        </div>
                                    @endif
                                </div>
                            @endif
                            <div class="table">
                                <table>
                                    @php
                                        $ifExist = false;
                                    @endphp
                                    @foreach($parameters['D'] as $a => $b)
                                        @if(file_exists(public_path('site/pdf/D'.$b[0]->sosestudy_id.'.pdf')))
                                            @php
                                                $pdfName = $b[0]->sosestudy_id;
                                                $ifExist = true;
                                                break;
                                            @endphp
                                        @endif
                                    @endforeach
                                    @if($ifExist)
                                        @php
                                            $pdfPath = env('APP_URL_NAME').'/site/pdf/D'.$pdfName.'.pdf';
                                        @endphp
                                        <tr style="display: flex;justify-content: space-between; width: 160%; @if(!$preview_mode)padding: 20px 0;@endif">
                                            @if(!$preview_mode)
                                                <td style=" color: aliceblue;font-weight: 400;text-align: center;border: 0 !important; padding: 20px 0; position: relative; left: 80%; text-transform: uppercase">
                                                    <a class="hover_btn" href="{{ $pdfPath }}" target="_blank" style="width: 100% !important;display: block;color: white !important;background-color: #47AB6B;padding: 8px 15px !important; border-radius: 5px; text-decoration: none; font-size: 14px">Коды товаров</a>
                                                </td>
                                            @endif
                                        </tr>
                                    @endif
                                    @foreach($parameters['D'] as $a => $b)

                                        @php
                                            $path =explode("|", $b[0]->sose_param->param_path)
                                        @endphp
                                        @if($loop->first)
                                            <tr>
                                                <td colspan="4" style="border: none!important; background-color: white; padding: 20px;">
                                                    <h2 style="font-size: 23px;font-weight: bold;">{{substr_replace($path[0], '', 0, 12)}}</h2></td>
                                            </tr>
                                        @endif
                                        @php

                                            $d = 0;
                                        @endphp
                                        @foreach($b as $parameter)
                                            @php $d++; @endphp
                                            @if(strpos($a, 'nosub') === false  && $d < 2)
                                                <tr>
                                                    <td colspan="4" style="border: none!important; background-color: white; padding: 20px;">
                                                        <h3 class="heading" style="font-size: 18px;font-weight: bold;"><b>{{$a}}</b></h3>
                                                    </td>
                                                </tr>


                                            @endif
                                            <tr>
                                                <td style="width: 60%; padding: 20px;" class="{{$parameter->sose_param->parameter_code}}">{{$parameter->sose_param->desc}}</td>
                                                <td style="padding: 20px 0;" class="{{$parameter->sose_param->parameter_code}}" data-target="{{$parameter->sose_param->parameter_code}}">{{$parameter->sose_param->parameter_code}}</td>
                                                <td style="padding: 20px 0;" class="{{$parameter->sose_param->parameter_code}}">
                                                    <span style="max-width: 100%; display:none; background-color: #FF5244 !important;color: white;"
                                                          class="alert alert-danger validation-error-span validation-error-{{$parameter->sose_param->parameter_code}}"></span>
                                                    <input class="delimetr_number"
                                                           @if($parameter->sose_param->value_type == 'CB') type="checkbox" style="width: 50%; height: 15px;"
                                                           @else type="text" style="width: 100px; height: 34px; border-radius: 5px;margin-left: 23%;"@endif
                                                           name="parameter_id[{{$parameter->sose_param->id}}]"
                                                           @if(isset($values)) @foreach($values as $value) @if($value->parameter_id == $parameter->sose_param->id ) value="{{$value->value}}"
                                                           @endif @endforeach @endif
                                                           data-id="{{$parameter->sose_param->id}}"
                                                           data-code="{{$parameter->sose_param->parameter_code}}"
                                                           onblur="saveQuestionnaire(this);"
                                                           @if($checkUser) disabled @endif
                                                    > <span
                                                            style="margin-left: 20%;"
                                                            class="help-inline">{{$parameter->sose_param->unit}}</span>
                                                    <br>

                                                </td>

                                            </tr>
                                        @endforeach
                                    @endforeach


                                </table>
                            </div>
                        </div>
                    </div>

                </section>
            </div>
        @endif
        @if(array_key_exists('E', $parameters))
            <div id="tabE" class="tab-pane fade">
                <section class="block blockE">
                    <div class="container">
                        <div class="block_content">

                            <div class="table">
                                <table>
                                    @foreach($parameters['E'] as $a => $b)
                                        @php
                                            $path =explode("|", $b[0]->sose_param->param_path)
                                        @endphp
                                        @if($loop->first)
                                            <tr>
                                                <td colspan="4" style="border: none!important; background-color: white; padding: 20px;">
                                                    <h2 style="font-size: 23px;font-weight: bold;">{{substr_replace($path[0], '', 0, 12)}}</h2></td>
                                            </tr>
                                        @endif
                                        @php

                                            $e = 0;
                                        @endphp
                                        @foreach($b as $parameter)
                                            @php $e++; @endphp
                                            @if(strpos($a, 'nosub') === false  && $e < 2)
                                                <tr>
                                                    <td colspan="4" style="border: none!important; background-color: white; padding: 20px;">
                                                        <h3 class="heading" style="font-size: 18px;font-weight: bold;"><b>{{$a}}</b></h3>
                                                    </td>
                                                </tr>


                                            @endif
                                            <tr>
                                                <td style="width: 60%; padding: 20px;" class="{{$parameter->sose_param->parameter_code}}">{{$parameter->sose_param->desc}}</td>
                                                <td style="padding: 20px 0;" class="{{$parameter->sose_param->parameter_code}}" data-target="{{$parameter->sose_param->parameter_code}}">{{$parameter->sose_param->parameter_code}}</td>
                                                <td style="padding: 20px 0;" class="{{$parameter->sose_param->parameter_code}}">
                                                    <span style="max-width: 100%; display:none; background-color: #FF5244 !important;color: white;"
                                                          class="alert alert-danger validation-error-span validation-error-{{$parameter->sose_param->parameter_code}}"></span>

                                                    <input class="delimetr_number"
                                                           @if($parameter->sose_param->value_type == 'CB') type="checkbox" style="width: 50%; height: 15px;"
                                                           @else type="text" style="width: 100px; height: 34px; border-radius: 5px;margin-left: 14%;" @endif
                                                           name="parameter_id[{{$parameter->sose_param->id}}]"
                                                           @if(isset($values)) @foreach($values as $value) @if($value->parameter_id == $parameter->sose_param->id ) value="{{$value->value}}"
                                                           @endif @endforeach @endif
                                                           data-id="{{$parameter->sose_param->id}}"
                                                           data-code="{{$parameter->sose_param->parameter_code}}"
                                                           onblur="saveQuestionnaire(this);"
                                                           @if($checkUser) disabled @endif
                                                    > <span
                                                            style="margin-left: 20%;"
                                                            class="help-inline">{{$parameter->sose_param->unit}}</span>
                                                    <br>

                                                </td>

                                            </tr>
                                        @endforeach
                                    @endforeach


                                </table>
                            </div>
                        </div>
                    </div>

                </section>
            </div>
        @endif
        @if(array_key_exists('F', $parameters))
            <div id="tabF" class="tab-pane fade">
                <section class="block blockF">
                    <div class="container">
                        <div class="block_content">

                            <div class="table">
                                <table>
                                    @foreach($parameters['F'] as $a => $b)
                                        @php
                                            $path =explode("|", $b[0]->sose_param->param_path)
                                        @endphp
                                        @if($loop->first)
                                            <tr>
                                                <td colspan="4" style="border: none!important; background-color: white; padding: 20px;">
                                                    <h2 style="font-size: 23px;font-weight: bold;">@if(!strpos($path[0], 'БЛОК F') && Config::get('app.locale') == 'ru') @endif{{$path[0]}}</h2></td>
                                            </tr>
                                        @endif
                                        @php

                                            $f = 0;
                                        @endphp
                                        @foreach($b as $parameter)
                                            @php $f++; @endphp
                                            @if(strpos($a, 'nosub') === false  && $f < 2)
                                                <tr>
                                                    <td colspan="4" style="border: none!important; background-color: white; padding: 20px;">
                                                        <h3 class="heading" style="font-size: 18px;font-weight: bold;"><b>{{$a}}</b></h3>
                                                    </td>
                                                </tr>


                                            @endif
                                            <tr>
                                                <td style="width: 60%; padding: 20px;" class="{{$parameter->sose_param->parameter_code}}">{{$parameter->sose_param->desc}}</td>
                                                <td style="padding: 20px 0;" class="{{$parameter->sose_param->parameter_code}}" data-target="{{$parameter->sose_param->parameter_code}}">{{$parameter->sose_param->parameter_code}}</td>
                                                <td style="padding: 20px 0;" class="{{$parameter->sose_param->parameter_code}}">
                                                    <span style="max-width: 100%; display:none; background-color: #FF5244 !important;color: white;"
                                                          class="alert alert-danger validation-error-span validation-error-{{$parameter->sose_param->parameter_code}}"></span>

                                                    <input class="delimetr_number"
                                                           @if($parameter->sose_param->value_type == 'CB') type="checkbox" style="width: 50%; height: 15px;"
                                                           @else type="text" style="width: 100px; height: 34px; border-radius: 5px;margin-left: 16%;" @endif
                                                           name="parameter_id[{{$parameter->sose_param->id}}]"
                                                           @if(isset($values)) @foreach($values as $value) @if($value->parameter_id == $parameter->sose_param->id ) value="{{$value->value}}"
                                                           @endif @endforeach @endif
                                                           data-id="{{$parameter->sose_param->id}}"
                                                           data-code="{{$parameter->sose_param->parameter_code}}"
                                                           onblur="saveQuestionnaire(this);"
                                                           @if($checkUser) disabled @endif
                                                    > <span
                                                            style="margin-left: 20%;"
                                                            class="help-inline">{{$parameter->sose_param->unit}}</span>
                                                    <br>

                                                </td>

                                            </tr>
                                        @endforeach
                                    @endforeach


                                </table>
                            </div>
                        </div>
                    </div>

                </section>
            </div>
        @endif
        @if(array_key_exists('L', $parameters))
            <div id="tabL" class="tab-pane fade">
                <section class="block blockL">
                    <div class="container">
                        <div class="block_content">

                            <div class="table">
                                <table>
                                    @foreach($parameters['L'] as $a => $b)
                                        @php
                                            $path =explode("|", $b[0]->sose_param->param_path)
                                        @endphp
                                        @if($loop->first)
                                            <tr>
                                                <td colspan="4" style="border: none!important; background-color: white; padding: 20px;">
                                                    <h2 style="font-size: 23px;font-weight: bold;">{{substr_replace($path[0], '', 0, 12)}}</h2></td>
                                            </tr>
                                        @endif
                                        @php

                                            $f = 0;
                                        @endphp
                                        @foreach($b as $parameter)
                                            @php $l++; @endphp
                                            @if(strpos($a, 'nosub') === false  && $l < 2)
                                                <tr>
                                                    <td colspan="4" style="border: none!important; background-color: white; padding: 20px;">
                                                        <h3 class="heading" style="font-size: 18px;font-weight: bold;"><b>{{$a}}</b></h3>
                                                    </td>
                                                </tr>


                                            @endif
                                            <tr>
                                                <td style="width: 60%; padding: 20px;" class="{{$parameter->sose_param->parameter_code}}">{{$parameter->sose_param->desc}}"</td>
                                                <td style="padding: 20px 0;" class="{{$parameter->sose_param->parameter_code}}" data-target="{{$parameter->sose_param->parameter_code}}>{{$parameter->sose_param->parameter_code}}</td>
                                                <td style="padding: 20px 0;" class="{{$parameter->sose_param->parameter_code}}">
                                                <span style="max-width: 100%; display:none; background-color: #FF5244 !important;color: white;"
                                                      class="alert alert-danger validation-error-span validation-error-{{$parameter->sose_param->parameter_code}}"></span>

                                                <input class="delimetr_number"
                                                       @if($parameter->sose_param->value_type == 'CB') type="checkbox" style="width: 50%; height: 15px;"
                                                       @else type="text" style="width: 100px; height: 34px; border-radius: 5px;margin-left: 13%;" @endif
                                                       name="parameter_id[{{$parameter->sose_param->id}}]"
                                                       @if(isset($values)) @foreach($values as $value) @if($value->parameter_id == $parameter->sose_param->id ) value="{{$value->value}}"
                                                       @endif @endforeach @endif
                                                       data-id="{{$parameter->sose_param->id}}"
                                                       data-code="{{$parameter->sose_param->parameter_code}}"
                                                       onblur="saveQuestionnaire(this);"
                                                       @if($checkUser) disabled @endif
                                                > <span
                                                        style="margin-left: 20%;"
                                                        class="help-inline">{{$parameter->sose_param->unit}}</span>
                                                <br>

                                                </td>

                                            </tr>
                                        @endforeach
                                    @endforeach


                                </table>
                            </div>
                        </div>
                    </div>

                </section>
            </div>
        @endif
        @if(array_key_exists('Z', $parameters))
            <div id="tabZ" class="tab-pane fade">
                <section class="block blockZ">
                    <div class="container">
                        <div class="block_content">

                            <div class="table">
                                <table>
                                    @foreach($parameters['Z'] as $a => $b)
                                        @php
                                            $path =explode("|", $b[0]->sose_param->param_path)
                                        @endphp
                                        @if($loop->first)
                                            <tr>
                                                <td colspan="4" style="border: none!important; background-color: white; padding: 20px;">
                                                    <h2 style="font-size: 23px;font-weight: bold;">{{substr_replace($path[0], '', 0, 12)}}</h2></td>
                                            </tr>
                                        @endif
                                        @php

                                            $z = 0;
                                        @endphp
                                        @foreach($b as $parameter)
                                            @php $z++; @endphp
                                            @if(strpos($a, 'nosub') === false  && $z < 2)
                                                <tr>
                                                    <td colspan="4" style="border: none!important; background-color: white; padding: 20px;">
                                                        <h3 class="heading" style="font-size: 18px;font-weight: bold;"><b>{{$a}}</b></h3>
                                                    </td>
                                                </tr>


                                            @endif
                                            <tr>
                                                <td style="width: 60%; padding: 20px;" class="{{$parameter->sose_param->parameter_code}}">{{$parameter->sose_param->desc}}</td>
                                                <td style="padding: 20px 0;" class="{{$parameter->sose_param->parameter_code}}" data-target="{{$parameter->sose_param->parameter_code}}">{{$parameter->sose_param->parameter_code}}</td>
                                                <td style="padding: 20px 0;" class="{{$parameter->sose_param->parameter_code}}">
                                                        <span style="max-width: 100%; display:none; background-color: #FF5244 !important;color: white;"
                                                              class="alert alert-danger validation-error-span validation-error-{{$parameter->sose_param->parameter_code}}"></span>

                                                    <input class="delimetr_number"
                                                           @if($parameter->sose_param->value_type == 'CB') type="checkbox" style="width: 50%; height: 15px;"
                                                           @else type="text" style="width: 100px; height: 34px; border-radius: 5px;margin-left: 13%;" @endif
                                                           name="parameter_id[{{$parameter->sose_param->id}}]"
                                                           @if(isset($values)) @foreach($values as $value) @if($value->parameter_id == $parameter->sose_param->id ) value="{{$value->value}}"
                                                           @endif @endforeach @endif
                                                           data-id="{{$parameter->sose_param->id}}"
                                                           data-code="{{$parameter->sose_param->parameter_code}}"
                                                           onblur="saveQuestionnaire(this);"
                                                           @if($checkUser) disabled @endif
                                                    > <span
                                                            style="margin-left: 20%;"
                                                            class="help-inline">{{$parameter->sose_param->unit}}</span>
                                                    <br>

                                                </td>

                                            </tr>
                                        @endforeach
                                    @endforeach

                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        @endif
    </div>

</form>
@if(!$preview_mode)
    <div class="container nextPrevBtns">
        <button style="border: none" class="btn_prev link_btn_grey hover_btn" onclick="prevBlock()">@lang('buttons.prev')</button>
        <button class="btn_next hover_btn" onclick="nextBlock()">@lang('buttons.next')</button>
    </div>
@endif

@if (!$preview_mode)
    <div class="modal" id="closeQuestionnaire" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Сохранить анкету ?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Сохранить анкету прежде чем закрыть ?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary confirm-save">Да</button>
                    <button type="button" class="btn btn-secondary delete-questionnaire-client" data-id="{{$client->ID_company}}" data-questionnaire="{{$questionnaire->id}}" >Нет</button>
                </div>
            </div>
        </div>
    </div>
@endif

{{--New Modal--}}

<div id="evaluate-div" class="modal fade">
    <div class="modal-dialog modal-confirm modal-dialog-centered" style="width: 460px">
        <div class="modal-content">
            <div class="modal-header">
                <div class="icon-box" style="background: #47AB6B;width: 103px !important; height: 103px !important;align-content: center;border-radius: 50%;align-items: center;display: flex;top: -73px !important;">
                    {{--                            <i class="material-icons">&#xE5CD;</i>--}}
                    <img src="{{asset('/site/img/success_icon.png')}}" style="width: 62px !important;height: auto;display: flex;margin: 0 auto;">
                </div>
            </div>
            <div class="modal-body">
                <h4 class="modal-title" style="color: black">@lang('global.eval_success')</h4>
            </div>
        </div>
    </div>
</div>
{{--<div class="modal" id="" tabindex="-1" role="dialog">--}}
{{--    <div class="modal-dialog" role="document">--}}
{{--        <div class="modal-content">--}}
{{--            <div class="modal-header">--}}
{{--                <h5 class="modal-title" style="color: #218838">@lang('global.eval_success')</h5>--}}
{{--            </div>--}}
{{--            <div class="modal-body evaluate-response-div">--}}
{{--                <p>Modal body text goes here.</p>--}}
{{--            </div>--}}
{{--            <div class="modal-footer">--}}
{{--                <button type="button" onclick="window.location.reload()" class="btn btn-primary" data-dismiss="modal">OK</button>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}

{{--<div class="modal" id="evaluate-div" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;height: 65%;text-align: center;overflow: hidden" >--}}
{{--    <div class="modal-dialog modal-dialog-centered" role="document">--}}
{{--        <h2 style="margin-top: 40px;color: darkgreen">@lang('global.eval_success')</h2>--}}
{{--        <div class="evaluate-response-div"></div>--}}
{{--        <button type="button" onclick="window.location.reload()" class="btn btn-secondary" data-dismiss="modal" style="padding: 5px 30px;">OK</button>--}}

{{--    </div>--}}
{{--</div>--}}

<style>
    .modal-content{
        padding: 20px;
    }

    .entry_form .modal-content form {
        max-width: 850px;
    }

    @media (min-width: 576px) {
        .modal-dialog {
            max-width: 900px;
        }
    }
</style>

@if ($preview_mode)
    <script>
        $('input.delimetr_number').css('margin-left', '5%');
        $('span.help-inline').css('margin-left', '5%');
        
        $('#questionnaire-form input').prop('disabled',true);

        $('.nav-item').on('click', function(){
            $(document).find('a.nav-link>span').css('color', '#818087');
            let errorBlocks = $(document).find('.nav-item.error');
            errorBlocks.map((key, value) => {
                $(document).find('.block-text-' + (value.className).slice(0, 1)).css('color', '#9D0C00');
            })
            let ballons = $(document).find('.nav-item');
            ballons.css({
                'background-color': '#C4C4C4',
            });
            $(this).css('background-color', '#F9B232');

            let classNameBallon = $(this).attr('class');
            classNameBallon = classNameBallon.slice(0, 1);
            if ($(this).hasClass('error')){
                $(this).css('background-color', '#FF5244');
                $('.block-text-' + classNameBallon).css('color', '#000');

            }
            else{
                $(this).css('background-color', '#F9B232');
                $('.block-text-' + classNameBallon).css('color', '#000');
            }
            $(document).find('.circle-text').css('display', 'none');
            $(document).find('.circle-' + classNameBallon).css('display', 'block');
        })
    </script>
@endif

@if(!$preview_mode)
    @include('client.footer')
@endif

<script>
    $(function(){
        $('input[type=checkbox]').each(function() {
            let attrObj = {}
            $.each(this.attributes, function() {
                if(this.specified && this.name != 'style') {
                    attrObj[this.name] = this.value
                }
            });

            let inputId = 'input-' + attrObj['data-id'];
            attrObj['id'] = inputId
            attrObj['onclick'] = attrObj['onblur']

            if (attrObj['value'] == 1) {
                attrObj['class'] += ' is_checked'
            }

            let attrString = '';
            for(attr in attrObj) {
                attrString += ` ${attr}="${attrObj[attr]}" `
            }

            $(this).parent().append(`
                <label class="beautyCheckbox" for="${inputId}">
                    <input ${attrString} />
                    <span></span>
                </label>
            `);
            $(this).hide()
        });

        ['A', 'B', 'C', 'D', 'E', 'F', 'Z'].forEach(function (blok) {
            if ($('#tab' + blok)[0]) {
                const tbody = $('#tab' + blok).find('tr');
                const blokLower = blok.toLowerCase();
                tbody.each(function(index){
                    let tr = $(this)
                    if(blok === 'B') {
                        let td = tr.find('td')
                        td[0].classList.add(blokLower + "-td-input");
                        if (td[1]) {
                            td[1].classList.add("code-td");
                        }
                    }
                    if (index !== 0) {
                        let td = tr.find('td')
                        if (td[1]) {
                            td[1].classList.add("code-td");
                        }
                        if (td[2]) {
                            td[2].classList.add(blokLower + "-td-input");

                        }
                    }
                })
            }
        });
    })
</script>