@php
    use Illuminate\Support\Facades\Request;
    $url1 = explode( '/', Request::path() );
 array_shift($url1);
 $url =  implode("/",$url1);


use App\Http\Models\Role;
use App\Http\Services\Constants;
@endphp
        <!doctype html>
<html>
<head>

    <link rel="stylesheet" href="{{asset('/site/img/flags/flags.css')}}"/>
    <link rel="stylesheet" href="{{asset('/site/css/blue.css')}}">
    <link rel="stylesheet" href="{{asset('/site/css/blue.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/blocks.css') }}">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <link rel="stylesheet" href="{{asset('/site/css/file-explore.css')}}">
    <link rel="stylesheet" href="{{asset('/site/css/public_style.css')}}">
    <link rel="shortcut icon" href="{{asset('/site/img/favicon_ecolo.png')}}" type="image/x-icon"/>
    <title>Ecolo</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <style>
        .navbar .nav.pull-right{
            float: right;
            margin-right: 0;
        }

        .full-width {
            width: 100%;
        }
        .header-menu .divider-vertical {
            height: 40px;
            border-right: 1px solid #fff;
            border-left: 1px solid #f2f2f2;
        }
        .header-menu .divider-vertical {
            border-left-color: #2078a1;
            border-right-color: #3497c2;
        }
        .header-menu.nav > li > a {
            text-shadow: none;
            display: inline-block;
            padding: 10px;
        }
        .header-menu.nav > li > a:hover {
            text-decoration: none;
        }
        .header-menu.nav > li > a i {
            display: block;
        }
        .header-menu .dropdown {
            display: flex;
            align-items: center;
            padding-right: 10px;
            padding-left: 10px;
        }
        .header-menu .dropdown-menu {
            background-color: #47AB6B;
            padding-right: 10px;
            padding-left: 10px;
            top:60px !important;
            border-radius: 5px;
        }
        .header-menu .dropdown-menu.language::before{
            right: 120px;
        }
        .header-menu .dropdown-menu::before {
            content: "";
            width: 20px;
            height: 20px;
            background-color:#47AB6B;
            position: absolute;
            top:-20px;
            right:9px;
            clip-path: polygon(51% 31%, 0% 100%, 100% 100%);
        }
        .header-menu .dropdown-menu li {
            padding-top: 5px;
            padding-bottom: 5px;
        }
        .header-menu .dropdown-menu li > a:hover {
            background: none !important;
        }
        .header-menu .dropdown-menu li > a {
            display: flex;
            align-items: center;
            color: white;
        }
        .header-menu .dropdown-menu li > a i {
            margin-right: 5px;
        }

        #loading_layer img {
            top: 50%;
            position: absolute;
        }

        #loading_layer {
            left: 0;
            position: fixed;
            z-index: 99999;
            background: #fffffff5;
            top: 0;
            width: 100%;
            text-align: center;
            height: 100%;
            display: block;
        }

        .js #loading_layer {
            visibility: visible;
            display: block !important;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: #fff;
            z-index: 10000
        }

        .js #loading_layer img {
            position: fixed;
            top: 50%;
            left: 50%;
            width: 64px;
            height: 10px;
            margin: -5px 0 0 -32px
        }
        .col{
            display: flex;
            align-items: center;
        }
        .col p{
            font-size: 20px;
            font-weight: 100;
            margin: 0;
        }

        .header-menu .dropdown-menu {
            left: calc(100% - 180px);
        }
    </style>
</head>
<body>
<header style="background: #eee">
    <div class="container">
        <div class="row full-width">
            <div class="col">
                <a href="/{{App::getLocale()}}/manager" style="position: absolute; left: -20px;">
                    <img src="{{asset('/site/img/alfa_bank.png')}}" alt="Alfa Logo" style="width: 90px;">
                </a>
            </div>
            <div class="navbar navbar-fixed-top" style="position: relative;left: 50px">
                <div class="navbar-inner">
                    <ul class="nav header-menu user_menu pull-right">
                        @if(Auth::check())
                            @if(Request::url() != env('APP_URL_NAME').'/ru/manager' && \Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Models\Role::get_role_id_by_name(\App\Http\Models\Role::ROLE_BANK_MANAGER))
                                <ul style="list-style-type: none;line-height: 22px; position: relative; left: -20px!important; color: black">
                                    <li style="font-size: 18px; font-weight: 600;letter-spacing: -0.41px;">Есть вопрос? | Есть ответ!</li>
                                    @if(env('works'))
                                        <li style="font-size: 16px;letter-spacing: -0.41px; text-align: right">мы в чате внизу страницы :)</li>
                                    @else
                                        <li style="font-size: 16px;letter-spacing: -0.41px; text-align: right">{{Constants::bankPhone()}}</li>
                                    @endif
                                </ul>
                            @else
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="display: flex; justify-content: space-around; align-items: center; color:black;font-size: 18px;">
                                        <i class="fas fa-user" style="font-size: 28px; margin-right:10px;"></i> {{\Illuminate\Support\Facades\Auth::user()->username}}
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="/{{App::getLocale()}}/logout">@lang('links.logout')</a>
                                        </li>
                                    </ul>
                                </li>
                            @endif
                        @else
                            <ul style="list-style-type: none;line-height: 22px;position: relative; left: 50px; color: black">
                                <li style="font-size: 18px; font-weight: 600;letter-spacing: -0.41px;">Есть вопрос? | Есть ответ!</li>
                                @if(env('works'))
                                    <li style="font-size: 16px;letter-spacing: -0.41px; text-align: right">мы в чате внизу страницы :)</li>
                                @else
                                    <li style="font-size: 16px;letter-spacing: -0.41px; text-align: right">{{Constants::bankPhone()}}</li>
                                @endif
                            </ul>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="faker"></div>
