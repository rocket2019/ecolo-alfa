@include('client.header')
<section class="mail">
    <div class="container">
        <div class="mail_inner text-center">
            <h1 class="block_content_title">Complete the questionnaire</h1>
            <p class="entry_form_subtitle">Dear <span>Username</span>, to complete filling your questionnaire, please visit the following link.</p>
            <a class="mail_btn" href="/">Complete</a>
        </div>
    </div>
</section>
@include('client.footer')
