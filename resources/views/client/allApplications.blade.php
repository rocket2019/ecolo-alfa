@include('client.header')
<section class="new_application all_apps">
    <div class="container">
        <div class="col">
            <div class="new_app_content">
                <div class="window">
                    <p>Мы всегда на связи<br>Звоните по номеру</p>
                    <p>+7 495-333-30-30<br>с 10-00 до 22-00</p>
                </div>
            </div>
        </div>
        <form>
            <p class="form_title">Заявки</p>
            <input type="text" name="search" placeholder="Поиск по клиентам">
        </form>
        <div class="table">
            <table>
                <tr>
                    <th>Дата</th>
                    <th>Название компании</th>
                    <th>Вид деятельности</th>
                    <th>Отделение</th>
                    <th>Анкета</th>
                </tr>
                <tr>
                    <td>19.11.2019</td>
                    <td>ИП Иванов</td>
                    <td>Торговля рыбными изделиями</td>
                    <td>ДО "АРМА"</td>
                    <td>
                        <a class="link_btn" href="/">Отчет</a>
                    </td>
                </tr>
                <tr>
                    <td>19.11.2019</td>
                    <td>ООО Грузовик</td>
                    <td>Перевозка грузов</td>
                    <td>ДО "Проспект Мира"</td>
                    <td>
                        <a class="profile_btn" href="/">Анкета</a>
                    </td>
                </tr>
                <tr>
                    <td>19.11.2019</td>
                    <td>ООО Ромашка</td>
                    <td>Торговля цветами</td>
                    <td>ДО БЦ "Преображенская площадь, 6"</td>
                    <td>
                        <a class="profile_btn" href="/">Анкета</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</section>
@include('client.footer')
