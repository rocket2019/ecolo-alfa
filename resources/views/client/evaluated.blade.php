@include('client.header')
<style>
    body{
        font-family: Roboto, 'sans-serif';
        background-color: #fff;
    }
    .content{
        max-width: 1323px;
        margin: 0 auto;
    }
    .clientMsg{
        margin-top: 30vh;
    }
    .clientMsg h1{
        color: #1D1D1B;
        font-weight: bold;
        text-align: center;
        font-size: 35px;
        line-height: 22px;
        text-transform: uppercase;
    }
    .clientMsg h3{
        color: #000;
        font-weight: normal;
        font-size: 20px;
        text-align: center;
    }
    .human-image{
        text-align: end;
        margin-top: 20vh;
    }
    .human-image img{
        width: 270px;
    }

    @media only screen and (max-width: 1330px){
        .human-image img{
            width: 210px;
        }
        .content{
            max-width: 1222px;
        }
    }

    @media only screen and (max-width: 768px){
        .clientMsg h1{
            font-size: 20px;
        }
        .clientMsg h3{
            font-size: 14px;
        }
    }
    @media only screen and (max-width: 768px){
        .clientMsg h1{
            font-size: 16px;
        }
        .clientMsg h3{
            font-size: 10px;
        }
    }
</style>

<body>
<div class="content">
    <div class="clientMsg">
        <h1>заявка была направлена на оценку</h1>
        <h3>Редактирование анкеты недоступно</h3>
    </div>
    <div class="human-image">
        <img src="{{asset('/site/img/human-big.png')}}" alt="Human">
    </div>
</div>
</body>
</html>
