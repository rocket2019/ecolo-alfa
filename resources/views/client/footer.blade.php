@php
    use App\Http\Models\Role;
@endphp

<div id="loading_layer" style="display:none"><img src="/site/img/ajax_loader.gif"/></div>

<div class="evabeta-logo-block" style="display: flex; flex-direction: row; max-width: 1190px; margin: 80px auto 21px; height: 50px; justify-content: center">
    <div style="position: relative;">
        <img src="{{asset('/site/img/evaBetaLogo.png')}}" alt="EvaBeta Logo" style="width: 140px; height: auto;">
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.0/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>

<script>

    if (typeof(lang_client) == "undefined") {
        var lang_client = window.location.pathname;
        lang_client.indexOf(1);

        lang_client.toLowerCase();

        lang_client = lang_client.split("/")[1];

        lang_client = '/' + lang_client;
    }

</script>

<!-- jQuery UI -->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<!-- Paging JS -->
<script type="text/javascript" src="{{asset('/site/js/paging.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.pagesFull').on('click', function () {
            let count = $(this).data('count') + 1;
            $('#tableData').paging({limit: count});
            $(".paging-nav a[data-page=0]").trigger( "click" );
            $(".paging-nav").hide();
            $(".pagesFull").hide();
        });
        $('#tableData').paging({limit: 25});
    });
</script>

<script src="https://rawgit.com/jackmoore/autosize/master/dist/autosize.min.js"></script>
{{--<script src="{{asset('/site/bootstrap/js/bootstrap.min.js')}}"></script>--}}
<script src="{{asset('/site/js/file-explore.js')}}"></script>
<script src="{{ asset('site/js/company.js?x='.time()) }}"></script>
<script src="{{ asset('site/js/client.js?x='.time()) }}"></script>
<script src="{{ asset('site/js/public.js?x='.time()) }}"></script>
<script src="//code.jivosite.com/widget/8qZpUETHIM" async></script>

</body>
</html>
