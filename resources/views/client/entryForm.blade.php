@include('client.header')
<style>
    #questionnaire-div {
        border: 1px solid #47b39c;
        border-radius: 5px;
        font-size: 18px;
        display: block;
        padding: 30px 0;
        position: relative;
    }

    #questionnaire-div .arrow-left,
    #questionnaire-div .arrow-right {
        position: absolute;
    }

    #questionnaire-div .arrow-left {
        top: -53px;
        left: -75px;
    }

    #questionnaire-div .arrow-right {
        right: -60px;
        bottom: -72px;
    }

    .questionnaire_inner {
        max-height: 300px;
        overflow: auto;
    }

    .suggestion-text {
        padding-right: 86px;
    }

    .file-tree {
        list-style-type: none;
    }

    .file-tree li:before {
        font-family: "Font Awesome 5 Free";
        content: "\f07b";
        font-weight: 900;
        color: #fec73a;
        font-size: 40px;
        padding-right: 5px;
    }

    .file-tree li:not(:last-child) {
        margin-bottom: 30px;
    }

    .file-tree li ul li:before {
        display: none;
    }

    .folder-root a {
        padding-left: 20px;
    }

    .folder-root .folder-root a {
        padding-left: 0;
    }

    .dropbtn {
        background-color: #4CAF50;
        color: white;
        padding: 16px;
        font-size: 16px;
        border: none;
        cursor: pointer;
    }

    .dropbtn:hover, .dropbtn:focus {
        background-color: #3e8e41;
    }

    #myInput {
        width: 551px;
        height: 43px;
        box-sizing: border-box;
        background-position: 14px 12px;
        background-repeat: no-repeat;
        font-size: 16px;
        padding: 5px 10px;
        border: 1px solid #47b39c;
        border-radius: 5px;
        display: block;
        margin-left: 45px;
    }

    #myInput:focus {
        border: 2px solid #47b39c;
    }

    .dropdown-content {
        overflow: auto;
        z-index: 1;
        padding-top: 15px;
    }


    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
    }

    .dropdown1 {
        height: 300px;
    }

    .dropdown1 a:hover {
        cursor: pointer;
        background-color: #ddd;
    }

    #selected {
        background: #DDE;
    }

    .file-tree li {
        cursor: pointer;
    }

    .text-in-left-example {
        display: flex;
        flex-direction: row-reverse;
        align-items: center;
        text-align: right;
        padding-right: 88px;
        padding-top: 53px;
        color: grey;
        letter-spacing: 0.5px;
        font-style: italic;
    }

    .text-in-left-example p {
        width: 374px;
    }

    @media only screen and (min-width: 1700px) {
        .text-in-left-example {
            padding-right: 41px;
        }
    }

    @media only screen and (min-width: 1900px) {
        .text-in-left-example {
            padding-right: 12px;
        }
    }

    @media only screen and (min-width: 2200px) {
        .text-in-left-example {
            padding-right: 0;
            left: 43px;
        }
    }

    @media only screen and (min-width: 2600px) {
        .text-in-left-example {
            left: 112px;
        }
    }

    @media only screen and (min-width: 3000px) {
        .text-in-left-example {
            left: 150px;
        }
    }

    .span9 .row-title {
        text-align: right;
        font-size: 23px;
        margin-top: 16px;
    }

    .span9 .row-title span {
        margin-left: 123px;
    }

    textarea.error-input {
        border: 2px solid red!important;
        color: red!important;
    }

    textarea.error-input::-webkit-input-placeholder {
        color: #FF0000;
    }

    textarea.error-input:-moz-placeholder { /* Firefox 18- */
        color: #FF0000;
    }

    textarea.error-input::-moz-placeholder {  /* Firefox 19+ */
        color: #FF0000;
    }

    textarea.error-input:-ms-input-placeholder {
        color: #FF0000;
    }

    .fill_hover_btn {
        margin: 0 18px!important;
        font-size: 18px!important;
        padding: 0 5px!important;
        height: 60px!important;
    }
    .fill_hover_btn:first-child {
        width: 200px!important;
    }
    .fill_hover_btn:last-child {
        width: 250px!important;
    }
</style>
<section class="form_section entry_form">
    <form id="client-registration">
        <div class="container">
            <h1>Новая Заявка</h1>
            <input id="code_id" type="hidden">
            <p class="entry_form_subtitle select_questionnaire">@lang('global.select_specialisation')</p>
            {{csrf_field()}}
            <div class="row">
                <div class="col-8 offset-2">
                    <div id="questionnaire-div">
                        <div class="questionnaire_inner">
                            <img src="/site/img/left.png" class="arrow-left">
                            <a href="#" class="btn btn-mini close_question" style="float: right"><i
                                        class="icon-remove"></i></a>
                            <ul class="file-tree">
                                <li class="file-icon"><a
                                            href="#">1 {{\App\Http\Models\CodeTree::get_by_code('1')->description}}</a>
                                    <ul>

                                        <li>
                                            <a href="#">1.1 {{\App\Http\Models\CodeTree::get_by_code('1.1')->description}}</a>
                                            <ul>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('1.1.1')->id}}">
                                                    1.1.1 {{\App\Http\Models\CodeTree::get_by_code('1.1.1')->description}}</li>
                                                <li>
                                                    <a href="#">1.1.2 {{\App\Http\Models\CodeTree::get_by_code('1.1.2')->description}}</a>
                                                    <ul>
                                                        <li class="code_id"
                                                            data-id="{{\App\Http\Models\CodeTree::get_by_code('1.1.2.1')->id}}">
                                                            1.1.2.1 {{\App\Http\Models\CodeTree::get_by_code('1.1.2.1')->description}}</li>
                                                        <li class="code_id"
                                                            data-id="{{\App\Http\Models\CodeTree::get_by_code('1.1.2.2')->id}}">
                                                            1.1.2.2 {{\App\Http\Models\CodeTree::get_by_code('1.1.2.2')->description}}</li>
                                                        <li class="code_id"
                                                            data-id="{{\App\Http\Models\CodeTree::get_by_code('1.1.2.3')->id}}">
                                                            1.1.2.3 {{\App\Http\Models\CodeTree::get_by_code('1.1.2.3')->description}}</li>
                                                        <li class="code_id"
                                                            data-id="{{\App\Http\Models\CodeTree::get_by_code('1.1.2.4')->id}}">
                                                            1.1.2.4 {{\App\Http\Models\CodeTree::get_by_code('1.1.2.4')->description}}</li>
                                                        <li class="code_id"
                                                            data-id="{{\App\Http\Models\CodeTree::get_by_code('1.1.2.5')->id}}">
                                                            1.1.2.5 {{\App\Http\Models\CodeTree::get_by_code('1.1.2.5')->description}}</li>
                                                        <li class="code_id"
                                                            data-id="{{\App\Http\Models\CodeTree::get_by_code('1.1.2.6')->id}}">
                                                            1.1.2.6 {{\App\Http\Models\CodeTree::get_by_code('1.1.2.6')->description}}</li>

                                                    </ul>
                                                </li>

                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#">1.2 {{\App\Http\Models\CodeTree::get_by_code('1.2')->description}}</a>
                                            <ul>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('1.2.1')->id}}">
                                                    1.2.1 {{\App\Http\Models\CodeTree::get_by_code('1.2.1')->description}}</li>
                                                <li>
                                                    <a href="#">1.2.2 {{\App\Http\Models\CodeTree::get_by_code('1.2.2')->description}}</a>
                                                    <ul>
                                                        <li class="code_id"
                                                            data-id="{{\App\Http\Models\CodeTree::get_by_code('1.2.2.1')->id}}">
                                                            1.2.2.1 {{\App\Http\Models\CodeTree::get_by_code('1.2.2.1')->description}}</li>
                                                        <li class="code_id"
                                                            data-id="{{\App\Http\Models\CodeTree::get_by_code('1.2.2.2')->id}}">
                                                            1.2.2.2 {{\App\Http\Models\CodeTree::get_by_code('1.2.2.2')->description}}</li>
                                                        <li class="code_id"
                                                            data-id="{{\App\Http\Models\CodeTree::get_by_code('1.2.2.3')->id}}">
                                                            1.2.2.3 {{\App\Http\Models\CodeTree::get_by_code('1.2.2.3')->description}}</li>
                                                        <li class="code_id"
                                                            data-id="{{\App\Http\Models\CodeTree::get_by_code('1.2.2.4')->id}}">
                                                            1.2.2.4 {{\App\Http\Models\CodeTree::get_by_code('1.2.2.4')->description}}</li>
                                                        <li class="code_id"
                                                            data-id="{{\App\Http\Models\CodeTree::get_by_code('1.2.2.5')->id}}">
                                                            1.2.2.5 {{\App\Http\Models\CodeTree::get_by_code('1.2.2.5')->description}}</li>
                                                        <li class="code_id"
                                                            data-id="{{\App\Http\Models\CodeTree::get_by_code('1.2.2.6')->id}}">
                                                            1.2.2.6 {{\App\Http\Models\CodeTree::get_by_code('1.2.2.6')->description}}</li>

                                                    </ul>
                                                </li>

                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#">1.3 {{\App\Http\Models\CodeTree::get_by_code('1.3')->description}}</a>
                                            <ul>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('1.3.2')->id}}">
                                                    1.3.2 {{\App\Http\Models\CodeTree::get_by_code('1.3.2')->description}}</li>

                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#">1.4 {{\App\Http\Models\CodeTree::get_by_code('1.4')->description}}</a>
                                            <ul>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('1.4.1')->id}}">
                                                    1.4.1 {{\App\Http\Models\CodeTree::get_by_code('1.4.1')->description}}</li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('1.4.2')->id}}">
                                                    1.4.2 {{\App\Http\Models\CodeTree::get_by_code('1.4.2')->description}}</li>

                                            </ul>
                                        </li>

                                    </ul>
                                </li>
                                <li class="file-icon"><a
                                            href="#">2 {{\App\Http\Models\CodeTree::get_by_code('2')->description}}</a>
                                    <ul>
                                        <li>
                                            <a href="#">2.1 {{\App\Http\Models\CodeTree::get_by_code('2.1')->description}}</a>
                                            <ul>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('2.1.1')->id}}">
                                                    2.1.1 {{\App\Http\Models\CodeTree::get_by_code('2.1.1')->description}}</li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('2.1.2')->id}}">
                                                    2.1.2 {{\App\Http\Models\CodeTree::get_by_code('2.1.2')->description}}</li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('2.1.3')->id}}">
                                                    2.1.3 {{\App\Http\Models\CodeTree::get_by_code('2.1.3')->description}}</li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('2.1.4')->id}}">
                                                    2.1.4 {{\App\Http\Models\CodeTree::get_by_code('2.1.4')->description}}</li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('2.1.5')->id}}">
                                                    2.1.5 {{\App\Http\Models\CodeTree::get_by_code('2.1.5')->description}}</li>

                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#">2.2 {{\App\Http\Models\CodeTree::get_by_code('2.2')->description}}</a>
                                            <ul>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('2.2.1')->id}}">
                                                    2.2.1 {{\App\Http\Models\CodeTree::get_by_code('2.2.1')->description}}</li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('2.2.3')->id}}">
                                                    2.2.3 {{\App\Http\Models\CodeTree::get_by_code('2.2.3')->description}}</li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('2.2.4')->id}}">
                                                    2.2.4 {{\App\Http\Models\CodeTree::get_by_code('2.2.4')->description}}</li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('2.2.5')->id}}">
                                                    2.2.5 {{\App\Http\Models\CodeTree::get_by_code('2.2.5')->description}}</li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('2.2.6')->id}}">
                                                    2.2.6 {{\App\Http\Models\CodeTree::get_by_code('2.2.6')->description}}</li>

                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#">2.3 {{\App\Http\Models\CodeTree::get_by_code('2.3')->description}}</a>
                                            <ul>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('2.3.1')->id}}">
                                                    2.3.1 {{\App\Http\Models\CodeTree::get_by_code('2.3.1')->description}}</li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('2.3.2')->id}}">
                                                    2.3.2 {{\App\Http\Models\CodeTree::get_by_code('2.3.2')->description}}</li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('2.3.3')->id}}">
                                                    2.3.3 {{\App\Http\Models\CodeTree::get_by_code('2.3.3')->description}}</li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('2.3.4')->id}}">
                                                    2.3.4 {{\App\Http\Models\CodeTree::get_by_code('2.3.4')->description}}</li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('2.3.5')->id}}">
                                                    2.3.5 {{\App\Http\Models\CodeTree::get_by_code('2.3.5')->description}}</li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('2.3.6')->id}}">
                                                    2.3.6 {{\App\Http\Models\CodeTree::get_by_code('2.3.6')->description}}</li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('2.3.7')->id}}">
                                                    2.3.7 {{\App\Http\Models\CodeTree::get_by_code('2.3.7')->description}}</li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('2.3.8')->id}}">
                                                    2.3.8 {{\App\Http\Models\CodeTree::get_by_code('2.3.8')->description}}</li>

                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#">2.4 {{\App\Http\Models\CodeTree::get_by_code('2.4')->description}}</a>
                                            <ul>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('2.4.1')->id}}">
                                                    2.4.1 {{\App\Http\Models\CodeTree::get_by_code('2.4.1')->description}}</li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('2.4.2')->id}}">
                                                    2.4.2 {{\App\Http\Models\CodeTree::get_by_code('2.4.2')->description}}</li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('2.4.3')->id}}">
                                                    2.4.3 {{\App\Http\Models\CodeTree::get_by_code('2.4.3')->description}}</li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('2.4.4')->id}}">
                                                    2.4.4 {{\App\Http\Models\CodeTree::get_by_code('2.4.4')->description}}</li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('2.4.5')->id}}">
                                                    2.4.5 {{\App\Http\Models\CodeTree::get_by_code('2.4.5')->description}}</li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('2.4.6')->id}}">
                                                    2.4.6 {{\App\Http\Models\CodeTree::get_by_code('2.4.6')->description}}</li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('2.4.7')->id}}">
                                                    2.4.7 {{\App\Http\Models\CodeTree::get_by_code('2.4.7')->description}}</li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('2.4.8')->id}}">
                                                    2.4.8 {{\App\Http\Models\CodeTree::get_by_code('2.4.8')->description}}</li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('2.4.9')->id}}">
                                                    2.4.9 {{\App\Http\Models\CodeTree::get_by_code('2.4.9')->description}}</li>

                                            </ul>
                                        </li>
                                        <li class="code_id"
                                            data-id="{{\App\Http\Models\CodeTree::get_by_code('2.5')->id}}">
                                            2.5 {{\App\Http\Models\CodeTree::get_by_code('2.5')->description}}</li>

                                    </ul>
                                </li>
                                <li class="file-icon"><a
                                            href="#">3 {{\App\Http\Models\CodeTree::get_by_code('3')->description}}</a>
                                    <ul>
                                        <li>
                                            <a href="#">3.1 {{\App\Http\Models\CodeTree::get_by_code('3.1')->description}}</a>
                                            <ul>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('3.1.1')->id}}">
                                                    3.1.1 {{\App\Http\Models\CodeTree::get_by_code('3.1.1')->description}}</li>
                                                <li>
                                                    <a href="#">3.1.2 {{\App\Http\Models\CodeTree::get_by_code('3.1.2')->description}}</a>
                                                    <ul>
                                                        <li class="code_id"
                                                            data-id="{{\App\Http\Models\CodeTree::get_by_code('3.1.2.1')->id}}">
                                                            3.1.2.1 {{\App\Http\Models\CodeTree::get_by_code('3.1.2.1')->description}}</li>
                                                        <li class="code_id"
                                                            data-id="{{\App\Http\Models\CodeTree::get_by_code('3.1.2.2')->id}}">
                                                            3.1.2.2 {{\App\Http\Models\CodeTree::get_by_code('3.1.2.2')->description}}</li>
                                                        <li class="code_id"
                                                            data-id="{{\App\Http\Models\CodeTree::get_by_code('3.1.2.3')->id}}">
                                                            3.1.2.3 {{\App\Http\Models\CodeTree::get_by_code('3.1.2.3')->description}}</li>
                                                        <li class="code_id"
                                                            data-id="{{\App\Http\Models\CodeTree::get_by_code('3.1.2.4')->id}}">
                                                            3.1.2.4 {{\App\Http\Models\CodeTree::get_by_code('3.1.2.4')->description}}</li>
                                                        <li class="code_id"
                                                            data-id="{{\App\Http\Models\CodeTree::get_by_code('3.1.2.5')->id}}">
                                                            3.1.2.5 {{\App\Http\Models\CodeTree::get_by_code('3.1.2.5')->description}}</li>

                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#">3.2 {{\App\Http\Models\CodeTree::get_by_code('3.2')->description}}</a>
                                            <ul>
                                                <li>
                                                    <a href="#">3.2.1 {{\App\Http\Models\CodeTree::get_by_code('3.2.1')->description}}</a>
                                                    <ul>
                                                        <li class="code_id"
                                                            data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.1.1')->id}}">
                                                            3.2.1.1 {{\App\Http\Models\CodeTree::get_by_code('3.2.1.1')->description}}</li>
                                                        <li class="code_id"
                                                            data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.1.2')->id}}">
                                                            3.2.1.2 {{\App\Http\Models\CodeTree::get_by_code('3.2.1.2')->description}}</li>
                                                        <li class="code_id"
                                                            data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.1.3')->id}}">
                                                            3.2.1.3 {{\App\Http\Models\CodeTree::get_by_code('3.2.1.3')->description}}</li>

                                                    </ul>
                                                </li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.2')->id}}">
                                                    3.2.2 {{\App\Http\Models\CodeTree::get_by_code('3.2.2')->description}}</li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.3')->id}}">
                                                    3.2.3 {{\App\Http\Models\CodeTree::get_by_code('3.2.3')->description}}</li>

                                                <li>
                                                    <a href="#">3.2.4 {{\App\Http\Models\CodeTree::get_by_code('3.2.4')->description}}</a>
                                                    <ul>
                                                        <li class="code_id"
                                                            data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.4.1')->id}}">
                                                            3.2.4.1 {{\App\Http\Models\CodeTree::get_by_code('3.2.4.1')->description}}</li>
                                                        <li class="code_id"
                                                            data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.4.2')->id}}">
                                                            3.2.4.2 {{\App\Http\Models\CodeTree::get_by_code('3.2.4.2')->description}}</li>

                                                    </ul>
                                                </li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.5')->id}}">
                                                    3.2.5 {{\App\Http\Models\CodeTree::get_by_code('3.2.5')->description}}</li>
                                                <li class="code_id"
                                                    data-id="{{\App\Http\Models\CodeTree::get_by_code('3.2.6')->id}}">
                                                    3.2.6 {{\App\Http\Models\CodeTree::get_by_code('3.2.6')->description}}</li>


                                            </ul>
                                        </li>

                                    </ul>
                                </li>
                            </ul>
                            <img src="/site/img/right.png" class="arrow-right">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="suggestion-block row">

                <div class="text-in-left-example col-4">
                    <p>Например: введите в строке поиска "рыба", чтобы найти рыбный магазин</p>
                </div>

                <div class="suggestion-text col-4">
                    <p class="large">или</p>
                    <p class="entry_form_subtitle">используйте поиск по ключевым словам</p>
                </div>

            </div>

        </div>

        <div class="container">
            <div class="choosen">
                <div class="span9 row">
                    <div class="col-2 row-title">
                        <span>АНКЕТА</span>
                    </div>
                    <div class="col-8" style="max-width: 56%;">
                        <div id="myDropdown" class="dropdown-content">

                            <input type="text" name="ateco_id" placeholder="Введите название вида деятельности или ключевое слово"
                                   onclick="open_sose()" id="myInput"
                                   onkeyup="filterFunction()" autocomplete="off" style="padding-left: 15px;">
                            <button class="btn clear_ateco"
                                    style="vertical-align: top;visibility: hidden; display: none;">
                                <i class="icon-remove icon-white"></i>
                            </button>
                            <div class="dropdown1" id='sose_okveds_select' style="
                                overflow-x: hidden;
                                display: none;
                                width: 551px;
                                border-top: none;
                                border-right: 1px solid rgb(71, 179, 156);
                                border-bottom: 1px solid rgb(71, 179, 156);
                                border-left: 1px solid rgb(71, 179, 156);
                                border-image: initial;
                                border-radius: 0px 0px 5px 5px;
                                margin-left: 45px;">

                                @foreach($ateco as $at)
                                    @php
                                        $description = $at->sose_ateco->description;
                                        $pieces = explode("[", $description);
                                    @endphp

                                    <a class="ateco_id_function" data-id="{{$at->sose_ateco->id}}"
                                       onclick="callmodifyAtecoImputValueAfterAnyMiliseconds(this)">
                                        <span class="key-for-search"
                                              style="display: none">{{$at->sose_okved->okved_code.' - '.$at->description}}</span>
                                        <span
                                                id="needed-value-for-inp">{{$at->sose_ateco->ateco_code.'-'.$pieces[0]}}</span>
                                        <span class="key-for-search"
                                              style="display: none">@if(isset($pieces[1])){{$pieces[1]}}@endif</span>
                                    </a>

                                @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="col-4" style="flex: 0 !important;">
                        <div class="btn_container">
                            <button type="button" class="profile_btn preview hover_btn"
                                    style="border-radius: 5px;display: none;text-transform: uppercase">Предпросмотр
                            </button>
                            <a style="display:none;align-items: center; text-transform: uppercase;border-radius:5px;font-weight: 400!important;text-align: center;color: black;padding: 3px 16px;width: 140px;text-decoration: none;font-size: 14px;"
                               target="_blank" class="link_btn_grey link-btn product_codes">Коды товаров</a>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <div class="container">
            <div class="row num_row">
                <div class="col-8 offset-2 entry-block">
                    <p class="entry_form_subtitle text-left">
                        Количество точек продаж/производственных структур
                    </p>
                    <select class="num" name="company_unit">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                    </select>
                </div>
            </div>
        </div>


        <div class="container">
            <div class="form_field">
                <div class="form_field_inner">
                    <label>Клиент</label>
                    <div class="input-block">
                        {{--                        <input type="text" name="company_name"--}}
                        {{--                               placeholder="Введите название организации (например ООО Ромашка)"--}}
                        {{--                               onkeypress="setTextColorBlack(this);">--}}
                        <textarea class="autoresizing ui-resizing" rows="" type="text" name="company_name"
                                  placeholder="Введите название организации (например ООО Ромашка)"
                                  onkeypress="setTextColorBlack(this);"></textarea>
                        {{--                        <span class="error error_company_name">@lang('global.field_organization_name')</span>--}}
                    </div>
                </div>
            </div>
            <div class="form_field">
                <div class="form_field_inner">
                    <label>Email Клиента</label>
                    <div class="input-block">
                        <input type="text" name="email_manager" placeholder="Введите email"
                               onkeypress="setTextColorBlack(this);">
                        {{--                        <span class="error error_email_manager">@lang('global.field_email_manager')</span>--}}
                        {{--                        <span class="error error_email_manager_not_valid">@lang('global.field_email_manager_not_valid')</span>--}}
                    </div>
                </div>
            </div>
            <div class="form_field">
                <div class="form_field_inner">
                    <label>ИНН</label>
                    <div class="input-block">
                        <input type="text" name="INN_number" placeholder="Введите ИНН организации"
                               onkeypress="setTextColorBlack(this);">
                        {{--                        <span class="error error_INN_number">@lang('global.field_INN_number')</span>--}}
                    </div>
                </div>
            </div>
            <div class="form_field">
                <div class="form_field_inner">
                    <label>Вид Деятельности</label>
                    <div class="input-block">
                        {{--                        <input type="text" name="main_activity" placeholder="Вид деятельности организации" onkeypress="setTextColorBlack(this);">--}}
                        <textarea class="autoresizing ui-resizing" rows="" type="text" name="main_activity" placeholder="Вид деятельности организации" onkeypress="setTextColorBlack(this);"></textarea>
                        {{--                        <span class="error error_main_activity">@lang('global.field_main_activity')</span>--}}
                    </div>
                </div>
            </div>
            <div class="form_field">
                <div class="form_field_inner">
                    <label>Регион/область</label>
                    <div class="input-block">
                        <select id="area_region" name="area_region" onchange="setSelectColorBlack(this);">
                            <option value="" selected="true" disabled="disabled">@lang('labels.select_region')</option>
                            @foreach($regions as $region)
                                <option
                                        @if(!empty($company) && $company->region == $region->area_code) selected
                                        @endif
                                        value="{{$region->area_code}}"
                                        style="color: black">{{$region->region_description}}</option>
                            @endforeach
                        </select>
                        {{--                        <span class="error error_area_region">@lang('global.field_area_region')</span>--}}
                    </div>
                </div>
            </div>
            <div class="form_field">
                <div class="form_field_inner">
                    <label>Город/Район</label>
                    <div class="input-block">
                        <select id="area_city" name="area_city" onchange="setSelectColorBlack(this);">
                            <option class="city" value="" selected="true"
                                    disabled="disabled">@lang('labels.select_city')</option>
                        </select>
                        {{--                        <span class="error error_area_city">@lang('global.field_area_city')</span>--}}
                    </div>
                </div>
            </div>
            <div class="form_field flex_row justify-content-center">
                <button name="later" type="button" class="later submitLater fill_hover_btn">Заполнить позже</button>
                <button name="office" type="button" class="office submitOffice fill_hover_btn">Заполнить в отделении</button>
            </div>

        </div>

        <div class="modal fade" id="preview-questionnaire" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content modal-content-preview">


                </div>
            </div>
        </div>
    </form>
</section>

<div id="sendLinkCompleted" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-confirm modal-dialog-centered" style="width: 460px">
        <div class="modal-content" style="width: 460px; height: 161px;border-radius: 15px;">
            <div class="modal-header">
                <div class="icon-box"
                     style="background: #47AB6B;width: 103px !important; height: 103px !important;align-content: center;border-radius: 50%;align-items: center;display: flex;top: -73px !important;">
                    {{--                            <i class="material-icons">&#xE5CD;</i>--}}
                    <img src="{{asset('/site/img/success_icon.png')}}"
                         style="width: 62px !important;height: auto;display: flex;margin: 0 auto;">
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="font-size: 0px">
                    <a href="/{{\Illuminate\Support\Facades\App::getLocale()}}/manager" class="modal-close"><img
                                src="{{asset('/site/img/close_icon.png')}}" style="width: 15px; height: 15px;" alt=""></a>
                </button>
            </div>
            <div class="modal-body" style="display: flex; justify-content: center; align-items: center;">
                <h4 class="modal-title"
                    style="font-size: 18px;margin: 0;color: black">@lang('global.entry_eval_success')</h4>
            </div>
        </div>
    </div>
</div>

@include('client.footer')
