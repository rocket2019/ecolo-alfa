@include('client.header')
@php
    use Carbon\Carbon;
    use Illuminate\Support\Facades\App;
    use Illuminate\Support\Facades\Auth;
    use App\Http\Models\Role;
    use App\Http\Models\User;
    use App\Http\Services\Constants;
    use App\Http\Services\Is;
@endphp
<style>
    th {
        text-align: left!important;
    }
    .paging-nav {
        padding-top: 15px;
        text-align: center;
    }
    .paging-nav a {
        text-decoration: none;
        color: #374051;
        opacity: 0.5;
        font-size: 16px;
        font-weight: 600;
        margin-left: 15px;
    }
    .p-hidden {
        display: none;
    }
    .paging-nav a:first-child, .paging-nav a:last-child {
        font-size: 25px;
        opacity: 1;
    }
    .paging-nav a:hover {
        opacity: 1
    }
    .paging-nav .selected-page {
        opacity: 1!important;
    }
    .pagesFull {
        text-transform: uppercase;
        float: right;
        position: relative;
        bottom: 25px;
        color: #374051;
        text-decoration: underline!important;
        cursor: pointer;
        font-weight: 600;
        font-size: 16px;
    }
    .count_folders {
        background: #F1F0F0;
        border-radius: 15px;
        padding: 10px 15px;
        height: 100px;
    }
    .count_folders img {
        float: right
    }
    .count_folders h1 {
        display: inline;
        font-size: 40px;
        font-weight: 700;
    }
    .count_folders p {
        text-transform: uppercase;
        font-size: 14px;
        font-weight: 700;
    }
    .arr-date {
        border: solid black;
        border-width: 0 2px 2px 0;
        display: inline-block;
        padding: 3px;
        position: relative;
        left: 5px;
        cursor: pointer;
    }
    .arr-up {
        transform: rotate(-135deg);
        -webkit-transform: rotate(-135deg);
    }
    .arr-down {
        transform: rotate(45deg);
        -webkit-transform: rotate(45deg);
        bottom: 2px;
    }

    span.plus {
        border: 4px solid;
        border-radius: 50%;
        width: 50px;
        height: 50px;
        display: flex;
        justify-content: center;
        align-items: center;
        margin-right: 5px;
    }
    div.info-table {
        background: #f8f8f8;
        display: flex;
        justify-content: center;
        padding: 50px 0;
    }
    div.info-table .info-box {
        width: 50%;
        background: #fff;
        padding: 30px;
        border-radius: 15px;
    }
    div.info-table .info-box h3 {
        text-align: center;
        text-transform: uppercase;
        font-weight: bold;
        font-size: 21px;
        line-height: 18px;
    }
    div.info-table .info-box p {
        font-size: 14px;
        line-height: 22px;
        font-weight: 400;
    }
    div.info-table .info-box form {
        text-align: center;
    }
    div.info-table .info-box button {
        text-transform: uppercase;
        border: none;
        background: #47AB6B;
        color: #FFF;
        border-radius: 5px;
        padding: 7px 15px;
        font-size: 14px;
    }
    div.info-table .info-box button:hover {
        background: #32774b;
    }
    div.info-table .info-box button:focus {
        outline: none;
    }
    .fill_hover_btn {
        width: 290px;
        height: 70px;
        font-size: 28px;
    }

    #works {
        width: 100%;
        height: 100%;
        position: fixed;
        top: 0;
        left: 0;
        z-index: 999;
        background: rgba(0,0,0,0.2);
    }
    #works .info {
        width: 550px;
        padding: 30px;
        background: #fff;
        position: absolute;
        top: 35%;
        left: 35%;
        border-radius: 10px;
    }
    #works .info h1 {
        font-size: 21px;
        font-weight: 700;
        line-height: 18px;
        text-align: center;
    }
    #works .info p {
        font-size: 16px;
        font-weight: 400;
        line-height: 22px;
        margin-top: 22px;
    }
</style>

@if(env('works'))
<div id="works">
    <div class="info">
        <h1>Дорогие пользователи!</h1>
        <p>
            С <b>{{env('work_from')}} по {{env('work_to')}} марта</b> наш колл-центр будет недоступен по причине проведения плановых технических работ. Все операторы колл-центра на время переводятся в чат-поддержку внизу страницы, поэтому пишите нам туда, всегда рады помочь :)
        </p>
        <div style="text-align: center; padding: 10px 0">
            <img style="position: absolute; margin-top: -4px;" src="{{asset('/site/img/jivochat2.svg')}}" alt="jivochat">
            <img src="{{asset('/site/img/jivochat.svg')}}" alt="jivochat">
        </div>
    </div>
</div>
<script>
    document.getElementById("works").addEventListener('click', e => {
        if(e.target === e.currentTarget) {
            document.getElementById("works").style.display = 'none'
        }
    });
</script>
@endif

<section class="new_application">
    <div class="container">
        <div class="row">
            <div class="@if(Is::bankManager()) col-md-4 @else col-md-8 @endif">
                @if(!Is::supervisor())
                    <a class="new_app_btn fill_hover_btn" href="/{{App::getLocale()}}/client/{{Auth::user()->client_token}}/register">
                        <span class="plus">+</span>
                        Новая Заявка
                    </a>
                @else
                    <div class="row col-md-10">
                        @if($counts['send'] > 0 && $counts['send'] != $counts['end'])
                            <div class="col-md-6 count_folders">
                                <img src="{{asset('/site/img/folder1-icon.png')}}">
                                <h1>{{ $counts['send'] }}</h1>
                                <p>полученные анкеты</p>
                            </div>
                        @endif
                        <div class="col-md-6 count_folders" @if($counts['send'] > 0 && $counts['send'] != $counts['end']) style="position: relative; left: 20px" @endif>
                            <img src="{{asset('/site/img/folder2-icon.png')}}">
                            <h1>{{ $counts['end'] }}</h1>
                            <p>оцененные анкеты</p>
                        </div>
                    </div>
                @endif
            </div>

            <div class="col-md-4 @if(Is::bankManager()) offset-md-4 @endif">
                <div class="new_app_content">
                    <div class="new_app_operator_img">
                        <img src="{{asset('site/img/eva_smile.png')}}">
                    </div>
                    <div class="window" style="padding: 20px 15px 15px; background: #F8F8F8; border: none">
                        <p style="text-transform: uppercase; font-size: 14px; line-height: 20px">Наши операторы на связи</p>
                        <img style="width: 45px;" src="{{asset('/site/img/eva-clock.svg')}}" alt="Clock">
                        <p style="font-weight: 700 !important; font-size: 16px; line-height: 20px;">с 10:00 до 22:00</p>
                        <p style="margin-top: -15px">по Москве</p>
                        @if(env('works'))
                            <p style="font-weight: 700 !important; font-size: 21px; line-height: 22px;">в чате на сайте</p>
                        @else
                            <p style="font-weight: 700 !important; font-size: 21px; line-height: 22px;">{{Constants::bankPhone()}}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <form>
            <p class="form_title">Клиенты</p>
            <input type="text" value="{{Request::get('search')}}" name="search" placeholder="Для поиска введите указанный ИНН или название компании">
        </form>
        <div class="table @if(count($clients) <= 0) info-table @endif">
            @if(count($clients) > 0)
                <table @if(count($clients) > 25) id="tableData" @endif class="tableDataSort">
                    <tr>
                        <th style="width: 92px;border-top: 1px solid #47b39c!important; border-bottom: 1px solid #47b39c!important; text-transform: uppercase; font-weight: bold;">Дата
                            <i class="arr-date arr-up" style="display: none"></i>
                            <i class="arr-date arr-down"></i>
                        </th>
                        <th style="width: 228px;border-top: 1px solid #47b39c!important; border-bottom: 1px solid #47b39c!important; text-transform: uppercase; font-weight: bold;">Название компании</th>
                        <th style="width: 271px;border-top: 1px solid #47b39c!important; border-bottom: 1px solid #47b39c!important; text-transform: uppercase; font-weight: bold;">Вид деятельности</th>
                        @if(Is::supervisor())
                            <th style="width: 371px;border-top: 1px solid #47b39c!important; border-bottom: 1px solid #47b39c!important; text-transform: uppercase; font-weight: bold;">Менеджер</th>
                        @endif
                        <th @if(Is::supervisor()) style="width: 292px;border-top: 1px solid #47b39c!important; border-bottom: 1px solid #47b39c!important; text-transform: uppercase; font-weight: bold;" @else style="width: 192px;border-top: 1px solid #47b39c!important; border-bottom: 1px solid #47b39c!important; text-transform: uppercase; font-weight: bold;" @endif>Статус</th>
                        <th style="border-top: 1px solid #47b39c!important; border-bottom: 1px solid #47b39c!important; text-transform: uppercase; font-weight: bold;" @if(!Is::supervisor()) style="width: 308px;border-top: 1px solid #47b39c!important; border-bottom: 1px solid #47b39c!important; text-transform: uppercase; font-weight: bold;" @endif></th>
                    </tr>
                    @foreach($clients as $client)
                        <tr>
                            <td>
                                @if(Is::supervisor())
                                    {{Carbon::parse($client->updated)->format('d.m.Y')}}
                                @else
                                    {{Carbon::parse($client->created)->format('d.m.Y')}}
                                @endif
                            </td>
                            <td>{{$client->company_name}}</td>
                            <td>{{$client->main_activity}}</td>
                            @if(Is::supervisor())
                                @php
                                    $user = User::query()->find($client->manager_id);
                                    $manager_name = is_object($user) ? $user->first_name.' '.$user->last_name : '';
                                @endphp
                                <td>{{ $manager_name }}</td>
                            @endif
                            <td>
                                @if(file_exists(base_path('public/site/reports/pdf/'.strtolower($client->company_name.$client->ID_company).'.pdf')))
                                    @lang('labels.eval_complete')
                                @else
                                    {{$client->questionnaire_status == \App\Http\Models\Client::QUESTIONNAIRE_STATUS_EDITING ? trans('labels.eval_edit') : trans('labels.in_eval')}}
                                @endif
                            </td>
                            <td>
                                <div class="btn_container">
                                    @if($client->questionnaire)
                                        @if ($client->questionnaire_status == \App\Http\Models\Client::QUESTIONNAIRE_STATUS_EVALUATE || $client->questionnaire_status == 3 || $client->questionnaire_status == 6)
                                            @php
                                                if(file_exists(base_path('public/site/reports/pdf/'.strtolower($client->company_name.$client->ID_company).'.pdf'))){
                                            @endphp
                                            @php
                                                if(file_exists(base_path('public/site/reports/pdf/'.strtolower($client->company_name.$client->ID_company).'.pdf'))){
                                            @endphp
                                            @if(Is::supervisor())
                                                <a class="profile_btn hover_btn"
                                                   href="{{asset('/site/reports/pdf/'.strtolower($client->company_name.$client->ID_company).'.pdf')}}"
                                                   target="_blank"
                                                   style="height: 30px; @if(Is::supervisor()) margin: 0 5px; @else margin: 0 25px 0 30px; @endif border-radius: 5px; border: none !important; text-align: center; color: #fff !important; width: 110px !important;">ОТЧЕТ</a>
                                            @endif
                                            @php
                                                }
                                            @endphp
                                            @php
                                                }
                                            @endphp
                                            @if(Is::supervisor())
                                                @if(file_exists(base_path('public/site/reports/pdf/'.strtolower($client->company_name.$client->ID_company).'.pdf')))
                                                    <a class="profile_btn link_btn_grey"
                                                       href="/{{App::getLocale()}}/client/questionnaire/view/{{$client->ID_company}}/{{$client->questionnaire->id}}"
                                                       style=" @if(Is::supervisor()) margin: 0 5px; @else margin-right: 25px; @endif border-radius: 5px;background-color: rgba(0, 0, 0, 0);color: #000;cursor: pointer"
                                                    >АНКЕТА</a>
                                                @else
                                                    <a class="profile_btn hover_btn" style="border-radius: 5px;"
                                                       href="/{{App::getLocale()}}/client/questionnaire/view/{{$client->ID_company}}/{{$client->questionnaire->id}}">АНКЕТА</a>
                                                @endif
                                            @elseif(Is::bankManager())
                                                <a class="pdf_btn hover_btn" style="border-radius: 5px;
                                                @if(Is::bankManager())
                                                        margin: 0 50px 0 32%;
                                                @elseif(!file_exists(base_path('public/site/reports/pdf/'.strtolower($client->company_name.$client->ID_company).'.pdf'))) margin: 0 50px 0 36%; @else margin: 0 50px; @endif"
                                                   href="/{{App::getLocale()}}/company/print/{{$client->questionnaire->id_company}}/{{$client->questionnaire->id}}/{{$client->questionnaire->sose_ateco->id}}"
                                                   target="_blank" download>СКАЧАТЬ PDF</a>
                                            @endif
                                        @else

                                            <a class="profile_btn hover_btn" style="margin: 0 0 0 50px; border-radius: 5px;"
                                               href="/{{App::getLocale()}}/client/questionnaire/view/{{$client->ID_company}}/{{$client->questionnaire->id}}">АНКЕТА</a>
                                            <a class="link_btn link_btn_grey" style="margin: 0 50px; border: none!important;"
                                               data-link="/client/questionnaire/view/{{$client->ID_company}}/{{$client->questionnaire->id}}">ССЫЛКА</a>
                                        @endif
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </table>
                @if(count($clients) > 25)
                    <a class="pagesFull" data-count="{{count($clients)}}">показать все анкеты</a>
                @endif
            @else
                <div class="info-box">
                    <h3>Привет!</h3>
                    <p>
                        Это ваша главная страница личного кабинета.
                        Здесь будут отражены все заведенные заявки на оценку.
                        При возникновении любых вопросов, можно обратиться по телефону или в чат поддержки на сайте :)
                    </p>
                    <p>
                        Также, до начала использования системой, вы можете скачать <b>инструкцию пользователя</b> для ознакомления.
                    </p>
                    <form action="{{route('downloadNewInstruction')}}" method="post">
                        @csrf
                        <button type="submit">Скачать инструкцию</button>
                    </form>
                </div>
            @endif
        </div>
    </div>
</section>
<style>
    .link_btn {
        cursor: pointer;
    }
</style>

<div class="modal" id="sendLink" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-confirm modal-dialog-centered" style="width: 460px;">
        <div class="modal-content" style="width: 460px;border-radius: 16px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <a href="/{{\Illuminate\Support\Facades\App::getLocale()}}/manager" class="modal-close"><img src="{{asset('/site/img/close_icon.png')}}" style="width: 15px;height: 15px;" alt=""></a>
                </button>
            </div>
            <div class="modal-body" style="padding: 0">
                <h5 class="modal-title" style="font-family: 'Roboto', sans-serif;color: black; font-size: 18px; margin-bottom: 18px;">Почта для отправки анкеты</h5>

                <form id="sendLinkForm">
                    <input type="text" name="client_email" class="form-control" placeholder="mail@email.com" style="font-size: 14px;text-align: center; margin: 10px auto 0; width: 255px; border-radius: 8px;">
                    <input class="client-url-mail" type="hidden" name="link">
                    <span class="error"></span>
                </form>
            </div>
            <div class="modal-footer" style="padding: 15px 0 0;">
                <button type="button" class="btn btn-primary confirm-send hover_btn" style="margin: 0 auto; border-radius: 5px; background-color: #47AB6B;">ОТПРАВИТЬ</button>
            </div>
        </div>
    </div>
</div>

{{--<div class="modal" id="sendLink" tabindex="-1" role="dialog">--}}
{{--    <div class="modal-dialog modal-confirm" style="width: 40%;left: 50%;position: absolute;width: 40%;transform: translate(-50%, -100%);top: 50%;">--}}
{{--        <div class="modal-content" style="border-radius: 16px;">--}}
{{--            <div class="modal-header">--}}
{{--                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">--}}
{{--                    <a href="/{{\Illuminate\Support\Facades\App::getLocale()}}/manager" class="modal-close"><img src="{{asset('/site/img/close_cross.svg')}}" alt=""></a>--}}
{{--                </button>--}}
{{--            </div>--}}
{{--            <div class="modal-body" style="padding: 0">--}}
{{--                <h5 class="modal-title" style="color: black; font-size: 20px; margin-bottom: 18px;">Введите почту для отправки анкеты</h5>--}}

{{--                <form id="sendLinkForm">--}}
{{--                    <input type="text" name="client_email" class="form-control" placeholder="example@mail.ru" style="text-align: center; margin: 10px auto 0; width: 65%; border-radius: 8px;">--}}
{{--                    <input type="hidden" name="link">--}}
{{--                    <span class="error"></span>--}}
{{--                </form>--}}
{{--            </div>--}}
{{--            <div class="modal-footer" style="padding: 15px 0 0;">--}}
{{--                <button type="button" class="btn btn-primary confirm-send" style="margin: 0 auto; border-radius: 5px; background-color: #47AB6B;">ОТПРАВИТЬ</button>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}

<div id="sendLinkCompleted" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-confirm modal-dialog-centered" style="width: 460px">
        <div class="modal-content" style="width: 460px; height: 161px;border-radius: 15px;">
            <div class="modal-header">
                <div class="icon-box" style="background: #47AB6B;width: 103px !important; height: 103px !important;align-content: center;border-radius: 50%;align-items: center;display: flex;top: -73px !important;">
                    {{--                            <i class="material-icons">&#xE5CD;</i>--}}
                    <img src="{{asset('/site/img/success_icon.png')}}" style="width: 62px !important;height: auto;display: flex;margin: 0 auto;">
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="font-size: 0px">
                    <a href="/{{\Illuminate\Support\Facades\App::getLocale()}}/manager" class="modal-close"><img src="{{asset('/site/img/close_icon.png')}}" style="width: 15px; height: 15px;" alt=""></a>
                </button>
            </div>
            <div class="modal-body" style="display: flex; justify-content: center; align-items: center;">
                <h4 class="modal-title" style="font-size: 18px;margin: 0;color: black">@lang('global.send_success')</h4>
            </div>
        </div>
    </div>
</div>

{{--<div class="modal" id="sendLinkCompleted" tabindex="-1" role="dialog">--}}
{{--    <div class="modal-dialog modal-confirm" style="width: 40%;left: 50%;position: absolute;width: 40%;transform: translate(-50%, -100%);top: 50%;">--}}
{{--        <div class="modal-content" style="border-radius: 16px;">--}}
{{--            <div class="modal-header">--}}
{{--                <div class="icon-box">--}}
{{--                    --}}{{--                            <i class="material-icons">&#xE5CD;</i>--}}
{{--                    <img src="{{asset('/site/img/successIcon.svg')}}" style="width: 120px">--}}
{{--                </div>--}}
{{--                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">--}}
{{--                    <a href="/{{\Illuminate\Support\Facades\App::getLocale()}}/manager" class="modal-close"><i class="fas fa-times"></i></a>--}}
{{--                </button>--}}
{{--            </div>--}}
{{--            <div class="modal-body">--}}
{{--                <h4 class="modal-title" style="color: black; font-size: 24px; margin-bottom: 18px;">Анкета отправлена на почту</h4>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}

@include('client.footer')
