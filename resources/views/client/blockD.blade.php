@include('client.header')
<section class="form_section">
    <div class="container">
        <span class="prf_close">
            Закрыть анкету
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 49.354 49.354">
                <g id="X" transform="translate(-496.323 -76.323)">
                    <line id="Line_1" data-name="Line 1" x2="49" y2="49" transform="translate(496.5 76.5)" fill="none" stroke="#ef342d" stroke-width="3"></line>
                    <line id="Line_2" data-name="Line 2" x1="49" y2="49" transform="translate(496.5 76.5)" fill="none" stroke="#ef342d" stroke-width="3"></line>
                </g>
            </svg>
        </span>
        <form class="row">
            <div class="col-lg-7">
                <div class="form_field">
                    <label>Клиент</label>
                    <input type="text" name="client" placeholder="ИП Иванов Иван Иванович">
                </div>
                <div class="form_field">
                    <label>Email Клиента</label>
                    <input type="text" name="client_mail" placeholder="ivavivan@yandex.ru">
                </div>
                <div class="form_field">
                    <label>ИНН</label>
                    <input type="text" name="inn" placeholder="789449657914">
                </div>
                <div class="form_field">
                    <label>Вид Деятельности</label>
                    <input type="text" name="activity" placeholder="Торговля рыбными консервами">
                </div>
                <div class="form_field">
                    <label>Субъект РФ</label>
                    <select name="rf_subject">
                        <option value="moscow">Москва</option>
                        <option value="novosibirsk">Новосибирск</option>
                    </select>
                </div>
                <div class="form_field">
                    <label>Муниципальное Образование</label>
                    <input type="text" name="municipality">
                </div>
            </div>
            <div class="col-lg-4 offset-xl-1">
                <div class="profile_info">
                    <div class="profile_number">
                        <p class="title">Анкета</p>
                        <p class="number">vm27b</p>
                    </div>
                    <div class="profile_doc">
                        <p class="number">472300</p>
                        <a class="doc_download" href="http://www.dreamscene.org/wallpapers/Firesky.jpg" download>Скачать PDF</a>
                    </div>
                </div>
                <p class="profile_text">Розничная торговля рыбой, ракообразными и молюсками</p>
                <div class="btn_container">
                    <a class="profile_btn" href="/">Сохранить</a>
                    <button type="submit" class="link_btn" href="/">Отправить на оценку</button>
                </div>
            </div>
        </form>
    </div>
</section>
login
@include('client.footer')

