<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h1 style="text-align: center; color: black;margin-bottom: 50px;">Новая анкета на оценку</h1>
<p style="font-size: 20px; color: black;">Поступила анкета на оценку. Клиент - <a
        href="{{env('APP_URL_NAME').'/'.Illuminate\Support\Facades\App::getLocale()}}/company/view/{{$client}}"
        style="font-weight: 600;color: black;text-decoration: none;">{{$company_name}}</a></p>
</body>
</html>
