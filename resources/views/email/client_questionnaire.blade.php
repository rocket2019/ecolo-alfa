<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
    <meta name="author" content="Ecolo Development Team">
    <meta name="description" content="Ecolo | OUR SOLUTIONS FOR YOUR EFFICIENCY">
    <meta name="keywords"
          content="admin@ecolo-evabeta.com, Ecolo">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400&display=swap" rel="stylesheet">
    <title>EvaBeta</title>
</head>
<body style="margin: 0;padding: 0;">
<div style="margin:0;padding:0;font-family: Roboto,sans-serif;font-weight:300;min-height:100%;height:100%;width:100%">
    <table id="m_-8830627616452772601wrapper" cellpadding="20" cellspacing="0" border="0"
           style="width:100%;background-color:#eaeaea;font-family: Roboto,sans-serif;font-weight:300;border-collapse:collapse;margin:0;padding:0;line-height:100%;height:100%">
        <tbody>
        <tr>
            <td style="border-collapse:collapse;vertical-align:top">
                <table id="m_-8830627616452772601contentTable" cellpadding="0" cellspacing="0" border="0"
                       style="background-color:#fff;margin:0 auto;width:680px;border:solid 1px #ddd;border-collapse:collapse">
                    <tbody>
                    <tr>
                        <td style="border-collapse:collapse;vertical-align:top">
                            <table id="m_-8830627616452772601header" cellpadding="20" cellspacing="0" border="0"
                                   style="width:100%!important;min-width: 100%!important; max-width: 100%!important;border-collapse:collapse;background: #eeeeee">
                                <tbody>
                                <tr>
                                    <td style="color:#eee;background-color: #eee;font-size:31px;font-weight:bold;border-collapse:collapse;vertical-align:top;text-align: center;">
                                        <img src="{{asset('/site/img/alfa_bank.png')}}"
                                             width="100" alt="EvaBeta" title="image"
                                             style="outline:none; text-decoration:none; margin: -10px -15px -5px 0!important;" class="CToWUd">
                                        <img src="{{asset('/site/img/yellow_line.png')}}" style="display: inline-block; margin: 0 0 10px 11px;" alt="">
                                        <img src="{{asset('/site/img/evabeta_risk.png')}}"
                                             width="160" alt="EvaBeta" title="image"
                                             style="outline:none;text-decoration:none" class="CToWUd">
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <table cellpadding="30" cellspacing="0" border="0"
                                   style="width:100%;border-collapse:collapse">
                                <tbody>
                                <tr>
                                    <td id="m_-8830627616452772601message"
                                        style="border-collapse:collapse;vertical-align:top; padding: 0;">
                                        <table cellpadding="10" cellspacing="0" border="0"
                                               style="width:100%;border-collapse:collapse">
                                            <tbody>
                                            <tr>
                                                <td style="border-collapse:collapse;vertical-align:top">
                                                    <h1 id="m_-8830627616452772601description"
                                                        style="text-align: center;color:#000;line-height:26px;font-weight:900;margin:40px 60px">
                                                        Спасибо за заявку!
                                                    </h1>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table cellpadding="20" cellspacing="0" border="0"
                                               style="width:100%;border-collapse:collapse">
                                            <tbody>
                                            <tr>
                                                <td style="border-collapse:collapse;vertical-align:top">
                                                    <p id="m_-8830627616452772601description"
                                                       style="font-size:14px;color:#000;line-height:24px;font-weight:500;margin:0 25px">
                                                        Для перехода к заполнению анкеты просто нажмите на кнопку ниже
                                                    </p>
                                                    <p style="text-align: center;">
                                                            <span>
                                                                <a href="{{env('APP_URL_NAME').'/'.Illuminate\Support\Facades\App::getLocale().$url}}" style="display: block;margin:60px auto;text-decoration:none;font-weight:bold;background:#F9B233;color:#000;width:150px;padding: 10px 20px;font-size:22px;border-radius: 5px">Анкета</a>
                                                            </span>
                                                    </p>
                                                    <p style="color: #000; margin: 50px 30px 0;line-height: 20px;font-weight: 500;font-size: 14px;">
                                                        В случае наличия проблем с открытием анкеты, свяжитесь с вашим клиентским менеджером. При возникновении вопросов в ходе заполнения анкеты задавайте их, используя чат на сайте.
                                                    </p>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table cellpadding="20" cellspacing="0" border="0"
                                               style="width:100%;border-collapse:collapse;margin-top: 130px;">
                                            <tbody>
                                            <tr>
                                                <td style="border-collapse:collapse;vertical-align:top; padding: 50px 0 0 ;">
                                                    <p id="m_-8830627616452772601description"
                                                       style="font-size:16px;color:#555;line-height:26px;font-weight:300;margin:0 60px; height: auto">
                                                        <img src="{{asset('/site/img/human.png')}}" alt="" style="float: right">
                                                    </p>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <table id="m_-8830627616452772601header" cellpadding="20" cellspacing="0" border="0"
                                   style="width:100%!important;min-width: 100%!important; max-width: 100%!important;border-collapse:collapse;background: #eee">
                                <tbody>
                                <tr>
                                    <td style="color:#444;font-size:12px;font-weight:100;border-collapse:collapse;vertical-align:top;"></td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>

            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>
