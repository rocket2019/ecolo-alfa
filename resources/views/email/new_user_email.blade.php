<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>New User</title>
</head>
<body>
<div style="padding: 10px 0 0 30px; font-family: sans-serif; font-size: 16px">
    <p>Здравствуйте, {{$first_name}}!</p>
    <p>Ссылка на рабочую среду Банка: <a href="https://otp-ecolo.evabeta.com/ru/login">https://alfa-ecolo.evabeta.com/ru/login</a></p>
    <p>Логин: {{$username}} <br> Пароль: {{$password}}</p>
    <p>Также прикрепляем путеводитель по работе с системой.</p>
</div>
</body>
</html>
