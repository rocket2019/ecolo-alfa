<table>
    <tr style="font-weight: bold">
        <th >ID</th>
        <th colspan="12">@lang('labels.results_description')</th>
        <th colspan="2">@lang('labels.field_code')</th>
        <th colspan="3">@lang('labels.meaning')</th>
    </tr>
    @foreach($raws as $key=> $param)
            <tr>
                <td>{{$param['id']}}</td>
                <td colspan="12">{{$param['description']}}</td>
                <td colspan="2">{{$param['code']}}</td>
                <td colspan="3">{{$param['value']}}</td>

            </tr>
    @endforeach


</table>



