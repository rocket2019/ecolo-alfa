<table>
    <tr style="font-weight: bold">
        <th >N</th>
        <th colspan="7">@lang('labels.description_of_parameter')</th>
        <th colspan="2">@lang('labels.parameter_code')</th>
        <th colspan="3">@lang('labels.parameter_value')</th>
        <th colspan="7"> </th>
    </tr>@php($number = 0)
    @foreach($params as $key=> $param)

        @if(isset($param->sose_param) && $param->sose_param->visible == 1)
            <tr>
                <td>{{++$number}}</td>
                <td colspan="7">{{$param->sose_param->description}}</td>
                <td colspan="2">@if(isset($param->sose_param->parameter_code)){{$param->sose_param->parameter_code}}@endif</td>
                <td colspan="3">
                    @foreach($values as $value)

                    @if($value->parameter_id == $param->sose_param->id )
                      {{$value->value}}
                    @endif

                @endforeach
                </td>
                <td colspan="7">{{$param->sose_param->path}}</td>

            </tr>
        @endif
    @endforeach


</table>



