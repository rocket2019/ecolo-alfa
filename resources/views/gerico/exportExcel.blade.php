<table>
    <tr>
        <td></td>
        <td></td>
        @foreach($group_code as $key=>$value)
            <td>{{ $value->industry_group_code }}</td>
            <td></td>
        @endforeach
    </tr>
    <tr>
        <td></td>
        <td></td>
        @for($count = 0; $count < 360; $count++)
            @if($count%2 == 0)
                <td>Without Gerico 2018</td>
            @else
                <td>With Gerico 2018</td>
            @endif
        @endfor
    </tr>

    @foreach($region_name as $key=>$value)
        @php
         $count = 0;
        @endphp
        <tr>
            <td>{{ $value->area_code }}</td>
            <td>{{ $value->area_name }}</td>
            @for($loopCount = $count; $loopCount < $count + 360; $loopCount++)
                <td>{{ $gerico[$loopCount]->micro }}</td>
                <td>{{ $gerico[$loopCount]->small }}</td>
            @endfor
        </tr>
        @php
        $count += 360;
        @endphp
    @endforeach

</table>
