
<table>
    <tr >
        <th style="font-weight: bolder;">N </th>
        <th colspan="6" >@lang('labels.basic_parameters')</th>
        <th colspan="4">@lang('labels.parameter_value')</th>

    </tr>
    <tr>
        <td>1</td>
        <td colspan="6">@lang('labels.company_name')</td>
        <td colspan="4"> {{$company->company_name}}</td>
    </tr>
    <tr>
        <td>2</td>
        <td colspan="6">@lang('labels.credit_request_id')</td>
        <td colspan="4">{{$questionnaire->id}}</td>
    </tr>
    <tr>
        <td>3</td>
        <td colspan="6">@lang('labels.description_of_questionnaire')</td>
        <td colspan="4">{{$study_code}}</td>
    </tr>
    <tr>
        <td>4</td>
        <td colspan="6">@lang('labels.date_of_filling')</td>
        <td colspan="4">{{$questionnaire->created_at}}</td>
    </tr>
    <tr>
        <td>5</td>
        <td colspan="6">@lang('labels.id_number')</td>
        <td colspan="4">{{$company->INN_number}}</td>

    </tr>
    <tr>
        <td>6</td>
        <td colspan="6">@lang('labels.legal_form')</td>
        <td colspan="4">{{$company->legal_forms->name}}</td>
    </tr>
    <tr>
        <td>7</td>
        <td colspan="6">@lang('labels.address')</td>
        <td colspan="4">{{$company->location_address}}</td>
    </tr>
    <tr>
        <td>8</td>
        <td colspan="6">@lang('labels.main_activity')</td>
        <td colspan="4">{{$company->main_activity}}</td>
    </tr>
    <tr>
        <td>9</td>
        <td colspan="6">@lang('labels.sectoral_code')</td>
        <td colspan="4">{{$ateco->ateco_code}}</td>
    </tr>
    <tr>
        <td>10</td>
        <td colspan="6">@lang('labels.ateco_description')</td>
        <td colspan="4">{{$description}}</td>
    </tr>
    <tr>
        <td>11</td>
        <td colspan="6">@lang('labels.regional_coefficient')</td>
        <td colspan="4">{{$coef}}</td>
    </tr>
    <tr>
        <td>12</td>
        <td colspan="6">@lang('labels.main_region')</td>
        <td colspan="4">{{$region_code}}</td>
    </tr>
    <tr>
        <td>13</td>
        <td colspan="6">@lang('labels.bank_code')</td>
        <td colspan="4">{{$company->ID_company}}</td>
    </tr>




</table>


