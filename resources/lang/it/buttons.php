<?php

return [


    'login' => 'Accesso',
    'register' => 'Registrati',
    'reset_password' => 'Reimposta password',
    'exit' => 'Esci',
    'choose_language' => 'Scegli lingua',
    'search' => 'Cerca',
    'save' => 'Salva',
    'cancel' => 'Annulla',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'ready' => 'Pronto',
    'add_new_user' => 'Aggiungi nuovo utente',
    'edit_user' => 'Modifica Utente',
    'remove_user' => 'Rimuovi utente',
    'add_new_bank' => 'aggiungi una nuova banca',
    'remove_bank' => 'rimuovere la banca',
    'new_questionnaire' => 'Nuovo questionario',
    'edit' => 'Modifica',
    'to_bank' => 'Alla Banca',
    'add_new_check_for_this_study' => 'Aggiungi un nuovo controllo a questo questionario',
    'download_pdf'=>'Scarica PDF',
    'print'=>'PDF',
    'report' =>'Upload',
    'action'=>'cambiare',
    'send'=>'Inviare',
    'evaluate'=>'Valutare',
    'export'=>'Esporto',
    'questionnaire' => 'Questionario',
    'prev' => 'Previous',
    'next' => 'Next',


];
