<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'forgot_password' => 'Ha dimenticato la password?',
    'rating_evaluation'=>'Valutazione',
    'administration'=>'Amministrazione',
    'search_company'=>'Ricerca dell\'azienda',
    'new_company'=>'Nuova azienda',
    'print_questionnaire'=>'Stampa questionario',
    'new_evaluation'=>'Nuova valutazione',
    'data_import'=>'Importo dati',
    'checks'=>'Controlli',
    'user_management'=>'Gestione utenti',
    'logout' => 'Logout',
    'import_pdf' => 'Importare file pdf',
    'data_import_update' => 'Aggiorna l\'albero del questionario',

];
