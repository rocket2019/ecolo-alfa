<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'company' => 'Azienda',
    'client_name' => 'Nome Cliente',
    'client_email' => 'Email cliente',
    'company_name' => 'Nome azienda',
    'id_number' => 'Numero IVA',
    'legal_form' => 'Forma legale',
    'address' => 'Indirizzo',
    'role' => 'Ruolo',
    'first_name' => 'Nome',
    'last_name' => 'Cognome',
    'patronymic' => 'Patronimico',
    'municipal_education' => 'Formazione comunale',
    'selection_of_the_questionnaire' => 'Selezione questionario',
    'full_name' => 'Nome completo',
    'text' => 'Testo',
    'posted_by' => 'Pubblicato da',
    'group' => 'Gruppo',
    'saved_questionnaires' => 'Questionari salvati', 'available_results' => 'Risultati disponibili:',
    'pre_check_condition' => 'Controllo preliminare',
    'validation_formula' => 'Formula di verifica',
    'translations' => 'Traduzioni',
    'add_e_new_check' => 'Aggiungi un nuovo controllo',
    'edit_check' => 'Modifica controllo',
    'check_will_be_performed' => 'Se questa condizione è vera, il controllo verrà eseguito',
    'check_will_be_ok' => 'Se questa formula è vera, il controllo sarà ok',
    'error_translate' => 'Traduzioni di messaggi di errore',
    'translated_message_prompted' => 'Se il controllo fallisce, un messaggio tradotto verrà richiesto all\'utente',
    'russian_text' => 'Testo russo',
    'english_text' => 'Testo inglese',
    'italian_text' => 'Testo italiano',
    'code_ecolo' => 'Codice questionario ECOLO',
    'mail_bank_employee' => 'E-mail  responsabile della Banca',
    'subject' => 'Soggetto',
    'edit_questionnaire' => 'Modifica questionario Ecolo',
    'credit_request_id' => 'ID richiesta di credito',
    'ateco_code' => 'Specializzazione',
    'questionnaire_status' => 'Stato del questionario',
    'parameter_code'=>'Codice del parametro',
    'units_cases'=>'Casi di unità',
    'field_type'=>'Tipo di campo',
    'english'=>'Inglese',
    'italian'=>'Italiano',
    'russian'=>'Russo',
    'path_english'=>'Percorso  Inglese',
    'path_russian'=>'Percorso  Russo',
    'path_italian'=>'Percorso  Italiano',
    'visible'=>'Visible',
    'edit_parameter'=>'Modifica parametro',
    'main_activity' => 'Main Activity',
    'area_code_region'=>'Regione',
    'select_region'=>'Seleziona regione',
    'select_city'=>'Seleziona città',
    'area_code_city'=>'Prefisso città',

    'eval_edit' => 'Editing',
    'eval_complete' => 'Evaluation completed',
    'in_eval' => 'Send To Evaluation',

    //Excel
    'basic_parameters'=>'PARAMETRI DI BASE DELL\'AZIENDA',
    'parameter_value'=>'Valore del parametro',
    'description_of_questionnaire'=>'Descrizione del questionario ECOLO',
    'date_of_filling'=>'Data di compilazione di una richiesta di prestito',
    'main_region'=>'La regione principale dell\'azienda',
    'regional_coefficient'=>'Coefficiente regionale',
    'bank_code'=>'Codice bancario per la richiesta di un prestito',
    'description_of_parameter'=>'Descrizione del parametro dai  dati di input',
    'sectoral_code'=>'Sectoral Code',
    'ateco_description'=>'Descrizione del codice Ateco',
    'results_description'=>'Descrizione dei risultati',
    'field_code'=>'Codice del campo',
    'meaning'=>'Significato',




];
