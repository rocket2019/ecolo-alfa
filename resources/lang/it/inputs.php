<?php

return [


    'username' => 'Nome Utente',
    'password' => 'Password',
    'confirm_password' => 'Conferma Password',
    'remember_me' => 'Ricorda',
    'email' => 'E-mail',
    'throttle' => 'To   o many login attempts. Please try again in :seconds seconds.',
    'bank_name' => 'Nome della banca',
    'bank_manager_email' => 'Email del direttore di banca',
    'add_new_logo' => 'Aggiungi nuovo logo',
    'import_pdf' => 'Importare file pdf',
    'import_excel' => 'Importare EXCEL pdf',
    'choose_pdf_file' => 'Selezione file pdf',
    'choose_excel_file' => 'Selezione EXCEL pdf',
    'add_new_phone_number'=>'Aggiungi un nuovo numero di telefono',
    'choose_language'=>'Sceglii la lingua',




];
