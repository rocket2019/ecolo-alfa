<?php

return [


    'login' => 'Логин',
    'register' => 'Регистрация',
    'reset_password' => 'Сбросить пароль',
    'exit' => 'Выход',
    'choose_language' => 'Выберите язык',
    'search' => 'Поиск',
    'save' => 'Сохранить',
    'cancel' => 'Отмена',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'ready' => 'Готово',
    'add_new_user' => 'Добавить нового пользователя',
    'edit_user' => 'Редактирование данных пользователя',
    'remove_user' => 'Удалить пользователя',
    'add_new_bank' => 'Добавить новый банк',
    'remove_bank' => 'Удалить банк',
    'new_questionnaire' => 'Новая анкета',
    'edit' => 'Редактировать',
    'to_bank' => 'В Банк',
    'add_new_check_for_this_study' => 'Добавить новый контроль к этой анкете',
    'download_pdf' => 'Скачать PDF',
    'print' => 'PDF',
    'report' => 'Загрузить',
    'action' => 'менять',
    'send' => 'Отправить',
    'evaluate' => 'Оценивать',
    'export'=>'Экспорт',
    'questionnaire'=>'Анкета',
    'prev' => 'Назад',
    'next' => 'Далее',

];
