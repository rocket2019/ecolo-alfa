<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'company' => 'Компания',

    'client_name' => 'Имя Клиента',
    'client_email' => 'Эл.адрес Клиента',
    'company_name' => 'Клиент',
    'id_number' => 'Номер ИНН',
    'legal_form' => 'Юридическая форма',
    'address' => 'Адрес местоположения',
    'role' => 'Роль',
    'first_name' => 'Имя',
    'last_name' => 'Фамилия',
    'patronymic' => 'Отчество',
    'municipal_education' => 'Муниципальное образование',
    'selection_of_the_questionnaire' => 'Подбор анкеты',
    'full_name' => 'ФИО',
    'text' => 'Текст',
    'posted_by' => 'Разместил',
    'group' => 'Блок',
    'saved_questionnaires' => 'Сохраненные анкеты', 'available_results' => 'Доступные результаты',
    'pre_check_condition' => 'Предварительный контроль',
    'validation_formula' => 'Проверочная формула',
    'translations' => 'Переводы',
    'add_e_new_check' => 'Добавить новый контроль',
    'edit_check' => 'Редактирование контроля',
    'check_will_be_performed' => 'Если это условие даст значение правды, то будет проверено и следующее условие',
    'check_will_be_ok' => 'Если эта формула даст значение правды, то контроль будет считаться выполненным',
    'error_translate' => 'Перевод сообщений о ошибках',
    'translated_message_prompted' => 'Если вышеприведенные условия не будут удовлетворены, то пользователю будет показано сообщение на его языке',
    'russian_text' => 'Русский текст',
    'english_text' => 'Английский текст',
    'italian_text' => 'Итальянский текст',
    'code_ecolo' => 'КОД АНКЕТЫ ECOLO',
    'mail_bank_employee' => 'E-mail сотрудника Банка',
    'subject' => 'Регион/область',
    'city' => 'Город/район',
    'edit_questionnaire' => 'Редактировать анкету Ecolo',
    'credit_request_id' => 'Номер кредитной заявки',
    'ateco_code' => 'Специализация',
    'questionnaire_status' => 'Статус анкеты',
    'parameter_code' => 'Код параметра',
    'units_cases' => 'Единица измерения',
    'field_type' => 'Тип поля',
    'english' => 'Английский',
    'italian' => 'Итальянский',
    'russian' => 'Русский',
    'path_english' => 'Подзаголовок английиский',
    'path_russian' => 'Подзаголовок русский',
    'path_italian' => 'Подзаголовок итальянский',
    'visible' => 'Видимый',
    'edit_parameter' => 'Редактирование параметра',
    'main_activity' => 'Вид деятельности',
    'area_code_region'=>'Регион',
    'select_region'=>'Выберите регион/область',
    'select_city'=>'Выберите город/район',
    'area_code_city'=>'Муниципальное образование',

    'eval_edit' => 'Редактируется',
    'eval_complete' => 'Оценка завершена',
    'in_eval' => 'Отправлена на оценку',

    //Excel
    'basic_parameters'=>'ОСНОВНЫЕ ПАРАМЕТРЫ КОМПАНИИ',
    'parameter_value'=>'Значение параметра',
    'description_of_questionnaire'=>'Описание анкеты ЭКОЛО',
    'date_of_filling'=>'Дата заполнения запроса на кредит',
    'main_region'=>'Основной регион работы компании',
    'regional_coefficient'=>'Региональный коэффициент',
    'bank_code'=>'Код банка, которому подается заявка на кредит',
    'description_of_parameter'=>'Описание параметра из входных данных',
    'sectoral_code'=>'Основной ОКВЕД Код',
    'ateco_description'=>'Описание кода Атеко',
    'results_description'=>'Описание результатов',
    'field_code'=>'Код поля',
    'meaning'=>'Значение',

];
