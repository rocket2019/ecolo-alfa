<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'forgot_password' => 'Забыли пароль?',
    'rating_evaluation'=>'Оценка рейтинга',
    'administration'=>'Администрация',
    'search_company'=>'Поиск компании',
    'new_company'=>'Новая Компания',
    'print_questionnaire'=>'Распечатать анкету',
    'new_evaluation'=>'Новая оценка',
    'data_import'=>'Импорт данных',
    'checks'=>'Проверки',
    'user_management'=>'Права пользователей',
    'logout' => 'Выйти из системы',
    'import_pdf' => 'Импорт PDF-файлов',
    'data_import_update' => 'Обновить дерево анкет',

];
