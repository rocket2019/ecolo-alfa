<?php

return [



    'username' => 'Имя пользователя',
    'password' => 'Пароль',
    'confirm_password' => 'Подтвердите Пароль',
    'remember_me' => 'Запомнить',
    'email' => 'Электронная почта',
    'city' => 'Город',
    'department' => 'Отдел',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'bank_name' => 'Имя банка',
    'bank_manager_email' => 'Эл.адрес менеджера банка',
    'add_new_logo'=>'Добавить новый логотип',
    'import_pdf'=>'Импортировать pdf файл',
    'import_excel'=>'Импортировать EXCEL файл',
    'choose_pdf_file'=>'Выберите  pdf файл',
    'choose_excel_file'=>'Выберите  EXCEL файл',
    'add_new_phone_number'=>'Добавить новый номер телефона',
    'choose_language'=>'Выберите язык',


];
