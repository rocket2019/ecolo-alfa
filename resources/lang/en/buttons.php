<?php

return [


    'login' => 'Login',
    'register' => 'Register',
    'reset_password' => 'Reset Password',
    'exit' => 'Exit',
    'choose_language' => 'Choose language',
    'search' => 'Search',
    'save' => 'Save',
    'cancel' => 'Cancel',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'add_new_user' => 'Add new user',
    'edit_user' => 'Edit user',
    'remove_user' => 'Remove user',
    'ready' => 'Ready',
    'add_new_bank' => 'Add new bank',
    'remove_bank' => 'Remove bank',
    'new_questionnaire' => 'New questionnaire',
    'edit' => 'Edit',
    'to_bank'=>'To Bank',
    'add_new_check_for_this_study'=>'Add new check for this study',
    'download_pdf' =>'Download PDF',
    'print' =>'PDF',
    'report' =>'Upload',
    'action'=>'change',
    'send'=>'Send',
    'evaluate'=>'Evaluate',
    'export'=>'Export',
    'questionnaire' =>'Questionnaire',
    'prev' => 'Previous',
    'next' => 'Next',






];
