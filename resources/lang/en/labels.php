<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'company' => 'Company',
    'client_name' => 'Client Name',
    'client_email' => 'Client Email',
    'company_name' => 'Company Name',
    'id_number' => 'VAT identification number',
    'legal_form' => 'Legal Form',
    'address' => 'Address',
    'role' => 'Role',
    'first_name' => 'First name',
    'last_name' => 'Last name',
    'patronymic' => 'Patronymic',
    'municipal_education' => 'Municipal Education',
    'selection_of_the_questionnaire' => 'Selection of the Questionnaire',
    'full_name' => 'Full name',
    'text'=>'Text',
    'posted_by'=>'Posted by',
    'group'=>'Group',
    'saved_questionnaires'=>'Saved questionnaires','available_results'=>'Available results',
    'pre_check_condition'=>'Pre-check condition',
    'validation_formula'=>'Validation formula',
    'translations'=>'Translations',
    'add_e_new_check'=>'Add a new check',
    'edit_check'=>'Edit check',
    'check_will_be_performed'=>'If this condition is true, the check will be performed',
    'check_will_be_ok'=>'If this formula is true, the check will be ok',
    'error_translate'=>'Error message translations',
    'translated_message_prompted'=>'If the check fails, a translated message will be prompted to the user',
    'russian_text'=>'Russian text',
    'english_text'=>'English text',
    'italian_text'=>'Italian text',
    'code_ecolo'=>'ECOLO questionnaire code',
    'mail_bank_employee'=>'E-mail  employee of the Bank',
    'subject' => 'Subject',
    'edit_questionnaire'=>'Edit Ecolo questionnaire',
    'credit_request_id'=>'Credit Request ID',
    'ateco_code'=>'Ateco code',
    'questionnaire_status'=>'Questionnaire Status',

    'parameter_code'=>'Parameter Code',
    'units_cases'=>'Units Cases',
    'field_type'=>'Field Type',
    'english'=>'English',
    'italian'=>'Italian',
    'russian'=>'Russian',
    'path_english'=>'Path English',
    'path_russian'=>'Path Russian',
    'path_italian'=>'Path Italian',
    'visible'=>'Visible',
    'edit_parameter'=>'Edit Parameter',
    'main_activity' => 'Main Activity',
    'area_code_region'=>'Region',
    'select_region'=>'Select a region',
    'select_city'=>'Select a city',
    'area_code_city'=>'Area code city',

    'eval_edit' => 'Editing',
    'eval_complete' => 'Evaluation completed',
    'in_eval' => 'Send To Evaluation',

    //Excel
    'basic_parameters'=>'BASIC PARAMETERS OF THE COMPANY',
    'parameter_value'=>'Parameter value',
    'description_of_questionnaire'=>'Description of the ECOLO  questionnaire',
    'date_of_filling'=>'Date of filling out a loan request',
    'main_region'=>'The main region of the company',
    'regional_coefficient'=>'Regional coefficient',
    'bank_code'=>'Bank code for applying for a loan',
    'description_of_parameter'=>'Description of the parameter from the input data',
    'sectoral_code'=>'Sectoral Code',
    'ateco_description'=>'Ateco Code Description',
    'results_description'=>'Results\'s description',
    'field_code'=>'Field code',
    'meaning'=>'Meaning',




];
