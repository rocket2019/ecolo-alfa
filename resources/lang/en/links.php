<?php

return [



    'forgot_password' => 'Forgot your password ?',
    'rating_evaluation'=>'Rating Evaluation',
    'administration'=>'Administration',
    'search_company'=>'Search Company',
    'new_company'=>'New Company',
    'print_questionnaire'=>'Print Questionnaire',
    'new_evaluation'=>'New Evaluation',
    'data_import'=>'Data Import',
    'checks'=>'Checks',
    'user_management'=>'User Management',
    'logout'=>'Logout',

    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'import_pdf' => 'Import pdf file',
    'data_import_update' => 'Update questionaire tree',


];
