<?php

return [



    'username' => 'Username',
    'password' => 'Password',
    'confirm_password' => 'Confirm Password',
    'remember_me' => 'Remember me',
    'email' => 'Email',
    'city' => 'City',
    'department' => 'Departments',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'bank_name' => 'Bank name',
    'bank_manager_email' => 'Bank manager email',
    'add_new_logo'=>'Add new logo',
    'import_pdf'=>'Import pdf file',
    'choose_pdf_file'=>'Choose pdf file',
    'import_excel'=>'Import EXCEL file',
    'choose_excel_file'=>'Choose EXCEL file',
    'add_new_phone_number'=>'Add new phone number',
    'choose_language'=>'choose the language',

];
