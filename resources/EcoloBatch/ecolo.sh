#!/bin/bash

# ecolo.sh <sose_input_file> <sose_console_output>
# Se va a buon fine, restituisce in stdout il file di output prodotta da Gerico

java -jar EcoloBatch.jar finanze.IDTE.studi2013.invisibile.avvio.EcoloBatch -elabora -memorizza $1 &> $2

OUTPUT_FILENAME=archivi/$(cat archivi/studi.txt | cut -d "*" -f1).txt

if [ -f $OUTPUT_FILENAME ] 
then 
	cat $OUTPUT_FILENAME
fi
#rm -Rf archivi archiviperunico sistema stampe
