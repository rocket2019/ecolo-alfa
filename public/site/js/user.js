$(document).ready(function () {


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).on('click', '.update_user', function () {
        // $('#loading_layer').css('display','block');

        let form = $('#new_user_form').serialize();
        let id = $('input[name = user_id]').val();

        $.ajax({
            url: lang + '/admin/edit-user/' + id,
            data: form,
            type: "POST",

            success: function () {
                window.location.href = lang + '/admin/user-management';
            },

            error: function (errors) {
                // $('#loading_layer').css('display','none');
                let error = errors.responseText;
                error = JSON.parse(error);
                $.each(error.body, function (key, value) {
                    $('.' + 'error-' + key).text(value);
                });


            },
        })
    });
    $(document).on('click', '.save_user', function () {
        // $('#loading_layer').css('display','block');

        let form = $('#new_user_form').serialize();
        $.ajax({
            url: lang + '/admin/add-new-user',
            data: form,
            type: "POST",

            success: function () {
                window.location.href = lang + '/admin/user-management';
            },

            error: function (errors) {
                // $('#loading_layer').css('display','none');
                let error = errors.responseText;
                error = JSON.parse(error);
                $.each(error.body, function (key, value) {
                    $('.' + 'error-' + key).text(value);
                });


            },
        })
    });


    $(document).on('click', '.delete', function (e) {
        e.preventDefault();
        let id = $(this).attr('data-id');

        $('#delete').modal()
        $('#delete').find('.delete-user').attr('data-id', id);
    })

    $(document).on('click', '.delete-user', function () {
        let id = $(this).data('id');
        $.ajax({
            url: lang + '/admin/delete-user/' + id,
            type: 'GET',
            success: function (response) {
                window.location.reload();
            },
            error: function () {
            }
        });

    })


});
