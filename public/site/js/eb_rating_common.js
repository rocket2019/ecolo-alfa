
function eb_add_actions() {
    $('.reset_search_form').live('click', function() {
        var id_hidden_field = $(this).attr('href');
        var id_search_field = id_hidden_field + '_search_field';
        $(id_hidden_field).val('');
        $(id_search_field).val('');
        $('#btn_info_company').popover('destroy');
        return false;
    });

    $('#company_search_idCompany').change(function() {
        $('#btn_info_company').popover('destroy');
        var val = $('#company_search_idCompany').val();
        if (val) {
            show_company_more_data(val);
        }
    });

    $(".tip").tooltip({});
    $(".tip_left").tooltip({
        position: {
            my		: 'right center',
            at		: 'left center',
            viewport: $(window)
        }
    });

    // $('button[type=submit]').click(function() {
    //     var submit_button = $(this);
    //     // submit_button.addClass('disabled');
    //     // submit_button.find('i').addClass('icon-spinner icon-spin');
    // });

//    $('form').submit(function() {
//        var form = $(this);
//        var submit_button = $(this).find('button[type=submit]');
//        submit_button.addClass('disabled');
//        submit_button.find('i').addClass('icon-spinner icon-spin');
//        /*
//        var loader = $(this).find('span.loader');
//        loader.show();
//        */
//        return true;
//    });

    /* enable submit */
    $(this).find('button[type=submit]').removeClass('disabled');
/*
    $('.menu_pop_over').live('hover', function() {
        var idCompany = $(this).attr('data-value');
        if (idCompany) {
            if (!companyCache[idCompany]) {
                $.get(Routing.generate('evabeta_rating_company_fetch', { idCompany: idCompany, _format:'json' }), function (data) {
                    if (data) {
                        companyCache[idCompany] = data;
                        var title = data.company_name + ' data:';
                        var content = '';
                        if (data.default_list) {
                            content += '<b>Deafult year:</b> ' + parseInt(data.default_list.default_date);
                        }
                        if (data.balances.length > 0) {
                            content += '<b>Available balances:</b><br>'
                            $.each(data.balances, function(bal_index, bal_value) {
                                content += parseInt(bal_value.balance_year) + ' ';
                            });
                        } else content += '<i>No balances</i>';
                        $('.menu_pop_over').popover({
                            title:      title,
                            content:    content,
                            placement:  'right',
                            trigger:    'hover'
                        });
                    } else {
                        console.err('data not found idCompany=' + idCompany);
                    }
                });
            } else {
                var data = companyCache[idCompany];
                var title = data.company_name + ' data:';
                var content = '';
                if (data.default_list) {
                    content += '<b>Deafult year:</b> ' + parseInt(data.default_list.default_date);
                }
                if (data.balances.length > 0) {
                    content += '<b>Available balances:</b><br>'
                    $.each(data.balances, function(bal_index, bal_value) {
                        content += parseInt(bal_value.balance_year) + ' ';
                    });
                } else content += '<i>No balances</i>';
                $('.menu_pop_over').popover({
                    title:      title,
                    content:    content,
                    placement:  'right',
                    trigger:    'hover'
                });
            }
        }
    });
*/
}

function show_company_more_data(item_id) {
    var idCompany = item_id ? item_id : $('#company_search_idCompany').val();
    if (idCompany) {
        $('#btn_info_company i').addClass('loader-icon');
        $.get(Routing.generate('evabeta_rating_company_fetch', { idCompany: idCompany, _format:'json' }), function (data) {
            if (data) {
                var title = data.company_name + ' ' + Translator.get('details');
                var content = '';
                if (data.default_list) {
                    content += '<b>'+Translator.get('Year of default')+':</b> ' + parseInt(data.default_list.default_date) + '<br><br>';
                }
                if (data.balances.length > 0) {
                    content += '<b>'+Translator.get('Available balances')+':</b><br>'
                        $.each(data.balances, function(bal_index, bal_value) {
                        content += parseInt(bal_value.balance_year) + ' ';
                    });
                } else content += '<i>'+Translator.get('No balances')+'</i>';
                content += '<br/><a href="' +
                    Routing.generate('evabeta_rating_company_show', { idCompany: idCompany })
                    + '" class="pull-right">'+Translator.get('more info')+'...</a><br/>';
                $('#btn_info_company').popover({
                    title:      title,
                    content:    content,
                    placement:  'right',
                    trigger:    'click'
                });
                $('#btn_info_company').popover('show');
            } else {
                console.err('data not found idCompany=' + idCompany);
            }
            $('#btn_info_company i').removeClass('loader-icon');
        });
    }
}


$(document).ready(function() {
    $('#btn_info_company').popover('destroy');

    //alert(Translator.get('Add new company', { "foo" : "bar" }));

    eb_add_actions();

    show_company_more_data();

    $('input[type=search].company-search-box').typeahead({
        source: function (query, process) {
            this.saved_items = [];
            var that = this;
            return $.get(Routing.generate('evabeta_rating_company_search', { query: query, _format:'json' }), function (data) {
                //var items = $.map(data.options, function(i) { return i.companyName });
                that.saved_items = data.options;
                if (!data.options.length) {
                    return that.shown ? that.hide() : that
                }

                html_items = $(data.options).map(function (i, item) {
                    var label = item.companyName + ' (' + item.innNumber +')';
                    i = $(that.options.item).attr('data-value', item.idCompany);
                    i.find('a')
                        .html(that.highlighter(label))
                        .addClass('menu_pop_over')
                        .attr('data-original-title', item.companyName + ' data:')
                        .attr('data-value', item.idCompany);
                    return i[0];
                })

                html_items.first().addClass('active');
                that.$menu.html(html_items);
                that.show();
                $('#btn_info_company').popover('destroy');
                // return that.render(items.slice(0, that.options.items)).show()
                //return process(data.options);
            });
        }
    ,   updater: function (item_id) {
            var hidden_field = $(this.$element.attr('href'));
            var selectedItem = $(this.saved_items).filter(function() {
                return this.idCompany == item_id;
            });
            selectedItem = selectedItem[0];
            hidden_field.val(item_id).trigger('change');
            return selectedItem.companyName + ' (' + selectedItem.innNumber +')';
        }
    ,   matcher: function() { return true }
    ,   sorter: function (items) { return items }
    ,   highlighter: function (item) { return item }
//    ,   minLength: 2
    });
});