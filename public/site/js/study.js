$(document).ready(function () {

    $('#study_checks').select2();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.btn-inverse').click(function () {
        if ($('#pdf_file').val()) {
            $('#loading_layer').css('display', 'block');
        }
    });


    $(document).on('click', '.add_study_check', function () {
        let form = $('.study_check').serialize();
        let study_id = $('input[name =study_id ]').val()
        $.ajax({
            url: lang + '/admin/add-study-check/' + study_id,
            type: 'POST',
            data: form,
            success: function (response) {
                window.location.href = lang + '/admin/study-checks/' + response;
            },
            error: function (errors) {
                let error = errors.responseText;
                error = JSON.parse(error);
                $.each(error.body, function (key, value) {
                    $('.' + 'error-' + key).text(value);
                });

            }
        });

    })

    $(document).on('click', '.edit_study_check', function () {
        let form = $('.study_check').serialize();
        let study_id = $('input[name =study_id ]').val()
        let check_id = $('input[name =check_id ]').val()

        $.ajax({
            url: lang + '/admin/' + study_id + '/edit-study-check/' + check_id,
            type: 'POST',
            data: form,
            success: function (response) {
                window.location.href = lang + '/admin/study-checks/' + response;
            },
            error: function (errors) {
                let error = errors.responseText;
                error = JSON.parse(error);
                $.each(error.body, function (key, value) {
                    $('.' + 'error-' + key).text(value);
                });

            }
        });

    })

    $(document).on('click', '.delete_study_check', function (e) {
        e.preventDefault();
        let id = $(this).attr('data-id');

        $('#delete_study_check').modal()
        $('#delete_study_check').find('.delete-study-check').attr('data-id', id);
    })

    $(document).on('click', '.delete-study-check', function () {
        let check_id = $(this).data('id');
        let study_id = $('input[name = study_id]').val();

        $.ajax({
            url: lang + '/admin/delete-study-check/' + study_id + '/' + check_id,
            type: 'GET',
            success: function () {
                window.location.reload();
            },
            error: function () {
            }
        });

    })

    // $('.get-all-data').click(function(){
    //     $.ajax({
    //         url: lang + '/admin/import-excel/get-data',
    //         type: 'GET',
    //         success: function (response) {
    //         }
    //     })
    // })


});
