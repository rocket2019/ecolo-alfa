$(document).ready(function () {
    if (typeof(lang) == 'undefined') {
        var lang = window.location.pathname;
        lang.indexOf(1);

        lang.toLowerCase();

        lang = lang.split("/")[1];

        lang = '/' + lang;
    }
    // *******Get all inputs value*******
    let allInputs = $('.delimetr_number');
    let oldInputsValues = {};
    $.each(allInputs, function (key, value) {
        let code = $(value).attr('name');
        if ($(value).attr('type') !== 'checkbox'){
            let oldVal = $(value).val();
            oldInputsValues[code] = oldVal;
        }
    })
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // First circle text of block should be displayed
    $(document).find('.circle-text').first().css('display', 'block');
    $(document).find('.A.nav-item.circle').css('background-color', '#F9B232');

    let finalBtn = $(document).find('.btn_prev');
    finalBtn.removeAttr('onclick');
    finalBtn.css({
        'visibility': 'hidden',
    });

    let editedInputs = {};
    $('.delimetr_number').keyup(function () {
        if($(this).attr('type') !== 'checkbox'){
            let code = $(this).attr('name');
            let newValue = $(this).val();
            editedInputs[code] = newValue;
        }
    });

    $('.prf_close').click(function () {
        if (parseInt($(this).data('client')) > 0)
        {
            $('.delete-questionnaire-client').trigger('click');
            return false;
        }

        window.location.href = lang_client + '/manager';
    });


    $(document).on('click', '.delete-questionnaire-client', function () {

        if (!$.isEmptyObject(editedInputs)){
            for (let elem in editedInputs){
                if(oldInputsValues[elem] !== editedInputs[elem]){
                    editedInputs[elem] = oldInputsValues[elem];
                }
            }
        }
        let client_id = $("input[name='client_id']").val();
        let company_id = $("input[name='company_id']").val();
        let questionnaire_id = $("input[name='questionnaire_id']").val();
        if (client_id){
            for (let key in editedInputs){
                $.ajax({
                    url: lang_client + '/company/fill-questionnaire-value/' + company_id + '/questionnaire/' + questionnaire_id,
                    type: 'POST',
                    data: {
                        current_parameter_id: key,
                        current_parameter_value: editedInputs[key],
                        client_id: client_id
                    }
                });
            }
            window.location.href = lang_client + '/manager/';
        }




        // let client_id = $(this).data('id');
        // let questionnaire = $(this).data('questionnaire');
        //
        //
        // $.ajax({
        //     url: lang_client + '/client/delete-questionnaire',
        //     type: "POST",
        //     data: {
        //         client_id: client_id,
        //         questionnaire: questionnaire
        //     },
        //     success: function (response) {
        //
        //         window.location.href = lang_client + '/manager';
        //     }
        // });
    })

    function createCompanyOffice(){

        $('#loading_layer').css('display','block');
        let office = false;
        if ($(this).hasClass('office')) {
            office = true;
        }
        let form = $('#client-registration').serialize();
        let ateco_id = $('#selected').data('id');
        form += form + "&ateco_id=" + ateco_id;
        form += form + "&checkBtn=" + office;
        if(!form.includes('company_unit')){
            form += form + "&company_unit=1";
        }

        $.ajax({
            url: window.location.pathname + '?debug=t',
            type: 'POST',
            data: form,

            success: function (response) {
                $('#loading_layer').css('display','none');
                $('#sendLinkCompleted').modal('show');
                // window.location.href = lang_client + '/manager';
            },
            error: function (response) {
                $('#loading_layer').css('display','none');
                let errors = response.responseJSON.body;
                $("input").removeClass('error-input');
                $(document).find('#client-registration select').css('border', '1px solid #47b39c');
                $(document).find('#client-registration input').css('border', '1px solid #47b39c');
                $("select").removeClass('error-select');
                $('.form_field .form_field_inner .input-block span.error').css('display','none');
                $.each(errors, function (key, value) {
                    if (!$("textarea[name='"+key+"']").val() || !$("input[name='"+key+"']").val() || !$("select[name='"+key+"']").val()) {
                        $("textarea[name='"+key+"']").addClass('error-input');
                        $("input[name='"+key+"']").addClass('error-input');
                        $("select[name='"+key+"']").addClass('error-select');
                        $('.error_' + key).css('display','block');
                    }
                    if (key == "email_manager" && $("input[name='"+key+"']").val()) {
                        $('.error_' + key).css('display','none');
                        $('.error_email_manager_not_valid').css('display','block');
                    }
                });

                setTimeout(() => {
                    $('.error-select option:not(:first-child)').css('color','black');
                },10);
            }
        })
    };


    function createCompanyLater(){
        $('#loading_layer').css('display','block');
        let office = false;
        if ($(this).hasClass('office')) {
            office = true;
        }
        let form = $('#client-registration').serialize();
        let ateco_id = $('#selected').data('id');
        form += form + "&ateco_id=" + ateco_id;
        form += form + "&checkBtn=" + office;
        if(!form.includes('company_unit')){
            form += form + "&company_unit=1";
        }

        $.ajax({
            url: window.location.pathname + '?debug=t',
            type: 'POST',
            data: form,

            success: function (response) {
                $('#loading_layer').css('display','none');
                window.location.href = lang_client + '/client/questionnaire/view/' + response.body.client_id + '/' + response.body.questionnaire_id;

            },

            error: function (response) {
                $('#loading_layer').css('display','none');
                let errors = response.responseJSON.body;
                $("input").removeClass('error-input');
                $(document).find('#client-registration select').css('border', '1px solid #47b39c');
                $(document).find('#client-registration input').css('border', '1px solid #47b39c');
                $("select").removeClass('error-select');
                $('.form_field .form_field_inner .input-block span.error').css('display','none');
                $.each(errors, function (key, value) {
                    if (!$("textarea[name='"+key+"']").val() || !$("input[name='"+key+"']").val() || !$("select[name='"+key+"']").val()) {
                        $("textarea[name='"+key+"']").addClass('error-input');
                        $("input[name='"+key+"']").addClass('error-input');
                        $("select[name='"+key+"']").addClass('error-select');
                        $('.error_' + key).css('display','block');
                    }
                    if (key == "email_manager" && $("input[name='"+key+"']").val()) {
                        $('.error_' + key).css('display','none');
                        $('.error_email_manager_not_valid').css('display','block');
                    }
                });
                setTimeout(() => {
                    $('.error-select option:not(:first-child)').css('color','black');
                },10);
            }
        })
    };

    $(".office").on('click', createCompanyLater);
    $(".later").on('click', createCompanyOffice);

    // $('.office, .later').click(function () {
    //
    //     $('#loading_layer').css('display','block');
    //     let office = false;
    //     if ($(this).hasClass('office')) {
    //         office = true;
    //     }
    //     let form = $('#client-registration').serialize();
    //     let ateco_id = $('#selected').data('id');
    //     form += form + "&ateco_id=" + ateco_id;
    //     form += form + "&checkBtn=" + office;
    //
    //     $.ajax({
    //         url: window.location.pathname + '?debug=t',
    //         type: 'POST',
    //         data: form,
    //
    //         success: function (response) {
    //             $('#loading_layer').css('display','none');
    //             if (office) {
    //                 window.location.href = lang_client + '/client/questionnaire/view/' + response.body.client_id + '/' + response.body.questionnaire_id;
    //             }
    //             else{
    //                 window.location.href = lang_client + '/manager';
    //             }
    //
    //         },
    //
    //         error: function (response) {
    //             $('#loading_layer').css('display','none');
    //             let errors = response.responseJSON.body;
    //             $.each(errors, function (key, value) {
    //                 $('.error_' + key).text(value[0]);
    //             })
    //         }
    //     })
    // });

    $('.preview').click(function () {
        $('.modal-content-preview').html('');
        $('#preview-questionnaire').modal('hide');
        let code_id = $('#code_id').val();
        let company_id = $('#company_id').val();
        let ateco_id = $('#selected').data('id');
        let company_unit = $('#company_unit option:selected').val();
        $.ajax({
            url: lang_client + '/client/preview/' + ateco_id,
            type: "GET",
            success: function (response) {
                $('.modal-content-preview').html(response.body);
                $('#preview-questionnaire').modal('show');
            },

            error: function () {

            }
        });
    });


    $('.ateco_id_function').on('click', function(){
        $(document).find('.preview').css('display', 'block');
        if(($(this).find('span.key-for-search').first().text().slice(-5)) === 'VG69U'){
            $('.num').attr('disabled', true);
        }
        else{
            $('.num').attr('disabled', false);
        }
    });

    $('.save_questionnaire').click(function () {
        $('#loading_layer').css('display','block');
        $(document).find('td.vertical-bottom').removeClass('vertical-bottom');
        $(document).find('.delimetr_number').css('border', '1px solid #47b39c');
        $(document).find('.delimetr_number.importantRuleBorder').removeClass('importantRuleBorder');
        $(document).find('td[data-target]').css('color', '#212529');
        $('.validation-error-span').text('');
        let action = $(this).data('action');
        let form = $('#questionnaire-form').serialize();
        let client_id = $("input[name='client_id']").val();
        let questionnaire_id = $("input[name='questionnaire_id']").val();
        let codes = {};
        $('input').each(function () {
            if (typeof $(this).data('code') !== 'undefined' && typeof $(this).data('id') !== 'undefined') {
                codes[$(this).data('code')] = $(this).data('id');
            }
        });

        codes = JSON.stringify(codes);

        form += '&codes=' + codes;
        form += '&action=' + action;

        let editForm = $('.client-questionnaire').serialize();
        if (action == "save") {
            $.ajax({
                url: lang_client + '/client/edit-questionnaire/' + client_id + '/questionnaire/' + questionnaire_id,
                type: 'POST',
                data: editForm,
                success: function (response) {
                }
            })
        }

        $.ajax({
            url: lang_client + '/client/fill-questionnaire/' + client_id + '/questionnaire/' + questionnaire_id,
            type: 'POST',
            data: form,

            beforeSend: function(){
                $("#final-validation").modal('hide');
            },
            success: function (response) {
                let prevError = $(document).find('.nav-item.error');
                prevError.map(function(key, value){
                    if(!$(document).find('.nav-link-' + value.className.slice(0, 1)).hasClass('active')){
                        $(document).find('.block-text-' + value.className.slice(0, 1)).css('color', '#818087');
                    }
                    else{
                        $(document).find('.nav-item.' + value.className.slice(0, 1)).css('background-color', '#F9B232');
                    }
                })
                let allBallons = $(".nav-item");
                $.each(allBallons, function(key, value){
                    if(value.className.indexOf("error") !== -1){
                        value.classList.toggle("error");
                    }
                })
                $.each(response["body"], function (key,value) {

                    let blockName = key.substring(0, 1);
                    $(".block-text-" + blockName).css('color', '#9D0C00');

                    if($('.nav-item.' + blockName + '>a').hasClass('active')){
                        $('.block-text-' + blockName).css('color', '#000');
                    }
                    $("." + blockName).addClass('error');
                    $(document).find('.validation-error-' + key).css('display', 'block').text(value);
                    $(document).find('td.' + key).addClass('vertical-bottom');
                    $(document).find('input[data-code="'+key+'"]').addClass('importantRuleBorder');
                })

                $('#loading_layer').css('display', 'none');

                if (action == 'evaluate') {
                  /*  response.output = false;*/
                    if (response.output) {
                        let userInfo = {
                            'lang_client': lang_client,
                            'client_id': client_id,
                            'questionnaire_id': questionnaire_id
                        };
                        userInfo = JSON.stringify(userInfo);
                        $.ajax({
                            url: lang_client + '/client/fill-questionnaire/' + client_id + '/questionnaire/' + questionnaire_id + '/send-mail',
                            type: 'POST',
                            data: userInfo,
                        })
                        $('#evaluate-div').modal('show');
                        return false;
                    } else {
                        // $(".alert-danger").css('display', 'block');
                        // $(".alert-danger").text(response.body);
                        $("#validation-failed-notify").modal('show');
                        var errorBlocks = [];
                        $.each(response.body, function (key, value) {
                            if (value === null) {
                                return true;
                            }

                            $(document).find('.validation-error-' + key).css('display', 'block').text(value);
                            $(document).find('td.' + key).addClass('vertical-bottom');
                            $(document).find('input[data-code="'+key+'"]').css('border', '2px solid #FF5244');

                            $(document).find('td[data-target="'+key+'"]').css('color', '#FF5244');
                            $(document).find('[data-code="' + key + '"]').css('border', '2px solid #FF5244');
                            if (key) {
                                errorBlocks.push(key.charAt(0));
                                $('#error-tab' + key.charAt(0)).css('color', 'red');
                            } else {

                            }

                        });
                        if (errorBlocks.length > 0)
                        {
                            var $container = $("html,body");
                            var $scrollTo = $('.block'+errorBlocks[0]);

                            $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top + $container.scrollTop(), scrollLeft: 0},500);
                        }
                    }
                }
            },
            error: function (errors) {
                $('#loading_layer').css('display', 'none');
                // let error = errors.responseText;
                // error = JSON.parse(error)
                // $("#final-validation-text").html(error.body);
                // $('#final-validation').modal('show');

                $('#evaluate-div').modal('show');
                $('html,body').animate({scrollTop: 0}, 'fast');
            }
        });

    });

    $('#evaluate-div').on('hidden.bs.modal', function (e) {
        window.location.href = lang + '/manager';
    });

    function displayNoneSpans(){
        $(document).find('.validation-error-span').css('display', 'none');
        $('#sendLink').modal('show');
        $('input[name=link]').val($(this).data('link'));
    }

    $('.confirm-send').click(function () {
        let form = $('#sendLinkForm').serialize();

        $.ajax({
            url: lang_client + '/client/send-email-link',
            type: 'POST',
            data: form,

            success: function (response) {
                $('#sendLink').modal('hide');
                $('#sendLinkCompleted').modal('show');
                // window.location.reload();
            },

            error: function (error) {
                let err = JSON.parse(error.responseText);
                $('#sendLinkForm').find('.error').text(err.body.client_email[0]);
            }
        });
    });

    $('.link_btn').click(displayNoneSpans);
    $('.btn_next').click(displayNoneSpans);

    $('.input').on('keyup keypress',function () {
        $(this).parent().find('.error').text('');
    })


    $('.send_to_evaluate').click(function () {

    })

//browsner checking
    var isFirefox = typeof InstallTrigger !== 'undefined';
    var isChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);
    var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) {
        return p.toString() === "[object SafariRemoteNotification]";
    })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
//browsner checking end

});

$('#area_region').on('change', function(){
    $(this).css('color', 'black');
})
$('#area_city').on('change', function(){
    $(this).css('color', 'black');
})

$('.close').on('click', function(){
    if(window.location.href.includes('register')){
        window.location.href = lang + '/manager';
    }
})


$('.modal-close').on('click', function(){
    window.location.href = lang + '/manager';
})
$('#sendLinkCompleted').on('hidden.bs.modal', function (e) {
    window.location.href = lang + '/manager';
});

$('.nav-item').on('click', function(){
    $(document).find('a.nav-link>span').css('color', '#818087');
    let errorBlocks = $(document).find('.nav-item.error');
    errorBlocks.map((key, value) => {
        $(document).find('.block-text-' + (value.className).slice(0, 1)).css('color', '#9D0C00');
    })
    let ballons = $(document).find('.nav-item');
    ballons.css({
        'background-color': '#C4C4C4',
    });
    $(this).css('background-color', '#F9B232');

    let classNameBallon = $(this).attr('class');
    classNameBallon = classNameBallon.slice(0, 1);
    if ($(this).hasClass('error')){
        $(this).css('background-color', '#FF5244');
        $('.block-text-' + classNameBallon).css('color', '#000');

    }
    else{
        $(this).css('background-color', '#F9B232');
        $('.block-text-' + classNameBallon).css('color', '#000');
    }
    $(document).find('.circle-text').css('display', 'none');
    $(document).find('.circle-' + classNameBallon).css('display', 'block');
})

function nextBlock(){
    let finalBtn = $(document).find('.btn_prev');
    finalBtn.attr('onclick', 'prevBlock()');
    finalBtn.css({
        'visibility': 'visible',
    });

    let nextBlock;
    let allBlocks = $('.nav-item');
    let activeElem = $(document).find('.nav-link.active').parent();

    allBlocks.map((key, value) => {
        if (value == activeElem[0]){
            nextBlock = allBlocks[key+1].children[0];
        }
    })
    let nextBlockHref = nextBlock.href;
    nextBlockHref = nextBlockHref.slice(-5);
    let clickOnThis = $(document).find('a[href="'+nextBlockHref+'"]');
    clickOnThis.trigger("click");
    if (allBlocks[allBlocks.length-1] == clickOnThis.parent()[0]){
        $.ajax({
            url: lang + '/client/questionnaire/view/get-role',
            type: 'POST',
            success: function(response){
                if(response.body){
                    let finalBtn = $(document).find('.btn_next');
                    finalBtn.html('ЗАВЕРШИТЬ');
                    finalBtn.prop('onclick', null).off('click');
                    finalBtn.on('click', function () {
                        //*****************************************************************************************
                        $('#loading_layer').css('display','block');
                        $(document).find('td.vertical-bottom').removeClass('vertical-bottom');
                        $(document).find('.delimetr_number').css('border', '1px solid #47b39c');
                        $(document).find('.delimetr_number.importantRuleBorder').removeClass('importantRuleBorder');
                        $(document).find('td[data-target]').css('color', '#212529');
                        $('.validation-error-span').text('');
                        let action = 'evaluate';
                        let form = $('#questionnaire-form').serialize();
                        let client_id = $("input[name='client_id']").val();
                        let questionnaire_id = $("input[name='questionnaire_id']").val();
                        let codes = {};
                        $('input').each(function () {
                            if (typeof $(this).data('code') !== 'undefined' && typeof $(this).data('id') !== 'undefined') {
                                codes[$(this).data('code')] = $(this).data('id');
                            }
                        });

                        if (action == "save") {
                            setTimeout(() => {
                                $('#loading_layer').css('display','none');
                            },2000);

                            return false;
                        }

                        codes = JSON.stringify(codes);

                        form += '&codes=' + codes;
                        form += '&action=' + action;
                        $.ajax({
                            url: lang_client + '/client/fill-questionnaire/' + client_id + '/questionnaire/' + questionnaire_id,
                            type: 'POST',
                            data: form,

                            beforeSend: function(){
                                $("#final-validation").modal('hide');
                            },
                            success: function (response) {
                                let prevError = $(document).find('.nav-item.error');
                                prevError.map(function(key, value){
                                    if(!$(document).find('.nav-link-' + value.className.slice(0, 1)).hasClass('active')){
                                        $(document).find('.block-text-' + value.className.slice(0, 1)).css('color', '#818087');
                                    }
                                    else{
                                        $(document).find('.nav-item.' + value.className.slice(0, 1)).css('background-color', '#F9B232');
                                    }
                                })
                                let allBallons = $(".nav-item");
                                $.each(allBallons, function(key, value){
                                    if(value.className.indexOf("error") !== -1){
                                        value.classList.toggle("error");
                                    }
                                })
                                $.each(response["body"], function (key,value) {

                                    let blockName = key.substring(0, 1);
                                    $(".block-text-" + blockName).css('color', '#9D0C00');

                                    if($('.nav-item.' + blockName + '>a').hasClass('active')){
                                        $('.block-text-' + blockName).css('color', '#000');
                                    }
                                    $("." + blockName).addClass('error');
                                    $(document).find('.validation-error-' + key).css('display', 'block').text(value);
                                    $(document).find('td.' + key).addClass('vertical-bottom');
                                    $(document).find('input[data-code="'+key+'"]').addClass('importantRuleBorder');
                                })

                                $('#loading_layer').css('display', 'none');

                                if (action == 'evaluate') {
                                    if (response.output) {
                                        $('#evaluate-div').modal('show');
                                        return false;
                                    } else {
                                        // $(".alert-danger").css('display', 'block');
                                        // $(".alert-danger").text(response.body);
                                        $("#validation-failed-notify").modal('show');
                                        var errorBlocks = [];
                                        $.each(response.body, function (key, value) {
                                            if (value === null) {
                                                return true;
                                            }

                                            $(document).find('.validation-error-' + key).css('display', 'block').text(value);
                                            $(document).find('td.' + key).addClass('vertical-bottom');
                                            $(document).find('input[data-code="'+key+'"]').addClass('importantRuleBorder');
                                            $(document).find('td[data-target="'+key+'"]').css('color', '#FF5244');
                                            $(document).find('[data-code="' + key + '"]').css('border', '2px solid #FF5244');
                                            if (key) {
                                                errorBlocks.push(key.charAt(0));
                                                $('#error-tab' + key.charAt(0)).css('color', 'red');
                                            } else {

                                            }

                                        });
                                        if (errorBlocks.length > 0)
                                        {
                                            var $container = $("html,body");
                                            var $scrollTo = $('.block'+errorBlocks[0]);

                                            $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top + $container.scrollTop(), scrollLeft: 0},500);
                                        }
                                    }
                                }
                            },
                            error: function (errors) {
                                $('#loading_layer').css('display', 'none');
                                // let error = errors.responseText;
                                // error = JSON.parse(error)
                                // $("#final-validation-text").html(error.body);
                                // $('#final-validation').modal('show');

                                $('#evaluate-div').modal('show');
                                $('html,body').animate({scrollTop: 0}, 'fast');
                            }
                        });
                    })
                }
                else{
                    let finalBtn = $(document).find('.btn_next');
                    finalBtn.html('ЗАВЕРШИТЬ');
                    finalBtn.prop('onclick', null).off('click');
                    finalBtn.attr('disabled', 'disabled');
                    finalBtn.css({
                        'display': 'none',
                        'background': 'gray',
                        'color': 'lightgray',
                    })
                }
            }
        })

        // finalBtn.attr('onclick', 'saveQuest()');
    }
}

function setTextColorBlack(elem){

    if (elem.classList.contains('error-input')){
        elem.classList.toggle('error-input');
        elem.style.border = '2px solid red';
        elem.style.color = 'black';
    }
}
function setSelectColorBlack(elem){
    if(elem.classList.contains('error-select')){
        elem.classList.toggle('error-select');
        elem.style.border = '2px solid red';
        elem.style.color = 'black';
    }
}

function prevBlock(){
    let finalBtn = $(document).find('.btn_next');
    finalBtn.html('Далее');
    finalBtn.css({
        'display': 'block',
        'background-color': '#47ab6b',
        'color': '#fff',
    });
    finalBtn.removeAttr('disabled');
    finalBtn.unbind();
    finalBtn.attr('onclick', 'nextBlock()');
    let nextBlock;
    let allBlocks = $('.nav-item');
    let activeElem = $(document).find('.nav-link.active').parent();

    allBlocks.map((key, value) => {
        if (value == activeElem[0]){
            nextBlock = allBlocks[key-1].children[0];
        }
    })
    let nextBlockHref = nextBlock.href;
    nextBlockHref = nextBlockHref.slice(-5);
    let clickOnThis = $(document).find('a[href="'+nextBlockHref+'"]');
    clickOnThis.trigger("click");
    if (allBlocks[allBlocks.length-1] == clickOnThis.parent()[0]){
        let finalBtn = $(document).find('.btn_next');
        finalBtn.removeAttr('onclick');
        finalBtn.css({
            'display': 'none',
            'background-color': 'lightgray',
            'cursor': 'auto',
        });
    }
    if (allBlocks[0] == clickOnThis.parent()[0]){
        let finalBtn = $(document).find('.btn_prev');
        finalBtn.removeAttr('onclick');
        finalBtn.css({
            'visibility': 'hidden',
        });
    }
}

// double save
// $('.quest_submit_btn').on('click', function () {
//     $("form#questionnaire-form :input").each(function(){
//         saveQuestionnaire($(this)[0])
//     });
// });

$('.nav-item').on('click', function(){
    let ballons = $(document).find('.nav-item');
    if(ballons[0] != $(this)[0]){
        let finalBtn = $(document).find('.btn_prev');
        finalBtn.attr('onclick', 'prevBlock()');
        finalBtn.css({
            'visibility': 'visible',
        });
    }
    else{
        let finalBtn = $(document).find('.btn_prev');
        finalBtn.removeAttr('onclick');
        finalBtn.css({
            'visibility': 'hidden',
        });
    }
    if(ballons[ballons.length - 1] == $(this)[0]){
        $.ajax({
            url: lang + '/client/questionnaire/view/get-role',
            type: 'POST',
            success: function(response){
                if(response.body){
                    let finalBtn = $(document).find('.btn_next');
                    finalBtn.html('ЗАВЕРШИТЬ');
                    finalBtn.prop('onclick', null).off('click');
                    finalBtn.addClass('quest_submit_btn')

                    finalBtn.on('click', function(){
                        //*********************************************************************************
                        $('#loading_layer').css('display','block');
                        $(document).find('td.vertical-bottom').removeClass('vertical-bottom');
                        $(document).find('.delimetr_number').css('border', '1px solid #47b39c');
                        $(document).find('.delimetr_number.importantRuleBorder').removeClass('importantRuleBorder');
                        $(document).find('td[data-target]').css('color', '#212529');
                        $('.validation-error-span').text('');
                        let action = 'evaluate';
                        let form = $('#questionnaire-form').serialize();
                        let client_id = $("input[name='client_id']").val();
                        let questionnaire_id = $("input[name='questionnaire_id']").val();
                        let codes = {};
                        $('input').each(function () {
                            if (typeof $(this).data('code') !== 'undefined' && typeof $(this).data('id') !== 'undefined') {
                                codes[$(this).data('code')] = $(this).data('id');
                            }
                        });

                        if (action == "save") {
                            setTimeout(() => {
                                $('#loading_layer').css('display','none');
                            },2000);

                            return false;
                        }

                        codes = JSON.stringify(codes);

                        form += '&codes=' + codes;
                        form += '&action=' + action;
                        $.ajax({
                            url: lang_client + '/client/fill-questionnaire/' + client_id + '/questionnaire/' + questionnaire_id,
                            type: 'POST',
                            data: form,

                            beforeSend: function(){
                                $("#final-validation").modal('hide');
                            },
                            success: function (response) {
                                let prevError = $(document).find('.nav-item.error');
                                prevError.map(function(key, value){
                                    if(!$(document).find('.nav-link-' + value.className.slice(0, 1)).hasClass('active')){
                                        $(document).find('.block-text-' + value.className.slice(0, 1)).css('color', '#818087');
                                    }
                                    else{
                                        $(document).find('.nav-item.' + value.className.slice(0, 1)).css('background-color', '#F9B232');
                                    }
                                })
                                let allBallons = $(".nav-item");
                                $.each(allBallons, function(key, value){
                                    if(value.className.indexOf("error") !== -1){
                                        value.classList.toggle("error");
                                    }
                                })
                                $.each(response["body"], function (key,value) {

                                    let blockName = key.substring(0, 1);
                                    $(".block-text-" + blockName).css('color', '#9D0C00');

                                    if($('.nav-item.' + blockName + '>a').hasClass('active')){
                                        $('.block-text-' + blockName).css('color', '#000');
                                    }
                                    $("." + blockName).addClass('error');
                                    $(document).find('.validation-error-' + key).css('display', 'block').text(value);
                                    $(document).find('td.' + key).addClass('vertical-bottom');
                                    $(document).find('input[data-code="'+key+'"]').addClass('importantRuleBorder');
                                })

                                $('#loading_layer').css('display', 'none');

                                if (action == 'evaluate') {
                                    if (response.output) {
                                        let userInfo = {
                                            'lang_client': lang_client,
                                            'client_id': client_id,
                                            'questionnaire_id': questionnaire_id
                                        };
                                        userInfo = JSON.stringify(userInfo);
                                        $.ajax({
                                            url: lang_client + '/client/fill-questionnaire/' + client_id + '/questionnaire/' + questionnaire_id + '/send-mail',
                                            type: 'POST',
                                            data: userInfo,
                                        })
                                        $('#evaluate-div').modal('show');
                                        return false;
                                    } else {
                                        // $(".alert-danger").css('display', 'block');
                                        // $(".alert-danger").text(response.body);
                                        $("#validation-failed-notify").modal('show');
                                        var errorBlocks = [];
                                        $.each(response.body, function (key, value) {
                                            if (value === null) {
                                                return true;
                                            }

                                            $(document).find('.validation-error-' + key).css('display', 'block').text(value);
                                            $(document).find('td.' + key).addClass('vertical-bottom');
                                            $(document).find('input[data-code="'+key+'"]').addClass('importantRuleBorder');
                                            $(document).find('td[data-target="'+key+'"]').css('color', '#FF5244');
                                            $(document).find('[data-code="' + key + '"]').css('border', '2px solid #FF5244');
                                            if (key) {
                                                errorBlocks.push(key.charAt(0));
                                                $('#error-tab' + key.charAt(0)).css('color', 'red');
                                            } else {

                                            }

                                        });
                                        if (errorBlocks.length > 0)
                                        {
                                            var $container = $("html,body");
                                            var $scrollTo = $('.block'+errorBlocks[0]);

                                            $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top + $container.scrollTop(), scrollLeft: 0},500);
                                        }
                                    }
                                }
                            },
                            error: function (errors) {
                                $('#loading_layer').css('display', 'none');
                                // let error = errors.responseText;
                                // error = JSON.parse(error)
                                // $("#final-validation-text").html(error.body);
                                // $('#final-validation').modal('show');

                                $('#evaluate-div').modal('show');
                                $('html,body').animate({scrollTop: 0}, 'fast');
                            }
                        });
                    })
                }
                else{
                    let finalBtn = $(document).find('.btn_next');
                    finalBtn.html('ЗАВЕРШИТЬ');
                    finalBtn.prop('onclick', null).off('click');
                    finalBtn.attr('disabled', 'disabled');
                    finalBtn.css({
                        'display': 'none',
                        'background': 'gray',
                        'color': 'lightgray',
                    })
                }
            }
        })
        // let finalBtn = $(document).find('.btn_next');
        // finalBtn.html('ЗАВЕРШИТЬ');
        // finalBtn.prop('onclick', null).off('click');
        //
        // finalBtn.on('click', function(){
        //     //*********************************************************************************
        //     $('#loading_layer').css('display','block');
        //     $(document).find('.delimetr_number').css('border', '1px solid #47b39c');
        //     $(document).find('td[data-target]').css('color', '#212529');
        //     $('.validation-error-span').text('');
        //     let action = 'evaluate';
        //     let form = $('#questionnaire-form').serialize();
        //     let client_id = $("input[name='client_id']").val();
        //     let questionnaire_id = $("input[name='questionnaire_id']").val();
        //     let codes = {};
        //     $('input').each(function () {
        //         if (typeof $(this).data('code') !== 'undefined' && typeof $(this).data('id') !== 'undefined') {
        //             codes[$(this).data('code')] = $(this).data('id');
        //         }
        //     });
        //
        //     if (action == "save") {
        //         setTimeout(() => {
        //             $('#loading_layer').css('display','none');
        //         },2000);
        //
        //         return false;
        //     }
        //
        //     codes = JSON.stringify(codes);
        //
        //     form += '&codes=' + codes;
        //     form += '&action=' + action;
        //     $.ajax({
        //         url: lang_client + '/client/fill-questionnaire/' + client_id + '/questionnaire/' + questionnaire_id,
        //         type: 'POST',
        //         data: form,
        //
        //         beforeSend: function(){
        //             $("#final-validation").modal('hide');
        //         },
        //         success: function (response) {
        //             let prevError = $(document).find('.nav-item.error');
        //             prevError.map(function(key, value){
        //                 if(!$(document).find('.nav-link-' + value.className.slice(0, 1)).hasClass('active')){
        //                     $(document).find('.block-text-' + value.className.slice(0, 1)).css('color', '#818087');
        //                 }
        //                 else{
        //                     $(document).find('.nav-item.' + value.className.slice(0, 1)).css('background-color', '#F9B232');
        //                 }
        //             })
        //             let allBallons = $(".nav-item");
        //             $.each(allBallons, function(key, value){
        //                 if(value.className.indexOf("error") !== -1){
        //                     value.classList.toggle("error");
        //                 }
        //             })
        //             $.each(response["body"], function (key,value) {
        //
        //                 let blockName = key.substring(0, 1);
        //                 $(".block-text-" + blockName).css('color', '#9D0C00');
        //
        //                 if($('.nav-item.' + blockName + '>a').hasClass('active')){
        //                     $('.block-text-' + blockName).css('color', '#000');
        //                 }
        //                 $("." + blockName).addClass('error');
        //                 $(document).find('.validation-error-' + key).css('display', 'block').text(value);
        //             })
        //
        //             $('#loading_layer').css('display', 'none');
        //
        //             if (action == 'evaluate') {
        //                 if (response.output) {
        //                     $('#evaluate-div').modal('show');
        //                     return false;
        //                 } else {
        //                     // $(".alert-danger").css('display', 'block');
        //                     // $(".alert-danger").text(response.body);
        //                     $("#validation-failed-notify").modal('show');
        //                     var errorBlocks = [];
        //                     $.each(response.body, function (key, value) {
        //                         if (value === null) {
        //                             return true;
        //                         }
        //
        //                         $(document).find('.validation-error-' + key).css('display', 'block').text(value);
        //                         $(document).find('td[data-target="'+key+'"]').css('color', '#FF5244');
        //                         $(document).find('[data-code="' + key + '"]').css('border', '2px solid #FF5244');
        //                         if (key) {
        //                             errorBlocks.push(key.charAt(0));
        //                             $('#error-tab' + key.charAt(0)).css('color', 'red');
        //                         } else {
        //
        //                         }
        //
        //                     });
        //                     if (errorBlocks.length > 0)
        //                     {
        //                         var $container = $("html,body");
        //                         var $scrollTo = $('.block'+errorBlocks[0]);
        //
        //                         $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top + $container.scrollTop(), scrollLeft: 0},500);
        //                     }
        //                 }
        //             }
        //         },
        //         error: function (errors) {
        //             $('#loading_layer').css('display', 'none');
        //             // let error = errors.responseText;
        //             // error = JSON.parse(error)
        //             // $("#final-validation-text").html(error.body);
        //             // $('#final-validation').modal('show');
        //
        //             $('#evaluate-div').modal('show');
        //             $('html,body').animate({scrollTop: 0}, 'fast');
        //         }
        //     });
        // })
    }
    else{
        let finalBtn = $(document).find('.btn_next');
        finalBtn.html('Далее');
        finalBtn.css({
            'display': 'block',
            'background-color': '#47ab6b',
            'color': '#fff',
        });
        finalBtn.removeAttr('disabled');
        finalBtn.unbind();
        finalBtn.attr('onclick', 'nextBlock()');
    }
})


function saveQuestionnaire(_this) {
    if (window.location.href.includes('edit-parameter/')) {
        return true;
    }
    if (_this != null && _this.getAttribute('type') == 'checkbox') {
        if (_this.classList.contains('is_checked')) {
            _this.classList.remove('is_checked');
            _this.setAttribute('value', 0);
        } else {
            _this.classList.add('is_checked');
            _this.setAttribute('value', 1);
        }
    }
    B00001()
    if (_this) {
        let parameter_id = _this.name;
        let parameter_value = _this.value;
        parameter_value = parameter_value.replace(/\s+/g, '');
        let client_id = $("input[name='client_id']").val();
        let company_id = $("input[name='company_id']").val();
        let questionnaire_id = $("input[name='questionnaire_id']").val();
        if (client_id) {
            $.ajax({
                url: lang_client + '/company/fill-questionnaire-value/' + company_id + '/questionnaire/' + questionnaire_id,
                type: 'POST',
                data: {
                    current_parameter_id: parameter_id,
                    current_parameter_value: parameter_value,
                    client_id: client_id
                },
                success: function (response) {
                    // let a = $("input[name='" + parameter_id + "']")
                    // a.nextAll("i").remove();
                    // a.after("<i class='fa fa-check' aria-hidden='true' style='color: green'></i>")

                },
                error: function (errors) {

                    let error = errors.responseText;
                    error = JSON.parse(error);

                }
            });
        }
    }
}

// function saveQuest() {
//     $('#loading_layer').css('display','block');
//     $(document).find('.delimetr_number').css('border', '1px solid #47b39c');
//     $(document).find('td[data-target]').css('color', '#212529');
//     $('.validation-error-span').text('');
//     let action = 'evaluate';
//     let form = $('#questionnaire-form').serialize();
//     let client_id = $("input[name='client_id']").val();
//     let questionnaire_id = $("input[name='questionnaire_id']").val();
//     let codes = {};
//     $('input').each(function () {
//         if (typeof $(this).data('code') !== 'undefined' && typeof $(this).data('id') !== 'undefined') {
//             codes[$(this).data('code')] = $(this).data('id');
//         }
//     });
//
//     if (action == "save") {
//         setTimeout(() => {
//             $('#loading_layer').css('display','none');
//         },2000);
//
//         return false;
//     }
//
//     codes = JSON.stringify(codes);
//
//     form += '&codes=' + codes;
//     form += '&action=' + action;
//     $.ajax({
//         url: lang_client + '/client/fill-questionnaire/' + client_id + '/questionnaire/' + questionnaire_id,
//         type: 'POST',
//         data: form,
//
//         beforeSend: function(){
//             $("#final-validation").modal('hide');
// //*********
//             let allBallons = $(".nav-item");
//             $.each(allBallons, function(key, value){
//                 if(value.className.indexOf("error") !== -1){
//                     value.classList.toggle("error");
//                 }
//             })
//         },
//         success: function (response) {
//             let prevError = $(document).find('.nav-item.error');
//             prevError.map(function(key, value){
//                 if(!$(document).find('.nav-link-' + value.className.slice(0, 1)).hasClass('active')){
//                     $(document).find('.block-text-' + value.className.slice(0, 1)).css('color', '#818087');
//                 }
//                 else{
//                     $(document).find('.nav-item.' + value.className.slice(0, 1)).css('background-color', '#F9B232');
//                 }
//             })
// //******
//             let allBallons = $(".nav-item");
//             $.each(allBallons, function(key, value){
//                 if(value.className.indexOf("error") !== -1){
//                     value.classList.toggle("error");
//                 }
//             })
//         },
//         success: function (response) {
//             $.each(response["body"], function (key,value) {
//                 let blockName = key.substring(0, 1);
//                 $("." + blockName).addClass('error');
//                 $(document).find('.validation-error-' + key).css('display', 'block').text(value);
//             })
//
//             $('#loading_layer').css('display', 'none');
//
//             if (action == 'evaluate') {
//                 if (response.output) {
//                     $('#evaluate-div').modal('show');
//                     return false;
//                 } else {
//                     // $(".alert-danger").css('display', 'block');
//                     // $(".alert-danger").text(response.body);
//                     $("#validation-failed-notify").modal('show');
//                     var errorBlocks = [];
//                     $.each(response.body, function (key, value) {
//                         if (value === null) {
//                             return true;
//                         }
//
//                         $(document).find('.validation-error-' + key).css('display', 'block').text(value);
//                         $(document).find('td[data-target="'+key+'"]').css('color', '#FF5244');
//                         $(document).find('[data-code="' + key + '"]').css('border', '2px solid #FF5244');
//                         if (key) {
//                             errorBlocks.push(key.charAt(0));
//                             $('#error-tab' + key.charAt(0)).css('color', 'red');
//                         } else {
//
//                         }
//
//                     });
//                     if (errorBlocks.length > 0)
//                     {
//                         var $container = $("html,body");
//                         var $scrollTo = $('.block'+errorBlocks[0]);
//
//                         $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top + $container.scrollTop(), scrollLeft: 0},500);
//                     }
//                 }
//             }
//         },
//         error: function (errors) {
//             $('#loading_layer').css('display', 'none');
//             let error = errors.responseText;
//             error = JSON.parse(error)
//             $('#final-validation').modal('show');
//             $("#final-validation-text").html(error.body);
//             $('html,body').animate({scrollTop: 0}, 'fast');
//         }
//     });
// }

