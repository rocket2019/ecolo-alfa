(function ($) {
    $(function () {
        $.widget("zpd.paging", {
            options: {
                limit: 5,
                rowDisplayStyle: 'block',
                activePage: 0,
                rows: [],
                pageNum: 1,
                showPages: 6,
                iIndex: 0,
                allPages: 0,
            },
            _create: function () {
                var rows = $("tbody", this.element).children();
                this.options.allPages = Math.ceil(rows.length / this.options.limit);
                this.options.rows = rows.slice(1);
                this.options.rowDisplayStyle = rows.css('display');
                var nav = this._getNavBar();
                this.element.after(nav);
                this.showPage(0);
            },
            _getNavBar: function () {
                var rows = this.options.rows;
                var nav = $('<div>', {class: 'paging-nav'});
                var allPages = Math.ceil(rows.length / this.options.limit)

                let pageNum = +this.options.pageNum
                let dots = true

                if (allPages > 7) {
                    if (pageNum == allPages) {
                        this.options.iIndex = pageNum - 6
                        this.options.showPages = pageNum - 1
                        dots = false

                    } else if (pageNum == allPages - 1 || pageNum == allPages) {
                        dots = false

                    } else if (pageNum == this.options.showPages - 1 && pageNum >= 6) {
                        this.options.iIndex = pageNum - 6;
                        this.options.showPages = pageNum;

                    } else if (pageNum >= 6) {
                        this.options.iIndex = pageNum - 5;
                        this.options.showPages = pageNum + 1;

                    } else {
                        this.options.iIndex = 0;
                        this.options.showPages = 6;
                    }

                    this._on($('<a>', {
                            href: '#',
                            class: 'p-hidden',
                            text: '',
                            "data-page": 0
                        }).appendTo(nav),
                        {click: "pageClickHandler"});

                    for (var i = this.options.iIndex; i < this.options.showPages; i++) {
                        this._on($('<a>', {
                                href: '#',
                                class: pageNum == (i + 1) ? 'selected-page' : '',
                                text: (i + 1),
                                "data-page": (i)
                            }).appendTo(nav),
                            {click: "pageClickHandler"});
                    }

                    if (dots) {
                        this._on($('<a>', {
                                text: '...',
                            }).appendTo(nav),
                            {click: ""});
                    }

                    this._on($('<a>', {
                            href: '#',
                            class: pageNum == allPages ? 'selected-page' : '',
                            text: allPages,
                            "data-page": allPages - 1
                        }).appendTo(nav),
                        {click: "pageClickHandler"});

                } else {
                    for (var i = 0; i < allPages; i++) {
                        this._on($('<a>', {
                                href: '#',
                                class: pageNum == (i + 1) ? 'selected-page' : '',
                                text: (i + 1),
                                "data-page": (i)
                            }).appendTo(nav),
                            {click: "pageClickHandler"});
                    }
                }

                //create previous link
                this._on($('<a>', {
                        href: '#',
                        html: '&#8592;',
                        "data-direction": -1
                    }).prependTo(nav),
                    {click: "pageStepHandler"});

                //create next link
                this._on($('<a>', {
                        href: '#',
                        html: '&#8594;',
                        "data-direction": +1
                    }).appendTo(nav),
                    {click: "pageStepHandler"});
                return nav;
            },
            showPage: function (pageNum) {
                var num = pageNum * 1; //it has to be numeric
                this.options.activePage = num;
                var rows = this.options.rows;
                var limit = this.options.limit;
                for (var i = 0; i < rows.length; i++) {
                    if (i >= limit * num && i < limit * (num + 1)) {
                        $(rows[i]).css('display', this.options.rowDisplayStyle);
                    } else {
                        $(rows[i]).css('display', 'none');
                    }
                }

                $('.paging-nav a[data-direction="-1"]').hide()
            },
            pageClickHandler: function (event) {
                event.preventDefault();
                $(event.target).siblings().attr('class', "");
                $(event.target).attr('class', "selected-page");
                var pageNum = $(event.target).attr('data-page');
                this.showPage(pageNum);

                this.options.pageNum = +pageNum + 1
                $('.paging-nav').remove();
                var nav = this._getNavBar();
                this.element.after(nav);
                this.showPage(pageNum);

                if (pageNum > 0) {
                    $('.paging-nav a[data-direction="-1"]').show();
                } else {
                    $('.paging-nav a[data-direction="-1"]').hide();
                }

                if (+pageNum + 1 >= Math.ceil(this.options.rows.length / this.options.limit)) {
                    $('.paging-nav a[data-direction="1"]').hide();
                } else {
                    $('.paging-nav a[data-direction="1"]').show();
                }
            },
            pageStepHandler: function (event) {
                event.preventDefault();
                //get the direction and ensure it's numeric
                var dir = $(event.target).attr('data-direction') * 1;
                var pageNum = this.options.activePage + dir;
                //if we're in limit, trigger the requested pages link
                if (pageNum >= 0 && pageNum < this.options.rows.length) {
                    $("a[data-page=" + pageNum + "]", $(event.target).parent()).click();
                }
            }
        });
    });
})(jQuery);



