//browsner checking
var isFirefox = typeof InstallTrigger !== 'undefined';
var isChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);
var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) {
    return p.toString() === "[object SafariRemoteNotification]";
})(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
//browsner checking end

if ($('.autoresizing').val() != undefined) {
//fields resizing
    autosize($('.autoresizing '));

    if ($('.last-field').offset() != undefined) {
        let last_field_top_def = $('.last-field').offset().top;
        $(".dynamic_top").offset({top: last_field_top_def + 5});

        $('.autoresizing').on('input', function () {
            let last_field_top = $('.last-field').offset().top;
            $(".dynamic_top").offset({top: last_field_top + 5});
        })
    }
//resizing end
}

$(document).ready(() => {
    $('.arr-down').on('click', () => {
        $('.tableDataSort tr').sort((a, b) => {
            const innerA = a.children[0].innerText.trim();
            const innerB = b.children[0].innerText.trim();
            return new Date(parseTableDate(innerA)) - new Date(parseTableDate(innerB));
        }).appendTo('.tableDataSort');
        $('.arr-down').hide()
        $('.arr-up').show()
    });

    $('.arr-up').on('click', () => {
        $('.tableDataSort tr').sort((a, b) => {
            const innerA = a.children[0].innerText.trim();
            const innerB = b.children[0].innerText.trim();
            return new Date(parseTableDate(innerB)) - new Date(parseTableDate(innerA));
        }).appendTo('.tableDataSort');
        $('.arr-up').hide()
        $('.arr-down').show()
    });

    function parseTableDate(date) {
        const parts = date.split('.');
        const day = parts[0];
        const month = parts[1];
        const year = parts[2];
        const newDate = `${month}.${day}.${year}`
        return newDate;
    }
})
