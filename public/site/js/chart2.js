var ctx = document.getElementById("successChart1").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["", "", "", ""],
        datasets: [{
            label: false,
            data: [450000, 390000, 320000, ''],
            backgroundColor: [
                '#f2f2f2',
                '#4472C4',
                '#7F7F7F',
                '',
            ],
            hoverBackgroundColor:[
                '#f2f2f2',
                '#4472C4',
                '#7F7F7F',
                '',
            ],
            borderWidth: 0
        }]
    },
    options: {
        showTooltips: false,
        scales: {
            yAxes: [{
                display: false,
                ticks: {
                    beginAtZero: false,
                }
            }],
            xAxes: [{
                display: false,
                categoryPercentage: 1.02,
                barPercentage: 1.0
            }]
        },
        plugins: {
            datalabels: {
                color: 'red'
            }
        },
        legend: {
            display: false,
            padding: 0,
            position: 'bottom',
        },
        line: {
            borderDash: [0, 0]
        },
        "hover": {
            "animationDuration": 0
        },
        tooltips: {
            "enabled": false
        },
        "animation": {
            "duration": 1,
            "onComplete": function() {
                var chartInstance = this.chart,
                    ctx = chartInstance.ctx;

                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                ctx.textAlign = 'center';
                ctx.textBaseline = 'bottom';

                this.data.datasets.forEach(function(dataset, i) {
                    var meta = chartInstance.controller.getDatasetMeta(i);
                    meta.data.forEach(function(bar, index) {
                        if (index !== 0){
                            var data = dataset.data[index];
                            ctx.fillText(data, bar._model.x, bar._model.y);
                        }
                    });
                });
            }
        },

    }
});

var ctx = document.getElementById("successChart2").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["", "", "", ""],
        datasets: [{
            label: false,
            data: [450000, 390000, 320000, ''],
            backgroundColor: [
                '#f2f2f2',
                '#4472C4',
                '#7F7F7F',
                '',
            ],
            hoverBackgroundColor:[
                '#f2f2f2',
                '#4472C4',
                '#7F7F7F',
                '',
            ],
            borderWidth: 0
        }]
    },
    options: {
        showTooltips: false,
        scales: {
            yAxes: [{
                display: false,
                ticks: {
                    beginAtZero: false,
                }
            }],
            xAxes: [{
                display: false,
                categoryPercentage: 1.02,
                barPercentage: 1.0
            }]
        },
        plugins: {
            datalabels: {
                color: 'red'
            }
        },
        legend: {
            display: false,
            padding: 0,
            position: 'bottom',
        },
        line: {
            borderDash: [0, 0]
        },
        "hover": {
            "animationDuration": 0
        },
        tooltips: {
            "enabled": false
        },
        "animation": {
            "duration": 1,
            "onComplete": function() {
                var chartInstance = this.chart,
                    ctx = chartInstance.ctx;

                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                ctx.textAlign = 'center';
                ctx.textBaseline = 'bottom';

                this.data.datasets.forEach(function(dataset, i) {
                    var meta = chartInstance.controller.getDatasetMeta(i);
                    meta.data.forEach(function(bar, index) {
                        if (index !== 0){
                            var data = dataset.data[index];
                            ctx.fillText(data, bar._model.x, bar._model.y);
                        }
                    });
                });
            }
        },

    }
});

var ctx = document.getElementById("successChart3").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["", "", "", ""],
        datasets: [{
            label: false,
            data: [450000, 390000, 320000, ''],
            backgroundColor: [
                '#f2f2f2',
                '#4472C4',
                '#7F7F7F',
                '',
            ],
            hoverBackgroundColor:[
                '#f2f2f2',
                '#4472C4',
                '#7F7F7F',
                '',
            ],
            borderWidth: 0
        }]
    },
    options: {
        showTooltips: false,
        scales: {
            yAxes: [{
                display: false,
                ticks: {
                    beginAtZero: false,
                }
            }],
            xAxes: [{
                display: false,
                categoryPercentage: 1.02,
                barPercentage: 1.0
            }]
        },
        plugins: {
            datalabels: {
                color: 'red'
            }
        },
        legend: {
            display: false,
            padding: 0,
            position: 'bottom',
        },
        line: {
            borderDash: [0, 0]
        },
        "hover": {
            "animationDuration": 0
        },
        tooltips: {
            "enabled": false
        },
        "animation": {
            "duration": 1,
            "onComplete": function() {
                var chartInstance = this.chart,
                    ctx = chartInstance.ctx;

                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                ctx.textAlign = 'center';
                ctx.textBaseline = 'bottom';

                this.data.datasets.forEach(function(dataset, i) {
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        meta.data.forEach(function(bar, index) {
                            if (index !== 0){
                                var data = dataset.data[index];
                                ctx.fillText(data, bar._model.x, bar._model.y);
                            }
                        });
                });
            }
        },

    }
});
