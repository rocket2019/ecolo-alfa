(function($){
    $.fn.filetree = function(method){

        var settings = { // settings to expose
            animationSpeed      : 'fast',
            collapsed           : true,
            console             : false
        }
        var methods = {
            init : function(options){
                // Get standard settings and merge with passed in values
                var options = $.extend(settings, options);
                // Do this for every file tree found in the document
                return this.each(function(){

                    var $fileList = $(this);

                    $fileList
                        .addClass('file-list')
                        .find('li')
                        .has('ul') // Any li that has a list inside is a folder root
                            .addClass('folder-root closed')
                            .on('click', 'a[href="#"]', function(e){ // Add a click override for the folder root links
                                e.preventDefault();
                                $(this).parent().toggleClass('closed').toggleClass('open');


                                return false;
                            });

                    //alert(options.animationSpeed); Are the settings coming in

                });


            }
        }




        if (typeof method === 'object' || !method){
            return methods.init.apply(this, arguments);
        }
    }

}(jQuery));

$(document).ready(function(){

    $(".nav").find(".nav-item:first").css("background", "#f1592a");

    $(".nav-link").click(function() {
        $(".nav").find(".nav-item").css("background", "#258DB4");
        $(this).parent(".nav-item").css("background", "#f1592a");
    })
});

