$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.register').click(function () {
        $('#loading_layer').css('display','block');

        let form = $('.register_form').serialize();

        console.log(form);

        $.ajax({
            url: lang + '/register',
            data: form,
            type: "POST",

            success: function () {
                $('#loading_layer').css('display','none');

                let alertLogin = $('.alert-login');
                alertLogin.removeClass('alert-error');
                alertLogin.addClass('alert-success');
                alertLogin.css('display', 'block');
                alertLogin.html("Please,check your email");            },

            error: function (errors) {
                $('#loading_layer').css('display','none');

                let error = errors.responseText;

                error = JSON.parse(error);
                console.log(typeof error.body);
                console.log(error.body);
                if (typeof error.body !== 'undefined') {
                    let alertLogin = $('.alert-login');
                    alertLogin.css('display', 'block');
                    alertLogin.html(error.body[Object.keys(error.body)[0]]);
                }


            },
        })
    });
    $(document).on('click', '.register2', function () {
        $('#loading_layer').css('display','block');

        let form = $('.register_form2').serialize();
        console.log(form);
        $.ajax({
            url: lang +'/confirm-register',
            data: form,
            type: "POST",

            success: function () {
                window.location.href = lang + '/account';
            },

            error: function (errors) {
                $('#loading_layer').css('display','none');

                let error = errors.responseText;

                error = JSON.parse(error);
                console.log(typeof error.body);
                console.log(error.body);
                if (typeof error.body !== 'undefined') {
                    let alertLogin = $('.alert-login');
                    alertLogin.css('display', 'block');
                    alertLogin.html(error.body[Object.keys(error.body)[0]]);
                }


            },
        })
    });

    $('input').on('keyup keypress keydown paste', function () {
        $(this).parent().find('.error').text('');
    });
});
