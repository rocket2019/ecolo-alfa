$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).on('click', '.reset_password', function () {
        $('#loading_layer').css('display','block');

        let form = $('#reset_password').serialize();

        console.log(form);
        $.ajax({
            url: lang + '/reset',
            type: "POST",
            data: form,

            success: function () {
                $('#loading_layer').css('display','none');

                let alertLogin = $('.alert-login');
                alertLogin.removeClass('alert-error');
                alertLogin.addClass('alert-success');
                alertLogin.css('display', 'block');
                alertLogin.html("Please,check your email");
            },

            error: function (errors) {
                $('#loading_layer').css('display','none');

                let error = errors.responseText;

                error = JSON.parse(error);
                console.log(typeof error.body);
                console.log(error.body);
                if (typeof error.body !== 'undefined') {
                    let alertLogin = $('.alert-login');
                    alertLogin.css('display', 'block');
                    alertLogin.html(error.body[Object.keys(error.body)[0]]);
                }


            },
        })
    });
    $(document).on('click', '.update_password', function () {
        $('#loading_layer').css('display','block');


        let form = $('#update_password').serialize();
        let reset_token = $('#reset_token').val()
        console.log(form);
        $.ajax({
            url: lang + '/update-password/' + reset_token,
            type: "POST",
            data: form,

            success: function () {
                window.location.href = lang + '/login';

            },

            error: function (errors) {
                $('#loading_layer').css('display','none');

                let error = errors.responseText;

                error = JSON.parse(error);
                console.log(typeof error.body);
                console.log(error.body);
                if (typeof error.body !== 'undefined') {
                    let alertLogin = $('.alert-login');
                    alertLogin.css('display', 'block');
                    alertLogin.html(error.body[Object.keys(error.body)[0]]);
                }


            },
        })
    });

    $('input').on('keyup keypress keydown paste', function () {
        $(this).parent().find('.error').text('');
    });
});
