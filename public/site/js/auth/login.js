$(document).ready(function () {


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#username,#password').keypress(function (e) {
        if (e.which === 13) {
            login();
            return false;    //<---- Add this line
        }
    });

    jQuery('.login').click(function () {
        login()
    });
    function login() {
        let form = $('.login_form').serialize();
        $('#loading_layer').css('display', 'block');


        $.ajax({
            url: lang + '/login',
            data: form,
            type: "POST",

            success: function (response) {
                console.log(response.body.redirect);
                window.location.href = lang + '/' + response.body.redirect;
            },

            error: function (errors) {
                $('#loading_layer').css('display', 'none');

                let error = errors.responseText;

                error = JSON.parse(error);
                console.log(typeof error.body);
                console.log(error.body);
                if (typeof error.body !== 'undefined') {
                    let alertLogin = $('.alert-login');
                    alertLogin.css('display', 'block');
                    alertLogin.html(error.body[Object.keys(error.body)[0]]);
                }


            },
        })


    }
    ;

    $('input').on('keyup keypress keydown paste', function () {
        $(this).parent().find('.error').text('');
    });
});
