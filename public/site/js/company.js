if (typeof(lang) == 'undefined') {
    var lang = window.location.pathname;
    lang.indexOf(1);

    lang.toLowerCase();

    lang = lang.split("/")[1];

    lang = '/' + lang;
}
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // $('.delimetr_number').val(function (index, value) {
    //
    //     if (value) {
    //         value =value
    //             .replace(/\D/g, "")
    //             .replace(/\B(?=(\d{3})+(?!\d))/g, " ")
    //         ;
    //         return
    //     }
    // });



    $('.js-example-basic-multiple').select2({
        // placeholder: "Select a specialisation from the list or use sectoral code... ",
    });



    $('input.number').keyup(function (event) {

        // skip for arrow keys
        if (event.which >= 37 && event.which <= 40) return;

        // format number
        $(this).val(function (index, value) {
            return value
                .replace(/\D/g, "")
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                ;
        });
    });

    $(document).on('click', '.visible', function () {
        let id = this.value;
        $.ajax({
            url: lang + '/admin/visible/' + id,
            type: "GET",
            success: function (response) {

            },

            error: function (errors) {

            },
        })
    })


    $(document).on('click', '.save_company', function () {
        $('#loading_layer').css('display', 'block');
        let form = $('#new_company_form').serialize();

        $.ajax({
            url: lang + '/company/new',
            data: form,
            type: "POST",

            success: function (response) {

                window.location.href = lang + '/company/view/' + response.body;
            },

            error: function (errors) {
                $('#loading_layer').css('display', 'none');
                let error = errors.responseText;
                error = JSON.parse(error);
                $.each(error.body, function (key, value) {
                    $('.' + 'error-' + key).text(value);
                });


            },
        })
    });
    $(".file-tree").filetree({
        animationSpeed: 'fast'

    });

    $(document).on('click', '.btn.edit-status', function(){
        let id = $(this).attr('data-id');
        $.ajax({
            url: lang + '/account/new-to-old/' + id,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: id,
            type: "POST",
        })
    })

    $(document).ready(function(){
        $(document).find('li.file-icon').on('click', function(){
            $(this).children('a').click();
        })
    });


    $(document).on('click', '.update_company', function () {
        $('#loading_layer').css('display', 'block');

        let form = $('#new_company_form').serialize();
        let id = $('input[name = company_id]').val();

        $.ajax({
            url: lang + '/company/edit/' + id,
            data: form,
            type: "POST",

            success: function (response) {
                window.location.href = lang + '/company/view/' + response.body;
            },

            error: function (errors) {
                $('#loading_layer').css('display', 'none');
                let error = errors.responseText;
                error = JSON.parse(error);
                $.each(error.body, function (key, value) {
                    $('.' + 'error-' + key).text(value);
                });


            },
        })
    });

    $(document).on('click', '.send_to_eval', function (e) {
        e.preventDefault();
        let id = $(this).attr('data-id');
        $('#send_to_eval').modal()
        $('#send_to_eval').find('.send-to-eval').attr('data-id', id);
    })
    $(document).on('click', '.delete_company', function (e) {
        e.preventDefault();
        let id = $(this).attr('data-id');

        $('#delete_company').modal()
        $('#delete_company').find('.delete-company').attr('data-id', id);
    })

    $(document).on('click', '.send-to-eval', function () {
        let id = $(this).data('id');
        $.ajax({
            url: lang + '/company/send-to-evaluation/' + id,
            type: 'GET',
            success: function (response) {
                window.location.reload();
            },
            error: function () {
            }
        });

    })
    $(document).on('click', '.to_bank', function () {
        let id = $(this).attr('data-id');

        $.ajax({
            url: lang + '/company/to-bank/' + id,
            type: 'GET',
            success: function (response) {
                window.location.reload();
            },
            error: function () {
            }
        });

    })

    $(document).on('click', '.delete-company', function () {
        let id = $(this).data('id');
        $.ajax({
            url: lang + '/company/delete/' + id,
            type: 'GET',
            success: function (response) {
                window.location.href = lang + '/account';
            },
            error: function () {
            }
        });

    })


    $(document).on('click', '.save_comment', function () {
        $('#loading_layer').css('display', 'block');

        let form = $('.new_comment').serialize();
        let id = $('input[name = company_id]').val();
        $.ajax({
            url: lang + '/company/comment/' + id,
            data: form,
            type: "POST",

            success: function (response) {
                window.location.href = lang + '/company/view/' + id;
            },

            error: function (errors) {
                $('#loading_layer').css('display', 'none');
                let error = errors.responseText;
                error = JSON.parse(error);
                $.each(error.body, function (key, value) {
                    $('.' + 'error-' + key).text(value);
                });


            },
        })
    });


    $('select[name=area_region]').on('change', function () {
        let region = this.value;
        $.ajax({
            url: lang + '/company/get-area-city',
            data: {region_number: region},
            type: "POST",

            success: function (response) {
                let city = [];
                let reg = /г. /;
                if (response.body[0].length > 1){
                    for(let key = 0; key < response.body[0].length; key++){
                        if (response.body[0][key]['area_name'] == response.body[1]){
                            response.body[0].splice(key, 1);
                            break;
                        }
                    }
                }
                for (let key=0; key<response.body[0].length; key++){
                    if(response.body[0][key]['area_name'].match(reg)){
                        city.push(response.body[0][key]);
                        response.body[0].splice(key, 1);
                        key--;
                    }
                }
                city.push(...response.body[0]);
                let selectList = $('select[name=area_city]');
                selectList.find("option:gt(0)").remove();
                $.each(city, function (key, value) {
                    $('select[name=area_city]')
                        .append($("<option></option>").css('color','black')
                            .attr("value", value.area_code)
                            .text(value.area_name));
                });

            },
            error: function () {

            },
        });
    });


    function regions() {

        let region = $('select[name=area_region]').val();

        if(region)
        {
            $.ajax({
                url: lang + '/client/get-area-city',
                data: {region_number: region},
                type: "POST",

                success: function (response) {
                    let city = [];
                    let reg = /г. /;
                    if (response.body[0].length > 1){
                        for(let key = 0; key < response.body[0].length; key++){
                            if (response.body[0][key]['area_name'] == response.body[1]){
                                response.body[0].splice(key, 1);
                                break;
                            }
                        }
                    }
                    for (let key=0; key<response.body[0].length; key++){
                        if(response.body[0][key]['area_name'].match(reg)){
                            city.push(response.body[0][key]);
                            response.body[0].splice(key, 1);
                            key--;
                        }
                    }
                    city.push(...response.body[0]);
                    let selectList = $('select[name=area_city]');
                    selectList.find("option:gt(0)").remove();
                    $.each(city, function (key, value) {

                        $('select[name=area_city]')
                            .append($("<option></option>")
                                .attr("value", value.area_code)
                                .text(value.area_name));
                    });

                    setTimeout(function () {
                        selectList.find('option[value="'+selectList.data('id')+'"]').attr('selected','selected');
                        // selectList.val(selectList.html())
                        // selectList.find('option[value="'+selectList.data('id')+'"]').html()
                    },4000);



                },
                error: function () {

                },
            });
        }





    }
    regions();
    if(!window.location.href.includes('edit-parameter/'))
    {
        setTimeout(function () {
            $.ajax({
                url: '/en/company/get-company-region',
                type: "GET",

                data: {company_id: $('input[name = company_id]').val()},


                success: function (response) {
                    $('select[name=area_city]').val(response.body)
                },

            });
        },2000)
    }

    if(window.location.href.includes('client/'))
    {
        setTimeout(function () {
            $.ajax({
                url: '/en/company/get-company-region',
                type: "GET",

                data: {company_id: $('input[name = client_id]').val()},


                success: function (response) {
                    $('select[name=area_city]').val(response.body)
                },

            });
        },2000)
    }






    $(document).on('click', '.code_id', function () {
        $(this).parents('li.file-icon').click();
        $('.code_id').css('font-weight', 'normal')
        $(this).css('font-weight', 'bolder')
        let code_id = $(this).data('id');
        $('#code_id').val(code_id);

        $.ajax({
            url: lang + '/company/get-ateco',
            data: {code_id: code_id},
            type: "POST",

            success: function (response) {

                $('#sose_okveds_select').empty();
                $.each(response.body, function (key, value) {
                    let description = value.sose_ateco.description.split('[');
                    $('#sose_okveds_select')
                        .append($("<a></a>")
                            .addClass('ateco_id_function')
                            .attr("data-id", value.sose_ateco.id)
                            .html(value.sose_ateco.ateco_code + '-' + description[0] + '<span style="display: none">'+ value.description + '-' + value.sose_okved.okved_code +' - '+ description[1] + '</span>'));
                });
                $('#sose_okveds_select').css('display', 'block');

            },

            error: function (errors) {
                $('#loading_layer').css('display', 'none');
                let error = errors.responseText;
                error = JSON.parse(error);
                $.each(error.body, function (key, value) {
                    $('.' + 'error-' + key).text(value);
                });


            },
        })


    })
    $(document).on('click', '.new_questionnaire', function () {
        $('#questionnaire-div').css('display', 'block');


    })


    $(document).on('click', '#add-questionnaire', function () {
        let code_id = $('#code_id').val();
        let company_id = $('#company_id').val();
        let ateco_id = $('#selected').data('id');
        let company_unit = $('#company_unit option:selected').val();


        $.ajax({
            url: lang + '/company/new-questionnaire',
            data: {code_id: code_id, company_id: company_id, ateco_id: ateco_id, company_unit: company_unit},
            type: "POST",

            success: function (response) {
                window.location.href = lang + '/company/fill-questionnaire/' + company_id + '/questionnaire/' + response.body;
            },

            error: function (errors) {
                $('#loading_layer').css('display', 'none');
                let error = errors.responseText;
                error = JSON.parse(error);
                $.each(error.body, function (key, value) {
                    $('.' + 'error-' + key).text(value);
                });


            },
        })
    })


    $(document).on('click', '.remove-questionnaire', function (e) {
        e.preventDefault();
        let id = $(this).attr('data-id');
        $('#delete_questionnaire').modal()
        $('#delete_questionnaire').find('.delete-questionnaire').attr('data-id', id);
    })

    $(document).on('click', '.delete-questionnaire', function () {
        let id = $(this).data('id');
        $.ajax({
            url: lang + '/company/delete-questionnaire/' + id,
            type: 'GET',
            success: function (response) {
                window.location.reload();
            },
            error: function () {
            }
        });

    })

    /**
     * Update Parameter
     * Update all parameters texts connected with this study
     */
    $(document).on('click', '.update_parameter', function () {
        $('#loading_layer').css('display', 'block');

        let form = $('#parameter_form').serialize();

        let id = $('input[name="parameter_id"]').val();

        $.ajax({
            url: lang + '/company/edit-parameter/' + id,
            type: 'POST',
            data: form,
            success: function (response) {
                window.location.reload();
            },
            error: function (errors) {
                $('#loading_layer').css('display', 'none');
                let error = errors.responseText;
                error = JSON.parse(error);
                $.each(error.body, function (key, value) {
                    $('.' + 'error-' + key).text(value);
                });
            }
        });

    });

    $('input[name="visible"]').change(function () {
        this.value = this.checked ? 1 : 0;
    }).change();

    $("#questionnaire_form input[value=1]:checkbox").each(function () {
        $(this).attr("checked", true);
    });

    $('#tabParameters').on('click', 'li', function () {

        $('#tabParameters li.active').removeClass('active');
        let id = $(this).data('id');
        $(this).addClass('active');

        $('#tab-content div.active').removeClass('active');
        $('#tab-content div#' + id).addClass('active');
        // DelimetrNumber();

    });


    $('.clear_ateco').on('click', function () {
        window.location.reload()
    });
    $('.close_question').on('click', function () {
        $('#questionnaire-div').css('display', 'none');

    });

    $('.fill_questionnaire').on('click', function () {


        $('#loading_layer').css('display', 'block');
        let form = $('#questionnaire_form').serialize();
        let company_id = $("input[name='company_id']").val();
        let questionnaire_id = $("input[name='questionnaire_id']").val();
        let codes = {};
        $('input').each(function () {
            if (typeof $(this).data('code') !== 'undefined' && typeof $(this).data('id') !== 'undefined') {
                codes[$(this).data('code')] = $(this).data('id');
            }
        });

        codes = JSON.stringify(codes);

        form += '&codes=' + codes;

        $.ajax({
            url: lang + '/company/fill-questionnaire/' + company_id + '/questionnaire/' + questionnaire_id,

            type: 'POST',
            data: form,
            success: function (response) {

                if (response.body === true) {
                    window.location.href = lang + '/company/view/' + company_id;

                } else {
                    $('#loading_layer').css('display', 'none');

                    // $(".alert-danger").css('display', 'block');
                    // $(".alert-danger").text(response.body);
                    $.each(response.body, function (key, value) {
                        if (value === null) {
                            return true;
                        }

                        $(document).find('.validation-error-' + key).css('display', 'block').text(value);
                        if (key) {
                            $('#error-tab' + key.charAt(0)).css('color', 'red');
                        } else {

                        }

                    });
                    $('html,body').animate({scrollTop: 0}, 'fast');
                }
            },
            error: function (errors) {
                $('#loading_layer').css('display', 'none');
                let error = errors.responseText;
                error = JSON.parse(error)
                $(".alert-danger").css('display', 'block');
                $(".alert-danger").text(error.body);
                $('html,body').animate({scrollTop: 0}, 'fast');
            }
        });

    });
    $('#buttonPrint').click(function (e) {

        e.preventDefault();

        let ateco_id = $('#selected').data('id');
        let company_unit = $('#company_unit option:selected').val();
        let url = lang + '/company/generate-pdf/' + ateco_id + '/' + company_unit
        $("#url").attr("href", url)

        $('#url')[0].click();

    })


    $(document).on('click', '.ateco_id_function', selected_data);
});

$('.delimetr_number').keyup(function (event) {

    $(this).parent().find('.alert-danger').text('').css('display', 'none');
    // skip for arrow keys
    if (event.which >= 37 && event.which <= 40) return;

    // format number
    if( $(this).attr('data-code') !== 'F02801') {
        $(this).val(function (index, value) {
            return value
                .replace(/\D/g, "")
                .replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        });
    }


});

$(document).on('click', '.btn-evaluate', function () {
    $('#loading_layer').css('display', 'block');

    let url = $(this).attr('data-url');

    $.ajax({
        url: url,
        type: "GET",
        success: function (response) {

            $('#loading_layer').css('display', 'none');

            $('#evaluate-div').modal()
            $('#evaluate-div').find('.evaluate-response-div').html( response.output);
            //
            // $.each(response.output, function (key, value) {
            //     $("#evaluate-div .modal-content ul").append("<li>"+value+"</li>");
            //
            // });

        },

        error: function (error) {
            $('#loading_layer').css('display', 'none');

            $('#evaluate-div-error').modal();
            $('#evaluate-div-error').find('.modal-dialog-centered').html('<h3>'+JSON.parse(error.responseText).body+'</h3>')
        }
    });
});


$(document).on('click', '.exportGericoTxt', function () {
    $('#loading_layer').css('display', 'block');

    let url = $(this).attr('data-url');

    $.ajax({
        url: url,
        type: "GET",
        success: function (response) {
            $('#loading_layer').css('display', 'none');

            var file_path = response.output_file_url;

            var a = document.createElement('A');
            a.href = file_path;
            a.download = file_path.substr(file_path.lastIndexOf('/') + 1);
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
        },

        error: function (error) {

        }
    });
});


function f_block_function(_this = null) {
    let F00101 = ($("input").is('#F00101') && $("#F00101").val() != '') ? $("#F00101").val() : '0';
    let F00201 = ($("input").is('#F00201') && $("#F00201").val() != '') ? $("#F00201").val() : '0';
    let F00301 = ($("input").is('#F00301') && $("#F00301").val() != '') ? $("#F00301").val() : '0';
    let F00401 = ($("input").is('#F00401') && $("#F00401").val() != '') ? $("#F00401").val() : '0';
    let F00501 = ($("input").is('#F00501') && $("#F00501").val() != '') ? $("#F00501").val() : '0';
    let F00601 = ($("input").is('#F00601') && $("#F00601").val() != '') ? $("#F00601").val() : '0';
    let F00701 = ($("input").is('#F00701') && $("#F00701").val() != '') ? $("#F00701").val() : '0';
    let F00801 = ($("input").is('#F00801') && $("#F00801").val() != '') ? $("#F00801").val() : '0';
    let F00901 = ($("input").is('#F00901') && $("#F00901").val() != '') ? $("#F00901").val() : '0';
    let F01001 = ($("input").is('#F01001') && $("#F01001").val() != '') ? $("#F01001").val() : '0';
    let F01101 = ($("input").is('#F01101') && $("#F01101").val() != '') ? $("#F01101").val() : '0';
    let F01201 = ($("input").is('#F01201') && $("#F01201").val() != '') ? $("#F01201").val() : '0';
    let F01301 = ($("input").is('#F01301') && $("#F01301").val() != '') ? $("#F01301").val() : '0';
    let F01401 = ($("input").is('#F01401') && $("#F01401").val() != '') ? $("#F01401").val() : '0';
    let F01501 = ($("input").is('#F01501') && $("#F01501").val() != '') ? $("#F01501").val() : '0';
    let F01601 = ($("input").is('#F01601') && $("#F01601").val() != '') ? $("#F01601").val() : '0';
    let F01701 = ($("input").is('#F01701') && $("#F01701").val() != '') ? $("#F01701").val() : '0';
    let F01801 = ($("input").is('#F01801') && $("#F01801").val() != '') ? $("#F01801").val() : '0';
    let F01901 = ($("input").is('#F01901') && $("#F01901").val() != '') ? $("#F01901").val() : '0';
    let F02001 = ($("input").is('#F02001') && $("#F02001").val() != '') ? $("#F02001").val() : '0';
    let F02101 = ($("input").is('#F02101') && $("#F02101").val() != '') ? $("#F02101").val() : '0';
    let F02201 = ($("input").is('#F02201') && $("#F02201").val() != '') ? $("#F02201").val() : '0';
    let F02301 = ($("input").is('#F02301') && $("#F02301").val() != '') ? $("#F02301").val() : '0';
    let F02401 = ($("input").is('#F02401') && $("#F02401").val() != '') ? $("#F02401").val() : '0';
    let F02501 = ($("input").is('#F02501') && $("#F02501").val() != '') ? $("#F02501").val() : '0';
    let F02601 = ($("input").is('#F02601') && $("#F02601").val() != '') ? $("#F02601").val() : '0';
    let F02701 = ($("input").is('#F02701') && $("#F02701").val() != '') ? $("#F02701").val() : '0';
    F00101 = F00101.replace(/\s+/g, '')
    F00201 = F00201.replace(/\s+/g, '')
    F00301 = F00301.replace(/\s+/g, '')
    F00401 = F00401.replace(/\s+/g, '')
    F00501 = F00501.replace(/\s+/g, '')
    F00601 = F00601.replace(/\s+/g, '')
    F00701 = F00701.replace(/\s+/g, '')
    F00801 = F00801.replace(/\s+/g, '')
    F00901 = F00901.replace(/\s+/g, '')
    F01001 = F01001.replace(/\s+/g, '')
    F01101 = F01101.replace(/\s+/g, '')
    F01201 = F01201.replace(/\s+/g, '')
    F01301 = F01301.replace(/\s+/g, '')
    F01401 = F01401.replace(/\s+/g, '')
    F01501 = F01501.replace(/\s+/g, '')
    F01601 = F01601.replace(/\s+/g, '')
    F01701 = F01701.replace(/\s+/g, '')
    F01801 = F01801.replace(/\s+/g, '')
    F01901 = F01901.replace(/\s+/g, '')
    F02001 = F02001.replace(/\s+/g, '')
    F02101 = F02101.replace(/\s+/g, '')
    F02201 = F02201.replace(/\s+/g, '')
    F02301 = F02301.replace(/\s+/g, '')
    F02401 = F02401.replace(/\s+/g, '')
    F02501 = F02501.replace(/\s+/g, '')
    F02601 = F02601.replace(/\s+/g, '')
    F02701 = F02701.replace(/\s+/g, '')
    let value =
        parseInt(F00101) +
        parseInt(F00201) +
        parseInt(F00301) +
        parseInt(F00401) +
        parseInt(F00501) -
        parseInt(F00601) +
        parseInt(F00701) +
        parseInt(F00801) -
        parseInt(F00901) +
        parseInt(F01001) -
        parseInt(F01101) -
        parseInt(F01201) +
        parseInt(F01301) -
        parseInt(F01401) -
        parseInt(F01501) -
        parseInt(F01601) -
        parseInt(F01701) -
        parseInt(F01801) -
        parseInt(F01901) -
        parseInt(F02001) -
        parseInt(F02101) -
        parseInt(F02201) -
        parseInt(F02301) +
        parseInt(F02401) -
        parseInt(F02501) +
        parseInt(F02601) -
        parseInt(F02701);
    if (value < 0) {
        $("#F02802").val(1);
        value *= -1;
    }
    else{
        $("#F02802").val(0);
    }

    $("#F02801").val(value);



    let parameter_id = $("#F02801").attr('name');
    let parameter_value = $("#F02801").val()
    if (parameter_value && parameter_value !== '') {
        let company_id = $("input[name='company_id']").val();
        let questionnaire_id = $("input[name='questionnaire_id']").val();
        if (company_id)
        {
            $.ajax({
                url: lang + '/company/fill-questionnaire-value/' + company_id + '/questionnaire/' + questionnaire_id,
                type: 'POST',
                data: {current_parameter_id: parameter_id, current_parameter_value: parameter_value},
                success: function (response) {
                    // let a = $("input[name='" + parameter_id + "']")
                    // a.nextAll("i").remove();
                    // a.after("<i class='fa fa-check' aria-hidden='true' style='color: green'></i>")

                },
                error: function (errors) {

                    let error = errors.responseText;
                    error = JSON.parse(error);

                }
            });
        }

    }
    F02802();


    myFunction(_this);
}
function F02802()
{
    let parameter_id = $("#F02802").attr('name');
    let parameter_value = $("#F02802").val()

    if (parameter_value && parameter_value !== '') {
        let company_id = $("input[name='company_id']").val();
        if(company_id)
        {
            let questionnaire_id = $("input[name='questionnaire_id']").val();
            $.ajax({
                url: lang + '/company/fill-questionnaire-value/' + company_id + '/questionnaire/' + questionnaire_id,
                type: 'POST',
                data: {current_parameter_id: parameter_id, current_parameter_value: parameter_value},
                success: function (response) {
                    // let a = $("input[name='" + parameter_id + "']")
                    // a.nextAll("i").remove();
                    // a.after("<i class='fa fa-check' aria-hidden='true' style='color: green'></i>")

                },
                error: function (errors) {

                    let error = errors.responseText;
                    error = JSON.parse(error);

                }
            });
        }

    }
}
function B00001()
{
    let parameter_id = $("#B00001").attr('name');
    let parameter_value = $("#B00001").val()


    if (parameter_value && parameter_value !== '') {
        let company_id = $("input[name='company_id']").val();
        if(company_id)
        {
            let questionnaire_id = $("input[name='questionnaire_id']").val();
            $.ajax({
                url: lang + '/company/fill-questionnaire-value/' + company_id + '/questionnaire/' + questionnaire_id,
                type: 'POST',
                data: {current_parameter_id: parameter_id, current_parameter_value: parameter_value},
                success: function (response) {
                    // let a = $("input[name='" + parameter_id + "']")
                    // a.nextAll("i").remove();
                    // a.after("<i class='fa fa-check' aria-hidden='true' style='color: green'></i>")

                },
                error: function (errors) {

                    let error = errors.responseText;
                    error = JSON.parse(error);

                }
            });
        }

    }
}
function myFunction(_this) {
    if(window.location.href.includes('edit-parameter/'))
    {
        return true;
    }
    if (_this!=null && _this.getAttribute('type') == 'checkbox') {
        _this.value = _this.checked ? 1 : 0;
    }
    B00001()
    if(_this)
    {
        let parameter_id = _this.name;
        let parameter_value = _this.value;
        parameter_value = parameter_value.replace(/\s+/g, '');
        let company_id = $("input[name='company_id']").val();
        let questionnaire_id = $("input[name='questionnaire_id']").val();
        if(company_id)
        {
            $.ajax({
                url: lang + '/company/fill-questionnaire-value/' + company_id + '/questionnaire/' + questionnaire_id,
                type: 'POST',
                data: {current_parameter_id: parameter_id, current_parameter_value: parameter_value},
                success: function (response) {
                    // let a = $("input[name='" + parameter_id + "']")
                    // a.nextAll("i").remove();
                    // a.after("<i class='fa fa-check' aria-hidden='true' style='color: green'></i>")

                },
                error: function (errors) {

                    let error = errors.responseText;
                    error = JSON.parse(error);

                }
            });
        }
    }





};

f_block_function()

function filterFunction() {
    var input, filter, ul, li, a, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    div = document.getElementById("myDropdown");
    a = div.getElementsByTagName("a");
    for (i = 0; i < a.length; i++) {
        txtValue = a[i].textContent || a[i].innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            a[i].style.display = "";
        } else {
            a[i].style.display = "none";
        }
    }
}

var set;

function setOnclick() {
    var a = document.getElementsByClassName('ateco_id_function');

    if (a && a[0].getAttribute('onclick') == null) {
        clearInterval(set);
        for (var i = 0; i < a.length; i++) {
            a[i].setAttribute('onclick', 'callmodifyAtecoImputValueAfterAnyMiliseconds(this)');
            a[i].getElementsByTagName('span')[0].classList.add('key-for-search');
        }
    }
}

$('li.code_id').on('click',function () {
    set = setInterval(() => {
        setOnclick();
    },1000);
});

function modifyAtecoImputValue(a) {
    var input, neededValue, key, i;
    input       = document.getElementById("myInput");
    key = a.getElementsByClassName("key-for-search");
    neededValue = input.value;

    if($(document).find('#myDropdown #myInput').hasClass('error-input')){
        $(document).find('#myDropdown #myInput').removeClass('error-input');
        $(document).find('#myDropdown #myInput').css({
            'border': '2px solid red',
            'color': 'black',
        })

    }

    for (i = 0; i < key.length; i++) {
        neededValue = neededValue.replace(new RegExp(key[i].innerHTML,'gi')," ");
    }

    input.value = neededValue.trim();
    $(document).find('.preview').css('display', 'block');
}

function callmodifyAtecoImputValueAfterAnyMiliseconds(a) {
    setTimeout(() =>{
        modifyAtecoImputValue(a);
    },1);
}

function open_sose() {
    let div = document.getElementById('sose_okveds_select');
    div.style.display = 'block';
    div.style.width = "551px";
    div.style.border = "1px solid #47b39c";
    div.style.borderRadius =  "0 0 5px 5px";
    div.style.borderTop = "none";
    div.style.marginLeft = "45px";
}

function open_sose_company() {
    let div = document.getElementById('sose_okveds_select');
    div.style.display = 'block';
}

function selected_data(e) {
    var element = document.getElementById('selected');
    if (element) {
        element.removeAttribute('id');
        $(this).attr('id', 'selected')
        $('#sose_okveds_select').css('display', 'none');
    } else {
        $(this).attr('id', 'selected')
        $('#sose_okveds_select').css('display', 'none');
    }

    //Todo check with previous version
    let value = $(this)[0].innerText;
    $(document).find('#myInput').val(value);

    if ($('.product_codes').length > 0)
    {
        let ateco_id = $('#selected').data('id');
        $.ajax({
            url: lang + '/client/product-codes/'+ateco_id,
            type: "GET",
            success: function (response) {
                if (response.body)
                {
                    $('.product_codes').css('display','flex');
                    $('.product_codes').attr('href',response.body);
                }else{
                    $('.product_codes').css('display','none');
                }
            },
        });
    }
    $('#sose_okveds_select').css('display', 'none')
    let button = document.getElementById("add-questionnaire")
    if (button) {
        button.disabled = false
    } else {
        if (document.getElementById("buttonPrint") !== null)
            document.getElementById("buttonPrint").disabled = false;

    }

}
