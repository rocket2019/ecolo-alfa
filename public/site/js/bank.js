jQuery(document).ready(function ($) {

   var  $uploadCrop = null
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

   $('.file_upload').on('click', function(){
       let compId = $(this).data('name');
       let id = $(this).data('id');
       $('.company_name').attr('value', $(this).data('company'));
       $('.modal-title').text($(this).data('company'));
       $('#myModal').find('form').attr('action', lang+'/company/edit/'+compId+'/'+id+'/upload');
   });

   $('.delete_file').click(function(){
       let clickedBtn = $(this).context;
       let data = clickedBtn.name.split('/');
       $.ajax({
           url: lang + '/company/edit/' + data[1] + '/' + data[2] + '/delete',
           data: {
               fileName: data[0],
               token1: data[1],
               token2: data[2]
           },
           type: 'POST',
           beforeSend: function(){
                $('#loading_layer').css('display', 'block');
           },
           success: function(response){
               $('#loading_layer').css('display', 'none');
               window.location.reload();
           }
       })
   });

    $('#bank_name').keypress(function (e) {
        if (e.which === 13) {
            new_bank();
            return false;    //<---- Add this line
        }
    });
    $('.save_bank').click(function () {
        new_bank()
    });

    $('#save_report').on('click', () => {

    });

    function new_bank() {
        $('#loading_layer').css('display', 'block');
        let form = $('#new_bank_form').serialize();
        $.ajax({
            url: lang + '/admin/new-bank',
            data: form,
            type: "POST",

            success: function (response) {

                window.location.href = lang + '/admin/banks-list/';
            },

            error: function (errors) {
                $('#loading_layer').css('display', 'none');
                let error = errors.responseText;
                error = JSON.parse(error);
                $.each(error.body, function (key, value) {
                    $('.' + 'error-' + key).text(value);
                });


            },
        })
    }


    $(document).on('click', '.delete_bank', function (e) {
        e.preventDefault();
        let id = $(this).attr('data-id');

        $('#delete_bank').modal()
        $('#delete_bank').find('.delete-bank').attr('data-id', id);
    })

    $(document).on('click', '.delete-bank', function () {
        let id = $(this).data('id');
        $.ajax({
            url: lang + '/admin/delete-bank/' + id,
            type: 'GET',
            success: function (response) {
                window.location.reload();
            },
            error: function () {
            }
        });

    })

    $('input').on('keyup keypress keydown paste', function () {
        $(this).parent().find('.error').text('');
    });


    $(document).on('click', '.add_logo', function () {
        $('#loading_layer').css('display', 'block');

        let form_data = $("#bank_logo_form").serialize();


        $.ajax({
            url: lang + '/admin/add-logo',
            type: 'POST',
            data:form_data,

            success: function (response) {
                window.location.reload();
            },
            error: function (errors) {
                $('#loading_layer').css('display', 'none');
                let error = errors.responseText;
                error = JSON.parse(error);
                $.each(error.body, function (key, value) {
                    $('.' + 'error-' + key).text(value);
                });


            },
        });
    });

    $(document).on('click', '.delete_logo', function (e) {
        e.preventDefault();
        $('#delete_logo').modal()
    })

    $(document).on('click', '.delete-logo', function () {
        $.ajax({
            url: lang + '/admin/delete-bank-logo',
            type: 'GET',
            success: function (response) {
                window.location.reload();
            },
            error: function () {
            }
        });

    })
    $(document).on('click', '.delete_number', function (e) {
        e.preventDefault();

        $('#delete_number').modal()
    })

    $(document).on('click', '.delete-number', function () {

        $.ajax({
            url: lang + '/admin/delete-bank-number',
            type: 'GET',
            success: function (response) {
                window.location.reload();
            },
            error: function () {
            }
        });

    })

    $(document).on('click', '#change', function () {
        $('#loading_layer').css('display', 'block');

        let form = $('.valuate').serialize();

        $.ajax({
            url: lang + '/admin/add-or-edit-globals',
            type: 'POST',
            data:form,
            success: function (response) {
                window.location.reload();
            },
            error: function () {
            }
        });

    })





$(document).on("change", "#upload", function() {
        var input = this;
        if (!!input.files.length) {
            if(!!$uploadCrop){
                $uploadCrop.croppie('destroy');
            }
            let Url = URL.createObjectURL(input.files[0])
            $uploadCrop = $('#upload-demo').croppie({
                enableExif: true,
                viewport: {
                    width: 200,
                    height: 200,
                    type: 'square',

                },
                boundary: {
                    width: 300,
                    height: 300
                },
                scale: 0.1


            });
            $uploadCrop.croppie('bind', {
                url: Url
            });
            // var reader = new FileReader();
            // reader.onload = function(e) {


                // $(input).replaceWith($(input).clone())
            }
            // reader.readAsDataURL(input.files[0]);

    });

    $('.upload-result').click(function () {
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: 'original'
        }).then(function (resp) {
            if( !!$('.bank-logo')[0]){
                $('.bank-logo').attr('src',resp);
            }
            else{
                $('.images-demo').html(` <img class="bank-logo"
                        src="${resp}"
                        style="height: 150px; width: 150px;border-right: 1px solid darkgrey;">
                <span class="delete_logo" style="cursor: pointer; position: absolute; top: 5px; right: 8px;" >
                    <i class="fa fa-times fa-2x" aria-hidden="true"></i></span>`)
            }
            $('input[name=image]').val(resp);
            $uploadCrop.croppie('destroy');
            $uploadCrop = null
        });


    });



});
