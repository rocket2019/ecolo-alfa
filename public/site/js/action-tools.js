$(document).ready(function () {
$('.rate-result').hide();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('select').change(function() {
        let myOpt = [];
        $("select").each(function () {
            myOpt.push($(this).val());
        });
        $("select").each(function () {
            $(this).find("option").prop('hidden', false);
            let sel = $(this);
            $.each(myOpt, function(key, value) {
                if((value != "") && (value != sel.val())) {
                    sel.find("option").filter('[value="' + value +'"]').prop('hidden', true);
                }
            });
        });
    });

    $('.change').click(function () {
        $('.rate-result').show();
        let unit_input = $('input[name=unit]');

        let unit = unit_input.val();
        let from = $('select[name=from]').val();
        let to = $('select[name=to]').val();

        if (!unit || unit === 0)
        {
            unit_input.css('border','1px solid red');
            return false;
        }
        calculateRates(unit);


    });

    $('input[name=unit]').on('keypress',function () {
        $(this).css('border','1px solid gray');
    });


    function calculateRates(unit) {
        let from = $('select[name=from]').val();
        let to = $('select[name=to]').val();

        $.ajax({
            url: 'https://frankfurter.app/latest?from='+from+'&to='+to+'&amount='+unit,
            dataType: 'jsonp',
            success: function(json) {

               $('.rate-result').text(json.rates[to]);
            }
        });
    }



});
